from netgen.meshing import *
from netgen.csg import *
from ngsolve import *
from math import pi
import matplotlib.colors as colors
import scipy.sparse.linalg
import numpy as np
from scipy.linalg import lu_factor,lu_solve


def BitArrayFromList(l,mesh,fes, elements = True):
    if elements:
        ba = BitArray(mesh.ne)
    else:
        ba = BitArray(fes.ndof)
    ba.Clear()
    for el in l:
        ba[el] = True
    return ba

def P_DoFs(M,test,basis):
    return (M[test,:][:,basis])

class Layered_Decomposition_cartesian():
    def __init__(self,mesh,fes,n_layers,intervals):
        self.mesh = mesh
        self.fes = fes
        self.n_layers = n_layers
        self.intervals = intervals
        # partition of dofs
        self.free_dofs = []
        for nr,b in enumerate(self.fes.FreeDofs()): 
            if b:
                self.free_dofs.append(nr)
        self.layer2el = [ [] for i in range(self.n_layers)]         #elements of layer
        self.layer2dof = [ [] for i in range(self.n_layers)]        #dofs of layer   
        self.IF2dof = [ [] for i in range(self.n_layers)]           #dofs of layer 
        self.Interior2dof = [ [] for i in range(self.n_layers)] 
        
        #self.InteriorPlusInnerdof = [ [] for i in range(self.n_layers)] 
        self.InteriorPlusIFdof = [ [] for i in range(self.n_layers)] 
        
        self.layer2all_outer_dof = [ [] for i in range(self.n_layers)] 
        self.layer2all_outer_el  = [ [] for i in range(self.n_layers)]
        self.layer_and_outer_dof = [ [] for i in range(self.n_layers)] 
        self.layer_and_outer_el  = [ [] for i in range(self.n_layers)]
        self.IF2el = [ [] for i in range(self.n_layers)] # elements on interface 
        self.centroids = None
    
    def centroid2layer(self,centr):
        cx,cy = centr
        r_c =  cy
        for l,I in enumerate(self.intervals):
            if r_c >= I[0] and r_c <= I[1]:
                return l
 
    def Decompose(self):     
        self.centroids = []

        for el in self.mesh.Elements():
            vs = el.vertices
            L = len(el.vertices)
            c_x = 0
            c_y = 0
            for vert in el.vertices:
                vx,vy,vz = self.mesh.ngmesh.Points()[vert.nr+1].p
                c_x = c_x + vx
                c_y = c_y + vy
            c_x = c_x / L
            c_y = c_y / L
            self.centroids.append((c_x,c_y))
        
        for cent,el in zip(self.centroids,self.fes.Elements()):
            mylayer = self.centroid2layer(cent)
            self.layer2el[mylayer].append(el.nr)
            for dof in el.dofs:
                self.layer2dof[mylayer].append(dof)
        for i in range(self.n_layers):
            self.layer2dof[i] = list( set(self.layer2dof[i]).intersection(self.free_dofs)  )
            self.layer2dof[i].sort()
        for i in range(1,self.n_layers):
            self.IF2dof[i] = list( set(self.layer2dof[i]).intersection( set(self.layer2dof[i-1])) )
            self.IF2dof[i].sort()
        for i in range(1,self.n_layers-1):
             self.Interior2dof[i] = list( set(self.layer2dof[i]) - set(self.IF2dof[i]) - set(self.IF2dof[i+1])  )
             self.Interior2dof[i].sort()
        self.Interior2dof[self.n_layers-1] = list( set(self.layer2dof[self.n_layers-1]) - set(self.IF2dof[self.n_layers-1])  )
        self.Interior2dof[self.n_layers-1].sort()
        self.Interior2dof[0] = list( set(self.layer2dof[0]) - set(self.IF2dof[1])  )
        for i in range(1,self.n_layers):
            for j in range(0,i-1):
                self.layer2all_outer_dof[i].extend(self.layer2dof[j])
            self.layer2all_outer_dof[i].extend(self.Interior2dof[i-1])
            self.layer2all_outer_dof[i] =  list( set(self.layer2all_outer_dof[i]))
            self.layer2all_outer_dof[i].sort() 
        for i in range(1,self.n_layers):
            for j in range(0,i):
                self.layer2all_outer_el[i].extend(self.layer2el[j])
            self.layer2all_outer_el[i] =  list( set(self.layer2all_outer_el[i]))  
        for i in range(self.n_layers):
            self.InteriorPlusIFdof[i] = list( set(self.Interior2dof[i]) | set(self.IF2dof[i])  )
            self.InteriorPlusIFdof[i].sort()
        for i in range(1,self.n_layers):
            self.layer_and_outer_dof[i] = self.layer2all_outer_dof[i].copy()
            self.layer_and_outer_dof[i].extend(self.InteriorPlusIFdof[i])
            self.layer_and_outer_dof[i] = list(set(self.layer_and_outer_dof[i]))
            self.layer_and_outer_dof[i].sort() 
        self.layer_and_outer_dof[0] = self.InteriorPlusIFdof[0]
        for i in range(1,self.n_layers):
            self.layer_and_outer_el[i] = self.layer2all_outer_el[i].copy()
            self.layer_and_outer_el[i].extend(self.layer2el[i]) 
            self.layer_and_outer_el[i].sort()
        self.layer_and_outer_el[0] = self.layer2el[0]

    def Xtend(self,X):
        new_dofs = list(np.arange(self.fes.ndof,X.ndof))
        self.layer2dof[0] += new_dofs 
        self.layer2dof[0].sort()
        self.Interior2dof[0] = list( set(self.layer2dof[0]) - set(self.IF2dof[1])  )
        #self.layer_and_outer_dof[0] = self.Interior2dof[0]
        for i in range(self.n_layers):
            self.layer2all_outer_dof[i] += new_dofs
            self.layer2all_outer_dof[i].sort()
            self.layer_and_outer_dof[i] += new_dofs
            self.layer_and_outer_dof[i].sort() 
        self.fes = X

class Layered_Decomposition():
    def __init__(self,mesh,fes,n_layers,intervals):
        self.mesh = mesh
        self.fes = fes
        self.n_layers = n_layers
        self.intervals = intervals
        # partition of dofs
        self.free_dofs = []
        for nr,b in enumerate(self.fes.FreeDofs()): 
            if b:
                self.free_dofs.append(nr)
        self.layer2el = [ [] for i in range(self.n_layers)]         #elements of layer
        self.layer2dof = [ [] for i in range(self.n_layers)]        #dofs of layer   
        self.IF2dof = [ [] for i in range(self.n_layers)]           #dofs of layer 
        self.Interior2dof = [ [] for i in range(self.n_layers)] 
        
        #self.InteriorPlusInnerdof = [ [] for i in range(self.n_layers)] 
        self.InteriorPlusIFdof = [ [] for i in range(self.n_layers)] 
        
        self.layer2all_outer_dof = [ [] for i in range(self.n_layers)] 
        self.layer2all_outer_el  = [ [] for i in range(self.n_layers)]
        self.layer_and_outer_dof = [ [] for i in range(self.n_layers)] 
        self.layer_and_outer_el  = [ [] for i in range(self.n_layers)]
        self.IF2el = [ [] for i in range(self.n_layers)] # elements on interface 
        self.centroids = None
    
    def centroid2layer(self,centr):
        cx,cy = centr
        #r_c =  sqrt(cx*cx + cy*cy)
        r_c =  cy
        for l,I in enumerate(self.intervals):
            if r_c >= I[0] and r_c <= I[1]:
                return l
    
    def inv_polar(self,vx,vy):
        r_radius = sqrt(vx**2+vy**2)
        phi_angle = atan2(vx,vy)
        return r_radius,phi_angle
    
    def Decompose(self):     
        self.centroids = []

        for el in self.mesh.Elements():
            vs = el.vertices
            L = len(el.vertices)
            c_x = 0
            c_y = 0
            for vert in el.vertices:
                vx,vy,vz = self.mesh.ngmesh.Points()[vert.nr+1].p
                r_radius,phi_angle = self.inv_polar(vx,vy)
                #c_x = c_x + vx
                #c_y = c_y + vy
                c_x = c_x + phi_angle
                c_y = c_y + r_radius
            c_x = c_x / L
            c_y = c_y / L
            self.centroids.append((c_x,c_y))
        
        for cent,el in zip(self.centroids,self.fes.Elements()):
            mylayer = self.centroid2layer(cent)
            self.layer2el[mylayer].append(el.nr)
            for dof in el.dofs:
                self.layer2dof[mylayer].append(dof)
        for i in range(self.n_layers):
            self.layer2dof[i] = list( set(self.layer2dof[i]).intersection(self.free_dofs)  )
            self.layer2dof[i].sort()
        for i in range(1,self.n_layers):
            self.IF2dof[i] = list( set(self.layer2dof[i]).intersection( set(self.layer2dof[i-1])) )
            self.IF2dof[i].sort()
        for i in range(1,self.n_layers-1):
             self.Interior2dof[i] = list( set(self.layer2dof[i]) - set(self.IF2dof[i]) - set(self.IF2dof[i+1])  )
             self.Interior2dof[i].sort()
        self.Interior2dof[self.n_layers-1] = list( set(self.layer2dof[self.n_layers-1]) - set(self.IF2dof[self.n_layers-1])  )
        self.Interior2dof[self.n_layers-1].sort()
        self.Interior2dof[0] = list( set(self.layer2dof[0]) - set(self.IF2dof[1])  )
        for i in range(1,self.n_layers):
            for j in range(0,i-1):
                self.layer2all_outer_dof[i].extend(self.layer2dof[j])
            self.layer2all_outer_dof[i].extend(self.Interior2dof[i-1])
            self.layer2all_outer_dof[i] =  list( set(self.layer2all_outer_dof[i]))
            self.layer2all_outer_dof[i].sort() 
        for i in range(1,self.n_layers):
            for j in range(0,i):
                self.layer2all_outer_el[i].extend(self.layer2el[j])
            self.layer2all_outer_el[i] =  list( set(self.layer2all_outer_el[i]))  
        for i in range(self.n_layers):
            self.InteriorPlusIFdof[i] = list( set(self.Interior2dof[i]) | set(self.IF2dof[i])  )
            self.InteriorPlusIFdof[i].sort()
        for i in range(1,self.n_layers):
            self.layer_and_outer_dof[i] = self.layer2all_outer_dof[i].copy()
            self.layer_and_outer_dof[i].extend(self.InteriorPlusIFdof[i])
            self.layer_and_outer_dof[i] = list(set(self.layer_and_outer_dof[i]))
            self.layer_and_outer_dof[i].sort() 
        self.layer_and_outer_dof[0] = self.InteriorPlusIFdof[0]
        for i in range(1,self.n_layers):
            self.layer_and_outer_el[i] = self.layer2all_outer_el[i].copy()
            self.layer_and_outer_el[i].extend(self.layer2el[i]) 
            self.layer_and_outer_el[i].sort()
        self.layer_and_outer_el[0] = self.layer2el[0]

    def Xtend(self,X):
        new_dofs = list(np.arange(self.fes.ndof,X.ndof))
        self.layer2dof[0] += new_dofs 
        self.layer2dof[0].sort()
        self.Interior2dof[0] = list( set(self.layer2dof[0]) - set(self.IF2dof[1])  )
        #self.layer_and_outer_dof[0] = self.Interior2dof[0]
        for i in range(self.n_layers):
            self.layer2all_outer_dof[i] += new_dofs
            self.layer2all_outer_dof[i].sort()
            self.layer_and_outer_dof[i] += new_dofs
            self.layer_and_outer_dof[i].sort() 
        self.fes = X


class DOSM_IFassemble():
    
    def __init__(self,decomp,layermat,layerinverse,P_outer,Q_outer,gfu):
        self.decomp = decomp
        self.layermat = layermat
        self.layerinverse = layerinverse
        self.gfu = gfu  
        self.n_layers = len(layermat)
        self.P_outer = P_outer
        self.Q_outer = Q_outer
        self.IF_lus = [None for i in range(self.n_layers)] 
        self.Assemble_IF_lus()

    def Assemble_IF_lus(self):
        for layer in range(1,self.n_layers):
            atmp = self.gfu.vec.CreateVector()
            atmp1 = self.gfu.vec.CreateVector()
            atmp_in = self.gfu.vec.CreateVector()
            atmp_out = self.gfu.vec.CreateVector()

            proj_IF_outer = Projector(BitArrayFromList(self.decomp.IF2dof[layer],self.decomp.mesh,self.decomp.fes,elements=False),False)
            proj_inner = Projector(BitArrayFromList(self.decomp.Interior2dof[layer],self.decomp.mesh,self.decomp.fes,elements=False),False)

            def apply_matvec(v):
                atmp_in.FV().NumPy()[:] = 0.0
                if len(v.shape) > 1:
                    atmp_in.FV().NumPy()[self.decomp.IF2dof[layer]] = v[:,0]
                else:
                    atmp_in.FV().NumPy()[self.decomp.IF2dof[layer]] = v

                self.P_outer(layer,atmp_in,atmp_out)

                atmp.data = atmp_in
                atmp.data -= proj_IF_outer * atmp
                atmp1.data = self.layermat[layer] * atmp
                atmp.data = atmp1
                atmp.data -= proj_IF_outer * atmp
                self.Q_outer(layer,atmp,atmp1)
                atmp_out.data += atmp1
                    
                atmp.data = atmp_in
                atmp.data -= proj_IF_outer * atmp
                atmp1.data = self.layermat[layer] * atmp
                atmp1.data -= proj_inner * atmp1
                atmp.data = self.layerinverse[layer] * atmp1
                atmp.data -= proj_inner * atmp
                atmp1.data = self.layermat[layer] * atmp
                atmp1.data -= proj_IF_outer * atmp1
                self.Q_outer(layer,atmp1,atmp)
                atmp_out.data -= atmp
                     
                return atmp_out.FV().NumPy()[self.decomp.IF2dof[layer]]
                    
            n_len = len(self.decomp.IF2dof[layer])
            lhs_IF_prep = scipy.sparse.linalg.LinearOperator( (len(self.decomp.IF2dof[layer]),len(self.decomp.IF2dof[layer])),dtype = complex, matvec=apply_matvec)
            layer_IF_mat = np.zeros((n_len,n_len),dtype=complex)

            ei = np.zeros(n_len,dtype = complex) 
            for ii in range(n_len):
                ei[:] = 0.0
                ei[ii] = 1.0
                rei = apply_matvec(ei)
                layer_IF_mat[:,ii] = rei[:]
            
            self.IF_lus[layer] = lu_factor(layer_IF_mat)

    
    def apply_DOSM_mats(self,layer,v_in,v_out):
        # v_in:   input vector
        # v_out:  output vector

        # clear output vector
        v_out.FV().NumPy()[:] = 0.0 

        rhs_IF_outer = self.gfu.vec.CreateVector()
        rhs_inner = self.gfu.vec.CreateVector()
        tmp = self.gfu.vec.CreateVector()
        tmp1 = self.gfu.vec.CreateVector()
        tmp_in = self.gfu.vec.CreateVector()
        tmp_out = self.gfu.vec.CreateVector()

        proj_IF_outer = Projector(BitArrayFromList(self.decomp.IF2dof[layer],self.decomp.mesh,self.decomp.fes,elements=False),False)
        proj_inner = Projector(BitArrayFromList(self.decomp.Interior2dof[layer],self.decomp.mesh,self.decomp.fes,elements=False),False)

        # Dirichlet b.c. on inner interface
        if layer + 1 < self.n_layers:
            proj_IF_inner = Projector(BitArrayFromList(self.decomp.IF2dof[layer+1],self.decomp.mesh,self.decomp.fes,elements=False),False)
            v_out.data = v_in
            v_out.data -= proj_IF_inner * v_out.data

        if layer == 0:
            # solve directly on layer 0
            inner_dof_plus_outer_IF = list( set(self.decomp.layer2dof[0]) - set(self.decomp.IF2dof[1])  )
            proj_inner_dof_plus_outer_IF = Projector(BitArrayFromList(inner_dof_plus_outer_IF,self.decomp.mesh,self.decomp.fes,elements=False),False)
            proj_IF_inner = Projector(BitArrayFromList(self.decomp.IF2dof[layer+1],self.decomp.mesh,self.decomp.fes,elements=False),False)
            rhs_inner.data = v_in
            rhs_inner.data -=  proj_inner_dof_plus_outer_IF * v_in
            tmp.data = v_out.data 
            tmp.data -= proj_IF_inner * v_out.data
            tmp1.data = self.layermat[layer] * tmp
            tmp1.data -= proj_inner_dof_plus_outer_IF * tmp1
            rhs_inner.data -= tmp1
            tmp.data = self.layerinverse[layer] * rhs_inner
            tmp.data -= proj_inner_dof_plus_outer_IF * tmp
            v_out.data += tmp

        else:
            # solve iteratively for unknowns on outer interface 
            # 1. set up the right hand side
            rhs_IF_outer.data = v_in
            rhs_IF_outer.data -= proj_inner * rhs_IF_outer  
            if  layer + 1 < self.n_layers:
                tmp.data = self.layermat[layer] * v_out
                tmp.data -= proj_inner * tmp
                rhs_IF_outer.data -= tmp
            tmp.data = self.layerinverse[layer] * rhs_IF_outer
            rhs_IF_outer.data = tmp
            rhs_IF_outer.data -= proj_inner * rhs_IF_outer
            tmp.data = self.layermat[layer] * rhs_IF_outer 
            rhs_IF_outer.data = tmp
            rhs_IF_outer.data -= proj_IF_outer * rhs_IF_outer
            self.Q_outer(layer,rhs_IF_outer,tmp)
            rhs_IF_outer.data = v_in
            rhs_IF_outer.data -= proj_IF_outer * rhs_IF_outer
            rhs_IF_outer.data -= tmp

            # 2. set up left hand side as linear operator
            def apply_matvec(v):
                tmp_in.FV().NumPy()[:] = 0.0
                tmp_in.FV().NumPy()[self.decomp.IF2dof[layer]] = v

                self.P_outer(layer,tmp_in,tmp_out)
                tmp.data = tmp_in
                tmp.data -= proj_IF_outer * tmp
                tmp1.data = self.layermat[layer] * tmp
                tmp.data = tmp1
                tmp.data -= proj_IF_outer * tmp
                self.Q_outer(layer,tmp,tmp1)
                tmp_out.data += tmp1
                
                tmp.data = tmp_in
                tmp.data -= proj_IF_outer * tmp
                tmp1.data = self.layermat[layer] * tmp
                tmp1.data -= proj_inner * tmp1
                tmp.data = self.layerinverse[layer] * tmp1
                tmp.data -= proj_inner * tmp
                tmp1.data = self.layermat[layer] * tmp
                tmp1.data -= proj_IF_outer * tmp1
                self.Q_outer(layer,tmp1,tmp)
                tmp_out.data -= tmp

                return tmp_out.FV().NumPy()[self.decomp.IF2dof[layer]]


            f_gmres = np.zeros(len(self.decomp.IF2dof[layer]),dtype = complex)
            f_gmres[:] = rhs_IF_outer.FV().NumPy()[self.decomp.IF2dof[layer]]

            tmp.FV().NumPy()[self.decomp.IF2dof[layer]] = lu_solve(self.IF_lus[layer],f_gmres)[:]
            tmp.data -= proj_IF_outer * tmp
            v_out.data += tmp

            # solve for inner unknowns on layer
            # 1. set up the right hand side
            rhs_inner.data =  v_in
            rhs_inner.data -= proj_inner * rhs_inner
            if layer < self.n_layers -1:
                proj_IF_inner = Projector(BitArrayFromList(self.decomp.IF2dof[layer+1],self.decomp.mesh,self.decomp.fes,elements=False),False)
                tmp.data = v_in
                tmp.data -= proj_IF_inner * v_in
                tmp1.data = self.layermat[layer] * tmp
                tmp1.data -= proj_inner * tmp1
                rhs_inner.data -= tmp1
            tmp.data = v_out
            tmp.data -= proj_IF_outer * tmp
            tmp1.data = self.layermat[layer] * tmp
            
            tmp1.data -=  proj_inner * tmp1
            rhs_inner.data -= tmp1
            
            # 2. Solve system for inner dofs
            tmp.data = self.layerinverse[layer] * rhs_inner
            tmp.data -= proj_inner * tmp
            v_out.data += tmp


    def ApplyDOSM(self,res,u_new,u_old):        
        unew = u_new.CreateVector()
        uold = u_old.CreateVector()
        uresult = u_old.CreateVector()
        unew[:] = 0.0
        uold.data = u_old
            
        ufwd = u_new.CreateVector()
        ufwd[:] = 0.0
            
        rhs = res.CreateVector()
        tmp = res.CreateVector()
        tmp1 = res.CreateVector()
        rhs.data = res
        rhs_layer = res.CreateVector()
            
        # forward sweep
        rhs_fwd = [res.CreateVector() for i in range(self.n_layers)]
        for l in range(self.n_layers):

            if l == 0:
                inner_dof_plus_outer_IF = list( set(self.decomp.layer2dof[0]) - set(self.decomp.IF2dof[1])  )
                proj_inner_dof_plus_outer_IF = Projector(BitArrayFromList(inner_dof_plus_outer_IF,self.decomp.mesh,self.decomp.fes,elements=False),False)
                rhs_layer.data = res
                rhs_layer.data -= proj_inner_dof_plus_outer_IF * rhs_layer
            else:
                proj_inner = Projector(BitArrayFromList(self.decomp.Interior2dof[l],self.decomp.mesh,self.decomp.fes,elements=False),False)
                rhs_layer.data = res
                rhs_layer.data -= proj_inner * rhs_layer
              
            if l < self.n_layers-1:
                proj_IF_inner = Projector(BitArrayFromList(self.decomp.IF2dof[l+1],self.decomp.mesh,self.decomp.fes,elements=False),False)
                tmp.data = uold
                tmp.data -= proj_IF_inner * tmp
                rhs_layer.data += tmp

            if l > 0:
                proj_IF_outer = Projector(BitArrayFromList(self.decomp.IF2dof[l],self.decomp.mesh,self.decomp.fes,elements=False),False)
                proj_inner_prev = Projector(BitArrayFromList(self.decomp.Interior2dof[l-1],self.decomp.mesh,self.decomp.fes,elements=False),False)
                tmp.data = unew.data 
                tmp.data -= proj_IF_outer * tmp
                tmp1.data = self.layermat[l-1] * tmp
                tmp1.data -= proj_IF_outer * tmp1
                self.Q_outer(l,tmp1,tmp)
                rhs_layer.data -= tmp
                tmp.data = unew.data 
                tmp.data -= proj_IF_outer * tmp
                self.P_outer(l,tmp,tmp1)
                rhs_layer.data += tmp1
                tmp.data = unew.data 
                tmp.data -= proj_inner_prev * tmp
                tmp1.data = self.layermat[l-1] * tmp
                tmp1.data -= proj_IF_outer * tmp1
                tmp.data  = res
                tmp.data -= proj_IF_outer * tmp
                tmp.data -= tmp1
                self.Q_outer(l,tmp,tmp1)
                rhs_layer.data += tmp1

                
            rhs_fwd[l].data = rhs_layer
            if l < self.n_layers-1:  
                self.apply_DOSM_mats(l,rhs_layer,uresult)  
                unew.FV().NumPy()[self.decomp.layer2dof[l]] = uresult.FV().NumPy()[self.decomp.layer2dof[l]]
                #gfu_DOSM_interm.vec.data = unew

        ufwd.data = unew
             
        # backward sweep 
        for l in range(self.n_layers-1,-1,-1):
            rhs_layer.data = rhs_fwd[l]
            if l != self.n_layers-1:   
                rhs_layer.FV().NumPy()[self.decomp.IF2dof[l+1]] = unew.FV().NumPy()[self.decomp.IF2dof[l+1]]
            self.apply_DOSM_mats(l,rhs_layer,uresult)  
            unew.FV().NumPy()[self.decomp.layer2dof[l]] = uresult.FV().NumPy()[self.decomp.layer2dof[l]]
            #gfu_DOSM_interm.vec.data = unew
                
        u_new.data = unew


class DOSM():
    
    def __init__(self,decomp,layermat,layer_and_truncated_ext_inv,P_outer,gfu):
        self.decomp = decomp
        self.layermat = layermat
        self.layer_and_truncated_ext_inv = layer_and_truncated_ext_inv
        self.gfu = gfu  
        self.n_layers = len(layermat)
        self.P_outer = P_outer
        self.proj_inner = [ Projector(BitArrayFromList(self.decomp.Interior2dof[l],self.decomp.mesh,self.decomp.fes,elements=False),False) for l in range(self.n_layers)] 
        self.proj_IF = [ Projector(BitArrayFromList(self.decomp.IF2dof[l],self.decomp.mesh,self.decomp.fes,elements=False),False) for l in range(self.n_layers)] 
        self.proj_inner_and_IF = [ Projector(BitArrayFromList(self.decomp.InteriorPlusIFdof[l],self.decomp.mesh,self.decomp.fes,elements=False),False) for l in range(self.n_layers)] 
        self.proj_inner_and_IF_True = [ Projector(BitArrayFromList(self.decomp.InteriorPlusIFdof[l],self.decomp.mesh,self.decomp.fes,elements=False),True) for l in range(self.n_layers)] 

        # create auxiliary vectors 
        self.rhs = self.gfu.vec.CreateVector()
        self.rhs_layer = self.gfu.vec.CreateVector()
        self.tmp = self.gfu.vec.CreateVector()
        self.tmp1 = self.gfu.vec.CreateVector()
        self.unew = self.gfu.vec.CreateVector() 
        self.uold = self.gfu.vec.CreateVector() 
        self.uresult = self.gfu.vec.CreateVector()
        self.rhs_fwd =  [self.gfu.vec.CreateVector() for i in range(self.n_layers)]


    def ApplyDOSM(self,res,u_new,u_old):        
        self.unew[:] = 0.0
        self.uold.data = u_old     
        self.rhs.data = res
            
        # forward sweep 
        for l in range(self.n_layers):

            self.rhs_layer.data = self.rhs
            self.rhs_layer.data -= self.proj_inner[l] * self.rhs_layer
              
            if l < self.n_layers-1:
                self.tmp.data = self.uold
                self.tmp.data -= self.proj_IF[l+1]  * self.tmp
                self.rhs_layer.data += self.tmp

            if l > 0:
                self.tmp.data = self.unew.data 
                self.tmp.data -= self.proj_IF[l] * self.tmp  
                self.tmp1.data = self.layermat[l-1] * self.tmp
                self.tmp1.data -=  self.proj_IF[l] * self.tmp1
                self.rhs_layer.data -= self.tmp1
                self.tmp.data = self.unew.data 
                self.tmp.data -= self.proj_IF[l]  * self.tmp
                self.P_outer(l,self.tmp,self.tmp1)
                self.rhs_layer.data += self.tmp1
                self.tmp.data = self.unew.data 
                self.tmp.data -=  self.proj_inner[l-1]  * self.tmp
                self.tmp1.data = self.layermat[l-1] * self.tmp
                self.tmp1.data -= self.proj_IF[l] * self.tmp1
                self.tmp.data  = self.rhs
                self.tmp.data -= self.proj_IF[l]  * self.tmp
                self.tmp.data -= self.tmp1
                self.rhs_layer.data += self.tmp
            
            self.rhs_fwd[l].data = self.rhs_layer
            if l < self.n_layers-1: 
                self.tmp1.data = self.uold
                self.tmp1.data -=  self.proj_IF[l+1]  * self.tmp1 
                self.unew.data += self.tmp1
                self.tmp.data =  self.layermat[l] *  self.tmp1                
                self.tmp.data -= self.proj_inner[l] * self.tmp 
                self.rhs_layer.data -= self.tmp
                self.uresult.data = self.layer_and_truncated_ext_inv[l] * self.rhs_layer
                self.uresult.data -= self.proj_inner_and_IF[l] * self.uresult  
                self.unew.data += self.uresult    
             
        # backward sweep 
        for l in range(self.n_layers-1,-1,-1): 
            self.rhs_layer.data = self.rhs_fwd[l]
            if l != self.n_layers-1:   
                self.tmp1.data = self.unew
                self.tmp1.data -=  self.proj_IF[l+1]  * self.tmp1
                self.tmp.data =  self.layermat[l] *  self.tmp1 
                self.tmp.data -= self.proj_inner[l] * self.tmp 
                self.rhs_layer.data -= self.tmp
            self.uresult.data = self.layer_and_truncated_ext_inv[l] * self.rhs_layer  
            self.uresult.data -= self.proj_inner_and_IF[l] * self.uresult  
            self.unew.data -= self.proj_inner_and_IF_True[l] * self.unew 
            self.unew.data += self.uresult
            
        u_new.data = self.unew


# tikz export of meshes 
def draw_mesh_tikz(decomp,name,spy_flags=None,label_subdom=False):  
    #rainbow = ["cyan","cyan","cyan","cyan","cyan" ,"cyan","cyan","white","white","white"]
    rainbow = ["cyan","white"]
    file = open("{0}.tex".format(name),"w+")

    file.write("\\documentclass{standalone} \n")
    file.write("\\usepackage{xr} \n")
    file.write("\\usepackage{tikz} \n")
    file.write("\\usepackage{xcolor} \n")
    file.write("\\usepackage{} \n")   
    file.write("\\usetikzlibrary{shapes,arrows,shadows,snakes,calendar,matrix,spy,backgrounds,folding,calc,positioning,patterns} \n")
    file.write("\\begin{document} \n")
    #file.write("\\begin{tikzpicture}[scale = 1.0,spy using outlines={circle, magnification=8, size=3.0cm, connect spies}] \n")
    #file.write("\\begin{tikzpicture}[] \n") 
    if spy_flags:
        file.write("\\begin{{tikzpicture}}[scale = 1.0,spy using outlines={{circle, magnification={0}, size={1}cm,connect spies}}] \n".format(spy_flags["magnification"],spy_flags["size"]))  
        #file.write("\\begin{{tikzpicture}}[spy scope={{magnification={0}, size={1}cm}}, every spy in node/.style={{ magnifying glass, circular drop shadow, fill=white, draw, ultra thick, cap=round}}] \n".format(spy_flags["magnification"],spy_flags["size"]))  
        #file.write("\\begin{{tikzpicture}}[spy scope={{magnification={0}, size={1}cm}}, every spy in node/.style={{ magnifying glass, circular drop shadow, fill=white, draw, ultra thick, cap=round}}] \n".format(spy_flags["magnification"],spy_flags["size"])) 
    else:
        file.write("\\begin{tikzpicture}[scale = 1.0] \n")  

    ddx = 10

    for el in decomp.fes.Elements():
        for j in range(len(decomp.layer2el)):
            #for j in [6,7,8,9,10,11,12]:
            if el.nr in decomp.layer2el[j]:
                coords = []
                for vert in el.vertices:
                    vx,vy,vz = decomp.mesh.ngmesh.Points()[vert.nr+1].p
                    coords.append((ddx*vx,ddx*vy))
                if len(coords) == 3:
                    file.write("\\draw[line width=0.01mm,draw =black, fill={0},fill opacity=1] {1} -- {2} -- {3} -- cycle; \n".format(rainbow[j % 2],coords[0],coords[1],coords[2] ))
                if len(coords) == 4:
                    file.write("\\draw[line width=0.01mm,draw =black, fill={0},fill opacity=1] {1} -- {2} -- {3} -- {4} -- cycle; \n".format(rainbow[j % 2],coords[0],coords[1],coords[2],coords[3]))

    #s1 = "\\draw[line width=2mm,draw =orange,->] (0.0, 0.0) -- node[at end,above]{ \\resizebox{ .125\\linewidth}{!}{\\itshape \\textcolor{orange}{$r$} } }"
    #s1 += "( 0.0, {0}); \n".format( 1.2*ddx)
    #file.write(s1)

    #file.write("\\draw[line width=2mm,draw =mLightBrown] (0,0) circle ({0}); \n".format( 0.326*dx))
    #s1 = "\\draw[line width=1mm,draw =black,dashed] (0.0, 0.0) -- node[midway,above]{ \\resizebox{ .125\\linewidth}{!}{\\itshape $r_{*}$} }"
    #s1 += "( {0}, 0.0); \n".format( 0.326*dx)
    #file.write(s1 )
    
    if spy_flags: 
        file.write("\\spy[orange,every spy on node/.append style={{ultra thick}},spy connection path={{\\draw[line width=1.5pt] (tikzspyonnode) -- (tikzspyinnode);}},size={4}cm] on ({0},{1}) in node [fill=white] at ({2},{3}); \n".format(ddx*spy_flags["on"][0],ddx*spy_flags["on"][1],ddx*spy_flags["at"][0],ddx*spy_flags["at"][1],spy_flags["spy_size"]))
        #file.write("\\spy on ({0},{1}) in node; \n".format(ddx*spy_flags["at"][0],ddx*spy_flags["at"][1]))
    
    if label_subdom:
        #file.write("\\draw[] (5.0,0.0)  node[above]{ \\textcolor{black}{$\Omega_{0}$} }; \n")
        file.write("\\draw[ ] (5.0,3.5)  node[above]{  \\resizebox{ .25\\linewidth}{!}{ \\textcolor{black}{$\Omega_{j-1}$} } };   \n")
        file.write("\\draw[ ] (5.0,5.0)  node[above]{  \\resizebox{ .175\\linewidth}{!}{ \\textcolor{black}{$\Omega_{j}$} } };   \n")
        file.write("\\draw[ ] (5.0,6.65)  node[above]{  \\resizebox{ .25\\linewidth}{!}{ \\textcolor{black}{$\Omega_{j+1}$} } };   \n")
    file.write("\\end{tikzpicture} \n") 
    file.write("\\end{document} \n")           
    file.close()

