import numpy as np
from ngsolve import *
from netgen.meshing import *
from netgen.csg import *
from netgen.geom2d import SplineGeometry
from netgen.meshing import Mesh as netmesh
from ngsolve import Mesh as NGSMesh
from math import sqrt,exp,pi
np.random.seed(123)
#from scipy.special import hankel1,h1vp,jv,yv,jvp,yvp
import ceres_dtn as opt
#from scipy.special import jv,yv,jvp,yvp
from ngs_refsol import FundamentalSolution
import mpmath as mp 
from GRPF import Poles_and_Roots 
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from cmath import sqrt as csqrt
from cmath import exp as cexp

from scipy.sparse import diags,csr_matrix
from scipy.special import spherical_jn,spherical_yn,sici

a = 1.0  # radius of coupling boundary
omega = 16
#omega = 4
Lmax = 144
Nmax  = 14
order = 10
Ns = list(range(Nmax))
D = 3 # dimension
intorder = 20
N_poles_extract = [3,6,9]

order_PML = 2 # order for tensor product PML
use_manual_PML = True # if True then the PML-stretching terms are manually incoporated into the bilinear form 
                      # if False this is done automatically (invisible for the user) by NGSolve 
C_PML = 40 # amplitude of the PML
#eta = a+0.02 # exterior problem posed on interval [a,eta]
eta = a+0.2 # exterior problem posed on interval [a,eta]
#eta = a+0.1 # exterior problem posed on interval [a,eta]

def hankel1(nu,omega):
    return mp.hankel1(nu,omega)

def hankel1p(nu,omega):
    return 0.5*(mp.hankel1(nu-1,omega) - mp.hankel1(nu+1,omega)) 

def sp_hankel1(nu,omega):
    return mp.sqrt(0.5*pi/omega)*mp.hankel1(nu+0.5,omega)

def sp_hankel1p(nu,omega):
    return mp.sqrt(0.5*pi)*( -0.5*mp.power(omega,-3/2)*hankel1(nu+0.5,omega) + np.sqrt(1/omega)*hankel1p(nu+0.5,omega))  

def lam_to_ell(z):
    return -0.5+csqrt( 0.25+a**2*z)

if D == 2:
    r_ext = min(0.5*omega/16,1/2)
    weights = np.array([10**2* abs(complex(mp.hankel1(l,omega*a) / mp.hankel1(l,omega*r_ext) )) for l in range(Lmax)])
    l0 = np.max( np.where(weights > 1e-12*weights[0])[0]) 
    weights = weights[:l0]
    lam = np.array([(l/a)**2 for l in range(l0)]) 
    
    def dtn_ref(nu):
        return complex(-omega*hankel1p(nu,omega*a) /  mp.hankel1(nu,omega*a) )
    
    dtn_nr = np.array([ dtn_ref(sqrt(lami)*a) for lami in lam ]) 
    lam_sample = np.array(lam) 
    dtn_ref_sampled = np.array([ dtn_ref(sqrt(lami)*a) for lami in lam_sample])
else:
    r_ext = 0.5
    weights = np.array([10**2* abs(complex(sp_hankel1(l,omega*a) / sp_hankel1(l,omega*r_ext) )) for l in range(Lmax)])
    #weights[:] = 1.0
    print("weights =", weights)
    l0 = np.max( np.where(weights > 1e-12*weights[0])[0]) 
    weights = weights[:l0]
    lam = np.array([ l*(l+1)/a**2 for l in range(l0)]) 
    
    def dtn_ref(nu):
        return complex(-omega*sp_hankel1p(nu,omega*a) /  sp_hankel1(nu,omega*a) )
    
    dtn_nr = np.array([ dtn_ref(lam_to_ell(lami)) for lami in lam ]) 
    
    #dtn_scipy = np.array([-omega*(spherical_jn(l,omega*a,True)+1j*spherical_yn(l,omega*a,True))/(spherical_jn(l,omega*a,False)+1j*spherical_yn(l,omega*a,False)) for l in range(len(lam))]) 
    
    #plt.plot(lam,dtn_nr.real,label="mpmath")
    #plt.plot(lam,dtn_scipy.real,label="scipy")
    #plt.legend()
    #plt.show()
    
    lam_sample = np.array(lam) 
    dtn_ref_sampled = np.array([ dtn_ref(lam_to_ell(lami)) for lami in lam_sample])

print("len(Ns[1:]) = {0} , len(lam) = {1}".format(len(Ns[1:]),len(lam)))

from GRPF import Poles_and_Roots 
param = {}
# rectangular domain definition z=x+jy x\in[xb,xe], y \in [yb,ye]
if omega == 64:
    #param["xrange"] = [-100,2000]
    #param["xrange"] = [-100,2000]
    param["xrange"] = [-100,10000]
    param["yrange"] = [1,3000]
    param["h"] = 25
else:
    param["xrange"] = [-500,500]
    param["yrange"] = [1,3000]
    param["h"] = 10 
#param["yrange"] = [1,12000]
#param["h"] = 100 
param["Tol"] = 1e-9
param["visual"] = 1 
param["ItMax"] = 20
param["NodesMax"] = 500000
if D == 2:
    result = Poles_and_Roots(lambda z:dtn_ref(mp.sqrt(z)*a),param)
else:
    result = Poles_and_Roots(lambda z:dtn_ref(lam_to_ell(z)),param)
analytic_poles = result["poles"]
analytic_poles = analytic_poles[np.argsort(analytic_poles.imag)]
print("\n analytic poles")
for pole in analytic_poles:
        print("({0},{1})".format(pole.real,pole.imag))

plot_collect_poles = [analytic_poles.real,analytic_poles.imag]
np.savetxt(fname='analytic-poles-omega{0}.dat'.format(omega),
               X=np.transpose(plot_collect_poles),
               header='Re Im',
               comments='')


def dtn_app(A_IE,B_IE,lami):
    n_L,_ = A_IE.shape
    S_lam = A_IE + lami*B_IE
    result = S_lam[0,0]
    if n_L > 1:
        invS_lam = np.linalg.inv(S_lam[1:,1:]) 
        for i in range(1,n_L):
            for j in range(1,n_L):
                result -= S_lam[0,i]*invS_lam[i-1,j-1]*S_lam[j,0]
    return result

def Make1DMesh_flex(R_min,R_max,N_elems=50):

    mesh_1D = netmesh(dim=1)
    pids = []
    coord = [] 
    delta_r = (R_max-R_min)/N_elems
    for i in range(N_elems+1):
        coord.append(R_min + delta_r*i)
        pids.append (mesh_1D.Add (MeshPoint(Pnt(R_min + delta_r*i, 0, 0))))                     
    n_mesh = len(pids)-1
    for i in range(n_mesh):
        mesh_1D.Add(Element1D([pids[i],pids[i+1]],index=1))
    mesh_1D.Add (Element0D( pids[0], index=1))
    mesh_1D.Add (Element0D( pids[n_mesh], index=2))
    mesh_1D.SetBCName(0,"left")
    mesh_1D.SetBCName(1,"right")
    mesh_1D = NGSMesh(mesh_1D)    
    return mesh_1D

mesh_1D_inner = Make1DMesh_flex(a,1000,10000)

ansatz = "minimalIC"
flags = {"max_num_iterations":5000000,
          "check_gradients":False, 
          "use_nonmonotonic_steps":True,
          "minimizer_progress_to_stdout":False,
          "num_threads":4,
          "report_level":"Brief",
          "function_tolerance":1e-6,
          "parameter_tolerance":1e-8}

# define some auxiliary functions

transparent_bc = ["TP-PML","learnedIE","BGT","Astley-Leis","U-Burnett","HSIE"] 

def new_initial_guess(A_old,B_old,ansatz):
    N = A_old.shape[0]
    A_guess = np.zeros((N+1,N+1),dtype='complex')
    B_guess = np.zeros((N+1,N+1),dtype='complex')
    if ansatz in ["medium","full"]:
        A_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        B_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        A_guess[:N,:N] = A_old[:]
        B_guess[:N,:N] = B_old[:]
        A_guess[N,N] = 1.0
    elif ansatz == "minimalIC":
        A_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        A_guess[:N,:N] = A_old[:]
        B_guess[:N,:N] = B_old[:]
        A_guess[N,N] = -100-100j
        B_guess[0,N] = 1e-3*(np.random.rand(1) + 1j*np.random.rand(1)) 
    return A_guess,B_guess


def Setup_TP_PML_matrices(N_r,D): 
    
    pml_min = a   # start of PML
    pml_max = eta # end of PML 
    w = pml_max-pml_min
    
    mesh_1D = Make1DMesh_flex(a,eta,N_r)
    fes_PML = H1(mesh_1D, complex=True,  order=order_PML, dirichlet=[])
    u_PML,v_PML = fes_PML.TnT()
    
    if use_manual_PML:    
        sigma_PML = (C_PML/w)*( (x-a)/w)**2
        s_PML = 1/( (1+1j*sigma_PML/omega))
        stilde_PML = 1 + (1j*C_PML/(x*omega*3))*((x-a)/w)**3
        xtilde = x   

        A_PML = BilinearForm (fes_PML, symmetric=False)
        A_PML += SymbolicBFI( stilde_PML**(D-1)*s_PML*grad(u_PML)*grad(v_PML)*(xtilde/a)**(D-1) ) 
        A_PML += SymbolicBFI( -((omega**2*(stilde_PML)**(D-1))/s_PML)*u_PML*v_PML*(xtilde/a)**(D-1))    
        A_PML.Assemble()

        B_PML = BilinearForm (fes_PML, symmetric=False )
        B_PML += SymbolicBFI( (a/xtilde)**2*(stilde_PML**(D-3)/(s_PML))*u_PML*v_PML*(xtilde/a)**(D-1) )    
        B_PML.Assemble()
   
    else: 
        physpml_sigma = IfPos(x-plm_min,IfPos(pml_max -x,C_PML/w*(x-plm_min)**2/w**2,0),0)
        G_PML =  (1j/omega)*(C_PML/3)*(w/C_PML)**(3/2)*physpml_sigma**(3/2)
        pml_phys = pml.Custom(trafo=x+(1j/omega)*(C_PML/3)*(w/C_PML)**(3/2)*physpml_sigma**(3/2), jac = -1 - 1j*physpml_sigma/omega )
        
        mesh_1D.SetPML(pml_phys,definedon=1)
 
        A_PML = BilinearForm (fes_PML, symmetric=False)
        A_PML += SymbolicBFI( ((x+G_PML)/rho_1D)*grad(u_PML)*grad(v_PML)) 
        A_PML += SymbolicBFI( (- (omega**2*(x+G_PML)  )/(rho_1D*c_1D**2))*u_PML*v_PML)    
        A_PML.Assemble()

        B_PML = BilinearForm (fes_PML, symmetric=False )
        B_PML += SymbolicBFI( (1/( (x+G_PML) *rho_1D))*u_PML*v_PML )    
        B_PML.Assemble()
        
        mesh_1D.UnSetPML(1)

    Nze =  A_PML.mat.nze
    print("A_PML.mat.nze = ",Nze) 
    rows_1,cols_1,vals_1 = A_PML.mat.COO()
    A_csr = csr_matrix((vals_1,(rows_1,cols_1))) 
    A_PML = A_csr.todense() 
    
    rows_2,cols_2,vals_2 = B_PML.mat.COO()
    B_csr = csr_matrix((vals_2,(rows_2,cols_2))) 
    B_PML = B_csr.todense() 
    
    return A_PML,B_PML,Nze,mesh_1D.ne


def diffOp(N_r,a,k0):
    udg = np.arange(1,N_r+1)
    mdg = np.arange(-1,-2*N_r-1-2,-2)
    ldg = udg
    diagonals = [mdg,ldg,udg]
    D = a*diags(np.ones(N_r+1))+(1/(2*1j*k0))*diags(diagonals,[0,-1,1])
    return D

def get_Tm(N_r):
    mdg = np.ones(N_r)
    udg = -np.ones(N_r)
    return diags([mdg,udg],[0,1],shape=(N_r,N_r))

def get_Tp(N_r):
    mdg = np.ones(N_r)
    udg = np.ones(N_r)
    return diags([mdg,udg],[0,1],shape=(N_r,N_r))

def get_HSIEM_matrices(N_r,a,k0,d):
    Cd = (D-1)*(3-D)/4
    D0long = diffOp(4*N_r,1.0,k0)
    I0long = np.linalg.inv(D0long.todense())
    I0 = I0long[:N_r,:N_r]
    Tp = 0.5*get_Tp(N_r)
    Tm = 0.5*get_Tm(N_r)
    
    L1 = (-2*1j*k0/a)* Tp.transpose()@Tp - (2*Cd*1j/(k0*a))*Tm.transpose()@ I0.transpose() @ I0 @ Tm
    L1[0,0] = L1[0,0] + (D-1)/(2*a)
    L2 = (2*a*1j/k0)*Tm.transpose() @ I0.transpose() @ I0 @ Tm
    L3 = (2*a*1j/k0)*Tm.transpose() @ Tm
    neum_repr = 2*1j*k0*np.ones(N_r)
    neum_repr[0] = 1j*k0-0.5
    return L1,L2,L3,(1/a)*neum_repr


def I_IE(j):
    if j >=2:
        return 1/(j-1) + 2*1j*omega*a*I_IE(j-1)/(j-1)
    if j==1:
        Si_integral,Ci_integral = sici(2*omega*a)
        return cexp(-2*1j*omega*a)*(-Ci_integral-1j*Si_integral+ 1j*0.5*pi)

def J_IE(mu,nu):
    if mu == 0 and nu ==0:
        return 1/a - 1j*omega
    else:
        return (1/a)*(mu+1)*(nu+1)*I_IE(mu+nu+2)-1j*omega*(mu+nu+2)*I_IE(mu+nu+1)-2*omega**2*a*I_IE(mu+nu)

def Q_IE(k,mu,nu):
    return a/(a**k*(nu+mu+k+1))

def R_IE(mu,nu):
    return -1j*omega*(mu+2-nu)*Q_IE(1,mu,nu)+(nu+1)*(mu+3)*Q_IE(2,mu,nu)

def get_classical_IE_calc(N,omega,formulation="Astley-Leis",D=3):
    if D == 2:
        print("Classical infinite element matrices currently not implemented for dimesion ", D)
        return None
    elif D == 3:
        A_IE = np.zeros((N+1,N+1),dtype='complex')
        B_IE = np.zeros((N+1,N+1),dtype='complex')
        if formulation == "Astley-Leis":
            for mu in range(N+1):
                for nu in range(N+1):
                    
                    f_mu = CoefficientFunction( (a/x)**(mu+3)) 
                    f_nu = CoefficientFunction( (a/x)**(nu+1))
                    df_mu = CoefficientFunction(-(1j*omega + (mu+3)/x)*(a/x)**(mu+3))  
                    df_nu = CoefficientFunction((1j*omega + (-1)*(nu+1)/x)*(a/x)**(nu+1))  
                    
                    f_mu0 = CoefficientFunction( (a/x)**(0+3)) 
                    f_nu0 = CoefficientFunction( (a/x)**(0+1))
                    df_mu0 = CoefficientFunction(-(1j*omega + (0+3)/x)*(a/x)**(0+3))  
                    df_nu0 = CoefficientFunction((1j*omega + (-1)*(0+1)/x)*(a/x)**(0+1))  

                    if mu == 0 and nu == 0:
                        g_mu = f_mu0
                        dg_mu = df_mu0
                        g_nu = f_nu0
                        dg_nu = df_nu0
                    elif mu == 0 and nu >= 1:
                        g_mu = f_mu0
                        dg_mu = df_mu0
                        g_nu =  f_nu-f_nu0
                        dg_nu =  df_nu-df_nu0
                    elif mu >= 1 and nu == 0:
                        g_mu = f_mu-f_mu0
                        dg_mu = df_mu-df_mu0
                        g_nu = f_nu0
                        dg_nu = df_nu0
                    else:
                        g_mu = f_mu-f_mu0
                        dg_mu = df_mu-df_mu0
                        g_nu =  f_nu-f_nu0
                        dg_nu =  df_nu-df_nu0
                    A_IE[mu,nu] = Integrate ( (dg_mu*dg_nu-omega**2*g_mu*g_nu)*(x/a)**2  , mesh_1D_inner, VOL,order=intorder)
                    B_IE[mu,nu] = Integrate ( g_mu*g_nu  , mesh_1D_inner, VOL,order=intorder)
            return A_IE,B_IE
        else: 
            print("Formulation {0} not implemented".format(formulation))
            return None


def get_classical_IE_matrices(N,omega,formulation="Astley-Leis",D=3):
    if D == 2:
        print("Classical infinite element matrices currently not implemented for dimesion ", D)
        return None
    elif D == 3:
        A_IE = np.zeros((N+1,N+1),dtype='complex')
        B_IE = np.zeros((N+1,N+1),dtype='complex')
        if formulation == "Astley-Leis":
            for mu in range(N+1):
                for nu in range(N+1):
                    if mu == 0 and nu == 0:
                        A_IE[mu,nu] = R_IE(0,0)
                        #A_IE[mu,nu] = (nu+1)*(mu+3)*Q_IE(2,0,0)-1j*omega*(mu+2-nu)*Q_IE(1,0,0)
                        B_IE[mu,nu] = a**2*Q_IE(2,0,0)
                    elif mu == 0 and nu >= 1:
                        A_IE[mu,nu] = R_IE(0,nu)-R_IE(0,0)
                        #A_IE[mu,nu] = (nu+1)*(mu+3)*(Q_IE(2,0,nu)-Q_IE(2,0,0))-1j*omega*(mu+2-nu)*(Q_IE(1,0,nu)-Q_IE(1,0,0)) 
                        B_IE[mu,nu] = a**2*(Q_IE(2,0,nu)-Q_IE(2,0,0))
                    elif mu >= 1 and nu == 0:
                        A_IE[mu,nu] = R_IE(mu,0)-R_IE(0,0)
                        #A_IE[mu,nu] =  (nu+1)*(mu+3)*(Q_IE(2,mu,0)-Q_IE(2,0,0))-1j*omega*(mu+2-nu)*(Q_IE(1,mu,0)-Q_IE(1,0,0)) 
                        B_IE[mu,nu] = a**2*(Q_IE(2,mu,0)-Q_IE(2,0,0))
                    else:
                        A_IE[mu,nu] = R_IE(mu,nu)-R_IE(0,nu)-R_IE(mu,0)+R_IE(0,0)
                        #A_IE[mu,nu] = (nu+1)*(mu+3)*(Q_IE(2,mu,nu)-Q_IE(2,0,nu)-Q_IE(2,mu,0)+Q_IE(2,0,0))-1j*omega*(mu+2-nu)*(Q_IE(1,mu,nu)-Q_IE(1,0,nu)-Q_IE(1,mu,0)+Q_IE(1,0,0))
                        B_IE[mu,nu] = a**2*(Q_IE(2,mu,nu)-Q_IE(2,0,nu)-Q_IE(2,mu,0)+Q_IE(2,0,0)) 
            return A_IE,B_IE

        if formulation == "U-Burnett":
            for mu in range(N+1):
                for nu in range(N+1):
                    if mu == 0 and nu == 0:
                        A_IE[mu,nu] = J_IE(0,0)
                        B_IE[mu,nu] = a*I_IE(2)
                    elif mu == 0 and nu >= 1:
                        A_IE[mu,nu] = J_IE(0,nu)-J_IE(0,0)
                        B_IE[mu,nu] = a*(I_IE(nu+2)-I_IE(2))
                    elif mu >= 1 and nu == 0:
                        A_IE[mu,nu] = J_IE(mu,0)-J_IE(0,0)
                        B_IE[mu,nu] = a*(I_IE(mu+2)-I_IE(2))
                    else:
                        A_IE[mu,nu] = J_IE(mu,nu)-J_IE(0,nu)-J_IE(mu,0)+J_IE(0,0)
                        B_IE[mu,nu] = a*(I_IE(nu+mu+2)-I_IE(nu+2)-I_IE(mu+2)+I_IE(2))

            return A_IE,B_IE
        else: 
            print("Formulation {0} not implemented".format(formulation))
            return None

def get_BGT(omega,D=3): 
        A_IE = np.zeros((1,1),dtype='complex')
        B_IE = np.zeros((1,1),dtype='complex')
        alp = -1j*omega+1/a
        A_IE[0,0] = alp
        B_IE[0,0] = 0.5/alp
        return A_IE,B_IE

def calc_poles(A,B):
    if A.shape[0] == 1:
        print("A.shape[0] == 1")
        return [] 
    else:
        return -np.linalg.eigvals( np.linalg.inv(B[1:,1:]) @ A[1:,1:])

for tbc in transparent_bc:
    print("---------------------------------------------------------------------")
    
    
    A_IE = []
    B_IE = []
    relative_residuals = []
    Nzes =[]
    Nelem_PML = []
    Ndim_PML = []

    np.random.seed(123)
    
    if tbc == "TP-PML":
        for N in Ns:
            A_N,B_N,Nze,Nel = Setup_TP_PML_matrices(N+1,D) 
            A_IE.append(A_N), B_IE.append(B_N),Nzes.append(Nze)
            Ndim_PML.append(A_N.shape[0])
            Nelem_PML.append(Nel)         
            print("cond(A_IE) = ", np.linalg.cond(A_N))
    
    if tbc == "learnedIE":
        l_dtn = opt.learned_dtn(lam,dtn_nr,weights**2)

        A_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
        B_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)

        for N in Ns: 
            l_dtn.Run(A_guess,B_guess,ansatz,flags)
            print("cond(A_learned) = ", np.linalg.cond(A_guess))
            A_IE.append(A_guess.copy()), B_IE.append(B_guess.copy())
            A_guess,B_guess = new_initial_guess(A_IE[N],B_IE[N],ansatz)
    
    if tbc == "Astley-Leis" or tbc =="U-Burnett":
        for N in Ns:
            A_N,B_N = get_classical_IE_matrices(N,omega,formulation=tbc,D=D) 
            #A_N,B_N = get_classical_IE_matrices(N,omega,formulation="U-Burnett",D=D) 
            
            #A_Nc,B_Nc = get_classical_IE_calc(N,omega,formulation="Astley-Leis",D=D)
            #print("analytic. \n A = {0}, \n B = {1}".format(A_N,B_N))
            #print("comp. \n A = {0}, \n B = {1}".format(A_Nc,B_Nc))
            #print("diff_A = {0}, diff_B = {1}".format(np.linalg.norm(A_N-A_Nc),np.linalg.norm(B_N-B_Nc)))
            #input("")
            #A_IE.append(A_Nc), B_IE.append(B_Nc)
            A_IE.append(A_N), B_IE.append(B_N)
            print("cond(A_IE) = ", np.linalg.cond(A_N))

    if tbc == "BGT":
        A_1,B_1 = get_BGT(omega,D=3)
        A_IE = [A_1]
        B_IE = [B_1]

    if tbc == "HSIE":
        for N in Ns:
            La,Lb,Lc,neum_repr = get_HSIEM_matrices(N+1,a,omega*a,D)
            A_tmp = La-omega**2*Lc
            A_full = np.zeros((N+1,N+1),dtype='complex')
            B_full = np.zeros((N+1,N+1),dtype='complex')
            A_full[:,:] = A_tmp[:,:]
            B_full[:,:] = Lb[:,:]
            A_IE.append(A_full)
            B_IE.append(B_full)
            #A_IE.append(La-omega**2*Lc)
            #B_IE.append(Lb)
    
    if tbc != "BGT":
        with open('condext-'+tbc+'-omega{0}.dat'.format(omega),'w') as the_file:
            the_file.write('lam N cond \n')
            cond_ext = np.zeros((len(Ns[1:]),len(lam)))
            
            if tbc != 'TP-PML':
                for N in Ns[1:]:
                    for l,lami in enumerate(lam):
                        cond_N_lami =  np.linalg.cond( (A_IE[N])[1:,1:] + lami*(B_IE[N])[1:,1:] )
                        cond_ext[N-1,l] = cond_N_lami
                        the_file.write('{0} {1} {2} \n'.format(lami,N,cond_N_lami)) 
                    the_file.write('\n')
            else:
                for N,Ndim in zip(Ns[1:],Ndim_PML[1:]):
                    for l,lami in enumerate(lam):
                        cond_N_lami =  np.linalg.cond( (A_IE[N])[1:,1:] + lami*(B_IE[N])[1:,1:] )
                        cond_ext[N-1,l] = cond_N_lami
                        the_file.write('{0} {1} {2} \n'.format(lami,Ndim-1,cond_N_lami)) 
                    the_file.write('\n')

        
        plt.imshow(cond_ext,norm=LogNorm(vmin=1e0,vmax=1e16),origin='lower',cmap=plt.get_cmap('jet'),interpolation=None)
        plt.colorbar()
        plt.title(tbc)
        plt.show()



    # results for real / imaginary part 
    plot_collect_real = [lam_sample.real,dtn_ref_sampled.real]
    fname_real  = "dtn-"+tbc+"-real-omega{0}.dat".format(omega)
    plot_collect_imag = [lam_sample.real,dtn_ref_sampled.imag]
    fname_imag  = "dtn-"+tbc+"-imag-omega{0}.dat".format(omega)
    header_str = 'lam zeta'


    plot_collect_relerr = [lam_sample.real]
    header_str_relerr = 'lam'
    fname_relerr  = "relerr"+tbc+"-omega{0}.dat".format(omega)
    
    rel_err = []
    dtn_N = []
    for N,(A_N,B_N) in enumerate(zip(A_IE,B_IE)):
        dtn_approx = np.zeros(len(lam_sample),dtype='complex')
        print(tbc)
        print("--------------------------")
        opt.eval_dtn_fct(A_N,B_N,lam_sample,dtn_approx,False)
        dtn_N.append(dtn_approx)
        rel_err.append(np.abs(dtn_ref_sampled-dtn_approx)/np.abs(dtn_ref_sampled))
        #if N > 2:
            #plt.plot(lam,dtn_approx.real,label='N={0}'.format(N))
            #plt.plot(lam,dtn_approx.imag,label='N={0}'.format(N))
        plot_collect_real.append(dtn_approx.real)
        plot_collect_imag.append(dtn_approx.imag)
        plot_collect_relerr.append(rel_err[-1])
        if tbc == "TP-PML":
            header_str += " Nze{0}".format(Nzes[N])
            header_str_relerr += " Nze{0}".format(Nzes[N])
        else:
            header_str += " N{0}".format(N)
            header_str_relerr += " N{0}".format(N)

    plt.plot(lam,dtn_ref_sampled.real,label='ref',linewidth=3,color='gray')
    for N in range(len(A_IE)):
        #if N > 4:
        plt.plot(lam,dtn_N[N].real,label='N={0}'.format(N))
    plt.title(tbc)
    plt.legend()
    plt.show()

    for N in range(len(A_IE)):
        #if N >= 5:
        plt.semilogy(lam,rel_err[N],label='N={0}'.format(N))
    plt.legend()
    plt.title(tbc)
    plt.show()
    
    if tbc != 'BGT':
        if tbc == 'TP-PML':
            N_cropped = Ns 
        else:
            N_cropped = Ns[1:]
        for N in N_cropped:
            app_poles_collect = [] 
            if tbc == 'TP-PML': 
                fname_app_poles = 'approx-poles-'+tbc+'-omega{0}-Nzes{1}.dat'.format(omega,Nzes[N])
            else:
                fname_app_poles = 'approx-poles-'+tbc+'-omega{0}-N{1}.dat'.format(omega,N)
            app_poles_header = 'Re Im'
            approx_poles = calc_poles(A_IE[N],B_IE[N])
            app_poles_collect.append(approx_poles.real)
            app_poles_collect.append(approx_poles.imag)    
            np.savetxt(fname=fname_app_poles,
                       X=np.transpose(app_poles_collect),
                       header=app_poles_header,
                       comments='')

    colors = ['b', 'c', 'y', 'm', 'r']
    marker = ['*','o','s','x','.']
    fig, ax = plt.subplots(figsize=(8,8))
    ax.scatter(analytic_poles.real,analytic_poles.imag,s=100, c='k', label="exact",marker='x',
                               alpha=1.0, edgecolors='none')
    for N,clr,mkr in zip(range(3,len(A_IE)),colors,marker):
        approx_poles = calc_poles(A_IE[N],B_IE[N])
        ax.scatter(approx_poles.real,approx_poles.imag,s=100, c=clr, label="$N$ = {0}".format(N),marker=mkr,
                               alpha=1.0, edgecolors='none')
    ax.legend()
    ax.grid(True)
    plt.title(tbc)
    plt.show()


    np.savetxt(fname=fname_real,
               X=np.transpose(plot_collect_real),
               header=header_str,
               comments='')
    np.savetxt(fname=fname_imag,
               X=np.transpose(plot_collect_imag),
               header=header_str,
               comments='')
    np.savetxt(fname=fname_relerr,
               X=np.transpose(plot_collect_relerr),
               header=header_str_relerr,
               comments='')

    if tbc == 'TP-PML':
        idx_select = np.array([0,1,3,5,7,9])
        Nzes = np.array(Nzes)[idx_select]
        Nelem_PML = np.array(Nelem_PML)[idx_select]
        Ndim_PML = np.array(Ndim_PML)[idx_select]
        fname_PML_table = "DtN-comp-TP-PML-table.dat"
        header_str_PML_table = "Ndim NdimSquared Nze Nelem" 
        plot_collect_PML_table = [Ndim_PML,Ndim_PML**2,Nzes,Nelem_PML] 
        np.savetxt(fname= fname_PML_table, 
                   X=np.transpose(plot_collect_PML_table),
                   header=header_str_PML_table,
                   comments='')



