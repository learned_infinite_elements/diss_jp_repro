from math import pi
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from scipy.integrate import simps 
import sys

if ( len(sys.argv) > 1 and sys.argv[1] == "download"):
    print("Downloading power spectra from Gro.data catalogue") 
    file_VALC_meshed, header_VALC_meshed = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/N0S2QJ/4VW7LH","power-spectrum-VALC-meshed.out")
    file_0, header_0 = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/N0S2QJ/ZJJDJD","power-spectrum-N0-VALC.out")
    file_1, header_1 = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/N0S2QJ/KL8NJU","power-spectrum-N1-VALC.out")
    file_2, header_2 = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/N0S2QJ/KAHJKF","power-spectrum-N2-VALC.out")
    file_3, header_3 = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/N0S2QJ/QA3B6H","power-spectrum-N3-VALC.out")
    file_4, header_4 = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/N0S2QJ/KPYHDU","power-spectrum-N4-VALC.out")

plt.rc('legend', fontsize=18)
plt.rc('axes', titlesize=18)
plt.rc('axes', labelsize=18)
plt.rc('xtick', labelsize=14)    
plt.rc('ytick', labelsize=14)

power_spectrum_meshed_atmo = np.loadtxt('power-spectrum-VALC-meshed.out',delimiter=',')
power_spectrum_learned = [] 

for N in range(5):
    power_spectrum_learned.append(np.loadtxt('power-spectrum-N{0}-VALC.out'.format(N),delimiter=','))

f_hz_comp = 1e-3*2*pi*np.linspace(0,8.3,power_spectrum_meshed_atmo.shape[0])
f_hz = f_hz_comp

Nt = 2**12
N_theta = 1000
thetas = np.linspace(0,pi,N_theta)

idxx = [ np.argmin(np.abs(thetas - i*pi/6 ) ) for i in range(1,4)]
idxx_label = ["30deg ","60deg ","90deg "]
max_vals = [0.95,0.75,0.8]


ts = np.linspace(0,300,Nt)
tss =  60*ts/(2*pi)
domega = f_hz[-1]/power_spectrum_meshed_atmo.shape[0]
dt = tss[-1]/(Nt)
dt_phys = 2*pi*dt
Ntmp = int(1/(domega*dt))

from scipy.special import eval_legendre

def compute_TD_diagram(power_spectrum,f_hz,name_str="dummy.png",min_scale = 2e11,only_omega=True,W_tau=False,obs=False):

    domega = f_hz[-1]/power_spectrum.shape[0]
    dt = tss[-1]/(Nt)
    dt_phys = 2*pi*dt
    Ntmp = int(1/(domega*dt))
    
    def F_ell(l):
        if l < 300:
            #print("l = {0}, val = {1}".format(l,0.5*( 1-np.tanh(0.03*l-3))))
            return 0.5*( 1-np.tanh(0.03*l-3))
        else:
            return 0.0
    
    def PI_scal(f):
        result = 1 + ((abs(f)-3.3*1e-3*2*pi)/ (0.6*1e-3*2*pi) )**2
        return 1/result 
   
    C_tau = np.zeros(( power_spectrum.shape[0], len(thetas)), dtype="float")
    
    C_tau_unscaled = np.zeros(( power_spectrum.shape[0], len(thetas)), dtype="float")
    
    if W_tau or obs:
        precomputed_factors = np.zeros( power_spectrum.shape) 
    precomputed_factors_unscaled = np.zeros( power_spectrum.shape) 
    
    #print("precomputed_factors.shape = ", precomputed_factors.shape ) 
    for i in range(power_spectrum.shape[0]):
        for l in range(power_spectrum.shape[1]):
            if W_tau:
                if abs(f_hz[i]) > 1e-13:
                    precomputed_factors[i,l] =(1/(f_hz[i]))*PI_scal(f_hz[i])*0.5*F_ell(l)
                else:
                    precomputed_factors[i,l] = PI_scal(f_hz[i])*0.5*F_ell(l)
            if obs:
                precomputed_factors[i,l] = 0.5*F_ell(l)
            
            if abs(f_hz[i]) > 1e-13:
                precomputed_factors_unscaled[i,l] = (1/(f_hz[i]))*PI_scal(f_hz[i])*0.5*F_ell(l)
            else:
                precomputed_factors_unscaled[i,l] = PI_scal(f_hz[i])*0.5*F_ell(l)
            #precomputed_factors_unscaled[i,l] = (1/(f_hz[i]))*PI_scal(f_hz[i])

    for j in range(len(thetas)):
        l_legendre = np.array([(l+0.5)*eval_legendre(l,np.cos(thetas[j])) for l in range(power_spectrum.shape[1]) ])
        print("thetas[j] = ", thetas[j])
        for i in range(power_spectrum.shape[0]):
            if W_tau or obs:
                li_factor = np.multiply(precomputed_factors[i,:],l_legendre)
                C_tau[i,j] = np.dot(power_spectrum[i,:],li_factor)
            li_factor_unscaled = np.multiply(precomputed_factors_unscaled[i,:],l_legendre)
            C_tau_unscaled[i,j] = np.dot(power_spectrum[i,:],li_factor_unscaled) 

    if W_tau: 
        C_t = np.zeros((Nt,len(thetas)), dtype="complex")
        
        C_omega_multiplied = np.zeros(( power_spectrum.shape[0], len(thetas)), dtype="complex")
        for i in range(power_spectrum.shape[0]):
            C_omega_multiplied[i,:] = -1j*f_hz[i]*C_tau_unscaled[i,:]
        
        C_partial_t = np.zeros((Nt,len(thetas)), dtype="float") 
        W_omega  = np.zeros(( power_spectrum.shape[0], len(thetas)), dtype="complex")
        for i in range(len(thetas)):
            C_partial_t[:,i] = np.fft.fft(C_omega_multiplied[:,i],n=Ntmp)[:Nt].real

        window_header = "t  " 
        cross_header = "t  " 
        plot_collect_window = [ts]
        plot_collect_cross  = [ts]
        for j in range(len(thetas)):  
            if j in idxx:
                cross_header += idxx_label[idxx.index(j)]
                window_header += idxx_label[idxx.index(j)]
                maxi = max_vals[idxx.index(j)]
                C_t[:,j] = np.fft.fft(C_tau[:,j],n=Ntmp)[:Nt]
                normalized_C = C_t[:,j].real/ np.max(np.abs(C_t[:,j].real))
                tg = ts[  np.where( normalized_C > maxi )[0][0] ]
                sigma = 12
                weights = np.exp(-0.5*(ts-tg)**2/sigma**2)
                plot_collect_window.append(weights)
                plot_collect_cross.append(normalized_C)
                plt.plot(ts,weights,label="weights")
                plt.plot(ts,normalized_C,label="C")
                plt.legend()
                plt.show()
            else:
                weights = np.ones(len(ts))

            l2_norm = simps(weights[:]*C_partial_t[:,j]**2,x=None,dx=dt_phys)
            W_omega[:,j] = np.fft.ifft(weights[:]*C_partial_t[:,j],n=Ntmp)[:power_spectrum.shape[0]]
            W_omega[:,j] *= 1/l2_norm
        
        np.savetxt(fname = "cross-tt.dat", 
                       X = np.transpose(plot_collect_cross),
                       header = cross_header,
                       comments = '')
            
        np.savetxt(fname = "window-tt.dat", 
                       X = np.transpose(plot_collect_window),
                       header = window_header,
                       comments = '')

        
        return C_tau_unscaled,W_omega
 
    if obs: 
        C_t = np.zeros((Nt,len(thetas)), dtype="complex")
         
        window_header = "t  " 
        cross_header = "t  " 
        plot_collect_window = [ts]
        plot_collect_cross  = [ts]
        for j in range(len(thetas)):  
            if j in idxx:
                cross_header += idxx_label[idxx.index(j)]
                window_header += idxx_label[idxx.index(j)]
                
                print(" C_tau[:,j] = " , C_tau[:,j])
                C_t[:,j] = np.fft.fft(C_tau[:,j],n=Ntmp)[:Nt]
                print(" C_t[:,j] =", C_t[:,j] )
                print("np.max(np.abs(C_t[:,j].real)) =", np.max(np.abs(C_t[:,j].real)) )
                normalized_C = C_t[:,j].real/ np.max(np.abs(C_t[:,j].real))
                plot_collect_cross.append(normalized_C)
                plt.plot(ts,normalized_C,label="C")
                plt.legend()
                plt.show()
        
        np.savetxt(fname = "cross-obs.dat", 
                       X = np.transpose(plot_collect_cross),
                       header = cross_header,
                       comments = '')
            
        return C_tau_unscaled
 

    if only_omega:
        return C_tau_unscaled

    print("Ntmp = ", Ntmp)
    C_t = np.zeros((Nt,len(thetas)), dtype="complex")
    for i in range(len(thetas)):
        C_t[:,i] = np.fft.fft(C_tau[:,i],n=Ntmp)[:Nt]
    print("C_t = ", C_t)
    print("np.max(C_t.real) = ", np.max(C_t.real)) 
    print("np.min(C_t.real) = ", np.min(C_t.real)) 
     
    return C_tau,C_t.real

C_tau,W_omega = compute_TD_diagram(power_spectrum = power_spectrum_meshed_atmo,f_hz=f_hz,name_str="time-distance-meshed-atmo",min_scale = 0.2,only_omega=True,W_tau=True)

C_t_learned = []
C_tau_learned = []
for N in range(5):
    C_tau_learned.append(compute_TD_diagram(power_spectrum=power_spectrum_learned[N],f_hz=f_hz, name_str="time-distance-N{0}".format(N),min_scale = 0.2) )


plot_collect_err = [list(range(5))]
err_header = "N "
for j in idxx:
    print("thetha =", thetas[j]) 
    err_header += idxx_label[ idxx.index(j) ]
    diffs = []
    for N in range(5):
        diff = simps( W_omega[:,j].conjugate()*(C_tau[:,j] - C_tau_learned[N][:,j]),x=None,dx=dt_phys)
        diffs.append(abs(diff))
    plot_collect_err.append( np.array(diffs))
    #plot_collect_err.append( np.array(diffs) / diffs[0])
    print("diff =" ,diffs)
#print("diffs =" ,diffs)

np.savetxt(fname = "tt-error.dat", 
            X = np.transpose(plot_collect_err),
            header = err_header,
            comments = '')
