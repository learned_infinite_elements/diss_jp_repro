#from mpmath import *

import mpmath as mp
from math import pi
from cmath import sqrt,exp,log
import numpy as np
import matplotlib.pyplot as plt
from GRPF import Poles_and_Roots
from scipy.optimize import newton,root_scalar
import scipy.integrate as sp_integrate
from scipy import special

param = {} # for searching poles
param["Tol"] = 1e-9
param["visual"] = 1 
param["ItMax"] = 10
param["NodesMax"] = 500000


#analytic_poles = np.append(analytic_poles1,analytic_poles2)   

k =16
a_gamma = 1
omega = k

mp.mp.dps = 50 # precision for evaluating special functions

mus =  [1/8,1/4,1/2,1,2,4,8]
mus_str = ["eights","quarter","half","one","two","four","eight"]
Ms = 1000 # number of sample points to evaluate uniform error 
lam_sample = np.array([2*k**2*i/Ms for i in range(Ms+1)]) # sample points 

lam_sample_mu = []
for mu in mus:
    lam_sample_mu.append( np.array([mu*k**2*i/Ms for i in range(Ms+1)]) ) 

show_plots = True # show intermediate plots 


def hankel1(nu,x):
    return mp.hankel1(nu,x)

def h1vp(nu,x):
    return 0.5*(mp.hankel1(nu-1,x) - mp.hankel1(nu+1,x)) 

def dtn_ref(lam):
    return complex(-k*h1vp(mp.sqrt(lam)*a_gamma,k*a_gamma) / hankel1(mp.sqrt(lam)*a_gamma,k*a_gamma))

def H(z):
    result = complex(mp.hankel1(z ,k*a_gamma))
    return result

def dlamH(z):
    result = complex( mp.diff( lambda x: mp.hankel1(x,k*a_gamma),z) )
    return result

def Hp(z):
    return complex( 0.5*( mp.hankel1(z-1,k*a_gamma) - mp.hankel1(z+1,k*a_gamma)) )

def dlamHp(z):
    return complex( mp.diff( lambda x: 0.5*(mp.hankel1(x-1,k*a_gamma) - mp.hankel1(x+1,k*a_gamma)), z,n=1) )


dtn_ref_samples = np.array( [ dtn_ref(lami) for lami in lam_sample] )
dtn_ref_sample_mu = [] 
for j in range(len(mus)):
    dtn_ref_sample_mu.append( np.array( [ dtn_ref(lami) for lami in lam_sample_mu[j] ] ) )


#################################################################################
### Compute poles with small imaginary part using discrete argument principle ###
#################################################################################

param["xrange"] = [-4500,500]
param["yrange"] = [0.5,11000]
param["h"] = 50.0
result = Poles_and_Roots(dtn_ref,param)
lam  = result["poles"]
lamp = result["roots"]

lam = lam[np.argsort(lam.imag)]
lamp = lamp[np.argsort(lamp.imag)]

Mmax = min(len(lam),len(lamp))

nu_s = [complex(mp.sqrt(lami)) for lami in lam]
nup_s = [complex(mp.sqrt(lami)) for lami in lamp]

# Refine roots and poles using Newton iteration
nus_newton = []
nups_newton = []

for guess,guess_p in zip(nu_s,nup_s):
    #print("guess =" , guess)
    root = newton(H,guess,dlamH)
    nus_newton.append(root)
    #print("guess_p = ", guess_p)
    rootp,r_results = newton(Hp,guess_p,dlamHp,full_output=True,disp=False)
    if r_results.converged:
        nups_newton.append(rootp)
    else:
        print("Newton did not converge for guess_p**2 = ", guess_p*guess_p)

vals = [ abs(H(nu)) for nu in nus_newton]
vals_p = [ abs(Hp(nu)) for nu in nups_newton]
print("REFINED WITH NEWTON \n")
print("vals = ", vals)
print("----") 
print("vals_p = ", vals_p)

lam = [nn*nn for nn in nus_newton]
lamp = [nn*nn for nn in nups_newton]

#################################################################################
### Compute poles with large imaginary using asymptotic formulas + Newton     ###
#################################################################################

N_start = 0
N_roots = 10000

zeta32 = -(3/2)* ( sp_integrate.quad(lambda t: sqrt(1-t*t).real/t,1,omega)[0] + 1j* sp_integrate.quad(lambda t: sqrt(1-t*t).imag/t,1,omega)[0] ) 
a,ap,ai,aip = special.ai_zeros(N_roots)

def solve_transcendental_eq(s,guess,deriv=False): 
    if deriv:
        aval = ap[s]
    else:
        aval = a[s]
    def h(x):
        x32 = exp(1.5*log(x))
        return x32*exp((2/3)*x32)-2*exp(1.5*log(aval))*exp(-1j*pi)/(exp(1)*omega)
    def hp(x):
        x32 = exp(1.5*log(x))
        x12 = exp(0.5*log(x))
        return (1.5*x12 + x*x )*exp((2/3)*x32)
    root = newton(h,guess,hp)
    #if deriv:
    #    print("hval = {0},root = {1} ".format(h(root),root))
    #print("hval = {0},root = {1} ".format(h(root),root))
    return root

alphas = []
betas = [] 
prev_alpha = 1 + 1j
prev_beta = 1 + 1j
for s in range(N_roots):
    root_alpha_s = solve_transcendental_eq(s,prev_alpha,deriv=False)
    prev_alpha = root_alpha_s
    alphas.append(root_alpha_s)
    root_beta_s = solve_transcendental_eq(s,prev_beta,deriv=True)
    prev_beta = root_beta_s
    betas.append(root_alpha_s)

nu_s = [  exp(-1j*pi)*exp(1.5*log(a[s]))/exp(1.5*log(alphas[s]))  for s in range(N_start,N_roots)  ] 
nup_s = [  exp(-1j*pi)*exp(1.5*log(ap[s]))/exp(1.5*log(betas[s]))  for s in range(N_start,N_roots)  ] 

#print("nu_s = ", nu_s)

vals = [ abs(H(nu)) for nu in nu_s]
vals_p = [ abs(Hp(nu)) for nu in nup_s]

#print("vals = ", vals)
#print("----") 
#print("vals_p = ", vals_p)

#print("nu_s =", nu_s)

nus_newton = []
nups_newton = []

for guess,guess_p in zip(nu_s,nup_s ):
    #print("guess =", guess)
    root = newton( H ,complex(guess), dlamH )
    nus_newton.append(root)
    #print("guess_p = ", guess_p)
    rootp,r_results = newton( np.frompyfunc(Hp,1,1) ,guess_p,np.frompyfunc(dlamHp,1,1) ,full_output=True,disp=False)
    if r_results.converged:
        nups_newton.append(rootp)
    else:
        print("Newton did not converge for guess_p**2 = ", guess_p*guess_p)
    #print("val = ", abs(complex(mp.hankel1(root,omega)))) 

#vals = [ abs(H(nu)) for nu in nus_newton]
#vals_p = [ abs(Hp(nu)) for nu in nups_newton]
#print("REFINED WITH NEWTON \n")
#print("vals = ", vals)
#print("----") 
#print("vals_p = ", vals_p)

##################################################################
# Combine both roots and perform checks  
##################################################################


lam_newton = np.array([nn*nn for nn in nus_newton])
lam_newton = lam_newton[np.argsort(lam_newton.imag)]
print("lam =", lam)
print("lam_newton =", lam_newton)
i0 = np.max(np.nonzero(np.abs(lam_newton-lam[-1])<1e-10)) + 1 
lam = np.concatenate((lam,lam_newton[i0:]),axis=0) 

lamp_newton = np.array([nn*nn for nn in nups_newton])
lamp_newton = lamp_newton[np.argsort(lamp_newton.imag)]
print("lamp =", lamp)
print("lamp_newton =", lamp_newton)
j0 = np.max(np.nonzero(np.abs(lamp_newton-lamp[-1])<1e-10)) + 1
lamp = np.concatenate((lamp,lamp_newton[j0:]),axis=0) 


m = np.zeros_like(lam,dtype=bool)
m[np.unique(lam,return_index=True)[1]] = True

#print("~m = ", ~m)
if len(lam[~m]) != 0:
    print("nonunique lam = ", lam[~m])

m = np.zeros_like(lamp,dtype=bool)
m[np.unique(lamp,return_index=True)[1]] = True
#print("~m = ", ~m)
if len( lamp[~m]) != 0:
    print("nonunique lam of deriv = ", lamp[~m])

#print("lam = ", lam[:100])
#print("lamp = ", lamp[:100])  
# Storage for results 
Ns_learned = [] 
err_learned = [] 

Ns_analytic = [] 
err_analytic = [] 

err_analytic_mu = []

inv_length = []
sup_inv_length = []

conj_sums = []

#for Mmax in [8,16,32] + list(range(33,64)):
Mmaxes = [2**(j+2) for j in range(12)]
for Mmax in Mmaxes: 

    Ns_analytic.append(Mmax)

    #print("Mmax = ", Mmax)
    #print("lam = ", lam[:Mmax])
    #print("lamp = ", lamp[:Mmax]) 
    
    def get_coeff(i,lin=False):
        result = 1.0
        for j in range(Mmax):
            fac = 1.0
            if j < Mmax -1:
                fac = (lam[i]-lamp[j])
            if j != i:
                result *= fac*(lam[j]/lamp[j])/(lam[i]-lam[j])
            else:
                result *= fac*(lam[j]/lamp[j])
        if lin:
            return result 
        else:
            return result*(lam[i]-lam[Mmax-1])

    lin_fac = 0.0
    for i in range(Mmax):
        lin_fac += get_coeff(i,True)

    coeff = [] 
    for j in range(Mmax):
        coeff.append(get_coeff(j))

    def dtn_frac(lami):
        result = lin_fac 
        for j in range(Mmax):
            result += coeff[j]/(lami-lam[j])
        return dtn_ref(0)*result

    #conj_sum = 0.0
    #for j in range(Mmax):
    #    conj_sum += abs(coeff[j])/(abs(np.imag(lam[j])))
    #conj_sum *= dtn_ref(0)
    #conj_sum = abs(conj_sum)
    #conj_sums.append(conj_sum)

    dtn_frac_samples = np.array( [ dtn_frac(lami) for lami in lam_sample] )
    err = np.abs( (dtn_frac_samples-dtn_ref_samples)/ dtn_ref_samples )  
    err_analytic.append(max(err))
     
    err_mu = [] 
    for j in range(len(mus)):    
        dtn_frac_samples_mu = np.array( [ dtn_frac(lami) for lami in lam_sample_mu[j] ] )
        err_mu.append(max( np.abs( (dtn_frac_samples_mu - dtn_ref_sample_mu[j]  )/ dtn_ref_sample_mu[j]  ) ))
    err_analytic_mu.append(err_mu) 
    #print("err = ", sum(err)  )
    print("N = {0}, sup-error = {1}".format(Mmax,max(err)))
    
#inv_length = np.array(inv_length)
#sup_inv_length = np.array(sup_inv_length)
#plt.loglog( inv_length,sup_inv_length, label ='inv-lenght-dep')
#plt.show()

Ns_analytic = np.array(Ns_analytic).real
fname = "dtn-expansion-err.dat"
results = [Ns_analytic,1/Ns_analytic] 
header_str = "N linear "
for j in range(len(mus)):    
    err_N = []
    for i in range(len(err_analytic_mu)):
        err_N.append( err_analytic_mu[i][j])
    results.append(err_N)
    header_str += "{0} ".format(mus_str[j])
    plt.loglog( Ns_analytic , err_N , label ='mu={0}'.format(mus[j]))

np.savetxt(fname =fname,
           X = np.transpose(results),
           header = header_str,
           comments = '')

plt.loglog( Ns_analytic , 1/ Ns_analytic  , label ='linear' )
plt.legend()
plt.show()

#conj_sums = np.array(conj_sums)
#plt.loglog( Ns_analytic , conj_sums , label ='conj_sums')
#plt.show()

err_analytic = np.array(err_analytic).real  

plt.loglog( Ns_analytic , err_analytic , label ='err' )
plt.loglog( Ns_analytic , 1/ Ns_analytic  , label ='linear' )
plt.show()

#fname = "dtn-expansion-err.dat"
#results = [Ns_analytic, err_analytic, 1/Ns_analytic] 
#header_str = "N relerr linear"
#np.savetxt(fname =fname,
#           X = np.transpose(results),
#           header = header_str,
#           comments = '')


fname_poles = "dtn_hom_poles.dat"
plot_collect_poles = [lam.real,lam.imag]
header_str_poles = "poles_real poles_imag"

np.savetxt(fname=fname_poles,
    X=np.transpose(plot_collect_poles),
    header=header_str_poles,
    comments='')

fname_roots = "dtn_hom_roots.dat"
plot_collect_roots = [lamp.real,lamp.imag]
header_str_roots = "roots_real roots_imag"

np.savetxt(fname=fname_roots,
    X=np.transpose(plot_collect_roots),
    header=header_str_roots,
    comments='')



