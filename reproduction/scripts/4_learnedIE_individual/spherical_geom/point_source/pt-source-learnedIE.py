import ceres_dtn as opt
import numpy as np
#from scipy.special import hankel1,h1vp
import mpmath as mp
from pt_source_shared import problem_data,SolveExteriorProblem,PlotSparsityPattern
import sys
from ngsolve import *
from netgen.geom2d import SplineGeometry
from ngs_refsol import FundamentalSolution

omega = problem_data["wavenumber"]  # wavenumber
a = problem_data["coupling_radius"] # coupling radius
x_source_pos = problem_data["x_source_pos"]  # x position of source
source_labels = problem_data["source_labels"] 
y_source = problem_data["y_source_pos"][0] # y position of source
order = problem_data["order_FEM_coupling_bnd"] # oder of FEM

##############################################
# parameter to be set by the user
ansatz_input = sys.argv[1]
print(ansatz_input)
if ansatz_input not in ["mediumSym","full"]:
    ValueError('I do not know this ansatz. Only "mediumSym" and "full" allowed')
#ansatz = "mediumSym"
ansatz  = ansatz_input
#############################################

Nmax = 7 # maximal number of infinite element dofs
Ns = list(range(Nmax))
weight_limits = [1e-5,1e-13] # consider only modes with weight > weight_limit  

A_IEs = {} # for storing results
B_IEs = {} # ... 

maxiter = 125000
if ansatz == "full":
    maxiter = 10000
flags = {"max_num_iterations":maxiter,
         "use_nonmonotonic_steps":True,
         "minimizer_progress_to_stdout":False,
         "num_threads":4,
         "report_level":"Brief",
         "function_tolerance":1e-13,
         "parameter_tolerance":1e-13,
         "gradient_tolerance":1e-13}


geo = SplineGeometry()
geo.AddCircle( (0,0), problem_data["coupling_radius"], leftdomain=1, rightdomain=0,bc="outer-bnd")
geo.AddCircle( (0,0), 0.9, leftdomain=0, rightdomain=1,bc="inner-bnd")
geo.SetMaterial(1, "inner")
mesh = Mesh(geo.GenerateMesh (maxh=0.05,quad_dominated=False))
mesh.Curve(problem_data["order_FEM_coupling_bnd"])
phi_arg = atan2(y,x)

def dtn_ref(nu):
    return -omega*0.5*complex( (mp.hankel1(nu-1,omega*a) - mp.hankel1(nu+1,omega*a) ) /  mp.hankel1(nu,omega*a) )

def calc_poles(A,B):
    if A.shape[0] == 1:
        print("A.shape[0] == 1")
        return [] 
    else:
        return -np.linalg.eigvals( np.linalg.inv(B[1:,1:]) @ A[1:,1:])

# perform minimization for each source position 
#for weight_type in ["opt","heu"]:
for weight_type in ["heu"]:
    for x_source,weight_limit in zip(x_source_pos,weight_limits):
        # determine optimal weights
        ref_sol = FundamentalSolution(omega,x_source,y_source,False)
        if weight_type == "opt":
            weights = []
            for l in range(500):
                weights.append( abs( Integrate ( InnerProduct(ref_sol,exp(1j*l*phi_arg)),mesh,definedon=mesh.Boundaries("outer-bnd"),order=40  )  )) 
            weights = 1e4*np.array(weights)
        else:
            weights = np.array([10**5* abs(complex(  mp.hankel1(l,omega*a) / mp.hankel1(l,omega*x_source)) ) for l in range(600)])
        #print("weights =", weights)
        
        Lmax = np.where(weights < weight_limit)[0][0]
        print("x_source = {0}, Lmax = {1}".format(x_source,Lmax))
        weights = weights[:Lmax]
        #print("weights =", weights)
        lam = np.array([(l/a)**2 for l in range(Lmax)]) 
        dtn_nr = np.array([ dtn_ref(mp.sqrt(lami)*a) for lami in lam ]) 
        
        A_IE = []
        B_IE = [] 
        relative_residuals = []

        np.random.seed(seed=123)
        
        A_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
        B_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)

        final_res = np.zeros(len(lam),dtype=float)

        l_dtn = opt.learned_dtn(lam,dtn_nr,weights**2)

        def new_initial_guess(A_old,B_old,ansatz):
            N = A_old.shape[0]
            A_guess = np.zeros((N+1,N+1),dtype='complex')
            B_guess = np.zeros((N+1,N+1),dtype='complex')
            if ansatz in ["medium","full"]:
                A_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
                B_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
                A_guess[:N,:N] = A_old[:]
                B_guess[:N,:N] = B_old[:]
                A_guess[N,N] = 1.0
            elif ansatz == "minimalIC":
                A_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
                A_guess[:N,:N] = A_old[:]
                B_guess[:N,:N] = B_old[:]
                A_guess[N,N] = (omega/16)*(-100-100j)
                B_guess[0,N] = 1e-3*(np.random.rand(1) + 1j*np.random.rand(1)) 
            elif ansatz == "mediumSym":
                A_guess[:N,:N] = A_old[:]
                B_guess[:N,:N] = B_old[:]
                A_guess[0,N]  = A_guess[N,0] = 1e-3*(np.random.rand() + 1j*np.random.rand())
                #B_guess[0,N] =  B_guess[N,0] = 1e-3*(np.random.rand() + 1j*np.random.rand())
                B_guess[0,N] =  B_guess[N,0] = 0.5*1e-1*(np.random.rand() + 1j*np.random.rand())
                A_guess[N,N] = (omega/16)*(-100-100j)
                B_guess[N,N] = 1.0
            return A_guess,B_guess

        for N in Ns: 
            l_dtn.Run(A_guess,B_guess,ansatz,flags,final_res)
            A_IE.append(A_guess.copy()), B_IE.append(B_guess.copy()),relative_residuals.append(final_res.copy())
            poles = calc_poles(A_IE[N], B_IE[N]) 
            print("N = {0}, poles = {1}".format(N,poles))
            A_guess,B_guess = new_initial_guess(A_IE[N],B_IE[N],ansatz)

        A_IEs[x_source] = A_IE
        B_IEs[x_source] = B_IE


    # now use learned infinite elements as transparent boundary condition 
    # for the pointsource inside unit disk at x-position x_source
    rel_error,ndofs,nzes = SolveExteriorProblem(A_IEs,B_IEs)

    # store results in file for plotting
    fname = "pt-source-learned-"+ansatz+"-"+weight_type+"-weights.dat"
    results = [np.array(Ns),np.array(ndofs[x_source_pos[0]]),np.array(nzes[x_source_pos[0]])]
    header_str = "N ndofs nzes "
    for x_source,label in zip(x_source_pos,source_labels):
        header_str += label +" "
        results.append(rel_error[x_source])
    np.savetxt(fname =fname,
                X = np.transpose(results),
                header = header_str,
                comments = '')

# Plot sparsity pattern
#N_plot = 4
#PlotSparsityPattern(A_IEs[x_source_pos[0]][N_plot], B_IEs[x_source_pos[0]][N_plot],"learnedIE-{0}-N{1}".format(ansatz,N_plot))

