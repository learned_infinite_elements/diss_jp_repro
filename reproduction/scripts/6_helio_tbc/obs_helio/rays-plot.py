import netgen.gui
from netgen.geom2d import unit_square
from ngsolve import *
from copy import copy
from math import pi
from math import atan2 as atan2_python

import numpy as np
from numpy import loadtxt
r_d,c_d,rho_d,p_d,gamma1_d,T_d = np.loadtxt("../modelSinput.txt", unpack=True) 
spline_order = 5

c_d = (10**-2)*c_d
rho_d = (10**3)*rho_d

r_d = r_d.tolist()[::-1]
c_d = c_d.tolist()[::-1]
rho_d = rho_d.tolist()[::-1]

r_beg = r_d[0]
c_beg = c_d[0]
rho_beg = rho_d[0]

r_d = np.append([r_beg for i in range(spline_order)],r_d)
c_d = np.append([c_beg for i in range(spline_order)],c_d)
rho_d = np.append([rho_beg for i in range(spline_order)],rho_d)

r_d = r_d.tolist()
c_d = c_d.tolist()
rho_d = rho_d.tolist()

c_d_u = copy(c_d)
rho_d_u = copy(rho_d)
    
c = BSpline(spline_order,r_d,c_d)
rho = BSpline(spline_order,r_d,rho_d)


import matplotlib.pyplot as plt

r_s = [r for r in r_d[:-10:]]
rho_s = [rho(rval) for rval in r_s]

from scipy import integrate,optimize

def project_at_point(px,pz,ax,az):
    scal = 2*(px*ax+pz*az)    
    return ax-scal*px,az-scal*pz

def calc_rays(f0=1.4294,l=2,iters=1,theta_start=0):
    ray_x_coords = []
    ray_z_coords = []
    
    L = sqrt(l*(l+1))
    R_sun = 6.96*10**8
    omega0 =  f0*2*pi*R_sun*(10**-3)

    # calculate lower turning point of rays
    g = lambda x: omega0**2/c(x)**2 - L**2/x**2
    rmin = optimize.brentq(g, 0.01, 1.0)
    print("inner turning point rmin = {0}".format(rmin))
    # discretization of radial steps
    rs = np.linspace(rmin+1e-13,rmin+0.01,100)
    rs = np.append(rs,np.linspace(rmin+0.01,1.0,300))

    f = lambda x: L/(x**2*sqrt(omega0**2/c(x)**2 - L**2/x**2))
    def theta(r1,r2):
        return  integrate.quad(f, r1,r2)[0]

    thetas_ref = [theta(r1=r,r2=1.0) for r in rs]  
 
    for it in range(iters):
        #thetas1 = [theta_start + theta(r1=r,r2=1.0) for r in rs]  
        thetas1 = [theta_start + thet for thet in thetas_ref]
        
        # ray starting at turning point travelling to surface
        xs1 = [r*sin(thet) for r,thet in zip(rs,thetas1)]
        zs1 = [r*cos(thet) for r,thet in zip(rs,thetas1)]
        
        ray_x_coords.append(xs1)
        ray_z_coords.append(zs1)

        # vector orthogonal to plane of reflection (Householder)   
        vx = 1
        vz = -xs1[0]/zs1[0]
        vn = sqrt(vx**2+vz**2)
        vx *= 1/vn
        vz *= 1/vn
        
        # constructing the other ray by reflection
        xs2  = []
        zs2 = []
        for ax,az in zip(xs1,zs1):
            reflection = project_at_point(vx,vz,ax,az)
            xs2.append(reflection[0])
            zs2.append(reflection[1])
            
        ray_x_coords.append(xs2)
        ray_z_coords.append(zs2)
        
        theta_start = pi/2 - atan2_python(zs2[-1],xs2[-1])
        print("theta_start = {0}".format(theta_start))

    return ray_x_coords,ray_z_coords,rmin

iters = 3
#1.4294

# dictionary of rays 
# key =(f0,l), f0 = frequency, l = harmonic degree
rays = []
ls = [2,20,60]
iters = [2,3,11]
rmins = []
#linestyles = [':','--','-']
colors = ['red','cyan','green']
for l,it in zip(ls,iters):
    ray_x_coords,ray_z_coords,rmin = calc_rays(f0=3.0,l=l,iters=it,theta_start =0)
    rays.append( (ray_x_coords,ray_z_coords))
    rmins.append(rmin)

for j,l in enumerate(ls):
    ray_x_coords,ray_z_coords = rays[j]
    for i in range(int(2*iters[j])):
        plt.scatter(ray_x_coords[i],ray_z_coords[i],linewidths=0.01,c=colors[j])
        plt.scatter(ray_x_coords[i],ray_z_coords[i],linewidths=0.01,c=colors[j])

plt.scatter(np.sin(np.linspace(0,2*pi,200)),np.cos(np.linspace(0,2*pi,200)),linewidths=0.01,c='blue')
plt.show()

# prepare rays for writing to tex
rays_tex = []
drop_last_ls = [120,250,140]
for j,l in enumerate(ls):
    ray_l = ""
    ray_x_coords,ray_z_coords = rays[j]
    for i in range(int(2*iters[j])):  
        if i % 2 == 0:
            for xx,zz in zip(ray_x_coords[i][::-1],ray_z_coords[i][::-1]):
                ray_l += "("+str(xx)+","+str(zz)+") "+" \n"
        else:
            if i == 2*iters[j] - 1:
                drop_x = ray_x_coords[i][:drop_last_ls[j]]
                drop_z = ray_z_coords[i][:drop_last_ls[j]]
                for xx,zz in (zip(drop_x,drop_z)):
                    ray_l += "("+str(xx)+","+str(zz)+") "+" \n"           
            else:
                for xx,zz in zip(ray_x_coords[i],ray_z_coords[i]):
                    ray_l += "("+str(xx)+","+str(zz)+") "+" \n"
                
    rays_tex.append(ray_l)

outer_circle = "" 
for xx,zz in zip(np.sin(np.linspace(0,2*pi,200)),np.cos(np.linspace(0,2*pi,200))):
    outer_circle += "("+str(xx)+","+str(zz)+") "+" \n"
    
circle_rmins = []
for rmin in rmins:
    circle = ""
    for xx,zz in zip(rmin*np.sin(np.linspace(0,2*pi,200)),rmin*np.cos(np.linspace(0,2*pi,200))):
        circle += "("+str(xx)+","+str(zz)+") "+" \n"
    circle_rmins.append(circle)
    
    
#print(rays_tex[0])
#input("")
tikzcolors = ["TolDarkBlue" ,"mLightGreen","mLightBrown"]


file = open("rays-modelS.tex","w+")
file.write("\\documentclass{standalone} \n")
file.write("\\usepackage{xr} \n")
file.write("\\usepackage{tikz} \n")
file.write("\\usetikzlibrary{shapes,arrows,snakes,calendar,matrix,backgrounds,folding,calc,positioning,patterns} \n")
file.write("\\usepackage{pgfplots} \n") 
file.write("\\definecolor{TolDarkPurple}{HTML}{332288} \n")
file.write("\\definecolor{TolDarkBlue}{HTML}{6699CC} \n")
file.write("\\definecolor{TolLightBlue}{HTML}{88CCEE} \n")
file.write("\\definecolor{TolLightGreen}{HTML}{44AA99} \n")
file.write("\\definecolor{TolDarkGreen}{HTML}{117733} \n")
file.write("\\definecolor{TolDarkBrown}{HTML}{999933} \n")
file.write("\\definecolor{TolLightBrown}{HTML}{DDCC77} \n")
file.write("\\definecolor{TolDarkRed}{HTML}{661100} \n")
file.write("\\definecolor{TolLightRed}{HTML}{CC6677} \n")
file.write("\\definecolor{TolLightPink}{HTML}{AA4466} \n")
file.write("\\definecolor{TolDarkPink}{HTML}{882255} \n")
file.write("\\definecolor{TolLightPurple}{HTML}{AA4499} \n")
file.write("\\definecolor{mLightBrown}{HTML}{EB811B} \n")
file.write("\\definecolor{mLightGreen}{HTML}{14B03D} \n")
file.write("\\begin{document} \n")
file.write("\\begin{tikzpicture}[scale = 1.0] \n")
file.write("\\begin{axis} \n")
#file.write(" [xlabel= x, ylabel= z,axis equal=true, x label style={at={(axis description cs:0.25,0.1)},anchor=north}, y label style={at={(axis description cs:0.1,0.25)},anchor=north} ] \n")
file.write(" [xlabel= {}, ylabel= {},axis equal=true, ticks=none, xticklabels={},  yticklabels={},axis line style={draw=none},legend style = { column sep = 1pt, legend columns = -1},legend style={at={(0.5,0.025)},anchor=north} ] \n")
for j,text_ray in enumerate(rays_tex):
    file.write("\\addplot[color="+tikzcolors[j]+",no markers,line width=0.35mm] coordinates { \n")
    file.write(" "+  text_ray + " \n")
    file.write(" }; \n" )
file.write("\\addplot[color=black,no markers,line width=0.35mm] coordinates { \n")
file.write(" "+  outer_circle + " \n")
file.write(" }; \n" )
for j,circle in enumerate(circle_rmins):
    file.write("\\addplot[color="+tikzcolors[j]+",no markers,line width=0.15mm,dashed] coordinates { \n")
    file.write(" "+  circle + " \n")
    file.write(" }; \n" )   
lss = "" 
for j,l in enumerate(ls):
    lss += "$\ell = "+str(l)+"$"
    if j != len(ls)-1:
        lss += ","
    else:
        lss += "}"
file.write("\\legend{"+lss+" \n")
file.write("\\end{axis} \n")
file.write("\\end{tikzpicture}  \n")
file.write("\\end{document} \n")           
file.close()
 
          
    

