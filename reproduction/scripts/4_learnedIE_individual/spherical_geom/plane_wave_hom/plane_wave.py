import ceres_dtn as opt
from ngsolve import *
from netgen.geom2d import SplineGeometry
from ngs_refsol import PlaneWaveHomSolution
import numpy as np
import matplotlib.pyplot as plt
#from scipy.special import hankel1,h1vp
import mpmath as mp

a = 1.0 # coupling radius
R = 1/2 # radius of scatterer
Lmax = 430 # number of considered modes for learning
Nmax = 7 # maximal number of infinite element dofs
Ns = list(range(Nmax))

orders = [4,6,8,10,12] # order of FEM
ks = [4,8,16,32] # wavenumbers 

A_IEs = {} # for storing results
B_IEs = {} # ... 

#ansatz = "minimalIC"
ansatz = "mediumSym"
flags = {"max_num_iterations":5000000,
         "use_nonmonotonic_steps":True,
         "minimizer_progress_to_stdout":False,
         "num_threads":4,
         "report_level":"Brief",
         "function_tolerance":1e-14,
         "parameter_tolerance":1e-14,
         "gradient_tolerance":1e-14}

# perform minimization for each wavenumber 
for k in ks:
   
    #r_ext = min(R*k/16,R)
    r_ext = R

    def dtn_ref(nu):
        return -k*0.5*complex( (mp.hankel1(nu-1,k*a) - mp.hankel1(nu+1,k*a) ) /  mp.hankel1(nu,k*a) )

    A_IE = []
    B_IE = [] 
    relative_residuals = []

    np.random.seed(seed=123)
    
    A_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
    B_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)

    weights = np.array([10**2* abs(complex(mp.hankel1(l,k*a) / mp.hankel1(l,k*r_ext) )) for l in range(Lmax)])
    #scale_fac = 1e4
    #weights = np.array([10**2* abs(complex(mp.hankel1(l,k*a) / mp.hankel1(l,k*r_ext) )) for l in range(Lmax)])
    #weights = [ abs( complex( mp.besselj(0,k*r_ext)*mp.sqrt(2*mp.pi*a)*mp.hankel1(0,k*a) /  mp.hankel1(0,k*r_ext)   ) ) ] 
    #for l in range(1,Lmax):
    #    weights.append( abs( complex( mp.besselj(l,k*r_ext)*2*mp.sqrt(2*mp.pi*a)*mp.hankel1(l,k*a) /  mp.hankel1(l,k*r_ext)   ) )  )
    #weights = scale_fac*np.array(weights)
    #print("weights =", weights)
    

    l0 = np.max( np.where(weights > 1e-12*weights[0])[0]) 
    weights = weights[:l0]
    lam = np.array([(l/a)**2 for l in range(l0)]) 
    dtn_nr = np.array([ dtn_ref(sqrt(lami)*a) for lami in lam ]) 

    final_res = np.zeros(len(lam),dtype=float)

    l_dtn = opt.learned_dtn(lam,dtn_nr,weights**2)

    def new_initial_guess(A_old,B_old,ansatz):
        N = A_old.shape[0]
        A_guess = np.zeros((N+1,N+1),dtype='complex')
        B_guess = np.zeros((N+1,N+1),dtype='complex')
        if ansatz in ["medium","full"]:
            A_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
            B_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
            A_guess[:N,:N] = A_old[:]
            B_guess[:N,:N] = B_old[:]
            A_guess[N,N] = 1.0
        elif ansatz == "minimalIC":
            A_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
            A_guess[:N,:N] = A_old[:]
            B_guess[:N,:N] = B_old[:]
            A_guess[N,N] = (k/16)*(-100-100j)
            B_guess[0,N] = 1e-3*(np.random.rand(1) + 1j*np.random.rand(1)) 
        elif ansatz == "mediumSym":
            A_guess[:N,:N] = A_old[:]
            B_guess[:N,:N] = B_old[:]
            A_guess[0,N]  = A_guess[N,0] = 1e-3*(np.random.rand() + 1j*np.random.rand())
            #B_guess[0,N] =  B_guess[N,0] = 1e-3*(np.random.rand() + 1j*np.random.rand())
            B_guess[0,N] =  B_guess[N,0] = 0.5*1e-1*(np.random.rand() + 1j*np.random.rand())
            A_guess[N,N] = (k/16)*(-100-100j)
            B_guess[N,N] = 1.0
        return A_guess,B_guess

    for N in Ns: 
        l_dtn.Run(A_guess,B_guess,ansatz,flags,final_res)
        A_IE.append(A_guess.copy()), B_IE.append(B_guess.copy()),relative_residuals.append(final_res.copy())
        A_guess,B_guess = new_initial_guess(A_IE[N],B_IE[N],ansatz)

    A_IEs[k] = A_IE
    B_IEs[k] = B_IE


# now use learned infinite elements as transparent boundary condition 
# for the scattering of a plane wave from a disk

rel_error = {} # key = (order,k)

geo = SplineGeometry()
geo.AddCircle( (0,0), a, leftdomain=1, rightdomain=0,bc="outer-bnd")
geo.AddCircle( (0,0), R, leftdomain=0, rightdomain=1,bc="inner-bnd")
geo.SetMaterial(1, "inner")
mesh = Mesh(geo.GenerateMesh (maxh=0.05,quad_dominated=False))
mesh.Curve(10)

def SolveProblem(order,k):
    
    print("Solving for k = {0} and order (p) = {1}".format(k,order))
    # reference solution for calculating the error
    ref_sol = PlaneWaveHomSolution(k,R,"sound-soft")
    if k == ks[2] and order == orders[-1]:
        vtk = VTKOutput(ma=mesh,coefs=[ref_sol.real],
                                    names=["sol"],
                                    filename="plane-wave-k{0}".format(k),
                                    subdivision=4)
        vtk.Do()
    l2_norm =  sqrt( Integrate ( InnerProduct(ref_sol,ref_sol),mesh,VOL ).real)
    
    rel_error_order = []
    fes_inner = H1(mesh,complex=True,order=order,dirichlet="inner-bnd")

    A_IE = A_IEs[k]
    B_IE = B_IEs[k]

    for N in Ns: 
        
        A_N = A_IE[N]
        B_N = B_IE[N]

        fes_surf = H1(mesh,order=order,complex=True,definedon=mesh.Boundaries("outer-bnd"))
        inf_outer = [fes_surf for i in range(A_N.shape[0]-1)]

        X = FESpace([fes_inner]+inf_outer)

        uX = X.TrialFunction() 
        vX = X.TestFunction() 

        f_X = LinearForm (X)
        f_X.Assemble()

        aX = BilinearForm(X, symmetric=False)
        aX += SymbolicBFI ( grad(uX[0])*grad(vX[0]) - k**2*uX[0]*vX[0] )

        for i in range(A_N.shape[0]):
            for j in range(A_N.shape[0]):
                if abs(A_N[j,i]) > 1e-10 or abs(B_N[j,i]>1e-10):
                    graduX = grad(uX[i]).Trace()
                    gradvX = grad(vX[j]).Trace()  
                    aX += SymbolicBFI ( B_N[j,i]*graduX*gradvX + A_N[j,i]*uX[i]*vX[j]  ,definedon=mesh.Boundaries("outer-bnd"))
        aX.Assemble()

        gfu_X = GridFunction (X)

        res_X = gfu_X.vec.CreateVector()
        gfu_X.vec[:] = 0.0
        gfu_X.components[0].Set(ref_sol,BND)
        res_X.data = f_X.vec - aX.mat * gfu_X.vec
        invX = aX.mat.Inverse(freedofs=X.FreeDofs(), inverse="umfpack")
        gfu_X.vec.data += invX * res_X

        l2_err = sqrt ( Integrate (  InnerProduct(gfu_X.components[0] - ref_sol,gfu_X.components[0] - ref_sol), mesh, VOL ).real)
        rel_error_order.append(l2_err/l2_norm)
        print("N = {0},rel l2_err ={1} \n ".format(A_N.shape[0]-1,l2_err/l2_norm))
        
    rel_error[(order,k)] = np.array(rel_error_order)

#solve and store the results
for order in orders:
    SolveProblem(order,ks[2])

for k in ks:
    SolveProblem(orders[-1],k)


# store results in file for plotting

# fixed k, variable order (p)
fname = "plane-wave"+"-k{0}.dat".format(ks[2])
results = [np.array(Ns)]
header_str = "N "
for order in orders:
    header_str += "p{0} ".format(order)
    results.append(rel_error[(order,ks[2])])
np.savetxt(fname =fname,
           X = np.transpose(results),
           header = header_str,
           comments = '')

# variable k, fixed order=10
fname = "plane-wave"+"-p{0}.dat".format(orders[-1])
results = [np.array(Ns)]
header_str = "N "
for k in ks:
    header_str += "k{0} ".format(k)
    results.append(rel_error[(orders[-1],k)])
np.savetxt(fname =fname,
           X = np.transpose(results),
           header = header_str,
           comments = '')
