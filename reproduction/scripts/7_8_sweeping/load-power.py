from math import pi
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors

L_min = 0
L_max = 1000  # how many DtN numbers to take
L_spacing = 1
L_range = np.arange(L_min,L_max+1,L_spacing)
f_hzs = np.linspace(1.0,8.3,1800)

index_1mhz = np.where( f_hzs > 1.0)[0][0]
f_hzs = (10**-3)*f_hzs

plt.rc('legend', fontsize=18)
plt.rc('axes', titlesize=18)
plt.rc('axes', labelsize=18)
plt.rc('xtick', labelsize=14)    
plt.rc('ytick', labelsize=14)

#################################################### 
# load reference power spectrum
####################################################

mdi_data = np.loadtxt("multiplets-mdi-2001.dat",unpack=False)

def select_data(data):
    l_ref = data[:,1]
    nu_ref = data[:,2]
    ref_selected_idx = np.array([],dtype=int)
    N_ref_samples = 12
    for l in [int(l_ref[20]+(l_ref[-100]-l_ref[20])*i/(N_ref_samples-1)) for i in range(N_ref_samples)]:
        idx_l = np.where(l_ref==l)[0]
        ref_selected_idx = np.append(ref_selected_idx,idx_l)
    return l_ref,nu_ref,ref_selected_idx

l_ref,nu_ref,ref_selected_idx = select_data(mdi_data) 

print("ref_selected_idx =" , ref_selected_idx)
print("l_ref[ref_selected_idx] = ", l_ref[ref_selected_idx])
print("nu_ref[ref_selected_idx] = ", nu_ref[ref_selected_idx])

def scale_power_spectrum(power):

    #return power
    def PI_scal(f):
        result = 1 + ((abs(f)-3.3*1e-3)/ (0.6*1e-3) )**2
        return 1/result 
    
    scaled_power = np.zeros(power.shape) 
    for i  in range(power.shape[0]):
        if abs(f_hzs[i]) > 1e-13:
            scaled_power[i,:] = power[i,:]*0.5*PI_scal(f_hzs[i])/(2*pi*f_hzs[i])
        else:
            scaled_power[i,:] = power[i,:]*0.5*PI_scal(f_hzs[i])
    return scaled_power


for idx,str_name, in enumerate(["power-spectrum_VAL-C_order3_angleref1p5_1800freq","power-spectrum_VAL-C_order5_angleref1p5_1800freq","power-spectrum_VAL-C_order7_angleref1p5_1800freq"]):
    power_spectrum = np.loadtxt(str_name+".out",delimiter=',')
    power_spectrum_scaled = scale_power_spectrum(power_spectrum) 
    power_spectrum_learned = [] 

    power_spectrum_scaled = np.abs(power_spectrum_scaled) #remove sign of values like -1e-14 ... 
    c_normalize = 1.0
    power_spectrum_scaled *= 1/np.max(power_spectrum_scaled) 
    if idx == 0:
        plt.figure(figsize=(6.5,6),constrained_layout=True)
    elif idx == 1:
        plt.figure(figsize=(6,6),constrained_layout=True)
    else:
        plt.figure(figsize=(7,6),constrained_layout=True)
    vmin = 2.0e-4

    im = plt.pcolormesh(L_range,10**3*f_hzs,power_spectrum_scaled,
                       cmap=plt.get_cmap('viridis'),
                       #cmap=plt.get_cmap('Reds'),
                       norm=colors.LogNorm( vmin= vmin ,vmax=1.0  ) )

    plt.rc('legend', fontsize=16)
    plt.rc('axes', titlesize=16)
    plt.rc('axes', labelsize=16)
    plt.xlabel("Harmonic degree $\ell$")
    if idx == 0:
        plt.ylabel("Frequency (mHz)")
    plt.scatter(l_ref[ref_selected_idx], 10**(-3)*nu_ref[ref_selected_idx],s=45,marker='+',color='w')
    #plt.scatter(l_ref[ref_selected_idx], 10**(-3)*nu_ref[ref_selected_idx],s=50,marker='+')
    if idx == 2:
        plt.colorbar(im)
    plt.savefig(str_name+".png",transparent=True)
    plt.show()
 



