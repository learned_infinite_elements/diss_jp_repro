from math import pi
import numpy as np
import copy
from ngsolve import *
from netgen.geom2d import unit_square
from netgen.geom2d import unit_square, MakeCircle, SplineGeometry, MakeRectangle
from netgen.meshing import Element0D, Element1D, Element2D, MeshPoint, FaceDescriptor, Mesh as NetMesh
from netgen.csg import Pnt
from ngsolve import Mesh as NGSMesh
from netgen.geom2d import *
from numpy import arange
from scipy.sparse import csr_matrix
import scipy.sparse.linalg
ngsglobals.msg_level = 1
from ngs_refsol import FundamentalSolution,FundamentalSolution_grad
from scipy.special import mathieu_a,mathieu_b,mathieu_modcem1,mathieu_modcem2,mathieu_modsem1,mathieu_modsem2,mathieu_cem,mathieu_sem
import ceres_dtn as opt
import matplotlib.pyplot as plt 
plt.rc('legend',fontsize=14)
plt.rc('axes',titlesize=14)
plt.rc('axes',labelsize=14)
plt.rc('xtick',labelsize=12)
plt.rc('ytick',labelsize=12)


def BitArrayFromList(l,mesh,fes, elements = True):
    if elements:
        ba = BitArray(mesh.ne)
    else:
        ba = BitArray(fes.ndof)
    ba.Clear()
    for el in l:
        ba[el] = True
    return ba

def P_DoFs(M,test,basis):
    return (M[test,:][:,basis])

omega = 32  # wavenumber 
maxh = 0.05 # mesh width
order = 10
order_geom = order # order for curved elements 
Nmax = 9
Ns = list(range(Nmax))

#solver = "pardiso"
solver = "umfpack"
show_plots = False 

source_posisitions = [(-0.1,-0.13),(0.1,0.15)]

ansatz = "mediumSym"
flags = {"max_num_iterations":150000,
         "use_nonmonotonic_steps":True,
         "minimizer_progress_to_stdout":False,
         "num_threads":4,
         "report_level":"Brief",
         "function_tolerance":1e-13,
         "parameter_tolerance":1e-13,
         "gradient_tolerance":1e-13}

def new_initial_guess(A_old,B_old,ansatz):
    N = A_old.shape[0]
    A_guess = np.zeros((N+1,N+1),dtype='complex')
    B_guess = np.zeros((N+1,N+1),dtype='complex')
    if ansatz in ["medium","full"]:
        A_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        B_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        A_guess[:N,:N] = A_old[:]
        B_guess[:N,:N] = B_old[:]
        A_guess[N,N] = 1.0
    elif ansatz == "minimalIC":
        A_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        A_guess[:N,:N] = A_old[:]
        B_guess[:N,:N] = B_old[:]
        A_guess[N,N] = (-100-100j)
        B_guess[0,N] = 1e-3*(np.random.rand(1) + 1j*np.random.rand(1))  
    elif ansatz == "mediumSym":
        scal = 1
        if N >= 5:
            scal *= 0.5*1e-1
        A_guess[:N,:N] = A_old[:]
        B_guess[:N,:N] = B_old[:]
        A_guess[0,N]  = A_guess[N,0] = scal*1e-3*(np.random.rand() + 1j*np.random.rand())
        B_guess[0,N] =  B_guess[N,0] = scal*1e-2*(np.random.rand() + 1j*np.random.rand())
        A_guess[N,N] = -100-100j
        B_guess[N,N] = 1.0

    return A_guess,B_guess


def EllipticalScatteringMesh(a_inner,b_inner,a,b,order):

    spherical = SplineGeometry()
    pnts = []
    pnts.extend([ (0,-b_Gamma), (a_Gamma,-b_Gamma), (a_Gamma,0), (a_Gamma,b_Gamma), (0,b_Gamma), (-a_Gamma,b_Gamma), (-a_Gamma,0), (-a_Gamma,-b_Gamma), 
                (0,-b_inner), (a_inner,-b_inner), (a_inner,0), (a_inner,b_inner), (0,b_inner), (-a_inner,b_inner), (-a_inner,0), (-a_inner,-b_inner) 
                ])

    pnums = [spherical.AppendPoint(*p) for p in pnts]

    cnt = 0
    inner_index = 1
    bc = "surface"
    outer_index=0
    inner_index=1
    last_segments1 = spherical.Append( ["spline3", pnums[cnt],pnums[cnt+1],pnums[cnt+2]], leftdomain=inner_index, rightdomain=outer_index, maxh=maxh, bc=bc) 
    last_segments2 = spherical.Append( ["spline3", pnums[cnt+2],pnums[cnt+3],pnums[cnt+4]], leftdomain=inner_index, rightdomain=outer_index, maxh=maxh, bc=bc) 
    last_segments3 = spherical.Append( ["spline3", pnums[cnt+4],pnums[cnt+5],pnums[cnt+6]], leftdomain=inner_index, rightdomain=outer_index, maxh=maxh, bc=bc) 
    last_segments4 = spherical.Append( ["spline3", pnums[cnt+6],pnums[cnt+7],pnums[cnt]], leftdomain=inner_index, rightdomain=outer_index, maxh=maxh, bc=bc)

    cnt += 8
    inner_index = 0
    bc = "obstacle"
    outer_index=1
    last_segments1 = spherical.Append( ["spline3", pnums[cnt],pnums[cnt+1],pnums[cnt+2]], leftdomain=inner_index, rightdomain=outer_index, maxh=maxh, bc=bc) 
    last_segments2 = spherical.Append( ["spline3", pnums[cnt+2],pnums[cnt+3],pnums[cnt+4]], leftdomain=inner_index, rightdomain=outer_index, maxh=maxh, bc=bc) 
    last_segments3 = spherical.Append( ["spline3", pnums[cnt+4],pnums[cnt+5],pnums[cnt+6]], leftdomain=inner_index, rightdomain=outer_index, maxh=maxh, bc=bc) 
    last_segments4 = spherical.Append( ["spline3", pnums[cnt+6],pnums[cnt+7],pnums[cnt]], leftdomain=inner_index, rightdomain=outer_index, maxh=maxh, bc=bc)

    mesh = Mesh(spherical.GenerateMesh(maxh=maxh))
    mesh.Curve(order_geom)
    return mesh


def SolveProblem(a_S,b_S,a_Gamma,b_Gamma,alpha_decay,Lmax,kappa_str):

    kappa = a_Gamma/b_Gamma

    c = sqrt(a_Gamma**2-b_Gamma**2)
    tmp = 0.5*(x**2+y**2-c**2)
    q = 0.25*c**2*omega**2
    cos_2nu = 1 - (2/c**2)*(-tmp+sqrt(tmp**2+c**2*y**2))
    J_munu = sqrt(2*(sqrt(tmp**2+c**2*y**2)))

    mesh = EllipticalScatteringMesh(a_S,b_S,a_Gamma,b_Gamma,order)
    Draw(mesh)

    ref_sol = CoefficientFunction(0.0)
    ref_sol_grad = CoefficientFunction((0.0,0.0))
    for source_pos in source_posisitions:
        ref_sol += FundamentalSolution(omega,source_pos[0],source_pos[1],False)
        ref_sol_grad += CoefficientFunction((FundamentalSolution_grad(omega,source_pos[0],source_pos[1],False),FundamentalSolution_grad(omega,source_pos[0],source_pos[1],True) ) )

    vtk = VTKOutput(ma=mesh,coefs=[ref_sol.real],
                                names=["sol"],
                                filename="ellipse-kappa{0}".format(kappa_str),
                                subdivision=4)
    vtk.Do()
    
    n = specialcf.normal(mesh.dim)
    Draw(ref_sol,mesh,"ref")
    Draw(ref_sol_grad,mesh,"refgrad")


    fes_D = H1(mesh, complex=True,  order=order,dirichlet="surface")
    surface_dofs = [] 
    for nr,bn in enumerate(fes_D.FreeDofs()):
        if not bn:
            surface_dofs.append(nr)

    surfa_vol  = Integrate  ( 1/J_munu  , mesh, definedon=mesh.Boundaries("surface") )  

    fes = H1(mesh, complex=True,  order=order,dirichlet=[])
    u,v = fes.TnT()
    gfu = GridFunction (fes)

    a_coupling = np.arccosh(a_Gamma/c)
    print("a = ", a_Gamma) 
    print("c = ",c)
    print("a/c = ", a_Gamma/c)
    print("a_coupling = ", a_coupling)
    print("q =", q)

    def get_even(m):
        mcem1,mcem1_p = mathieu_modcem1(m,q,a_coupling)
        mcem2,mcem2_p = mathieu_modcem2(m,q,a_coupling) 
        return -(mcem1_p+1j*mcem2_p)/(mcem1+1j*mcem2)

    def get_odd(m):
        msem1,msem1_p = mathieu_modsem1(m,q,a_coupling) 
        msem2,msem2_p = mathieu_modsem2(m,q,a_coupling) 
        return -(msem1_p+1j*msem2_p)/(msem1+1j*msem2)

    dtn_nr = [] 
    lam = [] 
    lam.append(mathieu_a(0,q))
    dtn_nr.append( get_even(0))
    for m in range(1,Lmax):
        lam.append(mathieu_b(m,q))
        dtn_nr.append( get_odd(m) )
        lam.append(mathieu_a(m,q))
        dtn_nr.append( get_even(m) )

    lam = np.array(lam) 
    dtn_nr = np.array(dtn_nr)
    print("dtn_nr =", dtn_nr)
    ##input("")

    idx_sorted = np.argsort(lam.real)
    lam = lam[idx_sorted]
    dtn_nr = dtn_nr[idx_sorted]

    A_IE = []
    B_IE = [] 
    dtn_learned = [] 
    relative_residuals = []

    np.random.seed(seed=123)

    A_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
    B_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
    
    
    weights = np.array([10**7* exp(-l*alpha_decay) for l in range(len(lam)) ] )
    print("weights =", weights) 

    final_res = np.zeros(len(lam),dtype=float)
    l_dtn = opt.learned_dtn(lam,dtn_nr,weights**2)

    for N in Ns: 
        l_dtn.Run(A_guess,B_guess,ansatz,flags,final_res)
        A_IE.append(A_guess.copy()), B_IE.append(B_guess.copy()),relative_residuals.append(final_res.copy())
        dtn_approx = np.zeros(len(lam),dtype='complex')
        lam_sample = np.array(lam.copy())
        opt.eval_dtn_fct(A_IE[N],B_IE[N],lam_sample,dtn_approx)
        dtn_learned.append(dtn_approx)
        A_guess,B_guess = new_initial_guess(A_IE[N],B_IE[N],ansatz)


    if show_plots:
        plt.plot(lam,np.array(dtn_nr).real,label='analytic',linewidth=4,color='gray')
        for N,st in zip([2,4],["dashdot","dashed"]):
            plt.plot(lam,np.array(  dtn_learned[N] ).real,label='N={0}'.format(N),linestyle=st)
            #plt.plot(range(len(dtn_learned[N])),np.array(  dtn_learned[N] ).real,label='N={0}'.format(N),linestyle=st)
        plt.legend()
        plt.xlabel("$\lambda(q)$")
        plt.title("$\Re(dtn(\lambda(q)))$")
        plt.show() 

        plt.plot(lam,np.array(dtn_nr).imag,label='analytic',linewidth=4,color='gray')
        for N,st in zip([2,4],["dashdot","dashed"]):
            plt.plot(lam,np.array(  dtn_learned[N] ).imag,label='N={0}'.format(N),linestyle=st)
        plt.legend()
        plt.xlabel("$\lambda(q)$")
        plt.title("$\Im(dtn(\lambda(q)))$")
        plt.show() 

    fname_ref = "dtn-ellipse-ref-kappa{0}.dat".format(kappa_str)
    np.savetxt(fname=fname_ref,
               X=np.transpose([ lam, np.array(dtn_nr).real,np.array(dtn_nr).imag  ]), 
               header = 'lam dtnReal dtnImag',
               comments='' 
              )
    
    # extra output to plot asymptotics
    if kappa_str == "EightThird":
        lam_extra = [] 
        dtn_nr_extra = [] 
        lam_extra.append(mathieu_a(0,q))
        dtn_nr_extra.append( get_even(0))
        for m in range(1,Lmax+100):
            lam_extra.append(mathieu_b(m,q))
            dtn_nr_extra.append( get_odd(m) )
            lam_extra.append(mathieu_a(m,q))
            dtn_nr_extra.append( get_even(m) )
        lam_extra = np.array(lam_extra) 
        dtn_nr_extra = np.array(dtn_nr_extra)
        idx_sorted_extra = np.argsort(lam_extra.real)
        lam_extra = lam_extra[idx_sorted_extra]
        dtn_nr_extra = dtn_nr_extra[idx_sorted_extra]
        fname_extra = "dtn-ellipse-ref-kappa{0}-asymptotics.dat".format(kappa_str)
        np.savetxt(fname=fname_extra,
                   X=np.transpose([ lam_extra, np.array(dtn_nr_extra).real,np.array(dtn_nr_extra).imag  ]), 
                   header = 'lam dtnReal dtnImag',
                   comments='' 
                  )


    plot_collect_real = [ lam ]
    plot_collect_imag = [ lam ]
    header_str = 'lam'
    for N in Ns:
        header_str += " N{0}".format(N)
        plot_collect_real.append(np.array(dtn_learned[N]).real)
        plot_collect_imag.append(np.array(dtn_learned[N]).imag)     

    fname_learned_real = "learned-dtn-ellipse-kappa{0}-real.dat".format(kappa_str)
    fname_learned_imag = "learned-dtn-ellipse-kappa{0}-imag.dat".format(kappa_str)

    np.savetxt(fname=fname_learned_real,
               X=np.transpose(plot_collect_real),
               header=header_str,
               comments='')

    np.savetxt(fname=fname_learned_imag,
               X=np.transpose(plot_collect_imag),
               header=header_str,
               comments='')

    l2_norm = sqrt ( Integrate (  InnerProduct(ref_sol,ref_sol), mesh, VOL ).real)
    fes_surf = Periodic(H1(mesh,order=order,complex=True,definedon=  mesh.Boundaries("surface")))
    
    l2_errors = []
    
    for N in Ns: 
            
        A_IE_N = A_IE[N]
        B_IE_N = B_IE[N]

        inf_outer = [fes_surf for i in range(A_IE_N.shape[0]-1)]

        X = FESpace([fes]+inf_outer)

        uX = X.TrialFunction() 
        vX = X.TestFunction() 

        f_X = LinearForm (X)
        f_X += SymbolicLFI ( -ref_sol_grad*n*vX[0], definedon=mesh.Boundaries("obstacle") )
        f_X.Assemble()

        aX = BilinearForm(X, symmetric=False)
        aX += SymbolicBFI ( grad(uX[0])*grad(vX[0]) - omega**2*uX[0]*vX[0] )
         
        for i in range(A_IE_N.shape[0]):
            for j in range(A_IE_N.shape[0]):
                if abs(A_IE_N[j,i]) > 1e-10 or abs(B_IE_N[j,i]>1e-10):
                    graduX = grad(uX[i]).Trace()
                    gradvX = grad(vX[j]).Trace()  
                    aX += SymbolicBFI ( B_IE_N[j,i]*(J_munu*graduX*gradvX +  (1/J_munu)* omega**2*c**2*0.5*cos_2nu*uX[i]*vX[j]) 
                                      + A_IE_N[j,i]* (1/J_munu)* uX[i]*vX[j]  ,definedon=mesh.Boundaries("surface"))
        aX.Assemble()

        gfu_X = GridFunction (X)

        res_X = gfu_X.vec.CreateVector()
        gfu_X.vec[:] = 0.0
        res_X.data = f_X.vec - aX.mat * gfu_X.vec
        invX = aX.mat.Inverse(freedofs=X.FreeDofs(), inverse=solver)
        #invX = aX.mat.Inverse(freedofs=X.FreeDofs(), inverse="umfpack")
        gfu_X.vec.data += invX * res_X
        Draw( gfu_X.components[0],  mesh,"appsol")
        Draw( InnerProduct(gfu_X.components[0] - ref_sol,gfu_X.components[0] - ref_sol), mesh,"err")
        l2_err = sqrt ( Integrate (  InnerProduct(gfu_X.components[0] - ref_sol,gfu_X.components[0] - ref_sol), mesh, VOL ).real)

        print("rel-error =", l2_err/l2_norm)
        l2_errors.append(l2_err/l2_norm)
    return l2_errors 

b_S = 1/4
b_Gamma = 2/4 # semi-minor axis of truncation boundary

# b_s = 1/3, b_Gamma = 2/3, kappa = 4/3 , alpha_decay = 1/4, Lmax = 40 
# kappa = 8/3


#Lmax = 50

fname = "ellipse"+"-k{0}-relerr.dat".format(omega)
header_str = "N "
results = [np.array(Ns)]
for kappa,kappa_str,Lmax in zip([4/3,8/3,12/3],["FourThird","EightThird","TwelveThird"],[40,50,60]):
    header_str += "kappa{0} ".format(kappa_str)
    a_Gamma = kappa*b_Gamma # semi-major axis of truncation boundary
    a_S = kappa*b_S
    alpha_decay =1/(3*kappa)
    results.append(SolveProblem(a_S,b_S,a_Gamma,b_Gamma, alpha_decay,Lmax,kappa_str))

np.savetxt(fname =fname,
           X = np.transpose(results),
           header = header_str,
           comments = '')
