from netgen.meshing import *
from netgen.csg import *
from netgen.meshing import Mesh as netmesh
from math import pi
import numpy as np
from scipy.sparse import csr_matrix
from ngsolve import Mesh as NGSMesh
from netgen.geom2d import SplineGeometry
from ngsolve import *
ngsglobals.msg_level = 0
SetNumThreads(1)
from pt_source_shared import problem_data,SolveExteriorProblem,PlotSparsityPattern

omega = problem_data["wavenumber"]  # wavenumber
a = problem_data["coupling_radius"] # coupling radius
x_source_pos = problem_data["x_source_pos"]  # x position of source
source_labels = problem_data["source_labels"] 
y_source = problem_data["y_source_pos"][0] # y position of source
order = problem_data["order_FEM_coupling_bnd"] # oder of FEM

order_PML = 4 # order for tensor product PML
#order_PML = 1 # order for tensor product PML
use_manual_PML = True # if True then the PML-stretching terms are manually incoporated into the bilinear form 
                      # if False this is done automatically (invisible for the user) by NGSolve 
C_PML = 40 # amplitude of the PML
eta = 1.02 # exterior problem posed on interval [a,eta]
pml_min = a   # start of PML
pml_max = eta # end of PML 
w = pml_max-pml_min

#bonus_intorder = 0 # bonus integration order for assembly

# coefficients of PDE 
r = sqrt(x*x+y*y)
rho = 1.0
rho_1D = 1.0  
c = CoefficientFunction(1)
c_1D = CoefficientFunction(1)

def Make1DMesh_flex(R_min,R_max,N_elems=50):

    mesh_1D = netmesh(dim=1)
    pids = []   
    delta_r = (R_max-R_min)/N_elems
    for i in range(N_elems+1):
        pids.append (mesh_1D.Add (MeshPoint(Pnt(R_min + delta_r*i, 0, 0))))                     
    n_mesh = len(pids)-1
    for i in range(n_mesh):
        mesh_1D.Add(Element1D([pids[i],pids[i+1]],index=1))
    mesh_1D.Add (Element0D( pids[0], index=1))
    mesh_1D.Add (Element0D( pids[n_mesh], index=2))
    mesh_1D.SetBCName(0,"left")
    mesh_1D.SetBCName(1,"right")
    mesh_1D = NGSMesh(mesh_1D)    
    return mesh_1D

A_PMLs = {} # for storing results
B_PMLs = {} # ... 

def Setup_TP_PML_matrices(N_r): 
    
    mesh_1D = Make1DMesh_flex(a,eta,N_r)
    fes_PML = H1(mesh_1D, complex=True,  order=order_PML, dirichlet=[])
    u_PML,v_PML = fes_PML.TnT()
    
    if use_manual_PML:    
        sigma_PML = (C_PML/w)*( (x-a)/w)**2
        s_PML = 1/( (1+1j*sigma_PML/omega))
        stilde_PML = 1 + (1j*C_PML/(x*omega*3))*((x-a)/w)**3
        xtilde = x   

        A_PML = BilinearForm (fes_PML, symmetric=False)
        A_PML += SymbolicBFI( (xtilde/rho_1D)*stilde_PML*s_PML*grad(u_PML)*grad(v_PML)) 
        A_PML += SymbolicBFI( (- (omega**2*(xtilde*stilde_PML)  )/(s_PML*rho_1D*c_1D**2))*u_PML*v_PML)    
        A_PML.Assemble()

        B_PML = BilinearForm (fes_PML, symmetric=False )
        B_PML += SymbolicBFI( (1/( xtilde*stilde_PML*s_PML*rho_1D))*u_PML*v_PML )    
        B_PML.Assemble()
   
    else: 
        physpml_sigma = IfPos(x-plm_min,IfPos(pml_max -x,C_PML/w*(x-plm_min)**2/w**2,0),0)
        G_PML =  (1j/omega)*(C_PML/3)*(w/C_PML)**(3/2)*physpml_sigma**(3/2)
        pml_phys = pml.Custom(trafo=x+(1j/omega)*(C_PML/3)*(w/C_PML)**(3/2)*physpml_sigma**(3/2), jac = -1 - 1j*physpml_sigma/omega )
        
        mesh_1D.SetPML(pml_phys,definedon=1)
 
        A_PML = BilinearForm (fes_PML, symmetric=False)
        A_PML += SymbolicBFI( ((x+G_PML)/rho_1D)*grad(u_PML)*grad(v_PML)) 
        A_PML += SymbolicBFI( (- (omega**2*(x+G_PML)  )/(rho_1D*c_1D**2))*u_PML*v_PML)    
        A_PML.Assemble()

        B_PML = BilinearForm (fes_PML, symmetric=False )
        B_PML += SymbolicBFI( (1/( (x+G_PML) *rho_1D))*u_PML*v_PML )    
        B_PML.Assemble()
        
        mesh_1D.UnSetPML(1)

    rows_1,cols_1,vals_1 = A_PML.mat.COO()
    A_csr = csr_matrix((vals_1,(rows_1,cols_1))) 
    A_PML = A_csr.todense() 
    
    rows_2,cols_2,vals_2 = B_PML.mat.COO()
    B_csr = csr_matrix((vals_2,(rows_2,cols_2))) 
    B_PML = B_csr.todense() 
    
    return A_PML,B_PML
   
Nrs = [i for i in range(1,15)]    
A_PML = []
B_PML = []

for N_r in Nrs:
    A_N,B_N = Setup_TP_PML_matrices(N_r) 
    A_PML.append(A_N.copy()), B_PML.append(B_N.copy())

for x_source in  x_source_pos:
    A_PMLs[x_source] = A_PML
    B_PMLs[x_source] = B_PML

rel_error,ndofs,nzes = SolveExteriorProblem(A_PMLs,B_PMLs)

fname = "pt-source-TP-PML.dat"
results = [np.array(ndofs[x_source_pos[0]]),np.array(nzes[x_source_pos[0]])]
header_str = "ndofs nzes "
for x_source,label in zip(x_source_pos,source_labels):
    header_str += label + " "
    results.append(rel_error[x_source])
np.savetxt(fname = fname,
           X = np.transpose(results),
           header = header_str,
           comments = '')

#for idx_plot in [0,3]:
#for idx_plot in [0,2]:
#    N_plot = A_PMLs[x_source_pos[0]][idx_plot].shape[0] - 1
#    print("N_plot =", N_plot)
#    PlotSparsityPattern(A_PMLs[x_source_pos[0]][idx_plot], B_PMLs[x_source_pos[0]][idx_plot],"TP-PML-N{0}".format(N_plot))


