import numpy as np
from ngsolve import *
from netgen.geom2d import SplineGeometry
from math import sqrt,exp
np.random.seed(123)
import ceres_dtn as opt
from ngs_refsol import PlaneWaveHomSolution,JumpSolution,JumpSolutionIncomingWave
import mpmath as mp 
from GRPF import Poles_and_Roots
import os
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

a = 1.0  # radius of coupling boundary
R_inf = 2.0 # location of wavespeed discontinuity
R_S = 0.5 # radius of scatterer
k_I = 16 # interior wavespeed
alpha_decay = 1.5 # decay of weights
check_system = False # check if coeff solve linear system
k_infs = [8,12,16,20] # exterior wavenumbers (for r > R_inf)
Lmax = 72
Nmax  = 9
order = 12
Ns = list(range(Nmax))
lam =  np.array([(mm/a)**2 for mm in range(Lmax)]) 
#r_ext = min(R_S*k_I/16,R_S)
r_ext = R_S
#x_source = 1/3
#y_source = -1/3

l2_errors_collect = [] 

def hankel1(nu,k):
    return mp.hankel1(nu,k)

#weights_ref = np.array([10**6* abs(hankel1(l,k_I*a) / hankel1(l,k_I*sqrt(x_source**2+y_source**2))) for l in range(len(lam))])
#print("weights_ref = ", weights_ref) 

#ansatz = "minimalIC"
ansatz = "mediumSym"
flags = {"max_num_iterations":1000000,
          "check_gradients":False, 
          "use_nonmonotonic_steps":True,
          "minimizer_progress_to_stdout":False,
          "num_threads":4,
          "report_level":"Brief",
          "function_tolerance":1e-14,
          "parameter_tolerance":1e-14,
          "gradient_tolerance":1e-14}
          

param = {} # for searching poles
param["Tol"] = 1e-9
param["visual"] = 1 
param["ItMax"] = 35
param["NodesMax"] = 500000

# define some auxiliary functions

def new_initial_guess(A_old,B_old,ansatz):
    N = A_old.shape[0]
    A_guess = np.zeros((N+1,N+1),dtype='complex')
    B_guess = np.zeros((N+1,N+1),dtype='complex')
    if ansatz in ["medium","full"]:
        A_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        B_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        A_guess[:N,:N] = A_old[:]
        B_guess[:N,:N] = B_old[:]
        A_guess[N,N] = 1.0
    elif ansatz == "minimalIC":
        A_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        A_guess[:N,:N] = A_old[:]
        B_guess[:N,:N] = B_old[:]
        A_guess[N,N] = -100-100j
        B_guess[0,N] = 1e-3*(np.random.rand(1) + 1j*np.random.rand(1)) 
    elif ansatz == "mediumSym":
        A_guess[:N,:N] = A_old[:]
        B_guess[:N,:N] = B_old[:]
        A_guess[0,N]  = A_guess[N,0] = 1e-3*(np.random.rand() + 1j*np.random.rand())
        #B_guess[0,N] =  B_guess[N,0] = 1e-3*(np.random.rand() + 1j*np.random.rand())
        B_guess[0,N] =  B_guess[N,0] = 0.5*1e-1*(np.random.rand() + 1j*np.random.rand())
        A_guess[N,N] = -100-100j
        B_guess[N,N] = 1.0
    return A_guess,B_guess


def h1vp(nu,k):
    return 0.5*(mp.hankel1(nu-1,k) - mp.hankel1(nu+1,k)) 

def jv(nu,k):
    return mp.besselj(nu,k)

def jvp(nu,k):
    return mp.besselj(nu,k,derivative=1)

def yv(nu,k):
    return mp.bessely(nu,k)

def yvp(nu,k):
    return mp.bessely(nu,k,derivative=1)

geo = SplineGeometry()
geo.AddCircle( (0,0), 1.0, leftdomain=1, rightdomain=0,bc="outer-bnd")
geo.AddCircle( (0,0), R_S, leftdomain=0, rightdomain=1,bc="inner-bnd")
geo.SetMaterial(1, "inner")
mesh = Mesh(geo.GenerateMesh (maxh=0.05,quad_dominated=False))
mesh.Curve(order)

fes_inner = H1(mesh,complex=True,order=order,dirichlet="inner-bnd")
fes_surf = H1(mesh,order=order  ,complex=True,definedon=mesh.Boundaries("outer-bnd"))

for k_inf in k_infs:
    l2_errors = []
    
    if k_I != k_inf:
        #ref_sol = JumpSolution(k_I,k_inf,R_S,R_inf) 
        ref_sol = JumpSolutionIncomingWave(k_I,k_inf,R_S,R_inf,k_inf) 
    else:
        ref_sol = PlaneWaveHomSolution(k_I,R_S,"sound-soft")
    l2_norm = sqrt ( Integrate (  InnerProduct(ref_sol,ref_sol), mesh, VOL ).real)

    vtk = VTKOutput(ma=mesh,coefs=[ref_sol.real,ref_sol.imag],
                                   names=["jump-sol-real","jump-sol-imag"],
                                   filename="jump-sol-k_inf{0}".format(k_inf),
                                   subdivision=4)
    vtk.Do()
    print("---------------------------------------------------------------------")
    print("Computing for k_inf = {0}".format(k_inf)) 
    A_IE = []
    B_IE = []
    relative_residuals = []
    
    np.random.seed(123)
    
    # define coefficients
    def M_nu(nu):
        tmp1  = -(k_inf/k_I)*(h1vp(nu,k_inf*R_inf)/jv(nu,k_I*a))*( jv(nu,k_I*a)*yv(nu,k_I*R_inf) - yv(nu,k_I*a)*jv(nu,k_I*R_inf) )
        tmp2 = (hankel1(nu,k_inf*R_inf)/jv(nu,k_I*a))*( yvp(nu,k_I*R_inf)*jv(nu,k_I*a) - yv(nu,k_I*a)*jvp(nu,k_I*R_inf))
        if abs(tmp1+tmp2) < 1e-12:
            print("nu = {0}, tmp1+tmp2 ={1} ".format(nu,tmp1+tmp2))
        return tmp1+tmp2

    def B_nu(nu):
        tmp1 = (k_inf/k_I)*h1vp(nu,k_inf*R_inf)*jv(nu,k_I*R_inf)/jv(nu,k_I*a) 
        tmp2 = -hankel1(nu,k_inf*R_inf)*jvp(nu,k_I*R_inf)/jv(nu,k_I*a)
        return (tmp1+tmp2)/M_nu(nu)

    def A_nu(nu):
        return (1-B_nu(nu)*yv(nu,k_I*a))/jv(nu,k_I*a)

    def C_nu(nu):
        tmp1 = ( yvp(nu,k_I*R_inf)*jv(nu,k_I*a) - yv(nu,k_I*a)*jvp(nu,k_I*R_inf) )*( jv(nu,k_I*R_inf) / jv(nu,k_I*a)**2)
        tmp2 = -( jv(nu,k_I*a)*yv(nu,k_I*R_inf) - yv(nu,k_I*a)*jv(nu,k_I*R_inf) )*( jvp(nu,k_I*R_inf) / jv(nu,k_I*a)**2   )
        return (tmp1+tmp2)/M_nu(nu)
    
    if check_system:
        def Check(nu):
            M = np.array( [ [complex(jv(nu,k_I*a)),complex(yv(nu,k_I*a)),0],
                            [complex(jv(nu,k_I*R_inf)),complex(yv(nu,k_I*R_inf)),-complex(hankel1(nu,k_inf*R_inf))],
                            [k_I*complex(jvp(nu,k_I*R_inf)),k_I*complex(yvp(nu,k_I*R_inf)),-k_inf*complex(h1vp(nu,k_inf*R_inf))]
                        ])
            print( "m = {0}, error = {1}".format(nu, np.linalg.norm( M @ np.array([complex(A_nu(nu)),complex(B_nu(nu)),complex(C_nu(nu))]) - np.array([1,0,0])) ))
            
        for nu in range(20):
            Check(nu)

    def dtn_ref(nu):
        return complex(-k_I*( A_nu(nu)*jvp(nu,k_I*a) + B_nu(nu)*yvp(nu,k_I*a)))

     
    if k_I != k_inf:
        def fun(z):
            return dtn_ref(mp.sqrt(z)*a)
    else:
        # While searching for the poles with k_I == k_inf the function M_nu(sqrt(z)*a) can become zero. 
        # This is a problem because we devide by this expression. To avoid this problem we replace dtn_ref by the equivalent 
        # expression for the dtn of the homogeneous problem dtn_hom. 
        def dtn_hom(nu):
            return complex(-k_I*h1vp(nu,k_I*a) / hankel1(nu,k_I*a))
        def fun(z):
            return dtn_hom(mp.sqrt(z)*a)

    #weights = np.array([10**6* abs(complex( (A_nu(l)*jv(l,k_I*a)+B_nu(l)*yv(l,k_I*a)) / 
    #          (A_nu(l)*jv(l,k_I*sqrt(x_source**2+y_source**2))+B_nu(l)*yv(l,k_I*sqrt(x_source**2+y_source**2))))) for l in range(len(lam))])
    weights = np.array([10**6* abs(complex( (A_nu(l)*jv(l,k_I*a)+B_nu(l)*yv(l,k_I*a)) / 
              (A_nu(l)*jv(l,k_I*r_ext)+B_nu(l)*yv(l,k_I*r_ext)))) for l in range(len(lam))])
    print("weights = ", weights)
    dtn_nr = np.array([ dtn_ref(mp.sqrt(lami)*a) for lami in lam ])
    l_dtn = opt.learned_dtn(lam,dtn_nr,weights**2)

    A_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
    B_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)

    for N in Ns: 
        l_dtn.Run(A_guess,B_guess,ansatz,flags)
        A_IE.append(A_guess.copy()), B_IE.append(B_guess.copy())
        A_guess,B_guess = new_initial_guess(A_IE[N],B_IE[N],ansatz)

    if k_inf == 8:
        lam_intervals = [(0.0,220),(220,370),(370,420),(420,530),(530,540),(540,1000),(1000,10000)]
        lam_Nsamples = [20,40,200,20,2000,20,30]
    else:
        lam_intervals = [(0,500),(500,1000),(1000,10000)] 
        lam_Nsamples = [200,20,30]
    lam_sample = []
    for (lam_min,lam_max),N_samples in zip(lam_intervals,lam_Nsamples):
        lam_sample.extend([lam_min +  j*(lam_max-lam_min)/N_samples for j in range(N_samples)])
    lam_sample = np.array(lam_sample) 
    dtn_ref_sampled = np.array([ dtn_ref(sqrt(lami)*a) for lami in lam_sample])
    
    # results for real / imaginary part 
    plot_collect_real = [lam_sample.real,dtn_ref_sampled.real]
    fname_real  = "jump-dtn-kinf{0}-real.dat".format(k_inf)
    plot_collect_imag = [lam_sample.real,dtn_ref_sampled.imag]
    fname_imag  = "jump-dtn-kinf{0}-imag.dat".format(k_inf)
    header_str = 'lam zeta'
    for N in Ns:
        dtn_approx = np.zeros(len(lam_sample),dtype='complex')
        opt.eval_dtn_fct(A_IE[N],B_IE[N],lam_sample,dtn_approx)
        plot_collect_real.append(dtn_approx.real)
        plot_collect_imag.append(dtn_approx.imag)
        header_str += " N{0}".format(N)
    np.savetxt(fname=fname_real,
               X=np.transpose(plot_collect_real),
               header=header_str,
               comments='')
    np.savetxt(fname=fname_imag,
               X=np.transpose(plot_collect_imag),
               header=header_str,
               comments='')

    if k_inf==8:
        np.savetxt(fname="jump-dtn-kinf{0}-interp.dat".format(k_inf),
                   X = np.transpose([lam,dtn_nr.real,dtn_nr.imag]),
                   header = "lam real imag",
                   comments='')

        
        print("Searching for poles of dtn at k_inf = {0}".format(k_inf))
        # first do a global search 
        param["xrange"] = [1,550]
        param["yrange"] = [-200,100]
        param["h"] = 3.0
        result = Poles_and_Roots(fun,param)
        analytic_poles1 = result["poles"]
        # now perform a local search to find the remaining pole 
        param["xrange"] = [530.8,530.9]
        param["yrange"] = [0,0.01]
        param["h"] = 0.0004
        result = Poles_and_Roots(fun,param)
        analytic_poles2 = result["poles"]
        analytic_poles = np.append(analytic_poles1,analytic_poles2)   
        analytic_poles = analytic_poles[np.argsort(analytic_poles.imag)]
        fname_analytic_poles = "poles_analytic_kinf{0}.dat".format(k_inf)
        plot_collect_analytic_poles = [analytic_poles.real,analytic_poles.imag]
        header_str_analytic_poles = "poles_real poles_imag"

        np.savetxt(fname=fname_analytic_poles,
            X=np.transpose(plot_collect_analytic_poles),
            header=header_str_analytic_poles,
            comments='')


        for N in Ns[1:]:
            print("\n learned poles for N = {0}".format(N))
            learned_poles = np.array([-A_IE[N][j,j] for j in range(1,N+1)])
            learned_poles = learned_poles[np.argsort(learned_poles.imag)]
            for pole in learned_poles:
                print("({0},{1})".format(pole.real,pole.imag))
                fname_learned_poles = "poles_learned_kinf{0}_N{1}.dat".format(k_inf,N)
                plot_collect_learned_poles = [learned_poles.real,learned_poles.imag]
                header_str_learned_poles = "poles_real poles_imag"

                np.savetxt(fname=fname_learned_poles,
                    X=np.transpose(plot_collect_learned_poles),
                    header=header_str_learned_poles,
                    comments='')
            
        fname_condext = 'condext-'+'learned-'+'kinf{0}.dat'.format(k_inf)
        try:
            os.remove(fname_condext)
        except OSError:
            pass

        with open(fname_condext,'w') as the_file:
            the_file.write('lam N cond \n')
            cond_ext = np.zeros((len(Ns[1:]),len(lam_sample)))
            for N in Ns[1:]:
                for l,lami in enumerate(lam_sample):
                    cond_N_lami =  np.linalg.cond( (A_IE[N])[1:,1:] + lami*(B_IE[N])[1:,1:] )
                    cond_ext[N-1,l] = cond_N_lami
                    the_file.write('{0} {1} {2} \n'.format(lami,N,cond_N_lami))
                the_file.write('\n')

        plt.imshow(cond_ext,norm=LogNorm(vmin=1e0,vmax=1e4),origin='lower',cmap=plt.get_cmap('jet'),interpolation=None)
        plt.colorbar()
        plt.title(ansatz)
        plt.show()
        
    
    if k_inf==12:
        param["xrange"] = [1,500]
        param["yrange"] = [-50,125]
        param["h"] = 3.0
        result = Poles_and_Roots(fun,param)
        
        param["xrange"] = [390,405]
        param["yrange"] = [25,35]
        param["h"] = 0.5
        result = Poles_and_Roots(fun,param)
        
        for N in Ns[1:]:
            print("\n learned poles for N = {0}".format(N))
            learned_poles = np.array([-A_IE[N][j,j] for j in range(1,N+1)])
            learned_poles = learned_poles[np.argsort(learned_poles.imag)]
            for pole in learned_poles:
                print("({0},{1})".format(pole.real,pole.imag))
    
    if k_inf==16:

        print("Searching for poles of dtn at k_inf = {0}".format(k_inf))
        # first do a global search 
        param["xrange"] = [50,400]
        param["yrange"] = [1,1000]
        param["h"] = 10
        result = Poles_and_Roots(fun,param)

        analytic_poles = result["poles"]
        analytic_poles = analytic_poles[np.argsort(analytic_poles.imag)]
        fname_analytic_poles = "poles_analytic_nojump.dat"
        plot_collect_analytic_poles = [analytic_poles.real,analytic_poles.imag]
        header_str_analytic_poles = "poles_real poles_imag"

        np.savetxt(fname=fname_analytic_poles,
            X=np.transpose(plot_collect_analytic_poles),
            header=header_str_analytic_poles,
            comments='')
        
        for N in Ns[1:]:
            print("\n learned poles for N = {0}".format(N))
            learned_poles = np.array([-A_IE[N][j,j] for j in range(1,N+1)])
            learned_poles = learned_poles[np.argsort(learned_poles.imag)]
            for pole in learned_poles:
                print("({0},{1})".format(pole.real,pole.imag))
                fname_learned_poles = "poles_learned_nojump_N{0}.dat".format(N)
                plot_collect_learned_poles = [learned_poles.real,learned_poles.imag]
                header_str_learned_poles = "poles_real poles_imag"

                np.savetxt(fname=fname_learned_poles,
                    X=np.transpose(plot_collect_learned_poles),
                    header=header_str_learned_poles,
                    comments='')


    for N in Ns:
        A_N = A_IE[N]
        B_N = B_IE[N]
        inf_outer = [fes_surf for i in range(A_N.shape[0]-1)]

        X = FESpace([fes_inner]+inf_outer)
        uX = X.TrialFunction() 
        vX = X.TestFunction()

        f_X = LinearForm (X)
        #f_X += SymbolicLFI ( -bnd_data*vX[0], definedon=mesh.Boundaries("inner-bnd") )
        f_X.Assemble()

        aX = BilinearForm(X, symmetric=False)
        aX += SymbolicBFI ( grad(uX[0])*grad(vX[0]) - k_I**2*uX[0]*vX[0] )

        for i in range(A_N.shape[0]):
            for j in range(A_N.shape[0]):
                if abs(A_N[j,i]) > 1e-10 or abs(B_N[j,i]>1e-10):
                    graduX = grad(uX[i]).Trace()
                    gradvX = grad(vX[j]).Trace()  
                    aX += SymbolicBFI ( B_N[j,i]*graduX*gradvX + A_N[j,i]*uX[i]*vX[j]  ,definedon=mesh.Boundaries("outer-bnd"))
        aX.Assemble()

        gfu_X = GridFunction (X)

        res_X = gfu_X.vec.CreateVector()
        gfu_X.vec[:] = 0.0
        gfu_X.components[0].Set(ref_sol,BND)
        Draw(gfu_X.components[0],mesh,"bnd-data")
        res_X.data = f_X.vec - aX.mat * gfu_X.vec
        invX = aX.mat.Inverse(freedofs=X.FreeDofs(), inverse="umfpack")
        gfu_X.vec.data += invX * res_X
 
        Draw(gfu_X.components[0],mesh,"learnedsol")
        l2_err = sqrt ( Integrate (  InnerProduct(gfu_X.components[0] - ref_sol,gfu_X.components[0] - ref_sol), mesh, VOL ).real)
        print("N = {0},rel l2_err ={1} \n ".format(A_N.shape[0]-1,l2_err/l2_norm))
        l2_errors.append(l2_err/l2_norm)
    l2_errors_collect.append(l2_errors)

plot_collect = [Ns]
header_str = 'N '
fname  = "jump-l2_relerr.dat"
for l2_error,k_inf in zip(l2_errors_collect,k_infs):
    plot_collect.append(l2_error)
    header_str += "kInf{0} ".format(k_inf)
np.savetxt(fname=fname,
           X=np.transpose(plot_collect),
           header=header_str,
           comments='')


