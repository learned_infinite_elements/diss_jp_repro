from math import pi
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib import ticker

L_min = 0
L_max = 1000  # how many DtN numbers to take
#L_max = 500  # how many DtN numbers to take
L_spacing = 1
L_range = np.arange(L_min,L_max+1,L_spacing)

f_hzs = np.linspace(0.001,8.3,1000)
Ns = range(4)

index_1mhz = np.where( f_hzs > 1.0)[0][0]
#f_hzs = np.linspace(0.001,8.3,300)
f_hzs = (10**-3)*f_hzs

freq_spec = "low"
freq_spec = "high"

plt.rc('legend', fontsize=18)
plt.rc('axes', titlesize=18)
plt.rc('axes', labelsize=18)
plt.rc('xtick', labelsize=14)    
plt.rc('ytick', labelsize=14)

plt.rcParams['savefig.dpi'] = 300
plt.rcParams["figure.dpi"] = 100

#################################################### 
# load reference power spectrum
####################################################

mdi_data = np.loadtxt("../multiplets-mdi-2001.dat",unpack=False)
hmi_data = np.loadtxt("../multiplets-hmi-2010.dat",unpack=False)


lowl_mdi_data = np.loadtxt("../wood.3016.360d.36",unpack=False)

ns_low = lowl_mdi_data[:,1]
ls_low = lowl_mdi_data[:,0]
nus_low =  lowl_mdi_data[:,2]
FWHM_low = lowl_mdi_data[:,4]

ns = mdi_data[:,0]
ls = mdi_data[:,1]
nus = mdi_data[:,2]
FWHM = mdi_data[:,8+6]

for i in range(len(FWHM)):
    print("FWHM[{0}] = {1}, l = {2}, nu = {3}, n = {4}".format(i,FWHM[i],ls[i],nus[i],ns[i] ))

idx = np.where( FWHM >0) 
ns = ns[idx]
ls = ls[idx]
nus = nus[idx]
FWHM = FWHM[idx]

idx_low = np.where( FWHM_low >0 ) 
ns_low = ns_low[idx_low]
ls_low = ls_low[idx_low]
nus_low = nus_low[idx_low]
FWHM_low = FWHM_low[idx_low]

print("FWHM_low.min() =", FWHM_low.min() )
print("FWHM_low.max() =", FWHM_low.max() )

ns = np.append(ns_low,ns)
ls = np.append(ls_low,ls)
nus = np.append(nus_low,nus)
FWHM = np.append(FWHM_low,FWHM)

l_eval = np.arange(0,400)
nu_eval = np.linspace(1000,4000,len(l_eval))

from scipy import interpolate
points = np.column_stack((ls,nus))
xv, yv = np.meshgrid(l_eval, nu_eval, sparse=False, indexing='ij')
xi = (xv,yv)
Z = interpolate.griddata(points,FWHM,xi,method='nearest')


import matplotlib
import matplotlib.pyplot as plt
from matplotlib.colors import BoundaryNorm
import matplotlib.colors as colors
import matplotlib.tri as tri

#levels = 14
#levels = 22
#fig, (ax1) = plt.subplots(nrows=1)
#ax1.tricontour(ls, nus, FWHM, levels=levels, linewidths=0.5, colors='k')
#cntr1 = ax1.tricontourf(ls, nus, FWHM, levels=levels, cmap="RdBu_r")

#fig.colorbar(cntr1, ax=ax1)
#ax1.plot(ls, nus, 'ko', ms=3)
#ax1.set(xlim=(-2, 2), ylim=(-2, 2))
#ax1.set_title('tricontour (%d points)' % npt)
#plt.show()
#plt.clf()

xi = np.arange(0,1001)
yi = np.linspace(0,8300,7200)

# Linearly interpolate the data (x, y) on a grid defined by (xi, yi).
triang = tri.Triangulation(ls, nus)
interpolator = tri.LinearTriInterpolator(triang, FWHM)
Xi, Yi = np.meshgrid(xi, yi)
zi = interpolator(Xi, Yi)

for j in range(len(yi)):
    for i in range(len(xi)):
        if zi[j,i] is np.ma.masked:
            zi[j,i] = np.nan

mask = np.isnan(zi)
zi[mask] = np.interp(np.flatnonzero(mask), np.flatnonzero(~mask), zi[~mask])

for j in range(len(yi)):
    for i in range(len(xi)):
        if i == 0:
            print("z[j,0]= ", zi[j,0])

import scipy.ndimage as ndimage
ZI = ndimage.gaussian_filter(zi, sigma=20.0, order=0)

np.savetxt('damping.out',ZI,delimiter=',')

nu0 = 3000
gamma0 = 2*4.29

Zp = ZI.copy() 
for j in range(len(yi)):
    for i in range(len(xi)):
        if yi[j] < 1200:
            Zp[j,i] = gamma0*(1200/nu0)**5.77
        elif yi[j] > 5300:
            Zp[j,i] = gamma0*(5300/nu0)**5.77
        else:
            Zp[j,i] = gamma0*(yi[j]/nu0)**5.77

#fig, (ax1) = plt.subplots(nrows=1)
#ax1.contour(xi, yi, zi, levels=14, linewidths=0.5, colors='k')
#cntr1 = ax1.contourf(xi, yi, zi, locator=ticker.LogLocator(), levels=18, cmap="RdBu_r")

#fig.colorbar(cntr1, ax=ax1)
#ax1.plot(ls, nus, 'ko', ms=.5)
#plt.show()


##fig, (ax1) = plt.subplots(nrows=1)
#ax1.contour(xi, yi, ZI, levels=14, linewidths=0.5, colors='k')
#cntr1 = ax1.contourf(xi, yi, ZI, locator=ticker.LogLocator(), levels=18, cmap="RdBu_r")

#fig.colorbar(cntr1, ax=ax1)
#ax1.plot(ls, nus, 'ko', ms=.5)
#plt.show()


#fig, (ax1) = plt.subplots(nrows=1)
#ax1.contour(xi, yi, Zp, levels=14, linewidths=0.5, colors='k')
#cntr1 = ax1.contourf(xi, yi, Zp, locator=ticker.LogLocator(), levels=5, cmap="RdBu_r")

#fig.colorbar(cntr1, ax=ax1)
#ax1.plot(ls, nus, 'ko', ms=.5)
#plt.show()

# multi-plot
widths = [5,1]
heights = [1]
levels = 20
gs_kw = dict(width_ratios=widths, height_ratios=heights)
idx_l = np.where(yi>1000)[0][0]
idx_h = np.where(yi>5355)[0][0]
yi_c = yi[idx_l:idx_h]
ZI_c = ZI[idx_l:idx_h,:]
Zp_c = Zp[idx_l:idx_h,:]

fig, ax = plt.subplots(figsize=(12,8),nrows=1,ncols=2,gridspec_kw=gs_kw)

cntr1 = ax[0].contourf(xi, yi_c, ZI_c, levels=levels, cmap="Reds",vmin=0,vmax=280)
ax[0].plot(ls, nus, 'ko', ms=.1)
ax[0].set_xlabel("$\ell$")
ax[0].set_ylabel("FWHM ($\mu$Hz)")
ax[0].set_title("$\ell$-dependent attenuation")

cntr2 = ax[1].contourf(xi[:50], yi_c, Zp_c[:,:50], levels=levels, cmap="Reds",vmin=0,vmax=280)

ax[1].set_yticks([])
ax[1].set_xticks([])
ax[1].set_title("power-law")
fig.colorbar(cntr1, ax=ax.ravel().tolist())
plt.savefig("damping-models-comp.png",transparent=True)
plt.show()

#h = plt.contourf(l_eval,nu_eval,Z)
#plt.colorbar(h)
#plt.show()
#plt.imshow( Z.T, origin='lower')
#plt.show()


all_l = np.arange(5,1000)
all_nu = np.arange(500,5300)

l0 = 100
power_l = 1e-3*(all_l*(all_l+1)/l0)**1.2

for n in range(1,23):
    idx = np.where(  (ns == n) & (FWHM >0) )
    idx_low = np.where( (ns_low == n) & (FWHM_low >0))
    ll = np.append(ls_low[idx_low],ls[idx])
    FWHM_c = np.append(FWHM_low[idx_low],FWHM[idx])
    plt.semilogy(ll,FWHM_c,label="{0}".format(n))
plt.semilogy(all_l,2*power_l,label="fit",color='k',linewidth=3)
plt.legend()
plt.show()


nu0 = 3000
gamma0 = 4.29
power_damping = gamma0*(all_nu/nu0)**5.77

FWHM_collect = np.array([])
nu_collect = np.array([])

for n in range(1,13):
    idx = np.where(ns == n)
    idx_low = np.where(ns_low == n)
    nunu  = np.append(nus_low[idx_low],nus[idx]) 
    nu_collect = np.append(nu_collect,nunu)
    FWHM_c = np.append(FWHM_low[idx_low],FWHM[idx])
    FWHM_collect = np.append(FWHM_collect,FWHM_c)
plt.semilogy(nu_collect,FWHM_collect,label="{0}".format(n),marker='o',color='black',linewidth=0,markersize=1)
plt.semilogy(all_nu,2*power_damping,label="fit",color='orange',linewidth=4)
plt.show()

header = "f FWHM"
plot_collect = [nu_collect,FWHM_collect]
np.savetxt(fname = "f-FWHM-modes.dat", 
           X = np.transpose(plot_collect),
           header = header,
           comments = '')

header = "f FWHM"
plot_collect = [all_nu,2*power_damping]
np.savetxt(fname = "power-law-damping.dat", 
           X = np.transpose(plot_collect),
           header = header,
           comments = '') 

