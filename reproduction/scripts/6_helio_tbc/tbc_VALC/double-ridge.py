#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from math import pi
import sys
import urllib.request 

if ( len(sys.argv) > 1 and sys.argv[1] == "download"):
    print("Downloading power spectra from Gro.data catalogue") 
    file_VALC_meshed, header_VALC_meshed = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/N0S2QJ/4VW7LH","power-spectrum-VALC-meshed.out")
    file_0, header_0 = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/N0S2QJ/ZJJDJD","power-spectrum-N0-VALC.out")

plt.rc('legend', fontsize=18)
plt.rc('axes', titlesize=18)
plt.rc('axes', labelsize=18)
plt.rc('xtick', labelsize=14)    
plt.rc('ytick', labelsize=14)


f_hz_comp = np.linspace(0,8.3,7200)
f_hzs = (10**-3)*f_hz_comp
power_spectrum_VALC_meshed = np.loadtxt('power-spectrum-VALC-meshed.out',delimiter=',')
power_spectrum_learned = np.loadtxt('power-spectrum-N0-VALC.out',delimiter=',')

def scale_power_spectrum(power,f_hzs):

    def PI_scal(f):
        result = 1 + ((abs(f)-3.3*1e-3)/ (0.6*1e-3) )**2
        return 1/result 
     
    scaled_power = np.zeros(power.shape) 
    for i  in range(power.shape[0]):
        if abs(f_hzs[i]) > 1e-10:
            scaled_power[i,:] = power[i,:]*0.5*PI_scal(f_hzs[i])/(2*pi*f_hzs[i]) 
    return scaled_power



def apply_Granulation_filter(power,f_hz):    
    
    def Granu_filter(f):
        #print("f = {0}, filtered = {1}".format(f,np.tanh(3*f/0.0015)))
        return np.tanh(3*f/0.0015)

    filtered_power = np.zeros(power.shape) 
    for i in range(power.shape[0]):
        filtered_power[i,:] = power[i,:]*Granu_filter(f_hz[i])

    return filtered_power

def post_process_power(ps):
    ps = np.abs(ps)
    ps = scale_power_spectrum(ps,f_hzs)
    return ps

power_spectrum_VALC_meshed = post_process_power(power_spectrum_VALC_meshed)
power_spectrum_VALC_meshed = apply_Granulation_filter(power_spectrum_VALC_meshed,2*pi*f_hzs)

power_spectrum_learned = post_process_power(power_spectrum_learned)
power_spectrum_learned = apply_Granulation_filter(power_spectrum_learned,2*pi*f_hzs)

from scipy.special import eval_legendre

def compute_TD_diagram(power_spectrum,f_hz,N_theta=100,Nt=2**12,name_str="dummy.png",vmin=1e-5,vmax=1e-0):
  

    def Gauss_scale(f,omega_center=6.75):
        return np.exp( -0.5*(abs(f)-omega_center)**2/(0.75)**2 )

    def velocity_filter(f,l,v_center=250.4): 
        #print("l = {0}, val = {1}".format(l,np.exp( -0.5*(l-125)**2/(33)**2 )))
        return np.exp( -0.5*(l-125)**2/(33)**2 )

    filter_freq = Gauss_scale(f_hz)  
    plot_collect_vals  = [ f_hz ,filter_freq] 
    filter_freq_header = "omega val"
    filter_freq_fname = "double-ridge-filter-freq.dat"
    np.savetxt(fname =filter_freq_fname , 
               X = np.transpose(plot_collect_vals),
               header = filter_freq_header,
               comments = '')
    
    lls = np.arange(power_spectrum.shape[1]) 
    filter_spatial = velocity_filter(f_hz[0],lls)
    plot_collect_vals  = [ lls ,filter_spatial] 
    filter_spatial_header = "l val"
    filter_spatial_fname = "double-ridge-filter-spatial.dat"
    np.savetxt(fname =filter_spatial_fname , 
               X = np.transpose(plot_collect_vals),
               header = filter_spatial_header,
               comments = '')

    thetas = np.linspace(0,0.5*pi,N_theta)
    #thetas = np.linspace(pi*40/180,pi*70/180,N_theta)
    print("thetas = ", thetas)

    C_tau = np.zeros(( power_spectrum.shape[0], len(thetas)), dtype="float")
    
    precomputed_factors = np.zeros( power_spectrum.shape) 
    #print("precomputed_factors.shape = ", precomputed_factors.shape ) 
    for i in range(power_spectrum.shape[0]):
        for l in range(power_spectrum.shape[1]):
            #precomputed_factors[i,l] = Gauss_scale(f_hz[i])**2*velocity_filter(f_hz[i],l)**2
            precomputed_factors[i,l] = Gauss_scale(f_hz[i])*velocity_filter(f_hz[i],l)
    print("precomputed_factors =", precomputed_factors ) 
    for j in range(len(thetas)):
        l_legendre = np.array([(l+0.5)*eval_legendre(l,np.cos(thetas[j])) for l in range(power_spectrum.shape[1]) ])
        print("thetas[j] = ", thetas[j])
        for i in range(power_spectrum.shape[0]):
            li_factor = np.multiply(precomputed_factors[i,:],l_legendre)
            C_tau[i,j] = np.dot(power_spectrum[i,:],li_factor)
     
    ts = np.linspace(0,300*60,Nt)/(2*pi)
    domega = 2*pi*(10**(-3))*f_hz[-1]/power_spectrum.shape[0]
    dt = ts[-1]/(Nt)
    Ntmp = int(1/(domega*dt))
    print("Ntmp = ", Ntmp)
    C_t = np.zeros((Nt,len(thetas)), dtype="complex")
    for i in range(len(thetas)):
        C_t[:,i] = np.fft.fft(C_tau[:,i],n=Ntmp)[:Nt]

    print("np.max(C_t.real) = ", np.max(C_t.real)) 
    print("np.min(C_t.real) = ", np.min(C_t.real)) 
    
    fig = plt.figure(figsize=(5,5),constrained_layout=True)
    im = plt.pcolormesh(180*thetas/pi,2*pi*ts/60, abs(C_t.real),
                           cmap=plt.get_cmap('hot'),
                           norm=colors.LogNorm( vmin=vmin ,vmax= vmax)   )
    #plt.colorbar(im)
    plt.xlabel("Distance (Degrees)")
    plt.ylabel("Time (Minutes)")
    #plt.yticks([])
    plt.savefig(name_str,transparent=True)
    plt.show()
    plt.cla()
    plt.clf()
    plt.close()
    
    return C_t

compute_TD_diagram(power_spectrum_VALC_meshed,f_hz_comp,N_theta=1000,Nt=2**12,name_str="double-ridge-VALC-meshed-atmo.png",vmin=6e-5,vmax=1e-2 )
compute_TD_diagram(power_spectrum_learned,f_hz_comp,N_theta=1000,Nt=2**12,name_str="double-ridge-learned-N0.png",vmin=6e-5,vmax=1e-2 )


