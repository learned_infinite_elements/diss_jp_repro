import numpy as np
from ngsolve import *
from netgen.meshing import *
from netgen.csg import *
from netgen.geom2d import SplineGeometry
from netgen.meshing import Mesh as netmesh
from ngsolve import Mesh as NGSMesh
from math import sqrt,exp,pi
np.random.seed(123)
#from scipy.special import hankel1,h1vp,jv,yv,jvp,yvp
import ceres_dtn as opt
#from scipy.special import jv,yv,jvp,yvp
from ngs_refsol import FundamentalSolution
import mpmath as mp 
from GRPF import Poles_and_Roots 
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from cmath import sqrt as csqrt
from cmath import exp as cexp

from scipy.sparse import diags,csr_matrix
from scipy.special import spherical_jn,spherical_yn,sici

omega = 16
transparent_bc = ["BGT","UB","AL","HSIE"] 

rel_err_BGT = [] 

for a in [1,4,16]:

    #omega = 4
    Lmax = 42
    Nmax  = 1
    order = 10
    Ns = list(range(Nmax))
    D = 3 # dimension
    intorder = 20

    def hankel1(nu,omega):
        return mp.hankel1(nu,omega)

    def hankel1p(nu,omega):
        return 0.5*(mp.hankel1(nu-1,omega) - mp.hankel1(nu+1,omega)) 

    def sp_hankel1(nu,omega):
        return mp.sqrt(0.5*pi/omega)*mp.hankel1(nu+0.5,omega)

    def sp_hankel1p(nu,omega):
        return mp.sqrt(0.5*pi)*( -0.5*mp.power(omega,-3/2)*hankel1(nu+0.5,omega) + np.sqrt(1/omega)*hankel1p(nu+0.5,omega))  

    def lam_to_ell(z):
        return -0.5+csqrt( 0.25+a**2*z)

    if D == 2:
        #r_ext = min(0.5*omega/16,1/2)
        #weights = np.array([10**2* abs(complex(mp.hankel1(l,omega*a) / mp.hankel1(l,omega*r_ext) )) for l in range(Lmax)])
        #l0 = np.max( np.where(weights > 1e-12*weights[0])[0]) 
        #weights = weights[:l0]
        l0 = Lmax
        lam = np.array([(l/a)**2 for l in range(l0)]) 
        
        def dtn_ref(nu):
            return complex(-omega*hankel1p(nu,omega*a) /  mp.hankel1(nu,omega*a) )
        
        dtn_nr = np.array([ dtn_ref(sqrt(lami)*a) for lami in lam ]) 
        lam_sample = np.array(lam) 
        dtn_ref_sampled = np.array([ dtn_ref(sqrt(lami)*a) for lami in lam_sample])
    else:
        r_ext = 0.5
        #weights = np.array([10**2* abs(complex(sp_hankel1(l,omega*a) / sp_hankel1(l,omega*r_ext) )) for l in range(Lmax)])
        #weights[:] = 1.0
        #print("weights =", weights)
        #l0 = np.max( np.where(weights > 1e-12*weights[0])[0]) 
        #weights = weights[:l0] 
        l0 = Lmax
        lam = np.array([ l*(l+1)/a**2 for l in range(l0)]) 
        
        def dtn_ref(nu):
            return complex(-omega*sp_hankel1p(nu,omega*a) /  sp_hankel1(nu,omega*a) )
        
        dtn_nr = np.array([ dtn_ref(lam_to_ell(lami)) for lami in lam ]) 
        
        #dtn_scipy = np.array([-omega*(spherical_jn(l,omega*a,True)+1j*spherical_yn(l,omega*a,True))/(spherical_jn(l,omega*a,False)+1j*spherical_yn(l,omega*a,False)) for l in range(len(lam))]) 
        
        #plt.plot(lam,dtn_nr.real,label="mpmath")
        #plt.plot(lam,dtn_scipy.real,label="scipy")
        #plt.legend()
        #plt.show()
        
        #lam_sample = np.array(lam) 
        lam_sample = np.linspace(0,450,200)
        
        dtn_ref_sampled = np.array([ dtn_ref(lam_to_ell(lami)) for lami in lam_sample])

    print("len(Ns[1:]) = {0} , len(lam) = {1}".format(len(Ns[1:]),len(lam)))

    # results for real / imaginary part 
    plot_collect_real = [lam_sample.real,dtn_ref_sampled.real]
    fname_real  = "dtn-real-loworder-omega{0}-a{1}.dat".format(omega,a)
    plot_collect_imag = [lam_sample.real,dtn_ref_sampled.imag]
    fname_imag  = "dtn-imag-loworder-omega{0}-a{1}.dat".format(omega,a)
    header_str = 'lam zeta'

    def diffOp(N_r,a,k0):
        udg = np.arange(1,N_r+1)
        mdg = np.arange(-1,-2*N_r-1-2,-2)
        ldg = udg
        diagonals = [mdg,ldg,udg]
        D = a*diags(np.ones(N_r+1))+(1/(2*1j*k0))*diags(diagonals,[0,-1,1])
        return D

    def get_Tm(N_r):
        mdg = np.ones(N_r)
        udg = -np.ones(N_r)
        return diags([mdg,udg],[0,1],shape=(N_r,N_r))

    def get_Tp(N_r):
        mdg = np.ones(N_r)
        udg = np.ones(N_r)
        return diags([mdg,udg],[0,1],shape=(N_r,N_r))

    def get_HSIEM_matrices(N_r,a,k0,d):
        Cd = (D-1)*(3-D)/4
        D0long = diffOp(4*N_r,1.0,k0)
        I0long = np.linalg.inv(D0long.todense())
        I0 = I0long[:N_r,:N_r]
        Tp = 0.5*get_Tp(N_r)
        Tm = 0.5*get_Tm(N_r)
        
        L1 = (-2*1j*k0/a)* Tp.transpose()@Tp - (2*Cd*1j/(k0*a))*Tm.transpose()@ I0.transpose() @ I0 @ Tm
        L1[0,0] = L1[0,0] + (D-1)/(2*a)
        L2 = (2*a*1j/k0)*Tm.transpose() @ I0.transpose() @ I0 @ Tm
        L3 = (2*a*1j/k0)*Tm.transpose() @ Tm
        neum_repr = 2*1j*k0*np.ones(N_r)
        neum_repr[0] = 1j*k0-0.5
        return L1,L2,L3,(1/a)*neum_repr


    def I_IE(j):
        if j >=2:
            return 1/(j-1) + 2*1j*omega*a*I_IE(j-1)/(j-1)
        if j==1:
            Si_integral,Ci_integral = sici(2*omega*a)
            return cexp(-2*1j*omega*a)*(-Ci_integral-1j*Si_integral+ 1j*0.5*pi)

    def J_IE(mu,nu):
        if mu == 0 and nu ==0:
            return 1/a - 1j*omega
        else:
            return (1/a)*(mu+1)*(nu+1)*I_IE(mu+nu+2)-1j*omega*(mu+nu+2)*I_IE(mu+nu+1)-2*omega**2*a*I_IE(mu+nu)

    def Q_IE(k,mu,nu):
        return a/(a**k*(nu+mu+k+1))

    def R_IE(mu,nu):
        return -1j*omega*(mu+2-nu)*Q_IE(1,mu,nu)+(nu+1)*(mu+3)*Q_IE(2,mu,nu)

    def get_classical_IE_calc(N,omega,formulation="AL",D=3):
        if D == 2:
            print("Classical infinite element matrices currently not implemented for dimesion ", D)
            return None
        elif D == 3:
            A_IE = np.zeros((N+1,N+1),dtype='complex')
            B_IE = np.zeros((N+1,N+1),dtype='complex')
            if formulation == "AL":
                for mu in range(N+1):
                    for nu in range(N+1):
                        
                        f_mu = CoefficientFunction( (a/x)**(mu+3)) 
                        f_nu = CoefficientFunction( (a/x)**(nu+1))
                        df_mu = CoefficientFunction(-(1j*omega + (mu+3)/x)*(a/x)**(mu+3))  
                        df_nu = CoefficientFunction((1j*omega + (-1)*(nu+1)/x)*(a/x)**(nu+1))  
                        
                        f_mu0 = CoefficientFunction( (a/x)**(0+3)) 
                        f_nu0 = CoefficientFunction( (a/x)**(0+1))
                        df_mu0 = CoefficientFunction(-(1j*omega + (0+3)/x)*(a/x)**(0+3))  
                        df_nu0 = CoefficientFunction((1j*omega + (-1)*(0+1)/x)*(a/x)**(0+1))  

                        if mu == 0 and nu == 0:
                            g_mu = f_mu0
                            dg_mu = df_mu0
                            g_nu = f_nu0
                            dg_nu = df_nu0
                        elif mu == 0 and nu >= 1:
                            g_mu = f_mu0
                            dg_mu = df_mu0
                            g_nu =  f_nu-f_nu0
                            dg_nu =  df_nu-df_nu0
                        elif mu >= 1 and nu == 0:
                            g_mu = f_mu-f_mu0
                            dg_mu = df_mu-df_mu0
                            g_nu = f_nu0
                            dg_nu = df_nu0
                        else:
                            g_mu = f_mu-f_mu0
                            dg_mu = df_mu-df_mu0
                            g_nu =  f_nu-f_nu0
                            dg_nu =  df_nu-df_nu0
                        A_IE[mu,nu] = Integrate ( (dg_mu*dg_nu-omega**2*g_mu*g_nu)*(x/a)**2  , mesh_1D_inner, VOL,order=intorder)
                        B_IE[mu,nu] = Integrate ( g_mu*g_nu  , mesh_1D_inner, VOL,order=intorder)
                return A_IE,B_IE
            else: 
                print("Formulation {0} not implemented".format(formulation))
                return None


    def get_classical_IE_matrices(N,omega,formulation="AL",D=3):
        if D == 2:
            print("Classical infinite element matrices currently not implemented for dimesion ", D)
            return None
        elif D == 3:
            A_IE = np.zeros((N+1,N+1),dtype='complex')
            B_IE = np.zeros((N+1,N+1),dtype='complex')
            if formulation == "AL":
                for mu in range(N+1):
                    for nu in range(N+1):
                        if mu == 0 and nu == 0:
                            A_IE[mu,nu] = R_IE(0,0)
                            #A_IE[mu,nu] = (nu+1)*(mu+3)*Q_IE(2,0,0)-1j*omega*(mu+2-nu)*Q_IE(1,0,0)
                            B_IE[mu,nu] = a**2*Q_IE(2,0,0)
                        elif mu == 0 and nu >= 1:
                            A_IE[mu,nu] = R_IE(0,nu)-R_IE(0,0)
                            #A_IE[mu,nu] = (nu+1)*(mu+3)*(Q_IE(2,0,nu)-Q_IE(2,0,0))-1j*omega*(mu+2-nu)*(Q_IE(1,0,nu)-Q_IE(1,0,0)) 
                            B_IE[mu,nu] = a**2*(Q_IE(2,0,nu)-Q_IE(2,0,0))
                        elif mu >= 1 and nu == 0:
                            A_IE[mu,nu] = R_IE(mu,0)-R_IE(0,0)
                            #A_IE[mu,nu] =  (nu+1)*(mu+3)*(Q_IE(2,mu,0)-Q_IE(2,0,0))-1j*omega*(mu+2-nu)*(Q_IE(1,mu,0)-Q_IE(1,0,0)) 
                            B_IE[mu,nu] = a**2*(Q_IE(2,mu,0)-Q_IE(2,0,0))
                        else:
                            A_IE[mu,nu] = R_IE(mu,nu)-R_IE(0,nu)-R_IE(mu,0)+R_IE(0,0)
                            #A_IE[mu,nu] = (nu+1)*(mu+3)*(Q_IE(2,mu,nu)-Q_IE(2,0,nu)-Q_IE(2,mu,0)+Q_IE(2,0,0))-1j*omega*(mu+2-nu)*(Q_IE(1,mu,nu)-Q_IE(1,0,nu)-Q_IE(1,mu,0)+Q_IE(1,0,0))
                            B_IE[mu,nu] = a**2*(Q_IE(2,mu,nu)-Q_IE(2,0,nu)-Q_IE(2,mu,0)+Q_IE(2,0,0)) 
                return A_IE,B_IE

            if formulation == "UB":
                for mu in range(N+1):
                    for nu in range(N+1):
                        if mu == 0 and nu == 0:
                            A_IE[mu,nu] = J_IE(0,0)
                            B_IE[mu,nu] = a*I_IE(2)
                        elif mu == 0 and nu >= 1:
                            A_IE[mu,nu] = J_IE(0,nu)-J_IE(0,0)
                            B_IE[mu,nu] = a*(I_IE(nu+2)-I_IE(2))
                        elif mu >= 1 and nu == 0:
                            A_IE[mu,nu] = J_IE(mu,0)-J_IE(0,0)
                            B_IE[mu,nu] = a*(I_IE(mu+2)-I_IE(2))
                        else:
                            A_IE[mu,nu] = J_IE(mu,nu)-J_IE(0,nu)-J_IE(mu,0)+J_IE(0,0)
                            B_IE[mu,nu] = a*(I_IE(nu+mu+2)-I_IE(nu+2)-I_IE(mu+2)+I_IE(2))

                return A_IE,B_IE
            else: 
                print("Formulation {0} not implemented".format(formulation))
                return None

    def get_BGT(omega,D=3): 
            A_IE = np.zeros((1,1),dtype='complex')
            B_IE = np.zeros((1,1),dtype='complex')
            alp = -1j*omega+1/a
            A_IE[0,0] = alp
            B_IE[0,0] = 0.5/alp
            return A_IE,B_IE


    for tbc in transparent_bc:
        print("---------------------------------------------------------------------")
        
        A_IE = []
        B_IE = []
        relative_residuals = []
        Nzes =[]

        if tbc == "AL" or tbc =="UB":
            for N in Ns:
                A_N,B_N = get_classical_IE_matrices(N,omega,formulation=tbc,D=D) 
                A_IE.append(A_N), B_IE.append(B_N)
                print("cond(A_IE) = ", np.linalg.cond(A_N))

        if tbc == "BGT":
            A_1,B_1 = get_BGT(omega,D=3)
            A_IE = [A_1]
            B_IE = [B_1]

        if tbc == "HSIE":
            for N in Ns:
                La,Lb,Lc,neum_repr = get_HSIEM_matrices(N+1,a,omega*a,D)
                A_tmp = La-omega**2*Lc
                A_full = np.zeros((N+1,N+1),dtype='complex')
                B_full = np.zeros((N+1,N+1),dtype='complex')
                A_full[:,:] = A_tmp[:,:]
                B_full[:,:] = Lb[:,:]
                A_IE.append(A_full)
                B_IE.append(B_full)
                #A_IE.append(La-omega**2*Lc)
                #B_IE.append(Lb)
       
        print("A_IE = {0}, B_IE = {1}".format(A_IE[0],B_IE[0]))

        rel_err = []
        dtn_N = []
        for N,(A_N,B_N) in enumerate(zip(A_IE,B_IE)):
            dtn_approx = np.zeros(len(lam_sample),dtype='complex')
            print(tbc)
            print("--------------------------")
            opt.eval_dtn_fct(A_N,B_N,lam_sample,dtn_approx,False)
            dtn_N.append(dtn_approx)
            rel_err.append(np.abs(dtn_ref_sampled-dtn_approx)/np.abs(dtn_ref_sampled))
            #if N > 2:
                #plt.plot(lam,dtn_approx.real,label='N={0}'.format(N))
                #plt.plot(lam,dtn_approx.imag,label='N={0}'.format(N))
            plot_collect_real.append(dtn_approx.real)
            plot_collect_imag.append(dtn_approx.imag)
            
            header_str += " "
            header_str += tbc

        plt.plot(lam_sample,dtn_ref_sampled.real,label='ref',linewidth=3,color='gray')
        for N in range(len(A_IE)):
            #if N > 4:
            plt.plot(lam_sample,dtn_N[N].real,label='N={0},a={1}'.format(N,a))
        plt.title(tbc)
        plt.legend()
        plt.show()
        
        plt.plot(lam_sample,dtn_ref_sampled.imag,label='ref',linewidth=3,color='gray')
        for N in range(len(A_IE)):
            #if N > 4:
            plt.plot(lam_sample,dtn_N[N].imag,label='N={0},a={1}'.format(N,a))
        plt.title(tbc)
        plt.legend()
        plt.show()

        if tbc == "BGT":
            rel_err_BGT.append(rel_err[0])

        #plt.semilogy(lam_sample,rel_err[0],label='N={0},a={1}'.format(N,a))
        #plt.legend()
        #plt.show()


    np.savetxt(fname=fname_real,
               X=np.transpose(plot_collect_real),
               header=header_str,
               comments='')
    np.savetxt(fname=fname_imag,
               X=np.transpose(plot_collect_imag),
               header=header_str,
               comments='')


for i,a in enumerate([1,4,16]):
    plt.semilogy(lam_sample,rel_err_BGT[i],label='a={0}'.format(a))
plt.legend()
plt.show()

