from netgen.meshing import *
from netgen.csg import *
from ngsolve import *
from math import pi,ceil
import scipy.sparse.linalg
import numpy as np
from ngsolve.solvers import GMRes
from decimal import Decimal
from scipy.linalg import lu_factor,lu_solve
from scipy.sparse import csr_matrix
import sys,os,copy 
sys.path.append(os.path.realpath(''))
from sweeping_geom import HybridLayered_Mesh,Layered_Mesh,Make1DMesh,Make1DMesh_refined,HybridLayered_Mesh_gen,HybridLayered_Mesh_U,HybridLayered_Mesh_halfDisk,Make1DMesh_givenpts
from sweeping_helper import BitArrayFromList,P_DoFs,Layered_Decomposition_cartesian,DOSM_IFassemble,DOSM,draw_mesh_tikz
import ceres_dtn as opt
import matplotlib.pyplot as plt
from xfem import * 
from academic_shared import new_initial_guess,precompute_DtN_coeff,calc_dtn_nr_layer,Learn_dtn_function,LearnedInverse,get_LearnedDtN_and_ext_inv,AlgebraicDtN,get_AlgebraicDtN_and_ext_inv,get_MovingPMLDtN_and_ext_inv,MovingPMLDtN,get_physical_pml,apply_DOSM_to_rhs,run_preconditioned_GMRES
plt.rc('legend',fontsize=14)
plt.rc('axes',titlesize=14)
plt.rc('axes',labelsize=14)
plt.rc('xtick',labelsize=12)
plt.rc('ytick',labelsize=16)

ngsglobals.msg_level = 0

#########################################################################
# Parameters (to be changed by the user)
#########################################################################
omegas = [12,24,48,96]
Ns_compute = [8,10]
eps_perturbs = 1/100*np.array([0.0] + [ 2**k/16 for k in range(7) ])
eps_perturbs = eps_perturbs.tolist()
eps_strs = ["zero","sixteenth","eigths","quarter","half","one","two","four"]

is_symmetric = True
gamma_damping = 0 # add damping to moving PML preconditioner 

show_dtn_approx = False
use_algebraic_DtN = False
use_discrete_eigenvalues = False

direct_solver_precond = False
Nmax = 10
Ns = list(range(Nmax+1))
a = 1.0 # radius of truncation boundary 
use_PML=True

order =  4  # order of FEM 
order_ODE = order # order of ODE discretization
order_geom = order

bonus_intorder = 0
solver = "sparsecholesky" # direct solver (e.g. used for subdomain problems)
#solver = "pardiso" 
C_PML = 50 # amplitude of the PML

ansatz = "mediumSym"
flags = {"max_num_iterations":25000,
         "use_nonmonotonic_steps":True,
         "minimizer_progress_to_stdout":False,
         "num_threads":4,
         "report_level":"Brief",
         "function_tolerance":1e-13,
         "parameter_tolerance":1e-13,
         "gradient_tolerance":1e-13}

def SolveProblem(omega,precond_type,eps_perturb,use_PML,output_dtn_on_layer=False, sweep_for_background_model = False,learned_IE_precomputed={}):

    print("")
    print("Computing for omega = {0}, eps = {1}".format(omega,eps_perturb))
    
    use_moving_PML = False 
    use_learnedIE = False
    direct_solver_precond = False
    if precond_type == "mPML":
        use_moving_PML = True
    elif precond_type == "learned":
        use_learnedIE = True
    else:
        direct_solver_precond = True

    type_str = 'c-perturbed-eps{0}'.format(eps_perturb)
    
    lam_max = int(2*omega)
    problem_config = {"omega":omega, 
                      "use_PML":use_PML,
                      "use_discrete_eigenvalues": use_discrete_eigenvalues,
                      "order_ODE": order_ODE,
                      "bonus_intorder":bonus_intorder,
                      "use_algebraic_DtN":use_algebraic_DtN,
                      "use_learnedIE":use_learnedIE,
                      "use_moving_PML":use_moving_PML,
                      "Ns":Ns,
                      "output_dtn_on_layer":output_dtn_on_layer,
                      "show_dtn_approx":show_dtn_approx, 
                      "ansatz":ansatz,
                      "flags":flags,
                      "type_str":type_str,
                      "is_symmetric":is_symmetric,
                      "solver":solver,
                      "order":order,
                      "C_PML":C_PML
                     }

    #########################################################################
    # Layered mesh
    #########################################################################

    n_layers = int(omega/4) # -> 12 dofs per wavelength 
    r_layer = [ i*a / (n_layers) for i in range(n_layers+1)]
    nx = 2*len(r_layer)
    refinement_marker = [False for i in range(len(r_layer))]
    mesh = Layered_Mesh(r_layer,nx,True,refinement_marker,'periodic',1.0)
    Draw(mesh)

    c = 1.0/sqrt(1+eps_perturb)
    c_1D = 1.0
    c_1D_perturbed  = 1.0/sqrt(1+eps_perturb)
    c_background = 1.0 

    problem_config["c_1D"] = c_1D
    problem_config["c"] = c

    #########################################################################
    # FEM space (Proxy and-GridFunctions) 
    #########################################################################

    fes_H1 = H1(mesh, complex=True,  order=order,dirichlet=[])
    fes = Periodic(fes_H1)
    fes = Compress(fes)

    free_dofs = [] 
    for nr,b in enumerate(fes.FreeDofs()): 
        if b:
            free_dofs.append(nr)

    u = fes.TrialFunction() 
    v = fes.TestFunction() 

    gfu_GMRES = GridFunction (fes)
    udirect = GridFunction (fes)

    gfu = GridFunction (fes)
    u_in = GridFunction (fes)
    u_out = GridFunction (fes)
    gfu_DOSM_in = GridFunction (fes)
    gfu_DOSM_out = GridFunction (fes)
    gfu_DOSM_interm = GridFunction (fes)

    #########################################################################
    # Domain decomposition 
    #########################################################################

    rr = r_layer
    r_layer = rr[1:]
    intervals = [ (rr[i],rr[i+1]) for i in range(len(rr)-1)]
    intervals = intervals[::-1]

    decomp = Layered_Decomposition_cartesian(mesh,fes,n_layers,intervals)
    decomp.Decompose()

    problem_config["r_layer"] = r_layer

    #########################################################################
    # PML 
    #########################################################################

    pml_phys,pml_1D = get_physical_pml(problem_config)
    definedon_PML = 'default'
    problem_config["pml_phys"] = pml_phys  
    problem_config["definedon_PML"] = definedon_PML
    problem_config["pml_1D"] = pml_1D


    #########################################################################
    # Solution with direct solver  
    #########################################################################

    if use_PML:
        mesh.SetPML(pml_phys,definedon='default')

    aX = BilinearForm (fes, symmetric=is_symmetric)
    aX += SymbolicBFI (  (grad(u)*grad(v) - (omega**2/c**2)*u*v) , bonus_intorder = bonus_intorder  )
    aX.Assemble()

    if direct_solver_precond or sweep_for_background_model:
        aX_background = BilinearForm (fes, symmetric=is_symmetric)
        aX_background += SymbolicBFI (  (grad(u)*grad(v) - (omega**2/c_background**2)*u*v) , bonus_intorder = bonus_intorder  )
        aX_background.Assemble()

    f = LinearForm (fes)
    f.Assemble()

    np.random.seed(123)
    rhs_random = np.random.rand(fes.ndof)
    f.vec.FV().NumPy()[free_dofs] = 0.1*rhs_random[free_dofs]
    f.vec.FV().NumPy()[decomp.layer2dof[0]] = 0.0

    if use_PML:
        mesh.UnSetPML(definedon_PML)

    gfu.vec[:] = 0.0
    res_direct = gfu.vec.CreateVector()
    udirect.vec[:] = 0.0
    res_direct.data = f.vec - aX.mat * udirect.vec

    invA = aX.mat.Inverse(freedofs=fes.FreeDofs(), inverse=solver)
    udirect.vec.data += invA * res_direct
    Draw (udirect, mesh, "direct")

       
    #########################################################################
    # Computation of DtN numbers (ODE solves) 
    #########################################################################

    problem_config["lam_max"] = lam_max
    DtN_coeff_layer,lam_layer,lam_vec_layer = precompute_DtN_coeff(decomp,problem_config)
    spectral_info = {"DtN_coeff_layer":DtN_coeff_layer,
                     "lam_layer": lam_layer,
                     "lam_vec_layer":lam_vec_layer  
                    }

    if output_dtn_on_layer: 
        
        max_lam = lam_layer[output_dtn_on_layer].real[-1]
        lam_small = omega**2
        lam_mid = 6*omega**2
        lam_sample = np.linspace(0,lam_small,2500) 

        problem_config["c_1D"] = c_1D_perturbed
        dtn_nr_sample = calc_dtn_nr_layer(output_dtn_on_layer,lam_sample,problem_config)


        a_layer = r_layer[-1-output_dtn_on_layer]
        b_layer = r_layer[-1]
        sigma_layer = np.sqrt(omega**2-lam_sample)
        poles_analytic = []
        roots_analytic = [] 
        jj = 0
        while jj >= 0:
            new_analytic_pole = omega**2- 0.25*pi**2*(2*jj+1)**2/(b_layer-a_layer)**2
            if new_analytic_pole.real < 0:
                jj = -1 
            else:
                poles_analytic.append(new_analytic_pole)
                jj += 1
        kk = 0
        while kk >= 0:
            new_analytic_root = omega**2- 0.25*pi**2*(2*kk)**2/(b_layer-a_layer)**2
            if new_analytic_root.real < 0:
                kk = -1 
            else:
                roots_analytic.append(new_analytic_root)
                kk += 1

        poles_analytic  = np.array(poles_analytic)
        roots_analytic = np.array(roots_analytic)

        print("poles_analytic = ", poles_analytic) 
        print("roots_analytic = ", roots_analytic) 

        dtn_layer_analytic = -sigma_layer*np.sin(sigma_layer*(b_layer-a_layer))/np.cos(sigma_layer*(b_layer-a_layer))
        if False:
            plt.plot(lam_sample.real,dtn_layer_analytic.real,label="analytic")
            plt.vlines(poles_analytic.real,ymin=-1e5,ymax=1e5,label="poles",color='g',linestyle='dotted')
            plt.vlines(roots_analytic.real,ymin=-1e5,ymax=1e5,label="roots",color='c',linestyle='dotted')
            #plt.show()

        dtn_eps = [] 
        plot_collect_c = [lam_sample.real] 
        plot_collect_relerr = [lam_sample.real] 
        header_str_c = "lam "
        for eps_j in eps_perturbs:
            header_str_c += "eps{0} ".format(eps_j)
            c_eps_j = 1.0/sqrt(1+eps_j)
            problem_config["c_1D"] = c_eps_j
            dtn_eps_j = np.array(calc_dtn_nr_layer(output_dtn_on_layer,lam_sample,problem_config),dtype=complex)
            dtn_eps.append(dtn_eps_j)
            plot_collect_c.append(dtn_eps_j.real)
            plot_collect_relerr.append(  np.abs( dtn_eps[0] - dtn_eps_j ) / np.abs( dtn_eps[0] )  )
       
        if False:
            plt.plot(lam_sample.real,np.array(dtn_eps[0]).real,label="numerical",linestyle='dashed')
            plt.legend()
            plt.show()

            for j in range(len(eps_perturbs)):
                plt.plot(lam_sample,np.array(dtn_eps[j]).real,label='eps = {0}'.format(eps_perturbs[j]),linewidth=1)
            plt.legend()
            plt.title("Real part of dtn for perturbed sound speed")
            plt.show()

            for j in [3,5,7]:
                plt.semilogy(lam_sample, plot_collect_relerr[j],label='eps = {0}'.format(eps_perturbs[j]),linewidth=1) 
            plt.vlines(poles_analytic.real,ymin=-1e5,ymax=1e5,label="poles",color='g',linestyle='dotted')
            plt.vlines(roots_analytic.real,ymin=-1e5,ymax=1e5,label="roots",color='c',linestyle='dotted')
            plt.legend()
            plt.show()
        
        if True:
            if use_PML:
                fname_c = "perturbed_sound_speed_PML.dat"
                fname_relerr = "perturbed_sound_speed_PML_relerr.dat"
            else:
                fname_c = "perturbed_sound_speed_Neumann.dat"
                fname_relerr = "perturbed_sound_speed_Neumann_relerr.dat"
            np.savetxt(fname=fname_c,
                X=np.transpose(plot_collect_c),
                header=header_str_c,
                comments='')
            np.savetxt(fname=fname_relerr,
                X=np.transpose(plot_collect_relerr),
                header=header_str_c,
                comments='')
            if not use_PML:
                fname_analytic_poles = "academic_Neumann_poles_analytic.dat"
                fname_analytic_roots = "academic_Neumann_roots_analytic.dat"
                plot_collect_analytic_poles = [poles_analytic.real,poles_analytic.imag]
                plot_collect_analytic_roots = [roots_analytic.real,roots_analytic.imag]
                header_str_analytic_poles = "poles_real poles_imag"
                header_str_analytic_roots = "roots_real roots_imag" 
                np.savetxt(fname=fname_analytic_poles,
                    X=np.transpose(plot_collect_analytic_poles),
                    header=header_str_analytic_poles,
                    comments='')
                np.savetxt(fname=fname_analytic_roots,
                    X=np.transpose(plot_collect_analytic_roots),
                    header=header_str_analytic_roots,
                    comments='')

        problem_config["c_1D"] = c_1D

        return None



    #########################################################################
    # Compute learned IE matrices L_one,L_two for all layers
    #########################################################################

    if use_learnedIE:        
        if learned_IE_precomputed=={}:
            output_learned_poles=False
            if omega == omegas[-1] and not use_PML:
                output_learned_poles = True
            Lones_layer,Ltwos_layer,learned_dtn_layer = Learn_dtn_function(decomp,problem_config,spectral_info,output_learned_poles)
            learned_IE_precomputed["Lones_layer"] = Lones_layer
            learned_IE_precomputed["Ltwos_layer"] = Ltwos_layer
            learned_IE_precomputed["learned_dtn_layer"] = learned_dtn_layer
            return learned_IE_precomputed
        else:
            Lones_layer = learned_IE_precomputed["Lones_layer"] 
            Ltwos_layer = learned_IE_precomputed["Ltwos_layer"]  
            learned_dtn_layer = learned_IE_precomputed["learned_dtn_layer"] 

    #########################################################################
    # Layer matrices
    #########################################################################

    layermat = []
    if sweep_for_background_model: 
        for layer in range(n_layers):
            layermat.append(aX_background.mat)    
    else:
        for layer in range(n_layers):
            layermat.append(aX.mat)    

    #########################################################################
    # Transmission operators of DOSM
    #########################################################################

    tmp_DtN = gfu.vec.CreateVector()
    tmp1_DtN = gfu.vec.CreateVector()

    if use_algebraic_DtN or use_moving_PML:
    
        if use_algebraic_DtN:
            
            AlgebraicDtNs,layer_and_truncated_ext_inv = get_AlgebraicDtN_and_ext_inv(decomp,problem_config) 

            def DtN_from_algebraic(layer,v_in,v_out):
                proj_IF_outer = Projector(BitArrayFromList(decomp.IF2dof[layer],mesh,fes,elements=False),False)
                AlgebraicDtNs[layer].Apply(v_in,v_out)
                v_out.data -= proj_IF_outer * v_out

        if use_moving_PML:
            
            MovingPMLDtNs,layer_and_truncated_ext_inv = get_MovingPMLDtN_and_ext_inv(decomp,problem_config)
            def DtN_from_moving_PML(layer,v_in,v_out):
                proj_IF_outer = Projector(BitArrayFromList(decomp.IF2dof[layer],mesh,fes,elements=False),False)
                MovingPMLDtNs[layer].Apply(v_in,v_out)
                v_out.data -= proj_IF_outer * v_out
    
        def P_outer(layer,v_in,v_out):
            if use_moving_PML:
                DtN_from_moving_PML(layer,v_in,v_out)
            else:
                DtN_from_algebraic(layer,v_in,v_out)
            
        gfu_DOSM_in.vec[:] = 0.0
        gfu_DOSM_out.vec[:] = 0.0

        dosm =  DOSM(decomp,layermat,layer_and_truncated_ext_inv,P_outer,gfu)
        class DOSMPrecond(BaseMatrix):
            
            def IsComplex(self):
                return True

            def Height(self):
                return len(gfu.vec)

            def Width(self):
                return len(gfu.vec)
            
            def Mult(self, x, y):
                gfu_DOSM_in.vec[:] = 0.0
                dosm.ApplyDOSM(x,y,gfu_DOSM_in.vec)
            
        C =  DOSMPrecond()

        apply_DOSM_to_rhs(dosm,f.vec,gfu_DOSM_in,gfu_DOSM_out,udirect,mesh,DOSM_iter=1)
        # Preconditioned GMRES iteration
        niter = run_preconditioned_GMRES(C,aX,f.vec,udirect,gfu_GMRES,mesh)
        return niter

    elif direct_solver_precond:    
        print("Precondition with direct solver inverse of background model")
        invA_background = aX_background.mat.Inverse(freedofs=fes.FreeDofs(), inverse=solver)
        niter = run_preconditioned_GMRES(invA_background,aX,f.vec,udirect,gfu_GMRES,mesh)
        return niter

    elif use_learnedIE : 
        learned_iter_omega = [] 
        for N_fixed in Ns_compute:
            print("")
            print("Use learned IEs of order N = {0}".format(N_fixed))
            
            if sweep_for_background_model: 
                problem_config["c"] = c_background
            LearnedDtNs,layer_and_truncated_ext_inv = get_LearnedDtN_and_ext_inv(decomp,problem_config,N_fixed,Lones_layer,Ltwos_layer) 
            if sweep_for_background_model: 
                problem_config["c"] = c

            def DtN_from_learnedIE(layer,v_in,v_out):        
                LearnedDtNs[layer].Apply(v_in,v_out)
                proj_IF_outer = Projector(BitArrayFromList(decomp.IF2dof[layer],mesh,fes,elements=False),False)
                v_out.data -= proj_IF_outer * v_out

            def P_outer(layer,v_in,v_out):
                DtN_from_learnedIE(layer,v_in,v_out)

            gfu_DOSM_in.vec[:] = 0.0
            gfu_DOSM_out.vec[:] = 0.0

            dosm =  DOSM(decomp,layermat,layer_and_truncated_ext_inv,P_outer,gfu)
            class DOSMPrecond(BaseMatrix):
                
                def IsComplex(self):
                    return True

                def Height(self):
                    return len(gfu.vec)

                def Width(self):
                    return len(gfu.vec)
                
                def Mult(self, x, y):
                    gfu_DOSM_in.vec[:] = 0.0
                    dosm.ApplyDOSM(x,y,gfu_DOSM_in.vec)
                
            C =  DOSMPrecond()

            apply_DOSM_to_rhs(dosm,f.vec,gfu_DOSM_in,gfu_DOSM_out,udirect,mesh,DOSM_iter=1)
            # Preconditioned GMRES iteration
            learned_iter_omega.append(run_preconditioned_GMRES(C,aX,f.vec,udirect,gfu_GMRES,mesh,50000))
        return learned_iter_omega
    
    else:
        raise ValueError('Do not know this precond_type.')

# extracting plots of dtn on layer
SolveProblem(omega=omegas[-1],precond_type="direct",eps_perturb=eps_perturbs[0],use_PML=True,output_dtn_on_layer=8)
SolveProblem(omega=omegas[-1],precond_type="direct",eps_perturb=eps_perturbs[0],use_PML=False,output_dtn_on_layer=8)

# Preconditioned GMRes iterations
np_omegas = np.array(omegas,dtype=int)
layer_n = np.array([int(omega/4) for omega in omegas],dtype=int)

# moving PML

for use_PML,bnd_cond_type in zip([True,False],["PML","Neumann"]):
    header_mPML = "omega nlayers "
    plot_collect_mPML = [np_omegas,layer_n]
    for eps_perturb,eps_str in zip(eps_perturbs,eps_strs):
        header_mPML += "{0} ".format(eps_str)
        moving_PML_iter = []
        for omega in omegas: 
            moving_PML_iter.append(SolveProblem(omega=omega,precond_type="mPML",eps_perturb=eps_perturb,use_PML=use_PML,output_dtn_on_layer=None))
        moving_PML_iter = np.array(moving_PML_iter,dtype=int) 
        plot_collect_mPML.append(moving_PML_iter) 
    fname_mPML ="perturb_iter_mPML_{0}.dat".format(bnd_cond_type)
    np.savetxt(fname=fname_mPML ,
               X=np.transpose(plot_collect_mPML),
               header=header_mPML,
               comments='',
               fmt='%i')


# Preconditioning with direct solver inverse of background model

for use_PML,bnd_cond_type in zip([True,False],["PML","Neumann"]):
    header_b = "omega nlayers "
    plot_collect_b = [np_omegas,layer_n]
    for eps_perturb,eps_str in zip(eps_perturbs,eps_strs):
        header_b += "{0} ".format(eps_str)
        b_iter = []
        for omega in omegas: 
            b_iter.append(SolveProblem(omega=omega,precond_type="DirectSolverBackground",eps_perturb=eps_perturb,use_PML=use_PML,output_dtn_on_layer=None))
        b_iter = np.array(b_iter,dtype=int) 
        plot_collect_b.append(b_iter) 
    fname_b ="perturb_iter_DirectSolverBackground_{0}.dat".format(bnd_cond_type)
    np.savetxt(fname=fname_b ,
               X=np.transpose(plot_collect_b),
               header=header_b,
               comments='',
               fmt='%i')


# learned IE 

precomputed_learned_data = {}
for use_PML,bnd_cond_type in zip([True,False],["PML","Neumann"]): 
    precomputed_learned_omega = []
    for omega in omegas:
        precomputed_learned_omega.append(SolveProblem(omega=omega,precond_type="learned",eps_perturb=eps_perturbs[0],use_PML=use_PML,output_dtn_on_layer=None,sweep_for_background_model=True,learned_IE_precomputed={}))   
    precomputed_learned_data[bnd_cond_type] = precomputed_learned_omega

for sweep_for_background_model, bs_str in zip( [True,False],["sweep4background","sweep4pert"]): 

    for use_PML,bnd_cond_type in zip([True,False],["PML","Neumann"]): 

        iteration_tensor = { } # key: (eps,omega,N) , val = iter
        for eps_perturb,eps_str in zip(eps_perturbs,eps_strs):
            for idx_omega,omega in enumerate(omegas):
                learned_iter = SolveProblem(omega,"learned",eps_perturb,use_PML,None,sweep_for_background_model,precomputed_learned_data[bnd_cond_type][idx_omega])
                for idx_N,N_fixed in enumerate(Ns_compute):
                    iteration_tensor[(eps_perturb,omega,N_fixed)] = learned_iter[idx_N]

        for idx_N,N_fixed in enumerate(Ns_compute): 
            header_learned = "omega nlayers "
            plot_collect_learned = [np_omegas,layer_n]
            for eps_perturb,eps_str in zip(eps_perturbs,eps_strs):
                header_learned += "{0} ".format(eps_str)
                learned_iter = []
                for omega in omegas: 
                    learned_iter.append(iteration_tensor[(eps_perturb,omega,N_fixed)])
                learned_iter = np.array(learned_iter,dtype=int) 
                plot_collect_learned.append(learned_iter) 
            fname_learned ="perturb_iter_learned_{0}_{1}_N{2}.dat".format(bnd_cond_type,bs_str,N_fixed)
            np.savetxt(fname=fname_learned ,
                   X=np.transpose(plot_collect_learned),
                   header=header_learned,
                   comments='',
                   fmt='%i')
