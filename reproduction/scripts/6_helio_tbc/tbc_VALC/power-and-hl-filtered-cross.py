from math import pi
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import sys
import urllib.request 

if ( len(sys.argv) > 1 and sys.argv[1] == "download"):
    print("Downloading power spectra from Gro.data catalogue") 
    file_VALC_meshed, header_VALC_meshed = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/N0S2QJ/4VW7LH","power-spectrum-VALC-meshed.out")
    file_0, header_0 = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/N0S2QJ/ZJJDJD","power-spectrum-N0-VALC.out")
    file_1, header_1 = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/N0S2QJ/KL8NJU","power-spectrum-N1-VALC.out")
    file_2, header_2 = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/N0S2QJ/KAHJKF","power-spectrum-N2-VALC.out")
    file_3, header_3 = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/N0S2QJ/QA3B6H","power-spectrum-N3-VALC.out")
    file_4, header_4 = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/N0S2QJ/KPYHDU","power-spectrum-N4-VALC.out")


L_min = 0
L_max = 1000  # how many DtN numbers to take
L_spacing = 1
L_range = np.arange(L_min,L_max+1,L_spacing)
f_hzs = np.linspace(0,8.3,7200)

index_1mhz = np.where( f_hzs > 1.0)[0][0]
#f_hzs = np.linspace(0.001,8.3,300)
f_hzs = (10**-3)*f_hzs

#freq_spec = "low"
#freq_spec = "high"


plt.rc('legend', fontsize=18)
plt.rc('axes', titlesize=18)
plt.rc('axes', labelsize=18)
plt.rc('xtick', labelsize=14)    
plt.rc('ytick', labelsize=14)

#################################################### 
# load reference power spectrum
####################################################

mdi_data = np.loadtxt("../multiplets-mdi-2001.dat",unpack=False)
hmi_data = np.loadtxt("../multiplets-hmi-2010.dat",unpack=False)
#n_ref = hmi_data[:,0]
print("hmi_data = ", hmi_data)

def select_data(data):
    l_ref = data[:,1]
    nu_ref = data[:,2]
    ref_selected_idx = np.array([],dtype=int)
    N_ref_samples = 12
    for l in [int(l_ref[20]+(l_ref[-100]-l_ref[20])*i/(N_ref_samples-1)) for i in range(N_ref_samples)]:
        idx_l = np.where(l_ref==l)[0]
        ref_selected_idx = np.append(ref_selected_idx,idx_l)
    return l_ref,nu_ref,ref_selected_idx

l_ref,nu_ref,ref_selected_idx = select_data(mdi_data) 
l_ref_hmi,nu_ref_hmi,ref_selected_idx_hmi = select_data(hmi_data) 

print("ref_selected_idx =" , ref_selected_idx)
print("l_ref[ref_selected_idx] = ", l_ref[ref_selected_idx])
print("nu_ref[ref_selected_idx] = ", nu_ref[ref_selected_idx])

#power_spectrum = np.loadtxt('power-spectrum-conj.out',delimiter=',')
#power_spectrum = np.loadtxt('power-spectrum-smoothed-ExtPML-fine.out',delimiter=',')

def scale_power_spectrum(power):

    #return power
    def PI_scal(f):
        result = 1 + ((abs(f)-3.3*1e-3)/ (0.6*1e-3) )**2
        return 1/result 
    
    scaled_power = np.zeros(power.shape) 
    for i  in range(power.shape[0]):
        if abs(f_hzs[i]) > 1e-13:
            scaled_power[i,:] = power[i,:]*0.5*PI_scal(f_hzs[i])/(2*pi*f_hzs[i])
        else:
            scaled_power[i,:] = power[i,:]*0.5*PI_scal(f_hzs[i])
    return scaled_power


power_spectrum = np.loadtxt('power-spectrum-VALC-meshed.out',delimiter=',')
power_spectrum_scaled = scale_power_spectrum(power_spectrum) 
power_spectrum_learned = [] 

for N in range(5):
    fig = plt.figure(figsize=(6,5),constrained_layout=True)
    power_spectrum_learned.append(np.loadtxt('power-spectrum-N{0}-VALC.out'.format(N),delimiter=','))
    power_spectrum_learned_scaled = scale_power_spectrum(power_spectrum_learned[N])
    diff = np.abs(  ( power_spectrum_scaled -  power_spectrum_learned_scaled  ) /  (power_spectrum_scaled) )
    diff[diff < 1e-13] = 1e-13
    print("diff = ", np.linalg.norm(power_spectrum_scaled- power_spectrum_learned_scaled  ))
    im = plt.pcolormesh(L_range,10**3*f_hzs[index_1mhz:],diff[index_1mhz:,:],
                       cmap=plt.get_cmap('jet'),
                       norm=colors.LogNorm( vmin= 1e-12 ,vmax= 1e0  ) )
    plt.xlabel("Harmonic degree $\ell$",labelpad=-2)
    plt.ylabel("Frequency (mHz)")
    #plt.title("Model S + PML")
    #plt.title("N = {0}".format(N))
    #plt.scatter(l_ref[ref_selected_idx], 10**(-3)*nu_ref[ref_selected_idx],marker='+')

    plt.colorbar(im)
    plt.savefig("power-computed-N{0}-VALC.png".format(N),transparent=True)
    plt.show()
    plt.cla()
    plt.clf()
    plt.close()


power_spectrum_scaled = np.abs(power_spectrum_scaled) #remove sign of values like -1e-14 ... 
c_normalize = 1.0
power_spectrum_scaled *= 1/np.max(power_spectrum_scaled) 
plt.figure(figsize=(7,6))
#vmin = 1.5e-3
vmin = 5.0e-4

#vmin = 2.5e-4

im = plt.pcolormesh(L_range,10**3*f_hzs,power_spectrum_scaled,
                   cmap=plt.get_cmap('Reds'),
                   norm=colors.LogNorm( vmin= vmin ,vmax=power_spectrum_scaled.max()   ) )

plt.rc('legend', fontsize=16)
plt.rc('axes', titlesize=16)
plt.rc('axes', labelsize=16)
plt.xlabel("Harmonic degree $\ell$")
plt.ylabel("Frequency (mHz)")
#plt.title("Model S + PML")
#plt.title("Model S + smoothed VAL-C + PML")
plt.scatter(l_ref[ref_selected_idx], 10**(-3)*nu_ref[ref_selected_idx],s=50,marker='+')
plt.colorbar(im)
plt.savefig("power-computed-ref.png",transparent=True)
plt.show()


f_hz = 1e-3*2*pi*np.linspace(0.001,8.3,power_spectrum.shape[0])
data = power_spectrum

from scipy.special import eval_legendre

for freq_spec in ["low","high"]:

    if freq_spec =="low":
        theta_strings = ['14'] 
        theta_idxs = [116]
        plot_param = { "ymin":40, 
                       "ymax":90,
                       "min_scale": 2.0,
                       "xline":14}
    else: 
        theta_strings = ['36'] 
        theta_idxs = [300]
        plot_param = { "ymin":70, 
                       "ymax":110,
                       "min_scale" : 0.0025,
                       "xline":36}

    def compute_TD_diagram(power_spectrum,N_theta=2000,Nt=2**13,str_name="dummy.png",plot_param={}):

        if freq_spec == "low":
            omega_center = 3.0*1e-3*2*pi
            v_center = 125.2 
        else:
            omega_center = 6.5*1e-3*2*pi
            v_center = 250.4

        def F_ell(l):
            if l < 100:
                return 0.5*( 1-np.tanh(0.03*l-3))
            else:
                return 0.0
        
        def PI_scal(f):
            result = 1 + ((abs(f)-3.3*1e-3*2*pi)/ (0.6*1e-3*2*pi) )**2
            return 1/result 
       
        def Gauss_scale(f):
            #return np.exp( -0.5*(abs(f)-omega_center)**2/(0.3*1e-3*2*pi)**2 )
            return np.exp( -0.5*(abs(f)-omega_center)**2/(0.6*1e-3*2*pi)**2 )

        def velocity_filter(f,l): 
            RSun = 6.963e5
            delta_v = 12.3 
            if l != 0:
                #print("l = {0}, f = {2}, vel_filterr = {1}".format(l,np.exp( - 0.5*(abs(f)*2*pi*RSun/l-v_center)**2/delta_v**2),f))
                return np.exp( - 0.5*(abs(f)*RSun/l-v_center)**2/delta_v**2)
            else:
                return 0.0

        filter_freq = Gauss_scale(f_hz)**2  
        plot_collect_vals  = [ 1e3*f_hz/(2*pi) ,filter_freq] 
        filter_freq_header = "omega val"
        filter_freq_fname = "filter-freq-hl-"+freq_spec+".dat"
        np.savetxt(fname =filter_freq_fname , 
                   X = np.transpose(plot_collect_vals),
                   header = filter_freq_header,
                   comments = '')
        
        lls = np.arange(power_spectrum.shape[1])
        thetas = np.linspace(0,2*pi/3,N_theta)

        C_tau = np.zeros(( power_spectrum.shape[0], len(thetas)), dtype="float")
        phase_filter = np.zeros( power_spectrum.shape, dtype="float")
        
        precomputed_factors = np.zeros( power_spectrum.shape) 
        #print("precomputed_factors.shape = ", precomputed_factors.shape ) 
        for i in range(power_spectrum.shape[0]):
            for l in range(power_spectrum.shape[1]):
                precomputed_factors[i,l] =(1/(f_hz[i]+1e-13))*0.5*PI_scal(f_hz[i])*Gauss_scale(f_hz[i])**2*velocity_filter(f_hz[i],l)**2
                phase_filter[i,l] = velocity_filter(f_hz[i],l)**2*Gauss_scale(f_hz[i])**2

        if str_name != "dummy.png":
            fig = plt.figure(figsize=(5,5),constrained_layout=True)
            im = plt.pcolormesh( lls , f_hz*1e3/(2*pi), phase_filter,
                               cmap=plt.get_cmap('Blues'),
                               norm=colors.Normalize(vmin=0 ,vmax= 1.0 ))
            plt.colorbar(im)
            plt.xlabel("$\ell$")
            plt.ylabel(" $\omega / 2 \pi$") 
            plt.savefig("phase-filter-"+freq_spec,transparent=True)
            #plt.show()
            plt.cla()
            plt.clf()
            plt.close()
        
        for j in range(len(thetas)):
            l_legendre = np.array([(l+0.5)*eval_legendre(l,np.cos(thetas[j])) for l in range(power_spectrum.shape[1]) ])
            print("thetas[j] = ", thetas[j])
            for i in range(power_spectrum.shape[0]):
                #li_factor = (1/(f_hz[i]+1e-13))*0.5*PI_scal(f_hz[i])*Gauss_scale(f_hz[i])**2* np.array([ l_leg for l_leg in l_legendre ]) 
                #li_factor = (1/(f_hz[i]+1e-13))*0.5*PI_scal(f_hz[i])*Gauss_scale(f_hz[i])**2* np.array([velocity_filter(f_hz[i],l)**2*l_legendre[l] for l in  range(power_spectrum.shape[1]) ]) 
                li_factor = np.multiply(precomputed_factors[i,:],l_legendre)
                C_tau[i,j] = np.dot(power_spectrum[i,:],li_factor)
        
        #for j in range(len(thetas)):
        #    l_factor = np.array([(l+0.5)*F_ell(l)**2*eval_legendre(l,np.cos(thetas[j])) for l in range(power_spectrum.shape[1]) ])
        #    print("thetas[j] = ", thetas[j])
        #    C_tau[:,j] = (power_spectrum @ l_factor)[:]
        
        ts = np.linspace(0,200*60,Nt)/(2*pi)
        domega = 2*pi*(10**(-3))*(8.3-0.001)/power_spectrum.shape[0]
        dt = ts[-1]/(Nt)
        Ntmp = int(1/(domega*dt))
        print("Ntmp = ", Ntmp)
        C_t = np.zeros((Nt,len(thetas)), dtype="complex")
        for i in range(len(thetas)):
            C_t[:,i] = np.fft.fft(C_tau[:,i],n=Ntmp)[:Nt]
        print("C_t = ", C_t)
        print("np.max(C_t.real) = ", np.max(C_t.real)) 
        print("np.min(C_t.real) = ", np.min(C_t.real)) 
        
        if str_name != "dummy.png":
            fig = plt.figure(figsize=(5,5),constrained_layout=True)
            im = plt.pcolormesh(180*thetas/pi,2*pi*ts/60, C_t.real,
                               cmap=plt.get_cmap('gray'),
                               norm=colors.Normalize( vmin= -plot_param["min_scale"] ,vmax= plot_param["min_scale"] ) )
            #plt.colorbar(im)
            plt.xlabel("Distance (Degrees)")
            plt.ylabel("Time (Minutes)")
            
            plt.axvline(x=plot_param["xline"],color='r',linestyle='--',linewidth=3,ymin= ( plot_param["ymin"]/ (2*pi*ts[-1]/60)), ymax=(plot_param["ymax"] / (2*pi*ts[-1]/60)) )
            #plt.axvline(x=14.0,color='r',linestyle='--',linewidth=3,ymin= (40/ (2*pi*ts[-1]/60)), ymax=(90/ (2*pi*ts[-1]/^60)) )
            plt.savefig(str_name,transparent=True)
            plt.show()
            plt.cla()
            plt.clf()
            plt.close()

        return C_t.real

    Nt = 2**12
    N_theta = 1000
    thetas = np.linspace(0,2*pi/3,N_theta)
    ts = np.linspace(0,200,Nt)

    C_t = compute_TD_diagram(power_spectrum,N_theta=N_theta,Nt=Nt,str_name="time-distance-"+freq_spec+".png",plot_param = plot_param)


    C_t_learned = [] 
    for N in range(5):
        C_t_learned.append(compute_TD_diagram(power_spectrum_learned[N],N_theta=N_theta,Nt=Nt,plot_param=plot_param))
        print("diff = ", np.linalg.norm(C_t-C_t_learned[N]))


    if freq_spec == "high":
        start_t_idx = 1300
    else:
        start_t_idx = 0

    #index_theta = 39
    for index_theta,theta_string in zip(theta_idxs,theta_strings):
        
        normalized_cross = C_t[start_t_idx:,index_theta]/np.max(np.abs(C_t[start_t_idx:,index_theta]))

        plot_collect_vals  = [ts[start_t_idx:],normalized_cross] 
        vals_header = "t ref " 
        fname_vals = "cross-covariance-val-"+freq_spec+"-theta{0}.dat".format(theta_string)
        plot_collect_diff = [ts[start_t_idx:]]  
        diffs_header = "t " 
        fname_diff = "cross-covariance-diff-"+freq_spec+"-theta{0}.dat".format(theta_string)
        plt.plot(ts[start_t_idx:],normalized_cross,label='ref')
        diff_cross = [] 
        for N in range(5):
            vals_header += "N{0} ".format(N)
            diffs_header += "N{0} ".format(N)
            learned_normalized_cross = (C_t_learned[N])[start_t_idx:,index_theta]/np.max(np.abs( (C_t_learned[N])[start_t_idx:,index_theta]))
            plot_collect_vals.append(learned_normalized_cross.real)  
            plt.plot(ts[start_t_idx:],learned_normalized_cross,label='N={0}'.format(N))
            diff_cross.append( np.abs(normalized_cross-learned_normalized_cross) )
        plt.legend() 
        plt.show() 
        plot_collect_diff.extend(diff_cross) 
          
        np.savetxt(fname = fname_vals, 
                   X = np.transpose(plot_collect_vals),
                   header = vals_header,
                   comments = '')
        np.savetxt(fname = fname_diff, 
                   X = np.transpose(plot_collect_diff),
                   header = diffs_header,
                   comments = '')

        for N in range(5):
            plt.semilogy(ts[start_t_idx:], diff_cross[N] ,label='N={0}'.format(N))
        plt.title("absolute error") 
        plt.legend() 
        plt.show() 

