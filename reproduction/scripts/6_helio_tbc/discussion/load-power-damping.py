from math import pi
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import urllib.request 

L_min = 0
#L_max = 1000  # how many DtN numbers to take
L_max = 1000  # how many DtN numbers to take
L_spacing = 1
L_range = np.arange(L_min,L_max+1,L_spacing)
f_hzs = np.linspace(0,8.3,7200)
Ns = range(4)

f_hzs_ref = np.linspace(0,8.3,7200)


file_MDI, header_MDI = urllib.request.urlretrieve("https://test.data.gro.uni-goettingen.de/api/access/datafile/:persistentId?persistentId=hdl:21.T11987/UCVKVX/YV8MGG","mdi_medium_l.np")
power_spectrum_obs = np.load('mdi_medium_l.np')
f_max = 0.0803755*103680/1e6  # according to website

f_hz_obs = 1e-3*np.linspace(0.0,f_max*1e3,power_spectrum_obs.shape[0])

index_1mhz = np.where( f_hzs > 1.0)[0][0]
#f_hzs = np.linspace(0.001,8.3,300)
f_hzs = (10**-3)*f_hzs
f_hzs_ref = (10**-3)*f_hzs_ref

freq_spec = "low"
#freq_spec = "high"

plt.rc('legend', fontsize=18)
plt.rc('axes', titlesize=18)
plt.rc('axes', labelsize=18)
plt.rc('xtick', labelsize=14)    
plt.rc('ytick', labelsize=14)

plt.rcParams['savefig.dpi'] = 300
plt.rcParams["figure.dpi"] = 100


def scale_power_spectrum(power,f_hzs):

    def PI_scal(f):
        result = 1 + ((abs(f)-3.3*1e-3)/ (0.6*1e-3) )**2
        return 1/result 
     
    scaled_power = np.zeros(power.shape) 
    for i  in range(power.shape[0]):
        if f_hzs[i] != 0:
            scaled_power[i,:] = power[i,:]*0.5*PI_scal(f_hzs[i])/(2*pi*f_hzs[i])
    
    return scaled_power

def apply_Granulation_filter(power,f_hz):    
    
    def Granu_filter(f):
        #print("f = {0}, filtered = {1}".format(f,np.tanh(3*f/0.0015)))
        return np.tanh(3*f/0.0015)
        #return np.tanh(0.2*f/0.0015)

    filtered_power = np.zeros(power.shape) 
    for i in range(power.shape[0]):
        filtered_power[i,:] = power[i,:]*Granu_filter(f_hz[i])
    
    return filtered_power

# load power spectra and apply scaling:
file_atmo, header_atmo = urllib.request.urlretrieve("https://test.data.gro.uni-goettingen.de/api/access/datafile/:persistentId?persistentId=hdl:21.T11987/UCVKVX/OWKZL8","power-spectrum-Atmo-whitw-powerlaw.out")
power_spectrum_power_damping = np.loadtxt('power-spectrum-Atmo-whitw-powerlaw.out',delimiter=',')

power_spectrum_opt_damping  = np.loadtxt('power-spectrum-Atmo-whitw-opt-damping.out',delimiter=',')

def post_process_power(ps,f_hzs):
    ps = np.abs(ps)
    ps = scale_power_spectrum(ps,f_hzs)
    return ps

power_spectrum_power_damping = post_process_power(power_spectrum_power_damping,f_hzs)
power_spectrum_opt_damping = post_process_power(power_spectrum_opt_damping,f_hzs_ref)

power_spectrum_obs = np.abs(power_spectrum_obs)/1e5 
power_spectrum_obs =  apply_Granulation_filter(power_spectrum_obs,2*pi*f_hz_obs)

power_spectrum_opt_daping = apply_Granulation_filter(power_spectrum_opt_damping,2*pi*f_hzs)

power_spectrum_power_damping = apply_Granulation_filter(power_spectrum_power_damping,2*pi*f_hzs_ref)

i0_obs = np.min( np.nonzero(f_hz_obs > 0.005)[0] ) 
i0 = np.min( np.nonzero(f_hzs > 0.005)[0] ) 
i1_obs = np.min( np.nonzero(f_hz_obs > 0.0049)[0] ) 
i1 = np.min( np.nonzero(f_hzs > 0.0049)[0] ) 

i2 = np.min( np.nonzero(f_hzs_ref > 0.005)[0] ) 

small_freqs = f_hzs[:i0]
small_freqs_ref = f_hzs_ref[:i2] 
small_freqs_obs = f_hz_obs[:i0_obs]
high_freqs_obs = f_hz_obs[i1_obs:]
high_freqs = f_hzs[i1:]

step_low = 15
step_high = 100
small_freqs_obs = small_freqs_obs[::step_low]
high_freqs_obs = high_freqs_obs[::step_high]
for l in [50,300]:
    print("l =",l)

    plot_collect = [] 
    plot_collect.append(1e3*small_freqs)
    header_comp = "f "
    
    p_normal_opt = power_spectrum_opt_damping[:i0,l]/np.max( power_spectrum_opt_damping[:i0,l] ) 
    plot_collect.append(p_normal_opt)
    header_comp += "opt "

    p_normal_power_damping = power_spectrum_power_damping[:i2,l]/np.max( power_spectrum_power_damping[:i2,l] ) 
    
    plot_collect.append(p_normal_power_damping)
    header_comp += "plaw "
     
    if l < 300:
        p_obs = power_spectrum_obs[:i0_obs,l]
        p_obs = p_obs[::step_low]
        p_normal_obs = p_obs / np.max(p_obs) 
    
    plt.plot(10**3*small_freqs,p_normal_opt,label="opt-damping" )
    plt.plot(10**3*small_freqs_ref,p_normal_power_damping,label="power-damping" )
    if l < 300:
        plt.plot(10**3*small_freqs_obs,p_normal_obs,label="MDI" )
    plt.xlabel("Frequency (mHz)")
    plt.legend()
    plt.show()

    fname  = "power-f-low-l{0}-damping-comp.dat".format(l) 
    np.savetxt(fname = fname, 
               X = np.transpose(plot_collect),
               header = header_comp,
               comments = '')
