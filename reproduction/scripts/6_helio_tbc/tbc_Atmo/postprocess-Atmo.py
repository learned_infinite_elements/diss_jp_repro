from math import pi
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import sys
import urllib.request 


if ( len(sys.argv) > 1 and sys.argv[1] == "download"):
    print("Downloading power spectra from Gro.data catalogue") 
    #file_MDI, header_MDI = urllib.request.urlretrieve("https://test.data.gro.uni-goettingen.de/api/access/datafile/:persistentId?persistentId=hdl:21.T11987/UCVKVX/YV8MGG","mdi_medium_l.np")
    file_atmo, header_atmo = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/WX473B/HRHCEA","power-spectrum-Atmo-whitw-comp.out")
    file_SHF1a, header_SHF1A = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/WX473B/TXOIKQ","power-spectrum-SHF1a.out")
    file_ARBC1, header_ARBC1 = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/WX473B/ONZI1A","power-spectrum-ARBC1.out")
    file_nonlocal, header_nonlocal = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/WX473B/NIVBON","power-spectrum-nonlocal.out")
    # learned 
    file_0,header_0 = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/WX473B/NC4YM4","power-spectrum-N0-Atmo.out")
    file_1,header_1 = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/WX473B/X6WLB2" ,"power-spectrum-N1-Atmo.out")
    file_2,header_2 = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/WX473B/BBDMTS" ,"power-spectrum-N2-Atmo.out")
    file_3,header_3 = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/WX473B/QZSRQY"  ,"power-spectrum-N3-Atmo.out")
    file_4,header_4 = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/WX473B/3GMMKL"  ,"power-spectrum-N4-Atmo.out")

    print("Downloading dtn numbers from Gro.data catalogue")
    file_atmo, header_atmo = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/WX473B/BOO2ML","dtn_nr_Whitw-comp.out") 
    file_SHF1a, header_SHF1a = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/WX473B/PAD4YQ","dtn_nr_S-HF-1a.out")
    file_ARBC1, header_ARBC1 = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/WX473B/OYE3QT","dtn_nr_ARBC1.out")
    file_nonlocal, header_nonlocal = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/WX473B/GFDLYD","dtn_nr_nonlocal.out")
    # learned 
    file_0,header_0 = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/WX473B/DK2ZVC","dtn_nr_learned_N0-Atmo.out")
    file_1,header_1 = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/WX473B/MXCKO8","dtn_nr_learned_N1-Atmo.out")
    file_2,header_2 = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/WX473B/G1HPJU","dtn_nr_learned_N2-Atmo.out")
    file_3,header_3 = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/WX473B/G4EPNJ","dtn_nr_learned_N3-Atmo.out")
    file_4,header_4 = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/WX473B/AGUGBY","dtn_nr_learned_N4-Atmo.out")
    print("done")



L_min = 0
L_max = 1000  # how many DtN numbers to take
#L_max = 500  # how many DtN numbers to take
L_spacing = 1
L_range = np.arange(L_min,L_max+1,L_spacing)

#f_hzs = np.linspace(0.001,8.3,1000)
f_hzs = np.linspace(0,12.0,2400)
Ns = range(5)

#power_spectrum_obs = np.load('mdi_medium_l.np')
#f_max = 0.0803755*103680/1e6  # according to website
#f_hz_obs = 1e-3*2*pi*np.linspace(0.0,f_max*1e3,power_spectrum_obs.shape[0])

#f_hz_obs = 1e-3*np.linspace(0.0,f_max*1e3,power_spectrum_obs.shape[0])

index_1mhz = np.where( f_hzs > 1.0)[0][0]


#f_hzs = np.linspace(0.001,8.3,300)
f_hzs = (10**-3)*f_hzs

#freq_spec = "low"
#freq_spec = "high"

plt.rc('legend', fontsize=18)
plt.rc('axes', titlesize=18)
plt.rc('axes', labelsize=18)
plt.rc('xtick', labelsize=14)    
plt.rc('ytick', labelsize=14)

plt.rcParams['savefig.dpi'] = 300
plt.rcParams["figure.dpi"] = 100

#################################################### 
# load reference power spectrum
####################################################
'''
mdi_data = np.loadtxt("../multiplets-mdi-2001.dat",unpack=False)
hmi_data = np.loadtxt("../multiplets-hmi-2010.dat",unpack=False)
#n_ref = hmi_data[:,0]
print("hmi_data = ", hmi_data)

def select_data(data):
    l_ref = data[:,1]
    nu_ref = data[:,2]
    ref_selected_idx = np.array([],dtype=int)
    N_ref_samples = 12
    for l in [int(l_ref[20]+(l_ref[-100]-l_ref[20])*i/(N_ref_samples-1)) for i in range(N_ref_samples)]:
        idx_l = np.where(l_ref==l)[0]
        ref_selected_idx = np.append(ref_selected_idx,idx_l)
    return l_ref,nu_ref,ref_selected_idx

l_ref,nu_ref,ref_selected_idx = select_data(mdi_data) 
l_ref_hmi,nu_ref_hmi,ref_selected_idx_hmi = select_data(hmi_data) 

print("ref_selected_idx =" , ref_selected_idx)
print("l_ref[ref_selected_idx] = ", l_ref[ref_selected_idx])
print("nu_ref[ref_selected_idx] = ", nu_ref[ref_selected_idx])
'''


def scale_power_spectrum(power,f_hzs):

    def PI_scal(f):
        result = 1 + ((abs(f)-3.3*1e-3)/ (0.6*1e-3) )**2
        #return 1.0
        return 1/result 

    #return power
    #def PI_scal(f):
    #    result = 1 + ((abs(f)-3.3*1e-3)/ (0.6*1e-3) )**2
    #    return 1/result 
    
    scaled_power = np.zeros(power.shape) 
    for i  in range(power.shape[0]):
        if abs(f_hzs[i]) > 1e-10:
            scaled_power[i,:] = power[i,:]*0.5*PI_scal(f_hzs[i])/(2*pi*f_hzs[i])
        #scaled_power[i,:] = power[i,:]*0.5*PI_scal(f_hzs[i])
    
    return scaled_power



def apply_Granulation_filter(power,f_hz):    
    
    def Granu_filter(f):
        #print("f = {0}, filtered = {1}".format(f,np.tanh(3*f/0.0015)))
        return np.tanh(3*f/0.0015)

    filtered_power = np.zeros(power.shape) 
    for i in range(power.shape[0]):
        filtered_power[i,:] = power[i,:]*Granu_filter(f_hz[i])
    
    return filtered_power



# load power spectra and apply scaling:
power_spectrum_Atmo = np.loadtxt('power-spectrum-Atmo-whitw-comp.out',delimiter=',')
power_spectrum_nonlocal = np.loadtxt('power-spectrum-nonlocal.out',delimiter=',')
power_spectrum_Atmo_learned = [np.loadtxt('power-spectrum-N{0}-Atmo.out'.format(N),delimiter=',') for N in Ns]  
power_spectrum_SHF1a = np.loadtxt('power-spectrum-SHF1a.out',delimiter=',')
power_spectrum_ARBC1 = np.loadtxt('power-spectrum-ARBC1.out',delimiter=',')

def post_process_power(ps):
    ps = np.abs(ps)
    #ps *= 1/np.max(ps)
    ps = scale_power_spectrum(ps,f_hzs)
    #ps = apply_Granulation_filter(ps,2*pi*f_hzs)
    #print("max =", np.max(ps))
    return ps
    #ps *= 1/np.max(ps)
    return ps

power_spectrum_Atmo = post_process_power(power_spectrum_Atmo)
power_spectrum_nonlocal = post_process_power(power_spectrum_nonlocal)
power_spectrum_ARBC1 = post_process_power(power_spectrum_ARBC1)

for N in Ns:
    power_spectrum_Atmo_learned[N] =  post_process_power(power_spectrum_Atmo_learned[N])
power_spectrum_SHF1a = post_process_power(power_spectrum_SHF1a)

# First power-spectra with Atmo condition
vmin = 1e-7
cnt = 1
for ps,p_str,leg_info in zip(  [power_spectrum_nonlocal,power_spectrum_SHF1a,power_spectrum_ARBC1] + power_spectrum_Atmo_learned 
                       , ["nonlocal","SHF1a","ARBC1"] + ["N{0}".format(N) for N in Ns] 
                       , [ (True,False),(False,False),(False,True), (True,False),(False,False),(False,False),(False,False),(False,True)]
                       ):
    print(p_str)
    fig = plt.figure(figsize=(6,5),constrained_layout=True)
    #power_spectrum_learned.append(np.loadtxt('power-spectrum-N{0}.out'.format(N),delimiter=','))
    diff = np.abs(  ( power_spectrum_Atmo -  ps ) /  (power_spectrum_Atmo) )
    #diff = np.abs(  power_spectrum_Atmo -  ps )
    #diff = np.abs(  ps  )
    print("abs diff =" , np.linalg.norm( power_spectrum_Atmo -  ps)) 
    #print("diff = ", np.linalg.norm(diff))
    diff[ diff < vmin] = vmin
    im = plt.pcolormesh(L_range,10**3*f_hzs[index_1mhz:],diff[index_1mhz:,:],
                       cmap=plt.get_cmap('jet'),
                       norm=colors.LogNorm( vmin= vmin ,vmax= 1e-1  ) )
    #im = plt.pcolormesh(L_range,10**3*f_hzs[index_1mhz:],ps[index_1mhz:,:],
    #                   cmap=plt.get_cmap('jet'),
    #                   norm=colors.LogNorm( vmin= 1e-6 ,vmax= 1e-0  ) )
    
    plt.xlabel("Harmonic degree $\ell$",labelpad=-2)
    if leg_info[0]:
        plt.ylabel("Frequency (mHz)")
    if leg_info[1]:
        plt.colorbar(im)
    plt.savefig("power-Atmo-"+p_str+".png",transparent=True)
    plt.show()
    plt.cla()
    plt.clf()
    plt.close()
    cnt += 1

power_spectrum_Atmo = apply_Granulation_filter(power_spectrum_Atmo,2*pi*f_hzs)
power_spectrum_nonlocal = apply_Granulation_filter(power_spectrum_nonlocal,2*pi*f_hzs)
power_spectrum_SHF1a = apply_Granulation_filter(power_spectrum_SHF1a,2*pi*f_hzs)
for N in Ns:
    power_spectrum_Atmo_learned[N] = apply_Granulation_filter(power_spectrum_Atmo_learned[N] ,2*pi*f_hzs)

# compute cross-covariance
N_theta = 1000
Nt = 2**12
thetas = np.linspace(0,pi,N_theta)

idxx = [ np.argmin(np.abs(thetas - i*pi/6 ) ) for i in range(1,4)]
idxx_label = ["30deg ","60deg ","90deg "]
max_vals = [0.95,0.75,0.8]
max_vals = [0.95,0.95,0.95]
#max_vals = [0.1,0.1,0.1]
print("thetas[idxx[0]] ={0}, pi/6 = {1}".format(thetas[idxx[0]],pi/6))

ts = np.linspace(0,300,Nt)

tss =  60*ts

from scipy.special import eval_legendre
from scipy.integrate import simps 


dtn_nr_whitw = np.loadtxt('dtn_nr_Whitw-comp.out',dtype="complex",delimiter=',')
dtn_nr_nonlocal = np.loadtxt('dtn_nr_nonlocal.out',dtype="complex",delimiter=',')
dtn_nr_SHF1a = np.loadtxt('dtn_nr_S-HF-1a.out',dtype="complex",delimiter=',')
dtn_nr_ARBC1 = np.loadtxt('dtn_nr_ARBC1.out',dtype="complex",delimiter=',')
dtn_nr_learned = []
for N in range(5):
        dtn_nr_learned.append( np.loadtxt('dtn_nr_learned_N{0}-Atmo.out'.format(N),dtype="complex",delimiter=',') )

freq_choice = [3.0,5.30,6.50]
freq_string =  ["3mHz","5p3mHz","6p5mHz"]
freq_idx = [ np.argmin( np.abs(1e3*f_hzs - nu_f) ) for nu_f in freq_choice ] 

for idx_f,idx_s in zip(freq_idx,freq_string):
    print("idx_f = {0}, f_hzs[idx_f] = {1}".format(idx_f,f_hzs[idx_f])) 
    
    L_plot = np.arange(len(dtn_nr_whitw[idx_f,:]))
    # real part 
    
    plt.plot(L_plot , dtn_nr_whitw[idx_f,:].real,label="exact")
    plt.plot(L_plot , dtn_nr_nonlocal[idx_f,:].real,label="nonlocal")
    plt.plot(L_plot , dtn_nr_SHF1a[idx_f,:].real,label="S-HF-1a")

    plot_collect_vals_real  = [L_plot,dtn_nr_whitw[idx_f,:].real,dtn_nr_nonlocal[idx_f,:].real,dtn_nr_SHF1a[idx_f,:].real] 
    vals_header = "l ref nonlocal SHF1a " 
    fname_vals_real = "dtn-Atmo-real-"+idx_s+".dat".format(idx_s)
    fname_vals_imag = "dtn-Atmo-imag-"+idx_s+".dat".format(idx_s)
    fname_diff = "dtn-Atmo-relerr-"+idx_s+".dat".format(idx_s)
    
    for N in range(5):
        plot_collect_vals_real.append(dtn_nr_learned[N][idx_f,:].real)
        vals_header += "N{0} ".format(N)
        plt.plot(L_plot, dtn_nr_learned[N][idx_f,:].real,label="N={0}".format(N))
    plt.legend()
    plt.show()

    np.savetxt(fname = fname_vals_real, 
               X = np.transpose(plot_collect_vals_real),
               header = vals_header,
               comments = '')

    # imaginary part
   
    plt.plot(L_plot , dtn_nr_whitw[idx_f,:].imag,label="exact")
    plt.plot(L_plot , dtn_nr_nonlocal[idx_f,:].imag,label="nonlocal")
    plt.plot(L_plot , dtn_nr_SHF1a[idx_f,:].imag,label="S-HF-1a")

    plot_collect_vals_imag  = [L_plot,dtn_nr_whitw[idx_f,:].imag,dtn_nr_nonlocal[idx_f,:].imag,dtn_nr_SHF1a[idx_f,:].imag]  

    for N in range(5):
        plot_collect_vals_imag.append(dtn_nr_learned[N][idx_f,:].imag)
        plt.plot(L_plot, dtn_nr_learned[N][idx_f,:].imag,label="N={0}".format(N))
    plt.legend()
    plt.show()
    
    np.savetxt(fname = fname_vals_imag, 
               X = np.transpose(plot_collect_vals_imag),
               header = vals_header,
               comments = '')
    
    # relative error 

    header_diff = "l nonlocal SHF1a " 
    diff_nonlocal =  np.abs( dtn_nr_whitw[idx_f,:] - dtn_nr_nonlocal[idx_f,:]) / np.abs(dtn_nr_whitw[idx_f,:])
    plt.semilogy(L_plot, diff_nonlocal,label="nonlocal")
    diff_SHF1a =  np.abs( dtn_nr_whitw[idx_f,:] - dtn_nr_SHF1a[idx_f,:]) / np.abs(dtn_nr_whitw[idx_f,:])
    plt.semilogy(L_plot , diff_SHF1a ,label="S-HF-1a")

    plot_collect_diff  = [L_plot,diff_nonlocal,diff_SHF1a]  
    for N in range(5):
        header_diff += "N{0} ".format(N)
        diff_learned =  np.abs( dtn_nr_whitw[idx_f,:] - dtn_nr_learned[N][idx_f,:]) / np.abs(dtn_nr_whitw[idx_f,:])
        plot_collect_diff.append(diff_learned)
        plt.semilogy(L_plot, diff_learned,label="N={0}".format(N))
    plt.legend()
    plt.show()

    np.savetxt(fname = fname_diff, 
               X = np.transpose(plot_collect_diff),
               header = header_diff,
               comments = '')


f_hz = 2*pi*f_hzs
from scipy.special import eval_legendre


for freq_spec in ["high","low"]:

    if freq_spec =="low":
        theta_strings = ['14','28'] 
        theta_choice = [14,28]
        #theta_idxs = [116]
        plot_param = { "ymin1":40,
                       "ymax1":90, 
                       "ymin2":105,
                       "ymax2":150,
                       "min_scale": 2e-1,
                       "xline1":14,
                       "xline2":28, 
                       "str_name":"time-distance-Atmo-low-filter"  }
    else:
        theta_strings = ['36','72']
        theta_choice = [36,72]
        plot_param = { "ymin1":70,
                       "ymax1":110, 
                       "ymin2":150,
                       "ymax2":190,
                       "min_scale": 1e-2,
                       "xline1":36,
                       "xline2":72, 
                       "str_name":"time-distance-Atmo-high-filter"  }

    def compute_TD_diagram(power_spectrum,N_theta=2000,Nt=2**13,plot_param={}):

        if freq_spec == "low":
            omega_center = 3.0*1e-3*2*pi
            v_center = 125.2 
        else:
            omega_center = 6.5*1e-3*2*pi
            v_center = 250.4

        def F_ell(l):
            if l < 100:
                return 0.5*( 1-np.tanh(0.03*l-3))
            else:
                return 0.0
        
        def PI_scal(f):
            result = 1 + ((abs(f)-3.3*1e-3*2*pi)/ (0.6*1e-3*2*pi) )**2
            return 1/result 
       
        def Gauss_scale(f):
            #return np.exp( -0.5*(abs(f)-omega_center)**2/(0.3*1e-3*2*pi)**2 )
            return np.exp( -0.5*(abs(f)-omega_center)**2/(0.6*1e-3*2*pi)**2 )

        def velocity_filter(f,l): 
            RSun = 6.963e5
            delta_v = 12.3 
            if l != 0:
                #print("l = {0}, f = {2}, vel_filterr = {1}".format(l,np.exp( - 0.5*(abs(f)*2*pi*RSun/l-v_center)**2/delta_v**2),f))
                return np.exp( - 0.5*(abs(f)*RSun/l-v_center)**2/delta_v**2)
            else:
                return 0.0

        thetas = np.linspace(0,2*pi/3,N_theta)

        C_tau = np.zeros(( power_spectrum.shape[0], len(thetas)), dtype="float")
        
        precomputed_factors = np.zeros( power_spectrum.shape) 
        #print("precomputed_factors.shape = ", precomputed_factors.shape ) 
        for i in range(power_spectrum.shape[0]):
            for l in range(power_spectrum.shape[1]):
                precomputed_factors[i,l] = Gauss_scale(f_hz[i])**2*velocity_filter(f_hz[i],l)**2
                #precomputed_factors[i,l] =(1/(f_hz[i]+1e-13))*0.5*PI_scal(f_hz[i])*Gauss_scale(f_hz[i])**2*velocity_filter(f_hz[i],l)**2

        for j in range(len(thetas)):
            l_legendre = np.array([(l+0.5)*eval_legendre(l,np.cos(thetas[j])) for l in range(power_spectrum.shape[1]) ])
            print("thetas[j] = ", thetas[j])
            for i in range(power_spectrum.shape[0]):
                #li_factor = (1/(f_hz[i]+1e-13))*0.5*PI_scal(f_hz[i])*Gauss_scale(f_hz[i])**2* np.array([ l_leg for l_leg in l_legendre ]) 
                #li_factor = (1/(f_hz[i]+1e-13))*0.5*PI_scal(f_hz[i])*Gauss_scale(f_hz[i])**2* np.array([velocity_filter(f_hz[i],l)**2*l_legendre[l] for l in  range(power_spectrum.shape[1]) ]) 
                li_factor = np.multiply(precomputed_factors[i,:],l_legendre)
                C_tau[i,j] = np.dot(power_spectrum[i,:],li_factor)
        
        #for j in range(len(thetas)):
        #    l_factor = np.array([(l+0.5)*F_ell(l)**2*eval_legendre(l,np.cos(thetas[j])) for l in range(power_spectrum.shape[1]) ])
        #    print("thetas[j] = ", thetas[j])
        #    C_tau[:,j] = (power_spectrum @ l_factor)[:]
        
        ts = np.linspace(0,200*60,Nt)/(2*pi)
        #domega = 2*pi*(10**(-3))*(8.3-0.001)/power_spectrum.shape[0]
        domega = 2*pi*(10**(-3))*(12.0)/power_spectrum.shape[0]
        #domega =  f_hzs[-1]/power_spectrum.shape[0]
        dt = ts[-1]/(Nt)
        Ntmp = int(1/(domega*dt))
        print("Ntmp = ", Ntmp)
        C_t = np.zeros((Nt,len(thetas)), dtype="complex")
        for i in range(len(thetas)):
            C_t[:,i] = np.fft.fft(C_tau[:,i],n=Ntmp)[:Nt]
        print("C_t = ", C_t)
        print("np.max(C_t.real) = ", np.max(C_t.real)) 
        print("np.min(C_t.real) = ", np.min(C_t.real)) 
        if plot_param != {}:
            fig = plt.figure(figsize=(5,5),constrained_layout=True)
            im = plt.pcolormesh(180*thetas/pi,2*pi*ts/60, C_t.real,
                               cmap=plt.get_cmap('gray'),
                               norm=colors.Normalize( vmin= -plot_param["min_scale"] ,vmax= plot_param["min_scale"]   ) )
            plt.axvline(x=plot_param["xline1"],color='r',linestyle='--',linewidth=3,ymin= ( plot_param["ymin1"]/ (2*pi*ts[-1]/60)), ymax=(plot_param["ymax1"] / (2*pi*ts[-1]/60)) )
            plt.axvline(x=plot_param["xline2"],color='r',linestyle='--',linewidth=3,ymin= ( plot_param["ymin2"]/ (2*pi*ts[-1]/60)), ymax=(plot_param["ymax2"] / (2*pi*ts[-1]/60)) )
            #plt.colorbar(im)
            plt.savefig(plot_param["str_name"],transparent=True)
            plt.xlabel("Distance (Degrees)")
            plt.ylabel("Time (Minutes)")
            plt.show()
        
        return C_t.real

    Nt = 2**12
    N_theta = 1000
    thetas = np.linspace(0,2*pi/3,N_theta)
    ts = np.linspace(0,200,Nt)

    #C_t_learned.append(compute_TD_diagram(power_spectrum_Atmo,N_theta=N_theta,Nt=Nt))

    C_t = compute_TD_diagram(power_spectrum_Atmo ,N_theta=N_theta,Nt=Nt,plot_param=plot_param)
    C_t_SHF1a = compute_TD_diagram(power_spectrum_SHF1a ,N_theta=N_theta,Nt=Nt)
    C_t_nonlocal = compute_TD_diagram(power_spectrum_nonlocal,N_theta=N_theta,Nt=Nt)


    C_t_learned = [] 
    for N in range(5):
        C_t_learned.append(compute_TD_diagram(power_spectrum_Atmo_learned[N],N_theta=N_theta,Nt=Nt))
        print("diff = ", np.linalg.norm(C_t-C_t_learned[N]))

    def theta_2_idx(theta_i):
        return np.argmin( np.abs( (180/pi)*thetas -theta_i) )

    theta_idxs = [ theta_2_idx(theta_i) for theta_i in theta_choice ]

    for index_theta,theta_string in zip(theta_idxs,theta_strings):
        
        normalized_cross = C_t[:,index_theta]/np.max(np.abs(C_t[:,index_theta]))
        normalized_cross_SHF1a = C_t_SHF1a[:,index_theta]/np.max(np.abs(C_t_SHF1a[:,index_theta]))
        normalized_cross_nonlocal = C_t_nonlocal[:,index_theta]/np.max(np.abs(C_t_nonlocal[:,index_theta]))
        plot_collect_vals  = [ts,normalized_cross,normalized_cross_nonlocal,normalized_cross_SHF1a] 
        vals_header = "t ref nonlocal SHF1a " 
        fname_vals = "cross-covariance-val-"+freq_spec+"-theta{0}.dat".format(theta_string)
        plot_collect_diff = [ts]  
        diffs_header = "t nonlocal SHF1a " 
        fname_diff = "cross-covariance-diff-"+freq_spec+"-theta{0}.dat".format(theta_string)
        plt.plot(ts,normalized_cross,label='ref')
        plt.show()
        plt.semilogy(ts,np.abs(normalized_cross-normalized_cross_SHF1a),label='SHF1a') 
        plt.semilogy(ts,np.abs(normalized_cross-normalized_cross_nonlocal),label='nonlocal')
        plot_collect_diff.append(np.abs(normalized_cross-normalized_cross_nonlocal))
        plot_collect_diff.append(np.abs(normalized_cross-normalized_cross_SHF1a))
        #plt.show()
        #input("")
        
        diff_cross = []
        for N in range(5):
            vals_header += "N{0} ".format(N)
            diffs_header += "N{0} ".format(N)
            learned_normalized_cross = (C_t_learned[N])[:,index_theta]/np.max(np.abs( (C_t_learned[N])[:,index_theta]))
            plot_collect_vals.append(learned_normalized_cross.real)  
            #plt.plot(ts,learned_normalized_cross,label='N={0}'.format(N))
            diff_cross.append( np.abs(normalized_cross-learned_normalized_cross) )
        
        plot_collect_diff += diff_cross
          
        np.savetxt(fname = fname_vals, 
                   X = np.transpose(plot_collect_vals),
                   header = vals_header,
                   comments = '')
        np.savetxt(fname = fname_diff, 
                   X = np.transpose(plot_collect_diff),
                   header = diffs_header,
                   comments = '')

        for N in range(5):
            plt.semilogy(ts, diff_cross[N] ,label='N={0}'.format(N))
        plt.title("absolute error") 
        plt.legend() 
        plt.show() 
        
