import ceres_dtn as opt
from ngsolve import *
from netgen.geom2d import SplineGeometry
from ngs_refsol import PlaneWaveHomSolution
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from scipy.special import hankel1,h1vp
import sys
#import mpmath as mp

a = 1.0 # coupling radius
R = 1/2 # radius of inner boundary
Lmax = 40 # number of considered modes for learning
#Nmax = 9 # maximal number of infinite element dofs

Nmax = 10
#Nmax = 4

Ns = list(range(Nmax))

orders = [4,6,8,10] # order of FEM
omegas = [4,8,16,32] # wavenumbers 

Nomega_input = int(sys.argv[1])
if Nomega_input not in [8,16]:
    print("You have chosen Nomega = {0} samples. Note that the plots in the thesis are for Nomega = 8, and Nomega = 16".format(Nomega_input))
#Nomega = 16
#Nomega = 8
Nomega = Nomega_input

omega_sample = np.linspace(8,32,Nomega) 
g_omega = omega_sample**2
g_omega = np.array(g_omega,dtype="complex")

omega_test = np.linspace(2,38,108)
g_omega_test = omega_test**2
g_omega_test = np.array(g_omega_test,dtype="complex")

print("omega_sample = ", omega_sample) 

A1_IEs = {} # for storing results
A2_IEs = {} 
B_IEs  = {}

#ansatz = "full"
ansatz = "symmetric"
#ansatz = "symmetric_A2real"
flags = {"max_num_iterations":5000,
         "use_nonmonotonic_steps":True,
         "minimizer_progress_to_stdout":False,
         "num_threads":4,
         "report_level":"Brief",
         "function_tolerance":1e-13,
         "parameter_tolerance":1e-13,
         "gradient_tolerance":1e-13,
         "check_gradients":False,
         }

def new_initial_guess(A1_old,A2_old,B_old,ansatz):
    eps_scale = 1e-2
    N = A1_old.shape[0]
    if N >= 3:
        eps_scale = 1e-3
    if N >= 8:
        eps_scale = 5e-4
    A1_guess = np.zeros((N+1,N+1),dtype='complex')
    A2_guess = np.zeros((N+1,N+1),dtype='complex')
    B_guess = np.zeros((N+1,N+1),dtype='complex')
    A1_guess = eps_scale*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
    A2_guess = 1e-1*eps_scale*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
    B_guess = 1e-1*eps_scale*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
    A1_guess[:N,:N] = A1_old[:]
    A2_guess[:N,:N] = A2_old[:]
    B_guess[:N,:N] = B_old[:]
    A1_guess[N,N] = 1.0
    return A1_guess,A2_guess,B_guess

def dtn_ref(omega,nu):
    return -omega*h1vp(nu,omega*a) / hankel1(nu,omega*a)
    #return -omega*0.5*complex( (mp.hankel1(nu-1,omega*a) - mp.hankel1(nu+1,omega*a) ) /  mp.hankel1(nu,omega*a) )


lam = np.array([(l/a)**2 for l in range(Lmax)]) 

dtn_omega = []
dtn_nr = np.zeros((Lmax,Nomega),dtype=complex) 
weights = np.zeros((Lmax,Nomega),dtype=float) 
weights_test = np.zeros((len(lam),len(omega_test)),dtype=float) 

dtn_nr_test = np.zeros((len(lam),len(omega_test)),dtype=complex) 

def get_weight(l,omega):
    #return   (10**2*abs(complex(mp.hankel1(l,omega*a) / mp.hankel1(l,omega*min(R*omega/16,R)) )))**2 
    #return   (10**2*abs(complex(hankel1(l,omega*a) / hankel1(l,omega*min(R*omega/16,R)) )))**2 
    return   (10**2*abs(complex(hankel1(l,omega*a) / hankel1(l,omega*R))))**2 

for i in range(len(omega_sample)):
    for l in range(len(lam)):
        dtn_nr[l,i] = dtn_ref(omega_sample[i],sqrt(lam[l])*a)
        weights[l,i] = get_weight(l,omega_sample[i]) 

for i in range(len(omega_test)):
    for l in range(len(lam)):
        dtn_nr_test[l,i] = dtn_ref(omega_test[i],sqrt(lam[l])*a)
        weights_test[l,i] = get_weight(l,omega_test[i]) 

print("weights =" , weights)

l_dtn = opt.learned_dtn_interval(lam,g_omega,dtn_nr,weights)


# perform minimization for each wavenumber 
A1_IE = []
A2_IE = []
B_IE = [] 
relative_residuals = []

sup_errors = {}  

np.random.seed(seed=123)

A1_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
A2_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
B_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)

learned_dtn_eval = [] 

for N in Ns: 
    l_dtn.Run(A1_guess,A2_guess,B_guess,ansatz,flags)
    A1_IE.append(A1_guess.copy()),A2_IE.append(A2_guess.copy()),B_IE.append(B_guess.copy())
    val_eval = np.zeros((len(lam),len(g_omega_test)),dtype=complex)
    opt.eval_dtn_fct_interval(A1_IE[N],A2_IE[N],B_IE[N],lam,g_omega_test,val_eval)
    learned_dtn_eval.append(val_eval)   
    A1_guess,A2_guess,B_guess = new_initial_guess(A1_IE[N],A2_IE[N],B_IE[N],ansatz)


for idx,omega in enumerate(omega_test):
    result_omega = [] 
    for N in Ns:
        val_eval = learned_dtn_eval[N] 
        result_omega.append(np.max(np.abs(np.multiply(weights_test[:,idx], dtn_nr_test[:,idx] - val_eval[:,idx] )))) 
    sup_errors[omega] = result_omega

with open('LIE-helm-interval-sup-Nomega{0}.dat'.format(Nomega), 'a') as the_file:
    the_file.write('N omega sup \n')
    for omega in omega_test:
        for N in Ns:
            the_file.write('{0} {1} {2} \n'.format(N,omega, sup_errors[omega][N] ) )
        the_file.write('\n')

sup_errors_last_N = [] 
for omega in omega_test:
    sup_errors_last_N.append(sup_errors[omega][Ns[-1]])
sup_errors_last_N = np.array(sup_errors_last_N)
fname = "LIE-helm-interval-sup-Nomega{0}-N{1}".format(Nomega,Ns[-1])+".dat"
results = [omega_test,sup_errors_last_N]
header_str = "omega sup"
np.savetxt(fname = fname,
           X = np.transpose(results),
           header = header_str,
           comments = '')


rel_error = {} # key = omega

geo = SplineGeometry()
geo.AddCircle( (0,0), a, leftdomain=1, rightdomain=0,bc="outer-bnd")
geo.AddCircle( (0,0), R, leftdomain=0, rightdomain=1,bc="inner-bnd")
geo.SetMaterial(1, "inner")
mesh = Mesh(geo.GenerateMesh (maxh=0.05,quad_dominated=False))
mesh.Curve(10)

dtn_fct_error = {} 

dtn_nr = np.zeros((Lmax,Nomega),dtype=complex) 

def SolveProblem(order,omega):
    
    print("Solving for omega = {0} and order (p) = {1}".format(omega,order))
    # reference solution for calculating the error
    ref_sol = PlaneWaveHomSolution(omega,R,"sound-soft")
    bnd_data =  exp(1j*omega*x )*omega*1j*2*x
    l2_norm =  sqrt( Integrate ( InnerProduct(ref_sol,ref_sol),mesh,VOL_or_BND=VOL   ).real)
    
    rel_error_order = []
    fes_inner = H1(mesh,complex=True,order=order,dirichlet="inner-bnd")

    for N in Ns: 
        
        A1_N = A1_IE[N]
        A2_N = A2_IE[N]
        B_N = B_IE[N]
        fes_surf = H1(mesh,order=order,complex=True,definedon=mesh.Boundaries("outer-bnd"))
        inf_outer = [fes_surf for i in range(A1_N.shape[0]-1)]

        X = FESpace([fes_inner]+inf_outer)

        uX = X.TrialFunction() 
        vX = X.TestFunction() 

        f_X = LinearForm (X)
        #f_X += SymbolicLFI ( bnd_data*vX[0], definedon=mesh.Boundaries("inner-bnd") )
        f_X.Assemble()

        aX = BilinearForm(X, symmetric=True)
        aX += SymbolicBFI ( grad(uX[0])*grad(vX[0]) - omega**2*uX[0]*vX[0] )

        for i in range(A1_N.shape[0]):
            for j in range(A1_N.shape[0]):
                graduX = grad(uX[i]).Trace()
                gradvX = grad(vX[j]).Trace()  
                aX += SymbolicBFI ( B_N[j,i]*graduX*gradvX + (A1_N[j,i]+omega**2*A2_N[j,i])*uX[i]*vX[j]  ,definedon=mesh.Boundaries("outer-bnd"))
        aX.Assemble()

        gfu_X = GridFunction (X)

        res_X = gfu_X.vec.CreateVector()
        gfu_X.vec[:] = 0.0
        gfu_X.components[0].Set(ref_sol,BND)
        res_X.data = f_X.vec - aX.mat * gfu_X.vec
        #invX = aX.mat.Inverse(freedofs=X.FreeDofs(), inverse="pardiso")
        invX = aX.mat.Inverse(freedofs=X.FreeDofs(), inverse="sparsecholesky")
        gfu_X.vec.data += invX * res_X

        l2_err = sqrt ( Integrate (  InnerProduct(gfu_X.components[0] - ref_sol,gfu_X.components[0] - ref_sol), mesh,VOL_or_BND=VOL ).real)
        rel_error_order.append(l2_err/l2_norm)
        print("N = {0},rel l2_err ={1} \n ".format(A1_N.shape[0]-1,l2_err/l2_norm))
        
    rel_error[omega] = np.array(rel_error_order)

for omega in omega_test: 
    SolveProblem(10,omega)

with open('LIE-helm-interval-Nomega{0}.dat'.format(Nomega), 'a') as the_file:
    the_file.write('N omega error \n')
    for omega in omega_test:
        for N in Ns:
            the_file.write('{0} {1} {2} \n'.format(N,omega, rel_error[omega][N] ) )
        the_file.write('\n')

relerr_last_N = [] 
for omega in omega_test:
    relerr_last_N.append(rel_error[omega][Ns[-1]])
relerr_last_N = np.array(relerr_last_N)
fname = "LIE-helm-interval-relerr-Nomega{0}-N{1}".format(Nomega,Ns[-1])+".dat"
results = [omega_test, relerr_last_N]
header_str = "omega relerr"
np.savetxt(fname = fname,
           X = np.transpose(results),
           header = header_str,
           comments = '')
