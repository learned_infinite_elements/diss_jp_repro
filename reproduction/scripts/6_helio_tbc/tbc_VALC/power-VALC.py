from netgen.meshing import *
from netgen.csg import *
from netgen.meshing import Mesh as netmesh
from math import pi
import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse import csr_matrix
from ngsolve import Mesh as NGSMesh

import sys,os
file_dir = os.path.dirname(os.path.realpath(__file__))
upfolder_path=  os.path.abspath(os.path.join( os.path.dirname( __file__ ), '..' ))
sys.path.append(upfolder_path)
from compute_pot import local_polynomial_estimator,logder

from cmath import sqrt as csqrt
np.random.seed(seed=1)
from ngsolve import *
ngsglobals.msg_level = 0
SetNumThreads(8)


#################################################### 
# parameters
####################################################

#use_VALC = False
atmospheric_models = ["VALC","Atmo"]
atmospheric_model = atmospheric_models[0] 
dtn_approximations = ["exact","learned","nonlocal","SHF1a","SAI0","AHF1","ARBC1"]
dtn_approx = dtn_approximations[1]
N_compute = 0

if ( len(sys.argv) > 1 and sys.argv[1] in ["exact","learned","only_learning"]):
    dtn_approx = sys.argv[1]
else:
    raise ValueError('I do not know this atmospheric Model!')

if ( dtn_approx ==  "learned"):
    if (len(sys.argv) > 2 and int(sys.argv[2]) in range(10) ):
        N_compute = int(sys.argv[2])
    else:
        raise ValueError("Invalid input for learned IE computation.") 


order_ODE = 8 # order of ODE discretization
L_min = 0 
L_max = 1000  # how many DtN numbers to take

L_max = 1000
#L_max = 500
L_range = range(L_min,L_max+1)

L_range = np.arange(L_min,L_max+1)

spline_order = 1 # order of Spline approx. for coefficients

R_max_ODE = 1.0036 # to how far out the ODE for VAL-C model should be solved

# depracated 
R_start_PML = 1.001
R_min_ODE = 0.0

prec = 3000

use_PML = False

if atmospheric_model == "VALC": 
    a = 1.0 # radius of trucation boundary
else:
    a = 1.0007126

Nmax = 5 # maximal dofs for learned IE in radial direction
alpha_decay = 1/60 # exponential decay factor of weights
Nrs = list(range(Nmax))
show_plots = True
#solver = "pardiso"
solver = "sparsecholesky"
ansatz = "mediumSym"
flags = {"max_num_iterations":10000,
         "check_gradients":False, 
         "use_nonmonotonic_steps":True,
         "minimizer_progress_to_stdout":False,
         "num_threads":4,
         "report_level":"Brief",
         "function_tolerance":1e-12,
         "parameter_tolerance":1e-12,
         "gradient_tolerance":1e-12}

nodamping  = False


RSun = 6.963e10 # radius of the Sun in cm

f_hzs = np.linspace(0,8.3,7200)

#f_hzs = np.linspace(0,8.3,10)

#f_hzs = np.linspace(0.001,8.3,100)
#f_hzs = np.linspace(0.001,8.3,1000)
#f_hzs = np.linspace(0,8.3,8000)
#f_hzs = np.linspace(0.0,12.0,2400)

f_hzs = (10**-3)*f_hzs

#################################################### 
# some auxiliary functions
####################################################

def sqrt_2branch(z):
    if z.real < 0 and z.imag <0:
        return - csqrt(z)
    else:
        return csqrt(z)

def P_DoFs(M,test,basis):
    return (M[test,:][:,basis])


def Make1DMesh_givenpts(mesh_r_pts):

    mesh_1D = netmesh(dim=1)
    pids = []   
    for r_coord in mesh_r_pts:
        pids.append (mesh_1D.Add (MeshPoint(Pnt(r_coord, 0, 0))))                     
    n_mesh = len(pids)-1
    for i in range(n_mesh):
        mesh_1D.Add(Element1D([pids[i],pids[i+1]],index=1))
    mesh_1D.Add (Element0D( pids[0], index=1))
    mesh_1D.Add (Element0D( pids[n_mesh], index=2))
    mesh_1D.SetBCName(0,"left")
    mesh_1D.SetBCName(1,"right")
    mesh_1D = NGSMesh(mesh_1D)    
    return mesh_1D

def Make1DMesh_flex(R_min,R_max,N_elems=50):

    n1 = 20
    n2 = 70
    n3 = 30
    R1 = 0.8
    delta_R1 = R1-R_min
    R2 = 1.0
    delta_R2 = R2-R1
    delta_R3 = R_max-R2

    rrs = [] 
    mesh_1D = netmesh(dim=1)
    pids = []   
    for i in range(n1):
        new_r = R_min + delta_R1*i/n1
        rrs.append(new_r)
        pids.append (mesh_1D.Add (MeshPoint(Pnt(new_r, 0, 0))))                     
    for i in range(n2):
        new_r = R1 + delta_R2*i/n2
        rrs.append(new_r)
        pids.append (mesh_1D.Add (MeshPoint(Pnt(new_r,0, 0))))                     
    for i in range(n3+1):
        new_r = R2 + delta_R3*i/n3
        rrs.append(new_r)
        pids.append (mesh_1D.Add (MeshPoint(Pnt(new_r,0, 0))))
    print("rrs = ", rrs)
    n_mesh = len(pids)-1
    for i in range(n_mesh):
        mesh_1D.Add(Element1D([pids[i],pids[i+1]],index=1))
    mesh_1D.Add (Element0D( pids[0], index=1))
    mesh_1D.Add (Element0D( pids[n_mesh], index=2))
    mesh_1D.SetBCName(0,"left")
    mesh_1D.SetBCName(1,"right")
    mesh_1D = NGSMesh(mesh_1D)    
    return mesh_1D

def new_initial_guess(l1_old,l2_old,ansatz):
    N = l1_old.shape[0]
    l1_guess = np.zeros((N+1,N+1),dtype='complex')
    l2_guess = np.zeros((N+1,N+1),dtype='complex')
    if ansatz in ["medium","full"]:
        l1_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        l2_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        l1_guess[:N,:N] = l1_old[:]
        l2_guess[:N,:N] = l2_old[:]
        l1_guess[N,N] = 1.0
    elif ansatz == "minimalIC":
        l1_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        l1_guess[:N,:N] = l1_old[:]
        l2_guess[:N,:N] = l2_old[:]
        l1_guess[N,N] = -1e-7*(-1)**N + 0.1j
        #l1_guess[N,N] = 1e+9
        #l1_guess[N,N] = (np.random.rand(1) + 1j*np.random.rand(1))
        #l2_guess[0,N] = (np.random.rand(1) + 1j*np.random.rand(1))
    elif ansatz == "mediumSym":
        l1_guess[:N,:N] = l1_old[:]
        l2_guess[:N,:N] = l2_old[:]
        if N <= 2:
            l1_guess[0,N]  = l1_guess[N,0] = 1e-4*(np.random.rand() + 1j*np.random.rand())
            l2_guess[0,N] =  l2_guess[N,0] = 1e-4*(np.random.rand() + 1j*np.random.rand()) 
        else:
            l1_guess[0,N]  = l1_guess[N,0] = 1*1e-5*(np.random.rand() + 1j*np.random.rand())
            l2_guess[0,N] =  l2_guess[N,0] = 1*1e-5*(np.random.rand() + 1j*np.random.rand()) 
        #l1_guess[N,N] = -1e-7*(-1)**N + 0.1j
        l1_guess[N,N] = -100 - 10j
        #l2_guess[N,N] = 1.0
    else:
        l1_guess[:N,:N] = l1_old[:]
        l2_guess[:N,:N] = l2_old[:]
        l1_guess[N,N] = -100-100j
        l1_guess[0,N] = l1_guess[N,0] = 10**(3/2)*(np.random.rand(1) + 1j*np.random.rand(1))
    return l1_guess,l2_guess



#input("")
#print("l_ref[:10] = ", l_ref[:10])
#print("nu_ref[:10] = ", nu_ref[:10])
#input("")

#################################################### 
# load solar models and compute potential
####################################################

rS,cS,rhoS,_,__,___ = np.loadtxt("../modelSinput.txt",unpack=True)
cS = (10**-2)*cS
rhoS = (10**3)*rhoS

if atmospheric_model == "VALC": 
    print("using Model S + VAL-C")
    #rV,vV,rhoV,Tv  = np.loadtxt("VALCinput_cropped.txt",unpack=True)
    #rV,cV,rhoV,_,__,___  = np.loadtxt("../VALC-smoothed.txt",unpack=True)
    #rV,cV,rhoV  = np.loadtxt("../VALC_mod.txt",unpack=True)
    #rV,cV,rhoV  = np.loadtxt("../VALCsmoothed.txt",unpack=True)
    #rV,cV,rhoV  = np.loadtxt("../VALCsmoothedExt.txt",unpack=True)
    rV,vV,rhoV,Tv  = np.loadtxt("../VALCinput.txt",unpack=True)

    # sound speed is not given in VAL-C model
    # calculate it from temperature using the ideal gas law
    gamma_ideal = 5/3 # adiabatic index
    mu_ideal = 1.3*10**(-3) # mean molecular weight 
    R_gas = 8.31446261815324 # universal gas constant 
    cV = np.sqrt(gamma_ideal*R_gas*Tv/mu_ideal)
    rhoV = (10**3)*rhoV

    # overlap interval
    rmin = rV[-1]
    rmax = rS[0]

    weightS = np.minimum(1, (rmax-rS)/(rmax-rmin))
    weightV = np.minimum(1, (rV-rmin)/(rmax-rmin))

    r = np.concatenate((rS,rV)) 
    ind = np.argsort(r)
    r = r[ind]
    c = np.concatenate((cS,cV))[ind]
    rho = np.concatenate((rhoS,rhoV))[ind]
    weight = np.concatenate((weightS,weightV))[ind]
else:
    print("using Model S + Atmo")
    ind = np.argsort(rS)
    r = rS[ind]
    c = cS[ind]
    rho = rhoS[ind]
    weight = np.ones(len(r))


#weight = np.ones(len(weight))
#alpha_decay = 1/45



RSun = 6.963e10 # radius of the Sun in cm
RSun =  6.96*10**8
c0 = 6.855e5 # sound speed at interface [cm/s]
H = 1.25e7 # density scale height in [cm]
omega_c = 0.0052*2*pi # c0/(2*H) cut-off frequency in Herz

rho_clean,drho,ddrho = logder(rho,r,weight,3)
#rho_clean = logder(rho,r,weight,1)
c_clean = logder(c,r,weight,1)
idx1 = np.argmin(np.abs(r - a))
alpha_infty = (-drho/rho_clean)[idx1]
c_infty = c_clean[idx1]
print("alpha_infty = ",  alpha_infty) 
print("c_infty = ", c_infty)
#input("")
i0 = np.min( np.nonzero(r > 0.99)[0] ) 

f,df,ddf = logder(1/np.sqrt(rho),r,weight,3)
pot_rho = np.sqrt(rho_clean)*(ddf+2*df/r)

pot_rho[0] = pot_rho[1] # treating the NaN value at r = 0

#print("pot_rho = ", pot_rho[:20])

pot_c = c_clean.copy()

#print("rS = ", rS)
#plt.semilogy(rS[::-1],rho_clean[:len(rS)],label="cleaned")
#plt.semilogy(rS[::-1],rhoS[::-1],label="original")
#plt.legend()
#plt.show()

#plt.plot(rS[::-1],c_clean[:len(rS)],label="cleaned")
#plt.plot(rS[::-1],cS[::-1],label="original")
#plt.legend()
#plt.show()

#################################################### 
# BSpline approximation of c,rho and potential 
####################################################

r_beg = r[0]
pot_rho_beg = pot_rho[0]
pot_c_beg = pot_c[0]
r_end = r[-1]
pot_rho_end = pot_rho[-1]
pot_c_end = pot_c[-1]

rho_beg = rho_clean[0]
c_beg = c_clean[0]
rho_end = rho_clean[-1]
c_end = c_clean[-1]

#print("c_clean[:-10] = ", c_clean[:-10])

r = np.append([r_beg for i in range(spline_order)],r)
pot_rho = np.append([pot_rho_beg for i in range(spline_order)],pot_rho)
pot_c = np.append([pot_c_beg for i in range(spline_order)],pot_c)

c_clean = np.append([c_beg for i in range(spline_order)],c_clean)
rho_clean = np.append([rho_beg for i in range(spline_order)],rho_clean)
 
r = np.append(r,[r_end for i in range(spline_order)])
pot_rho = np.append(pot_rho,[pot_rho_end for i in range(spline_order)])
pot_c = np.append(pot_c,[pot_c_end for i in range(spline_order)])

c_clean = np.append(c_clean,[c_end for i in range(spline_order)])
rho_clean = np.append(rho_clean,[rho_end for i in range(spline_order)])


r = r.tolist()
#print("r = ", r)
pot_rho = pot_rho.tolist()
pot_c = pot_c.tolist()
c_clean = c_clean.tolist()
#print("c_clean = ", c_clean)
rho_clean = rho_clean.tolist()

#r_filter = [] 
#c_filter = [] 
#for i in range(len(r)):
#    if i <= spline_order +2 or i >= len(r)-spline_order or i % 10 == 0:
#        r_filter.append(r[i])
#        c_filter.append(c_clean[i])
#c_filtered = BSpline(spline_order,r_filter,c_filter)


pot_rho_B = BSpline(spline_order,r,pot_rho)(x)
pot_c_B = BSpline(spline_order,r,pot_c)(x)

c_cleanB = BSpline(spline_order,r,c_clean)(x)
rho_cleanB = BSpline(spline_order,r,rho_clean)(x)

cBB = BSpline(spline_order,r,c_clean)
rhoBB =BSpline(spline_order,r,rho_clean)
eval_c = [cBB(pp) for pp in rS[::-1]]
eval_c[-1] = cBB(rS[::-1][-1]-1e-11)
#eval_c_filter = [c_filtered(pp) for pp in rS[::-1]]
eval_rho = [rhoBB(pp) for pp in rS[::-1]]


#plt.semilogy(rS[::-1],eval_c[:len(rS)],label="cB")
#plt.semilogy(rS[::-1],eval_c_filter[:len(rS)],label="c_filtered")
#plt.semilogy(rS[::-1],cS[::-1],label="original")
#plt.legend()
#plt.show()

#plt.semilogy(rS[::-1], np.abs(cS[::-1]-eval_c[:len(rS)])/np.abs(cS[::-1]) ,label="errorB")
#plt.semilogy(rS[::-1], np.abs(cS[::-1]-eval_c_filter[:len(rS)])/np.abs(cS[::-1]) ,label="error_filter")
#plt.legend()
#plt.show()

#plt.semilogy(rS[::-1], np.abs(rhoS[::-1]-eval_rho[:len(rS)])/np.abs(rhoS[::-1]) ,label="error")
#plt.legend()
#plt.show()

print("Computing mesh nodes according to local wavelength")
mesh_r_pts = [] 
c_w = BSpline(spline_order,r,c_clean)
omega_max_inner = RSun*53.0*10**(-3) 
#omega_max_inner = RSun*45.0*10**(-3) 
omega_max_outer = RSun*128*10**(-3) 
def get_lambda_r(r_coord,omega_max):
    local_lambda = 2*pi*c_w(r_coord)/omega_max
    print("r = {0}, local_lambda = {1}".format(r_coord,local_lambda))
    return local_lambda
r_next = 1.0
#while r_next > 0.35:
while r_next > 0.15:
    local_lambda = get_lambda_r(r_next,omega_max_inner)
    r_next -= local_lambda
    mesh_r_pts.append(r_next)
mesh_r_pts.append(0.0)
mesh_r_pts = mesh_r_pts[::-1]

mesh_r_pts.append(1.0) 
if a > 1.0:
    mesh_r_pts.append(a) 
mesh_pts_to_surface = mesh_r_pts.copy() 
#mesh_pts_to_surface.append(1.0) 
mesh_above_surface = [a] 

#r_next = 1.0
#
#while r_next < R_start_PML:
#    mesh_above_surface.append(r_next)
#    mesh_r_pts.append(r_next)
#    local_lambda = get_lambda_r(r_next,omega_max_inner)
#    r_next += local_lambda

n_outer = 10
start_point = a
for i in range(1,n_outer+1):
    new_point = start_point + (R_max_ODE - start_point)*i/n_outer
    mesh_r_pts.append(new_point) 
    mesh_above_surface.append(new_point)

#mesh_r_pts.append(R_start_PML) 
#mesh_r_pts.append(R_start_PML +  (R_max_ODE-R_start_PML)/2 )
#mesh_r_pts.append(R_max_ODE)
#mesh_r_pts.append(R_start_PML +  (R_max_ODE-R_start_PML)/3 )
#mesh_r_pts.append(R_start_PML + 2*(R_max_ODE-R_start_PML)/3 )
#mesh_r_pts.append(R_max_ODE)

#mesh_above_surface.append(R_start_PML) 
#mesh_above_surface.append(R_start_PML +  (R_max_ODE-R_start_PML)/3 )
#mesh_above_surface.append(R_start_PML +  2*(R_max_ODE-R_start_PML)/3 )
#mesh_above_surface.append(R_max_ODE)
#mesh_above_surface.append(R_start_PML +  (R_max_ODE-R_start_PML)/2 )
#mesh_above_surface.append(R_max_ODE)
print("mesh_r_pts = ", mesh_r_pts) 
print("len(mesh_r_pts) = ", len(mesh_r_pts)) 

print("mesh_pts_to_surface = ", mesh_pts_to_surface) 
print("mesh_above_surface = ", mesh_above_surface) 
#input("")

 
#################################################### 
# damping model
####################################################

def get_damping(f_hz,lami=0):
   
    
    omega_f = f_hz*2*pi*RSun
    gamma0 = 2*pi*4.29*1e-6*RSun
    omega0 = 0.003*2*pi*RSun
    if f_hz < 0.0053:
        gamma = gamma0*(omega_f/omega0)**(5.77)
    else:
        gamma = gamma0*(0.0053*2*pi*RSun/omega0)**(5.77)

    #print("gamma_power = {0},gamma_fixed = {1}".format(gamma,2*pi*20*1e-6*RSun))
    #gamma = 2*pi*20*1e-6*RSun
    #gamma *= 1+ lami**0.35
    #gamma = gamma0*(0.003*2*pi*RSun/omega0)**(5.77)

    #if nodamping:
    #    gamma = 0
    omega  = sqrt_2branch(1+2*1j*gamma)*omega_f
    
    omega_squared = omega_f**2+2*1j*omega_f*gamma

    prefac = 1 + (  (omega_f/RSun-0.0033*2*pi)/( 0.5*0.0012*2*pi )   )**2
    prefac = 1/prefac
    return omega,omega_squared

#################################################### 
# PML 
####################################################

def get_PML(f_hz):
    f0 = 10**3*f_hz
    #f0 = 10**3*0.003
    rad_min = R_start_PML
    rad_max = R_max_ODE
    C_PML = 100
    #C_PML = 0.01
    plm_min = rad_min
    w = rad_max-plm_min
    physpml_sigma = IfPos(x-plm_min,IfPos(rad_max -x,C_PML/w*(x-plm_min)**2/w**2,0),0)
    G_PML =  (1j/f0)*(C_PML/3)*(w/C_PML)**(3/2)*physpml_sigma**(3/2)
    pml_phys = pml.Custom(trafo=x+(1j/f0)*(C_PML/3)*(w/C_PML)**(3/2)*physpml_sigma**(3/2), jac = -1 - 1j*physpml_sigma/f0 )
    return pml_phys


#################################################### 
# Compute DtN numbers by solving ODEs
####################################################


DtN_continuous_eigvals = [] 
filtered_lam_all = []
DtN_ODE_filtered_all = [] 

#print("L_range = ", L_range)

lam_continuous = np.array( [l*(l+1)/a**2 for l in L_range ] ) 

def get_ODE_DtN(dtn_nr,use_PML):
    
    mesh_1D = Make1DMesh_givenpts(mesh_above_surface)  
    fes_DtN = H1(mesh_1D, complex=True,  order=order_ODE, dirichlet=[1])
    u_DtN,v_DtN = fes_DtN.TnT()
    gfu_DtN = GridFunction (fes_DtN)

    f_DtN = LinearForm(fes_DtN)
    f_DtN.Assemble()
    r_DtN = f_DtN.vec.CreateVector()
    frees_DtN = [i for i in range(1,fes_DtN.ndof)]

    for idx_f,f_hz in enumerate(f_hzs):
        print("f_hz = ", f_hz)

                
        if use_PML:
            pml_phys = get_PML(f_hz)
            mesh_1D.SetPML(pml_phys,definedon=1)
        
        for idx_lami,lami in enumerate(lam_continuous):    
        
            omega, omega_squared = get_damping(f_hz,lami)
            pot_1D = pot_rho_B - omega_squared/pot_c_B**2
            
            a_DtN = BilinearForm (fes_DtN, symmetric=False)
            a_DtN += SymbolicBFI((x**2)*grad(u_DtN)*grad(v_DtN)) 
            a_DtN += SymbolicBFI(( lami.item()*a**2 +  pot_1D*x**2 )*u_DtN*v_DtN)
            a_DtN.Assemble()

            gfu_DtN.vec[:] = 0.0
            gfu_DtN.Set(1.0, BND)
            r_DtN.data = f_DtN.vec - a_DtN.mat * gfu_DtN.vec
            gfu_DtN.vec.data += a_DtN.mat.Inverse(freedofs=fes_DtN.FreeDofs(),inverse=solver) * r_DtN
           
            rows_DtN,cols_DtN,vals_DtN = a_DtN.mat.COO()
            A_DtN = csr_matrix((vals_DtN,(rows_DtN,cols_DtN)))
            val = -P_DoFs(A_DtN,[0],frees_DtN).dot(gfu_DtN.vec.FV().NumPy()[frees_DtN])-P_DoFs(A_DtN,[0],[0]).dot(gfu_DtN.vec.FV().NumPy()[0])

            dtn_nr[idx_f,idx_lami] =  -val.item()/a**2 

        if use_PML:
            mesh_1D.UnSetPML(1)

    np.savetxt('ODE_dtn_nr.out',dtn_nr,delimiter=',')



def get_atmo_dtn(dtn_nr):
    print("Getting Atmo dtn numbers from whittaker function")
    from ngs_arb import Whitw_deriv_over_Whitw,Whitw_deriv_over_Whitw_par,Whitw_deriv_over_Whitw_spar

    for idx_f,f_hz in enumerate(f_hzs):
        print("idx = {0}, f_hz = {1}".format(idx_f,f_hz))
        sigma, sigma_squared = get_damping(f_hz)
        k_squared = sigma_squared/c_infty**2 - 0.25*alpha_infty**2
        k_omega = sqrt_2branch(k_squared)
        eta = alpha_infty/(2*k_omega)
        chi = -1j*eta
        z_arg = -2*1j*k_omega*a
        #kappas_l = [chi for l in L_range ]
        #mus_l = [l+0.5 for l in L_range ]
        #zs_l = [z_arg for l in L_range ]
        #wh_qout =  np.array(Whitw_deriv_over_Whitw( kappas_l,mus_l,zs_l,prec)) 
        #wh_qout =  np.array(Whitw_deriv_over_Whitw_par( kappas_l,mus_l,zs_l,prec)) 
        
        wh_qout =  np.array(Whitw_deriv_over_Whitw_spar(chi,z_arg,L_max,prec)) 
        
        DtN_atmo =  1/a + 2*1j*k_omega*wh_qout
        #DtN_atmo = 1/a + 0.5*alpha_infty + 2*1j*k_omega*wh_qout
        print("dtn ", DtN_atmo[:-10])
        dtn_nr[idx_f,:] = DtN_atmo[:]
        #if idx_f == 5:
        #plt.plot(L_range,DtN_atmo.real)
        #plt.show() 
        #plt.plot(L_range,DtN_atmo.imag)
        #plt.show()
    np.savetxt('dtn_nr_Whitw.out',dtn_nr,delimiter=',')


def get_atmo_nonlocal(dtn_nr):
    for idx_f,f_hz in enumerate(f_hzs):
        print("idx = {0}, f_hz = {1}".format(idx_f,f_hz))
        sigma, sigma_squared = get_damping(f_hz)
        k_squared = sigma_squared/c_infty**2 - 0.25*alpha_infty**2
        k_omega = sqrt_2branch(k_squared)
        #DtN_atmo = [ -1j*csqrt(sigma_squared/c_infty**2 - 0.25*alpha_infty**2 - alpha_infty/a - l*(l+1)/a**2 )  for l in L_range]
        DtN_atmo = [ 1/a - 1j*k_omega*csqrt( 1 - alpha_infty/(a*k_squared)  - l*(l+1)/(a**2*k_squared) )  for l in L_range]
        dtn_nr[idx_f,:] = DtN_atmo[:]
    np.savetxt('dtn_nr_nonlocal.out',dtn_nr,delimiter=',')

def get_atmo_SHF1a(dtn_nr):
    for idx_f,f_hz in enumerate(f_hzs):
        print("idx = {0}, f_hz = {1}".format(idx_f,f_hz))
        sigma, sigma_squared = get_damping(f_hz)
        k_squared = sigma_squared/c_infty**2 - 0.25*alpha_infty**2
        k_omega = sqrt_2branch(k_squared)
        DtN_atmo = [ 1/a  - 1j*k_omega + (1j/(2*k_omega))*alpha_infty/a  for l in L_range]
        dtn_nr[idx_f,:] = DtN_atmo[:]
    np.savetxt('dtn_nr_S-HF-1a.out',dtn_nr,delimiter=',')

def get_atmo_SAI0(dtn_nr):
    for idx_f,f_hz in enumerate(f_hzs):
        print("idx = {0}, f_hz = {1}".format(idx_f,f_hz))
        sigma, sigma_squared = get_damping(f_hz)
        k_squared = sigma_squared/c_infty**2 - 0.25*alpha_infty**2
        k_omega = sqrt_2branch(k_squared)
        DtN_atmo = [ 1/a  -1j*k_omega*csqrt( 1- alpha_infty/(a*k_squared) )  for l in L_range]
        dtn_nr[idx_f,:] = DtN_atmo[:]
    np.savetxt('dtn_nr_SAI-0.out',dtn_nr,delimiter=',')

def get_atmo_AHF1(dtn_nr):
    for idx_f,f_hz in enumerate(f_hzs):
        print("idx = {0}, f_hz = {1}".format(idx_f,f_hz))
        sigma, sigma_squared = get_damping(f_hz)
        k_squared = sigma_squared/c_infty**2 - 0.25*alpha_infty**2
        k_omega = sqrt_2branch(k_squared)
        DtN_atmo = [ 1/a - 1j*sigma/c_infty - (c_infty/(2*1j*sigma))*( l*(l+1)/a**2 + (alpha_infty/a) + 0.25*alpha_infty**2 )   for l in L_range]
        dtn_nr[idx_f,:] = DtN_atmo[:]
    np.savetxt('dtn_nr_AHF1.out',dtn_nr,delimiter=',')

def get_atmo_ARBC1(dtn_nr):
    for idx_f,f_hz in enumerate(f_hzs):
        print("idx = {0}, f_hz = {1}".format(idx_f,f_hz))
        sigma, sigma_squared = get_damping(f_hz)
        k_squared = sigma_squared/c_infty**2 - 0.25*alpha_infty**2
        k_omega = sqrt_2branch(k_squared)
        DtN_atmo = [ 1/a - 1/a - 1j*k_omega for l in L_range]
        dtn_nr[idx_f,:] = DtN_atmo[:]
    np.savetxt('dtn_nr_ARBC1.out',dtn_nr,delimiter=',')

def computed_learned_dtn(dtn_nr,Nrs):

    step = 10
    L_range_filtered = L_range[::step]
    lam_continuous_filtered = lam_continuous[::step]

    import ceres_dtn as opt
    result = {}
    for N in Nrs:
        result[N] = np.zeros(dtn_nr.shape,dtype="complex")
   
    for idx_f,f_hz in enumerate(f_hzs):
        print("f_hz = ", f_hz)
        dtn_ref = dtn_nr[idx_f,:]
        dtn_ref_filtered = dtn_ref[::step]
        #weights = np.array([10**6*np.exp(-l*alpha_decay) for l in L_range])
        weights = np.array([10**6*np.exp(-l*alpha_decay) for l in L_range_filtered])
        print("weights = ", weights) 
        #l_dtn = opt.learned_dtn(lam_continuous,dtn_ref,weights**2)
        l_dtn = opt.learned_dtn(lam_continuous_filtered,dtn_ref_filtered,weights**2)

        A_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
        B_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)

        zeta_approx_evals = []

        A_IE = []
        B_IE = []
        
        #plt.plot(lam_continuous,dtn_ref.imag,label="ref",color="gray",linewidth=4)
        for N in Nrs:
            l_dtn.Run(A_guess,B_guess,ansatz,flags)
            A_IE.append(A_guess.copy()), B_IE.append(B_guess.copy()) 
            zeta_learned = np.zeros(len(lam_continuous),dtype=complex)
            opt.eval_dtn_fct(A_IE[N],B_IE[N],np.array(lam_continuous,dtype=float),zeta_learned)
            (result[N])[idx_f,:] = zeta_learned[:]   
            A_guess,B_guess = new_initial_guess(A_IE[N],B_IE[N],ansatz)
        
            #plt.plot(lam_continuous,zeta_learned.imag,label="N={0}".format(N))
        #plt.legend()
        #plt.show() 
            #zeta_approx_evals.append(zeta_learned)
    for N in Nrs:
        if atmospheric_model == "VALC": 
            np.savetxt('dtn_nr_learned_N{0}-VALC.out'.format(N),result[N],delimiter=',')
        else:
            np.savetxt('dtn_nr_learned_N{0}-Atmo.out'.format(N),result[N],delimiter=',')
    return result



def compute_power_spectrum_full(result,use_PML=True,use_conjugate=True):
    
    mesh_1D = Make1DMesh_givenpts(mesh_r_pts)
    
    #NN = 500000
    #bpts = [j*R_max_ODE/NN for j in range(NN+1) ]
    
    #sample_bpts = [mesh_1D(bpt) for bpt in mesh_r_pts]
    #sample_bpts = [mesh_1D(bpt) for bpt in bpts]
    #c_bpts = [c_cleanB(sbpt) for sbpt in sample_bpts] 

    #plt.plot( mesh_r_pts ,c_bpts)
    #plt.plot(bpts,c_bpts)
    #plt.show()
    
    mesh_pt_rtilde = mesh_1D(1.0)
    rho_surface_sq = sqrt(rho_cleanB(mesh_pt_rtilde))
    
    fes_DtN = H1(mesh_1D, complex=True,  order=order_ODE, dirichlet=[])
    u_DtN,v_DtN = fes_DtN.TnT()
    gfu_DtN = GridFunction (fes_DtN)

    f_DtN = LinearForm(fes_DtN)
    f_DtN.Assemble()
    r_DtN = f_DtN.vec.CreateVector()

    for i in range(fes_DtN.ndof):
        gfu_DtN.vec[i] = 1.0
        #f_DtN.vec[i] =  RSun*gfu_DtN(mesh_pt_rtilde)
        f_DtN.vec[i] =  gfu_DtN(mesh_pt_rtilde)
        gfu_DtN.vec[i] = 0.0

    for idx_f,f_hz in enumerate(f_hzs):

        omega, omega_squared = get_damping(f_hz)
        print("omega_squared = ", omega_squared)
        pot_1D = pot_rho_B - omega_squared/pot_c_B**2
         
        if use_PML:
            pml_phys = get_PML(f_hz)
            mesh_1D.SetPML(pml_phys,definedon=1)
        
        for idx_lami,lami in enumerate(lam_continuous):    
            
            a_DtN = BilinearForm (fes_DtN, symmetric=True)
            a_DtN += SymbolicBFI((x**2)*grad(u_DtN)*grad(v_DtN)) 
            a_DtN += SymbolicBFI(( lami.item() +  pot_1D*x**2 )*u_DtN*v_DtN)
            
            #a_DtN += SymbolicBFI((x**2/rho_cleanB)*grad(u_DtN)*grad(v_DtN)) 
            #a_DtN += SymbolicBFI(( lami.item()/rho_cleanB - omega_squared*x**2/(rho_cleanB*c_cleanB**2) )*u_DtN*v_DtN)
            
            a_DtN.Assemble()

            gfu_DtN.vec[:] = 0.0
            r_DtN.data = f_DtN.vec - a_DtN.mat * gfu_DtN.vec
            gfu_DtN.vec.data += a_DtN.mat.Inverse(freedofs=fes_DtN.FreeDofs(),inverse=solver) * r_DtN
             
            result[idx_f,idx_lami] = rho_surface_sq*gfu_DtN(mesh_pt_rtilde).imag
        
        if use_PML:
            mesh_1D.UnSetPML(1)

def compute_power_spectrum_dtn(result,dtn_nr,use_conjugate=True):
    
    mesh_1D = Make1DMesh_givenpts(mesh_pts_to_surface)
    mesh_pt_rtilde = mesh_1D(1.0)
    rho_surface_sq = sqrt(rho_cleanB(mesh_pt_rtilde))
    
    fes_DtN = H1(mesh_1D, complex=True,  order=order_ODE, dirichlet=[])
    u_DtN,v_DtN = fes_DtN.TnT()
    gfu_DtN = GridFunction (fes_DtN)

    f_DtN = LinearForm(fes_DtN)
    f_DtN.Assemble()
    r_DtN = f_DtN.vec.CreateVector()

    for i in range(fes_DtN.ndof):
        gfu_DtN.vec[i] = 1.0
        f_DtN.vec[i] =  gfu_DtN(mesh_pt_rtilde)
        gfu_DtN.vec[i] = 0.0

    for idx_f,f_hz in enumerate(f_hzs):
        print("idx_f =", idx_f)

        for idx_lami,lami in enumerate(lam_continuous):    
            omega, omega_squared = get_damping(f_hz,lami)
            #print("omega_squared = ", omega_squared)
            pot_1D = pot_rho_B - omega_squared/pot_c_B**2
            
            a_DtN = BilinearForm (fes_DtN, symmetric=True)
            a_DtN += SymbolicBFI((x**2)*grad(u_DtN)*grad(v_DtN)) 
            a_DtN += SymbolicBFI(( lami.item() +  pot_1D*x**2 )*u_DtN*v_DtN)
            
            #a_DtN += SymbolicBFI((x**2/rho_cleanB)*grad(u_DtN)*grad(v_DtN)) 
            #a_DtN += SymbolicBFI(( lami.item()/rho_cleanB - omega_squared*x**2/(rho_cleanB*c_cleanB**2) )*u_DtN*v_DtN)
            #a_DtN += SymbolicBFI(dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            a_DtN += SymbolicBFI(x**2*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            #a_DtN += SymbolicBFI((x**2/rho_cleanB)*dtn_nr[idx_f,idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))   
            a_DtN.Assemble()

            gfu_DtN.vec[:] = 0.0
            r_DtN.data = f_DtN.vec - a_DtN.mat * gfu_DtN.vec
            gfu_DtN.vec.data += a_DtN.mat.Inverse(freedofs=fes_DtN.FreeDofs(),inverse=solver) * r_DtN
             
            result[idx_f,idx_lami] = rho_surface_sq*gfu_DtN(mesh_pt_rtilde).imag
            #result[idx_f,idx_lami] = gfu_DtN(mesh_pt_rtilde).imag


dtn_nr = np.zeros((len(f_hzs),len(L_range)),dtype="complex")
#loaded_dtn_nr = np.zeros((len(f_hzs),len(L_range)),dtype="complex")
power_spectrum_dtn = np.zeros((len(f_hzs),len(L_range)))
power_spectrum = np.zeros((len(f_hzs),len(L_range)))

if atmospheric_model == "VALC": 
    if dtn_approx == "exact":
        with TaskManager():
            get_ODE_DtN(dtn_nr,use_PML)
            dtn_nr = np.loadtxt('ODE_dtn_nr.out',dtype="complex",delimiter=',')
            compute_power_spectrum_dtn(power_spectrum, dtn_nr )
            np.savetxt('power-spectrum-VALC-meshed.out',power_spectrum,delimiter=',')
    if dtn_approx == "only_learning":
        try: 
            dtn_nr = np.loadtxt('ODE_dtn_nr.out',dtype="complex",delimiter=',')
        except:
            get_ODE_DtN(dtn_nr,use_PML)
        learned_dtn_nrs = computed_learned_dtn(dtn_nr,Nrs)
    if dtn_approx == "learned":
        for N in [N_compute]:
            print("Computing power spectrum for learned IEs using N = {0}".format(N))
            try:
                dtn_nr_learned = np.loadtxt('dtn_nr_learned_N{0}-VALC.out'.format(N),dtype="complex",delimiter=',')
            except:
                try: 
                    dtn_nr = np.loadtxt('ODE_dtn_nr.out',dtype="complex",delimiter=',')
                except:
                    get_ODE_DtN(dtn_nr,use_PML)
                learned_dtn_nrs = computed_learned_dtn(dtn_nr,Nrs)
                dtn_nr_learned = np.loadtxt('dtn_nr_learned_N{0}-VALC.out'.format(N),dtype="complex",delimiter=',')
            with TaskManager():
                compute_power_spectrum_dtn(power_spectrum, dtn_nr_learned )
            np.savetxt('power-spectrum-N{0}-VALC.out'.format(N),power_spectrum,delimiter=',')
else:
    if dtn_approx == "exact":
        #get_atmo_dtn(dtn_nr)
        dtn_nr = np.loadtxt('dtn_nr_Whitw.out',dtype="complex",delimiter=',')
        with TaskManager():
            compute_power_spectrum_dtn(power_spectrum, dtn_nr )
        np.savetxt('power-spectrum-Atmo-whitw.out',power_spectrum,delimiter=',')
        #np.savetxt('power-spectrum-nonlocal.out',power_spectrum,delimiter=',')
    if dtn_approx == "nonlocal":
        get_atmo_nonlocal(dtn_nr)
        dtn_nr = np.loadtxt('dtn_nr_nonlocal.out',dtype="complex",delimiter=',')
        with TaskManager():
            compute_power_spectrum_dtn(power_spectrum, dtn_nr )
        np.savetxt('power-spectrum-nonlocal.out',power_spectrum,delimiter=',')
    if dtn_approx == "SHF1a":
        get_atmo_SHF1a(dtn_nr)
        dtn_nr = np.loadtxt('dtn_nr_S-HF-1a.out',dtype="complex",delimiter=',') 
        with TaskManager():
            compute_power_spectrum_dtn(power_spectrum, dtn_nr )
        np.savetxt('power-spectrum-SHF1a.out',power_spectrum,delimiter=',') 
    if dtn_approx == "SAI0":
        get_atmo_SAI0(dtn_nr)
        #get_atmo_SHF1a(dtn_nr)
        dtn_nr = np.loadtxt('dtn_nr_SAI-0.out',dtype="complex",delimiter=',') 
        with TaskManager():
            compute_power_spectrum_dtn(power_spectrum, dtn_nr )
        np.savetxt('power-spectrum-SAI-0.out',power_spectrum,delimiter=',')
    if dtn_approx == "AHF1":
        get_atmo_AHF1(dtn_nr)
        dtn_nr = np.loadtxt('dtn_nr_AHF1.out',dtype="complex",delimiter=',') 
        with TaskManager():
            compute_power_spectrum_dtn(power_spectrum, dtn_nr )
        np.savetxt('power-spectrum-AHF1.out',power_spectrum,delimiter=',')
    if dtn_approx == "ARBC1":
        get_atmo_ARBC1(dtn_nr)
        dtn_nr = np.loadtxt('dtn_nr_ARBC1.out',dtype="complex",delimiter=',') 
        with TaskManager():
            compute_power_spectrum_dtn(power_spectrum, dtn_nr )
        np.savetxt('power-spectrum-ARBC1.out',power_spectrum,delimiter=',')
    if dtn_approx == "learned":
        #try:
        #    dtn_nr = np.loadtxt('dtn_nr_Whitw.out',dtype="complex",delimiter=',')
        #except:
        #    get_atmo_dtn(dtn_nr)
        #learned_dtn_nrs = computed_learned_dtn(dtn_nr,Nrs)
        #for N in Nrs:
        for N in [0]:
            dtn_nr_learned = np.loadtxt('dtn_nr_learned_N{0}-Atmo.out'.format(N),dtype="complex",delimiter=',')
            with TaskManager():
                compute_power_spectrum_dtn(power_spectrum, dtn_nr_learned )
            np.savetxt('power-spectrum-N{0}-Atmo.out'.format(N),power_spectrum,delimiter=',')
    
