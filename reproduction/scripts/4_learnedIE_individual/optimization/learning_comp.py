import numpy as np
from math import sqrt,exp
from scipy.special import hankel1,h1vp
import ceres_dtn as opt
import mpmath as mp
import os
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

k = 16 # wavenumber
a = 1.0 # coupling radius
Lmax = 72 # number of modes
alpha_decay = 2/3 # decay of weights
Nmax = 7 # maximal number of infinite element dofs
Ns = list(range(Nmax))


def hankel1(nu,k):
    return mp.hankel1(nu,k)

def h1vp(nu,k):
    return 0.5*(mp.hankel1(nu-1,k) - mp.hankel1(nu+1,k)) 

def dtn_ref(nu):
    return -k*h1vp(nu,k*a) / hankel1(nu,k*a)

lam = np.array([(l/a)**2 for l in range(Lmax)]) 
dtn_nr = np.array([ dtn_ref(sqrt(lami)*a) for lami in lam ]) 
weights = np.array([10**6*np.exp(-l*alpha_decay) for l in range(Lmax)])
final_res = np.zeros(len(lam),dtype=float)

l_dtn = opt.learned_dtn(lam,dtn_nr,weights**2)

def new_initial_guess(A_old,B_old,ansatz):
    N = A_old.shape[0]
    A_guess = np.zeros((N+1,N+1),dtype='complex')
    B_guess = np.zeros((N+1,N+1),dtype='complex')
    if ansatz in ["medium","full"]:
        A_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        B_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        A_guess[:N,:N] = A_old[:]
        B_guess[:N,:N] = B_old[:]
        A_guess[N,N] = 1.0
    elif ansatz == "minimalIC":
        A_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        A_guess[:N,:N] = A_old[:]
        B_guess[:N,:N] = B_old[:]
        A_guess[N,N] = -100-100j
        B_guess[0,N] = 1e-3*(np.random.rand(1) + 1j*np.random.rand(1))
    elif ansatz == "mediumSym":
        A_guess[:N,:N] = A_old[:]
        B_guess[:N,:N] = B_old[:]
        A_guess[0,N]  = A_guess[N,0] = 1e-3*(np.random.rand() + 1j*np.random.rand())
        #B_guess[0,N] =  B_guess[N,0] = 1e-3*(np.random.rand() + 1j*np.random.rand())
        B_guess[0,N] =  B_guess[N,0] = 0.5*1e-1*(np.random.rand() + 1j*np.random.rand())
        A_guess[N,N] = -100-100j
        B_guess[N,N] = 1.0
    return A_guess,B_guess


flags = {"max_num_iterations":5000000,
         "use_nonmonotonic_steps":True,
         "minimizer_progress_to_stdout":False,
         "num_threads":4,
         "report_level":"solver_stats",
         "function_tolerance":1e-9,
         "parameter_tolerance":1e-9,
         "gradient_tolerance":1e-9,
         "solver_stats":{}
         }

results = {}
ansatzes = ["mediumSym","full"]
header_str = "l "
A_IEs = {}
B_IEs = {}

header_solver_stats = "N iterations finalcost time"

for ansatz in ansatzes:
    
    fname_solver_stats = "solver_stats_"+ansatz+".dat"
    iterations_ansatz = [] 
    final_cost_ansatz = []
    time_ansatz = []
    
    print("Minimizing for {0} ansatz".format(ansatz))
    header_str += ansatz 
    header_str += " "
    np.random.seed(123)
    A_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
    B_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
    A_IE = []
    B_IE = [] 
    relative_residuals = []
    for N in Ns: 
        l_dtn.Run(A_guess,B_guess,ansatz,flags,final_res)
        A_IE.append(A_guess.copy()), B_IE.append(B_guess.copy()),relative_residuals.append(final_res.copy())
        A_guess,B_guess = new_initial_guess(A_IE[N],B_IE[N],ansatz)
        
        iterations_ansatz.append(flags["solver_stats"]["iterations"]) 
        final_cost_ansatz.append(flags["solver_stats"]["final_cost"]) 
        time_ansatz.append(flags["solver_stats"]["total_time_in_seconds"]) 

        #if ansatz == "mediumSym":
        #    print("solver_stats", flags["solver_stats"] )

    
    results_stats = [np.array(Ns,dtype=int),np.array(iterations_ansatz,dtype=int),np.array( final_cost_ansatz),np.array(time_ansatz)]
    np.savetxt(fname =fname_solver_stats ,
               X = np.transpose(results_stats),
               header = header_solver_stats,
               comments = '')
    
    
    fname_condext = 'condext-'+'learned-'+'{0}'.format(ansatz)+'-k{0}.dat'.format(k)
    try:
        os.remove(fname_condext)
    except OSError:
        pass

    with open(fname_condext,'w') as the_file:
        the_file.write('lam N cond \n')
        cond_ext = np.zeros((len(Ns[1:]),len(lam)))
        for N in Ns[1:]:
            for l,lami in enumerate(lam):
                cond_N_lami =  np.linalg.cond( (A_IE[N])[1:,1:] + lami*(B_IE[N])[1:,1:] )
                cond_ext[N-1,l] = cond_N_lami
                the_file.write('{0} {1} {2} \n'.format(lami,N,cond_N_lami))
            the_file.write('\n')

        plt.imshow(cond_ext,norm=LogNorm(vmin=1e0,vmax=1e4),origin='lower',cmap=plt.get_cmap('jet'),interpolation=None)
        plt.colorbar()
        plt.title(ansatz)
        plt.show()


    results[ansatz] = relative_residuals
    A_IEs[ansatz] = A_IE
    B_IEs[ansatz] = B_IE

# residuals

for N in Ns:
    results_N = [np.arange(Lmax)]
    for ansatz in ansatzes:
        results_N.append(results[ansatz][N])
    fname = "ansatz-residuals-"+"N{0}.dat".format(N)
    np.savetxt(fname =fname,
               X = np.transpose(results_N),
               header = header_str,
               comments = '') 


# poles for reduced ansatz
print("Poles for learned infinite elements")
A_IE = A_IEs["mediumSym"]
B_IE = B_IEs["mediumSym"]
for N in Ns[1:]:
    print("poles for N = {0}".format(N))
    learned_poles = np.array([-A_IE[N][j,j] for j in range(1,N+1)])
    learned_poles = learned_poles[np.argsort(learned_poles.imag)]
    for pole in learned_poles:
        print("({0},{1})".format(pole.real,pole.imag))

        fname_learned_poles = "poles_learned_opt_N{0}.dat".format(N)
        plot_collect_learned_poles = [learned_poles.real,learned_poles.imag]
        header_str_learned_poles = "poles_real poles_imag"

        np.savetxt(fname=fname_learned_poles,
            X=np.transpose(plot_collect_learned_poles),
            header=header_str_learned_poles,
            comments='')

print("Computing analytic poles")
from GRPF import Poles_and_Roots 
param = {}
# rectangular domain definition z=x+jy x\in[xb,xe], y \in [yb,ye]
param["xrange"] = [-500,500]
param["yrange"] = [1,3000]
param["h"] = 10 
param["Tol"] = 1e-9
param["visual"] = 1 
param["ItMax"] = 20
param["NodesMax"] = 500000 
result = Poles_and_Roots(lambda z:dtn_ref(mp.sqrt(z)*a),param)

analytic_poles = result["poles"]
analytic_poles = analytic_poles[np.argsort(analytic_poles.imag)]
print("\n analytic poles")
for pole in analytic_poles:
    print("({0},{1})".format(pole.real,pole.imag))

fname_analytic_poles = "poles_analytic_opt.dat"
plot_collect_analytic_poles = [analytic_poles.real,analytic_poles.imag]
header_str_analytic_poles = "poles_real poles_imag"

np.savetxt(fname=fname_analytic_poles,
    X=np.transpose(plot_collect_analytic_poles),
    header=header_str_analytic_poles,
    comments='')

#print("function value at poles = ", [abs(complex(dtn_ref(mp.sqrt(z)*a))) for z in analytic_poles])
# roots/poles could be refined further e.g. by a Newton iteration

