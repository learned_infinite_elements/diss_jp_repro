import ceres_dtn as opt
from ngsolve import *
from netgen.geom2d import SplineGeometry
from ngs_refsol import PlaneWaveHomSolution
import numpy as np
from scipy.special import hankel1,h1vp
import mpmath as mp

a_s = [9/16,10/16,12/16,16/16] # radius of coupling boundary
R = 1/2 # radius of inner boundary 
Lmax = 430 # number of considered modes for learning

Nmax = 13 # maximal number of infinite element dofs
Ns = list(range(Nmax))

k = 16
#r_ext = min(R*k/16,R)
#r_ext = R
order = 12
show_plots = False

A_IEs = {} # for storing results
B_IEs = {} # ... 

ansatz = "mediumSym"
flags = {"max_num_iterations":100000,
         "use_nonmonotonic_steps":True,
         "minimizer_progress_to_stdout":False,
         "num_threads":4,
         "report_level":"Brief",
         "function_tolerance":1e-12,
         "parameter_tolerance":1e-12,
         "gradient_tolerance":1e-12,
         "misfit_val":42}


for weight_type in ["heu","opt"]:

    wweight = {}
    Lmaxes = {} 
    uniform_errors = {}
    weighted_l2_errors = {}
    misfit_vals = {}

    # perform minimization for each wavenumber 
    for a in a_s:

        def dtn_ref(nu):
            return -k*0.5*complex( (mp.hankel1(nu-1,k*a) - mp.hankel1(nu+1,k*a) ) /  mp.hankel1(nu,k*a) )
     
        #weights = np.array([10**2* abs(complex(  mp.hankel1(l,k*a) / mp.hankel1(l,k*r_ext)) ) for l in range(Lmax)])
        
        if weight_type == "opt":
            r_ext = R
            scale_fac = 1e4
            weights = np.array([10**2* abs(complex(mp.hankel1(l,k*a) / mp.hankel1(l,k*r_ext) )) for l in range(Lmax)])
            weights = [ abs( complex( mp.besselj(0,k*r_ext)*mp.sqrt(2*mp.pi*a)*mp.hankel1(0,k*a) /  mp.hankel1(0,k*r_ext)   ) ) ] 
            for l in range(1,Lmax):
                weights.append( abs( complex( mp.besselj(l,k*r_ext)*2*mp.sqrt(2*mp.pi*a)*mp.hankel1(l,k*a) /  mp.hankel1(l,k*r_ext)   ) )  )
            weights = scale_fac*np.array(weights)
        else:
            #r_ext = min(R*k/16,R)
            r_ext = R
            weights = np.array([10**2* abs(complex(  mp.hankel1(l,k*a) / mp.hankel1(l,k*r_ext)) ) for l in range(Lmax)])
            #alpha_decay = 1/10
            #weights = np.array([10**6*np.exp(-l*alpha_decay) for l in range(Lmax)])
            #print("weights =" , weights)

        A_IE = []
        B_IE = [] 
        relative_residuals = []

        np.random.seed(seed=123)
        
        A_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
        B_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
        
        l0 = np.max( np.where(weights > 1e-12*weights[0])[0]) 
        weights = weights[:l0]
        #print("weights = ", weights)
        lam = np.array([(l/a)**2 for l in range(l0)]) 

        sample_pts = lam
        dtn_nr = np.array([ dtn_ref(sqrt(lami)*a) for lami in lam ]) 
        ref_samples = np.array([dtn_ref(sqrt(asample)*a) for asample in sample_pts]) 

        dtn_infty = np.max(np.abs(ref_samples))
        L_interval =  sample_pts[-1] - sample_pts[0]
     
        final_res = np.zeros(len(lam),dtype=float)

        l_dtn = opt.learned_dtn(lam,dtn_nr,weights**2)
        
        uniform_err = []
        weighted_l2 = []

        delta_s  = 1e-3
        def new_initial_guess(A_old,B_old,ansatz):
            N = A_old.shape[0]
            scal = 1
            if N >=8:
                scal *= 1e-1
            A_guess = np.zeros((N+1,N+1),dtype='complex')
            B_guess = np.zeros((N+1,N+1),dtype='complex')
            if ansatz in ["medium","full"]:
                A_guess = delta_s*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
                B_guess = delta_s*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
                A_guess[:N,:N] = A_old[:]
                B_guess[:N,:N] = B_old[:]
                A_guess[N,N] = 1.0
            elif ansatz == "minimalIC":
                A_guess = delta_s*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
                A_guess[:N,:N] = A_old[:]
                B_guess[:N,:N] = B_old[:]
                A_guess[N,N] = (k/16)*(-100-100j)
                B_guess[0,N] = delta_s*(np.random.rand(1) + 1j*np.random.rand(1)) 
            elif ansatz == "mediumSym":
                A_guess[:N,:N] = A_old[:]
                B_guess[:N,:N] = B_old[:]
                A_guess[0,N]  = A_guess[N,0] = scal*1e-3*(np.random.rand() + 1j*np.random.rand())
                #B_guess[0,N] =  B_guess[N,0] = 1e-3*(np.random.rand() + 1j*np.random.rand())
                B_guess[0,N] =  B_guess[N,0] = scal*0.5*1e-1*(np.random.rand() + 1j*np.random.rand())
                A_guess[N,N] = (k/16)*(-100-100j)
                B_guess[N,N] = 1.0
            return A_guess,B_guess
        
        misfit_final_val = []
        for N in Ns: 
            l_dtn.Run(A_guess,B_guess,ansatz,flags,final_res)
            A_IE.append(A_guess.copy()), B_IE.append(B_guess.copy()),relative_residuals.append(final_res.copy())
            misfit_final_val.append(flags["misfit_val"]) 
            learned_samples = np.zeros(len(sample_pts),dtype=complex)
            opt.eval_dtn_fct(A_guess,B_guess,sample_pts,learned_samples)
            uniform_err.append( np.max( np.abs( weights*(ref_samples - learned_samples) ) ) )  
            weighted_l2.append( sqrt( np.sum(  weights**2*np.abs(ref_samples - learned_samples  )**2  ))  ) 
            A_guess,B_guess = new_initial_guess(A_IE[N],B_IE[N],ansatz) 
         
        A_IEs[a] = A_IE
        B_IEs[a] = B_IE
        wweight[a] = weights
        uniform_errors[a] = uniform_err
        weighted_l2_errors[a] = weighted_l2 
        misfit_vals[a] = np.array(misfit_final_val)
        
    # now use learned infinite elements as transparent boundary condition 
    # for the scattering of a plane wave from a disk

    if show_plots:
        import matplotlib.pyplot as plt
        for a in a_s:
            plt.semilogy(Ns,uniform_errors[a],label='$a=${0}'.format(a))
        plt.title("uniform error")
        plt.legend()
        plt.show()

        for a in a_s:
            plt.semilogy(Ns,misfit_vals[a],label='$k =${0}'.format(a))
        plt.title("misfit")
        plt.legend()
        plt.show()

    rel_error = {} # key = (order,k)

    # solve PDE with learned IEs as transparent boundary condition
    def SolveProblem(a):
        
        geo = SplineGeometry()
        geo.AddCircle( (0,0), a, leftdomain=1, rightdomain=0,bc="outer-bnd")
        geo.AddCircle( (0,0), R, leftdomain=0, rightdomain=1,bc="inner-bnd")
        geo.SetMaterial(1, "inner")
        h_shrink = 1
        if abs(a-R) < 4/16:
            print("mesh refined")
            h_shrink = 0.5
        mesh = Mesh(geo.GenerateMesh (maxh=(h_shrink*0.04),quad_dominated=False))
        mesh.Curve(order)
       
        print("Solving for a = {0}".format(a))
        # reference solution for calculating the error
        ref_sol = PlaneWaveHomSolution(k,R,"sound-soft")
        l2_norm =  sqrt( Integrate ( InnerProduct(ref_sol,ref_sol),mesh,VOL ).real)
         
        rel_error_order = []
        fes_inner = H1(mesh,complex=True,order=order,dirichlet="inner-bnd")

        A_IE = A_IEs[a]
        B_IE = B_IEs[a]

        for N in Ns: 
            
            A_N = A_IE[N]
            B_N = B_IE[N]

            fes_surf = H1(mesh,order=order,complex=True,definedon=mesh.Boundaries("outer-bnd"))
            inf_outer = [fes_surf for i in range(A_N.shape[0]-1)]

            X = FESpace([fes_inner]+inf_outer)

            uX = X.TrialFunction() 
            vX = X.TestFunction() 

            f_X = LinearForm (X)
            f_X.Assemble()

            aX = BilinearForm(X, symmetric=False)
            aX += SymbolicBFI ( grad(uX[0])*grad(vX[0]) - k**2*uX[0]*vX[0] )

            for i in range(A_N.shape[0]):
                for j in range(A_N.shape[0]):
                    if abs(A_N[j,i]) > 1e-14 or abs(B_N[j,i]>1e-14):
                        graduX = grad(uX[i]).Trace()
                        gradvX = grad(vX[j]).Trace()  
                        aX += SymbolicBFI ( B_N[j,i]*graduX*gradvX + A_N[j,i]*uX[i]*vX[j]  ,definedon=mesh.Boundaries("outer-bnd"))
            aX.Assemble()

            gfu_X = GridFunction (X)

            res_X = gfu_X.vec.CreateVector()
            gfu_X.vec[:] = 0.0
            gfu_X.components[0].Set(ref_sol,BND)
            res_X.data = f_X.vec - aX.mat * gfu_X.vec
            invX = aX.mat.Inverse(freedofs=X.FreeDofs(), inverse="umfpack")
            gfu_X.vec.data += invX * res_X

            l2_err = sqrt ( Integrate (  InnerProduct(gfu_X.components[0] - ref_sol,gfu_X.components[0] - ref_sol), mesh, VOL ).real)
            rel_error_order.append(l2_err/l2_norm)
            print("N = {0},rel l2_err ={1} \n ".format(A_N.shape[0]-1,l2_err/l2_norm))

        rel_error[a] = np.array(rel_error_order)

    #solve and store the results for different positions of coupling boundary
    for a in a_s:
        SolveProblem(a)

    # store results in file for plotting

    fname = "plane-wave-a-relerror-"+weight_type+"-weights.dat"
    results = [np.array(Ns)]
    header_str = "N "
    for a in a_s:
        header_str += "a{0} ".format(a)
        results.append(rel_error[a])
    np.savetxt(fname =fname,
               X = np.transpose(results),
               header = header_str,
               comments = '')

    fname = "plane-wave-a-misfit-"+weight_type+"-weights.dat"
    results = [np.array(Ns)]
    header_str = "N "
    for a in a_s:
        header_str += "a{0} ".format(a)
        results.append(misfit_vals[a])
    np.savetxt(fname =fname,
               X = np.transpose(results),
               header = header_str,
               comments = '')

    fname = "plane-wave-a-uniform-err-"+weight_type+"-weights.dat"
    results = [np.array(Ns)]
    header_str = "N "
    for a in a_s:
        header_str += "a{0} ".format(a)
        results.append(uniform_errors[a])
    np.savetxt(fname =fname,
               X = np.transpose(results),
               header = header_str,
               comments = '')

    fname = "plane-wave-a-weightedl2-err-"+weight_type+"-weights.dat"
    results = [np.array(Ns)]
    header_str = "N "
    for a in a_s:
        header_str += "a{0} ".format(a)
        results.append(  weighted_l2_errors[a])
    np.savetxt(fname =fname,
               X = np.transpose(results),
               header = header_str,
               comments = '')

