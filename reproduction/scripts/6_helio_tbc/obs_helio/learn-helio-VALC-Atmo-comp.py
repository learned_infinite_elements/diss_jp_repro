from netgen.meshing import *
from netgen.csg import *
from netgen.meshing import Mesh as netmesh
from math import pi
import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse import csr_matrix
#from scipy.optimize import minimize,root,least_squares
#import scipy.sparse.linalg
from ngsolve import Mesh as NGSMesh
from cmath import sqrt as csqrt
import os,sys
file_dir = os.path.dirname(os.path.realpath(__file__))
upfolder_path=  os.path.abspath(os.path.join( os.path.dirname( __file__ ), '..' ))
sys.path.append(upfolder_path)
print(sys.path)
from compute_pot import local_polynomial_estimator,logder

#np.random.seed(seed=99)
np.random.seed(seed=1)

from ngsolve import *

import ceres_dtn as opt

ngsglobals.msg_level = 0
SetNumThreads(4)

#################################################### 
# parameters
####################################################

power_damping = True
atmospheric_models = ["VALC","Atmo"]
atmospheric_model = atmospheric_models[0] 

order_ODE = 8 # order of ODE discretization

for idx_choice in [0,1]:

    f_hzs = [0.0035,0.008]
    L_maxs = [2200,4950]
    f_hz,L_max = f_hzs[idx_choice],L_maxs[idx_choice]

    #a = 1.0 # coupling radius

    if atmospheric_model == "VALC": 
        a = 1.0007126
    #a = 1.0 # radius of trucation boundary
    else:
        a = 1.0007126

    L_spacing = 100 # take only DtN numbers with index % L_spacing == 0 
    spline_order = 1 # order of Spline approx. for coefficients
    R_max_ODE = 1.0033 # to how far out the ODE should be solved
    Nmax = 6 # maximal dofs for learned IE in radial direction
    Nrs = list(range(Nmax))
    show_plots = False

    prec = 5000

    L_min = 0 
    #L_max = 500
    L_range = range(L_min,L_max)

    L_range = np.arange(L_min,L_max+1)

    solver = "sparsecholesky"
    ansatz = "mediumSym"
    flags = {"max_num_iterations":10000,
         "check_gradients":False, 
         "use_nonmonotonic_steps":True,
         "minimizer_progress_to_stdout":False,
         "num_threads":4,
         "report_level":"Brief",
         "function_tolerance":1e-12,
         "parameter_tolerance":1e-12,
         "gradient_tolerance":1e-12}


    nodamping  = False
    sample_rr = np.linspace(a,R_max_ODE,400)
    sample_r = sample_rr.tolist()
    l_eval = [10,1000,5000]
    eval_lami = [l*(l+1)/a**2 for l in l_eval ]


    #################################################### 
    # some auxiliary functions
    ####################################################

    def sqrt_2branch(z):
        if z.real < 0 and z.imag <0:
            return - csqrt(z)
        else:
            return csqrt(z)

    def P_DoFs(M,test,basis):
        return (M[test,:][:,basis])


    def Make1DMesh_givenpts(mesh_r_pts):

        mesh_1D = netmesh(dim=1)
        pids = []   
        for r_coord in mesh_r_pts:
            pids.append (mesh_1D.Add (MeshPoint(Pnt(r_coord, 0, 0))))                     
        n_mesh = len(pids)-1
        for i in range(n_mesh):
            mesh_1D.Add(Element1D([pids[i],pids[i+1]],index=1))
        mesh_1D.Add (Element0D( pids[0], index=1))
        mesh_1D.Add (Element0D( pids[n_mesh], index=2))
        mesh_1D.SetBCName(0,"left")
        mesh_1D.SetBCName(1,"right")
        mesh_1D = NGSMesh(mesh_1D)    
        return mesh_1D

    def Make1DMesh_flex(R_min,R_max,N_elems=50):

        n1 = 20
        n2 = 70
        n3 = 30
        R1 = 0.8
        delta_R1 = R1-R_min
        R2 = 1.0
        delta_R2 = R2-R1
        delta_R3 = R_max-R2

        rrs = [] 
        mesh_1D = netmesh(dim=1)
        pids = []   
        for i in range(n1):
            new_r = R_min + delta_R1*i/n1
            rrs.append(new_r)
            pids.append (mesh_1D.Add (MeshPoint(Pnt(new_r, 0, 0))))                     
        for i in range(n2):
            new_r = R1 + delta_R2*i/n2
            rrs.append(new_r)
            pids.append (mesh_1D.Add (MeshPoint(Pnt(new_r,0, 0))))                     
        for i in range(n3+1):
            new_r = R2 + delta_R3*i/n3
            rrs.append(new_r)
            pids.append (mesh_1D.Add (MeshPoint(Pnt(new_r,0, 0))))
        print("rrs = ", rrs)
        n_mesh = len(pids)-1
        for i in range(n_mesh):
            mesh_1D.Add(Element1D([pids[i],pids[i+1]],index=1))
        mesh_1D.Add (Element0D( pids[0], index=1))
        mesh_1D.Add (Element0D( pids[n_mesh], index=2))
        mesh_1D.SetBCName(0,"left")
        mesh_1D.SetBCName(1,"right")
        mesh_1D = NGSMesh(mesh_1D)    
        return mesh_1D

    def new_initial_guess(l1_old,l2_old,ansatz):
        N = l1_old.shape[0]
        l1_guess = np.zeros((N+1,N+1),dtype='complex')
        l2_guess = np.zeros((N+1,N+1),dtype='complex')
        if ansatz in ["medium","full"]:
            l1_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
            l2_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
            l1_guess[:N,:N] = l1_old[:]
            l2_guess[:N,:N] = l2_old[:]
            l1_guess[N,N] = 1.0
        elif ansatz == "minimalIC":
            l1_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
            l1_guess[:N,:N] = l1_old[:]
            l2_guess[:N,:N] = l2_old[:]
            l1_guess[N,N] = -1e-7*(-1)**N + 0.1j
            #l1_guess[N,N] = 1e+9
            #l1_guess[N,N] = (np.random.rand(1) + 1j*np.random.rand(1))
            #l2_guess[0,N] = (np.random.rand(1) + 1j*np.random.rand(1))
        elif ansatz == "mediumSym":
            l1_guess[:N,:N] = l1_old[:]
            l2_guess[:N,:N] = l2_old[:]
            if N <= 2:
                l1_guess[0,N]  = l1_guess[N,0] = 1e-4*(np.random.rand() + 1j*np.random.rand())
                l2_guess[0,N] =  l2_guess[N,0] = 1e-4*(np.random.rand() + 1j*np.random.rand()) 
            else:
                l1_guess[0,N]  = l1_guess[N,0] = 1*1e-5*(np.random.rand() + 1j*np.random.rand())
                l2_guess[0,N] =  l2_guess[N,0] = 1*1e-5*(np.random.rand() + 1j*np.random.rand()) 
            #l1_guess[N,N] = -1e-7*(-1)**N + 0.1j
            l1_guess[N,N] = -100 - 10j
            #l2_guess[N,N] = 1.0
        else:
            l1_guess[:N,:N] = l1_old[:]
            l2_guess[:N,:N] = l2_old[:]
            l1_guess[N,N] = -100-100j
            l1_guess[0,N] = l1_guess[N,0] = 10**(3/2)*(np.random.rand(1) + 1j*np.random.rand(1))
        return l1_guess,l2_guess


    #################################################### 
    # load solar models and compute potential
    ####################################################

    rS,cS,rhoS,_,__,___ = np.loadtxt("../modelSinput.txt",unpack=True)
    cS = (10**-2)*cS
    rhoS = (10**3)*rhoS

    #if atmospheric_model == "VALC": 
    print("using Model S + VAL-C")
    rV,vV,rhoV,Tv  = np.loadtxt("../VALCinput.txt",unpack=True)

    # sound speed is not given in VAL-C model
    # calculate it from temperature using the ideal gas law
    gamma_ideal = 5/3 # adiabatic index
    mu_ideal = 1.3*10**(-3) # mean molecular weight 
    R_gas = 8.31446261815324 # universal gas constant 
    cV = np.sqrt(gamma_ideal*R_gas*Tv/mu_ideal)
    rhoV = (10**3)*rhoV

    # overlap interval
    rmin = rV[-1]
    rmax = rS[0]

    weightS = np.minimum(1, (rmax-rS)/(rmax-rmin))
    weightV = np.minimum(1, (rV-rmin)/(rmax-rmin))

    r = np.concatenate((rS,rV)) 
    ind = np.argsort(r)
    r = r[ind]
    c = np.concatenate((cS,cV))[ind]
    rho = np.concatenate((rhoS,rhoV))[ind]
    weight = np.concatenate((weightS,weightV))[ind]
    #else:
    #    print("using Model S + Atmo")
    #    ind = np.argsort(rS)
    #    r = rS[ind]
    #    c = cS[ind]
    #    rho = rhoS[ind]
    #    weight = np.ones(len(r))


        #weight = np.ones(len(weight))
        #alpha_decay = 1/45

    RSun = 6.963e10 # radius of the Sun in cm
    RSun =  6.96*10**8
    c0 = 6.855e5 # sound speed at interface [cm/s]
    H = 1.25e7 # density scale height in [cm]
    omega_c = 0.0052*2*pi # c0/(2*H) cut-off frequency in Herz

    rho_clean,drho,ddrho = logder(rho,r,weight,3)
    #rho_clean = logder(rho,r,weight,1)
    c_clean = logder(c,r,weight,1)
    idx1 = np.argmin(np.abs(r - a))
    alpha_infty = (-drho/rho_clean)[idx1]
    c_infty = c_clean[idx1]
    print("alpha_infty = ",  alpha_infty) 
    print("c_infty = ", c_infty)
    #input("")
    i0 = np.min( np.nonzero(r > 0.99)[0] ) 

    f,df,ddf = logder(1/np.sqrt(rho),r,weight,3)
    pot_rho = np.sqrt(rho_clean)*(ddf+2*df/r)

    pot_rho[0] = pot_rho[1] # treating the NaN value at r = 0

    pot_c = c_clean.copy()

    #################################################### 
    # BSpline approximation of c,rho and potential 
    ####################################################

    r_beg = r[0]
    pot_rho_beg = pot_rho[0]
    pot_c_beg = pot_c[0]
    r_end = r[-1]
    pot_rho_end = pot_rho[-1]
    pot_c_end = pot_c[-1]

    rho_beg = rho_clean[0]
    c_beg = c_clean[0]
    rho_end = rho_clean[-1]
    c_end = c_clean[-1]


    r = np.append([r_beg for i in range(spline_order)],r)
    pot_rho = np.append([pot_rho_beg for i in range(spline_order)],pot_rho)
    pot_c = np.append([pot_c_beg for i in range(spline_order)],pot_c)

    c_clean = np.append([c_beg for i in range(spline_order)],c_clean)
    rho_clean = np.append([rho_beg for i in range(spline_order)],rho_clean)

    r = np.append(r,[r_end for i in range(spline_order)])
    pot_rho = np.append(pot_rho,[pot_rho_end for i in range(spline_order)])
    pot_c = np.append(pot_c,[pot_c_end for i in range(spline_order)])

    c_clean = np.append(c_clean,[c_end for i in range(spline_order)])
    rho_clean = np.append(rho_clean,[rho_end for i in range(spline_order)])


    r = r.tolist()
    #print("r = ", r)
    pot_rho = pot_rho.tolist()
    pot_c = pot_c.tolist()
    c_clean = c_clean.tolist()
    #print("c_clean = ", c_clean)
    rho_clean = rho_clean.tolist()

    pot_rho_B = BSpline(spline_order,r,pot_rho)(x)
    pot_c_B = BSpline(spline_order,r,pot_c)(x)

    c_cleanB = BSpline(spline_order,r,c_clean)(x)
    rho_cleanB = BSpline(spline_order,r,rho_clean)(x)

    cBB = BSpline(spline_order,r,c_clean)
    rhoBB =BSpline(spline_order,r,rho_clean)
    eval_c = [cBB(pp) for pp in rS[::-1]]
    eval_c[-1] = cBB(rS[::-1][-1]-1e-11)
    #eval_c_filter = [c_filtered(pp) for pp in rS[::-1]]
    eval_rho = [rhoBB(pp) for pp in rS[::-1]]

    if show_plots:
        plt.semilogy(rS[::-1],eval_c[:len(rS)],label="cB")
        #plt.semilogy(rS[::-1],eval_c_filter[:len(rS)],label="c_filtered")
        plt.semilogy(rS[::-1],cS[::-1],label="original")
        plt.legend()
        plt.show()

        plt.semilogy(rS[::-1], np.abs(cS[::-1]-eval_c[:len(rS)])/np.abs(cS[::-1]) ,label="errorB")
        #plt.semilogy(rS[::-1], np.abs(cS[::-1]-eval_c_filter[:len(rS)])/np.abs(cS[::-1]) ,label="error_filter")
        plt.legend()
        plt.show()

        plt.semilogy(rS[::-1], np.abs(rhoS[::-1]-eval_rho[:len(rS)])/np.abs(rhoS[::-1]) ,label="error")
        plt.legend()
        plt.show()

    print("Computing mesh nodes according to local wavelength")
    mesh_r_pts = [] 
    c_w = BSpline(spline_order,r,c_clean)
    omega_max_inner = RSun*53.0*10**(-3) 
    #omega_max_inner = RSun*45.0*10**(-3) 
    omega_max_outer = RSun*128*10**(-3) 
    def get_lambda_r(r_coord,omega_max):
        local_lambda = 2*pi*c_w(r_coord)/omega_max
        print("r = {0}, local_lambda = {1}".format(r_coord,local_lambda))
        return local_lambda
    r_next = 1.0
    #while r_next > 0.35:
    while r_next > 0.15:
        local_lambda = get_lambda_r(r_next,omega_max_inner)
        r_next -= local_lambda
        mesh_r_pts.append(r_next)
    mesh_r_pts.append(0.0)
    mesh_r_pts = mesh_r_pts[::-1]

    mesh_r_pts.append(1.0) 
    if a > 1.0:
        mesh_r_pts.append(a) 
    mesh_pts_to_surface = mesh_r_pts.copy() 
    #mesh_pts_to_surface.append(1.0) 
    mesh_above_surface = [a] 

    n_outer = 10
    start_point = a
    for i in range(1,n_outer+1):
        new_point = start_point + (R_max_ODE - start_point)*i/n_outer
        mesh_r_pts.append(new_point) 
        mesh_above_surface.append(new_point)

    print("mesh_r_pts = ", mesh_r_pts) 
    print("len(mesh_r_pts) = ", len(mesh_r_pts)) 

    print("mesh_pts_to_surface = ", mesh_pts_to_surface) 
    print("mesh_above_surface = ", mesh_above_surface) 

    #################################################### 
    # damping model
    ####################################################

    def get_damping(f_hz,lami=0):

        omega_f = f_hz*2*pi*RSun
        gamma0 = 2*pi*4.29*1e-6*RSun
        omega0 = 0.003*2*pi*RSun
        if f_hz < 0.0053:
            gamma = gamma0*(omega_f/omega0)**(5.77)
        else:
            gamma = gamma0*(0.0053*2*pi*RSun/omega0)**(5.77)

        #print("gamma_power = {0},gamma_fixed = {1}".format(gamma,2*pi*20*1e-6*RSun))
        if not power_damping:
            gamma = 2*pi*20*1e-6*RSun

        #gamma = 2*pi*10*1e-6*RSun
        #gamma *= 1+ lami**0.35
        #gamma = gamma0*(0.003*2*pi*RSun/omega0)**(5.77)

        #if nodamping:
        #    gamma = 0
        omega  = sqrt_2branch(1+2*1j*gamma)*omega_f

        omega_squared = omega_f**2+2*1j*omega_f*gamma

        prefac = 1 + (  (omega_f/RSun-0.0033*2*pi)/( 0.5*0.0012*2*pi )   )**2
        prefac = 1/prefac
        return omega,omega_squared

    #################################################### 
    # PML 
    ####################################################

    def get_PML(f_hz):
        f0 = 10**3*f_hz
        #f0 = 10**3*0.003
        rad_min = R_start_PML
        rad_max = R_max_ODE
        C_PML = 100
        #C_PML = 0.01
        plm_min = rad_min
        w = rad_max-plm_min
        physpml_sigma = IfPos(x-plm_min,IfPos(rad_max -x,C_PML/w*(x-plm_min)**2/w**2,0),0)
        G_PML =  (1j/f0)*(C_PML/3)*(w/C_PML)**(3/2)*physpml_sigma**(3/2)
        pml_phys = pml.Custom(trafo=x+(1j/f0)*(C_PML/3)*(w/C_PML)**(3/2)*physpml_sigma**(3/2), jac = -1 - 1j*physpml_sigma/f0 )
        return pml_phys


    #################################################### 
    # Compute DtN numbers by solving ODEs
    ####################################################


    DtN_continuous_eigvals = [] 
    filtered_lam_all = []
    DtN_ODE_filtered_all = [] 
    #dtn_atmo = [] 

    #print("L_range = ", L_range)
    lam_continuous = np.array([l*(l+1)/a**2 for l in np.arange(L_max) ] ) 

    def get_ODE_DtN(lam_continuous,dtn_nr,use_PML=False):

        mesh_1D = Make1DMesh_givenpts(mesh_above_surface)  
        fes_DtN = H1(mesh_1D, complex=True,  order=order_ODE, dirichlet=[1])
        u_DtN,v_DtN = fes_DtN.TnT()
        gfu_DtN = GridFunction (fes_DtN)

        f_DtN = LinearForm(fes_DtN)
        f_DtN.Assemble()
        r_DtN = f_DtN.vec.CreateVector()
        frees_DtN = [i for i in range(1,fes_DtN.ndof)]

        if use_PML:
            pml_phys = get_PML(f_hz)
            mesh_1D.SetPML(pml_phys,definedon=1)

        for lami in lam_continuous:    

            omega, omega_squared = get_damping(f_hz,lami)
            pot_1D = pot_rho_B - omega_squared/pot_c_B**2
            
            a_DtN = BilinearForm (fes_DtN, symmetric=False)
            a_DtN += SymbolicBFI((x**2)*grad(u_DtN)*grad(v_DtN)) 
            a_DtN += SymbolicBFI(( lami.item()*a**2 +  pot_1D*x**2 )*u_DtN*v_DtN)
            a_DtN.Assemble()

            gfu_DtN.vec[:] = 0.0
            gfu_DtN.Set(1.0, BND)
            r_DtN.data = f_DtN.vec - a_DtN.mat * gfu_DtN.vec
            gfu_DtN.vec.data += a_DtN.mat.Inverse(freedofs=fes_DtN.FreeDofs(),inverse=solver) * r_DtN
           
            rows_DtN,cols_DtN,vals_DtN = a_DtN.mat.COO()
            A_DtN = csr_matrix((vals_DtN,(rows_DtN,cols_DtN)))
            val = -P_DoFs(A_DtN,[0],frees_DtN).dot(gfu_DtN.vec.FV().NumPy()[frees_DtN])-P_DoFs(A_DtN,[0],[0]).dot(gfu_DtN.vec.FV().NumPy()[0])

            dtn_nr.append(-val.item()/a**2)

        if use_PML:
            mesh_1D.UnSetPML(1)
    #np.savetxt('ODE_dtn_nr.out',dtn_nr,delimiter=',')


    def get_atmo_dtn():
        print("Getting Atmo dtn numbers from whittaker function")
        from ngs_arb import Whitw_deriv_over_Whitw,Whitw_deriv_over_Whitw_par,Whitw_deriv_over_Whitw_spar
        print("f_hz = {0}".format(f_hz))
        sigma, sigma_squared = get_damping(f_hz)
        k_squared = sigma_squared/c_infty**2 - 0.25*alpha_infty**2
        k_omega = sqrt_2branch(k_squared)
        eta = alpha_infty/(2*k_omega)
        chi = -1j*eta
        z_arg = -2*1j*k_omega*a
        wh_qout =  np.array(Whitw_deriv_over_Whitw_spar(chi,z_arg,L_max-1,prec)) 
        DtN_atmo =  1/a + 2*1j*k_omega*wh_qout
        print("dtn ", DtN_atmo[:-10])
        return DtN_atmo.tolist().copy()
        #print("dtn_nr =", dtn_nr)

        print("Compute reference DtN numbers by solving ODEs")
    get_ODE_DtN(lam_continuous,DtN_continuous_eigvals)

    dtn_atmo = get_atmo_dtn()
    #print("dtn_atmo =", dtn_atmo)

    if show_plots:
        plt.plot(np.arange(L_max) ,np.array(DtN_continuous_eigvals).real,label='VAL-C' )
        plt.plot(np.arange(L_max) ,np.array(dtn_atmo).real,label='Atmo' )
        plt.title("analytic dtn function (real)")
        plt.legend()
        plt.show()

    fname_ref = "dtn-VALC-ref-{0}Hz.dat".format(f_hz)
    np.savetxt(fname=fname_ref,
           X=np.transpose([ np.arange(L_max), np.array(DtN_continuous_eigvals).real,np.array(DtN_continuous_eigvals).imag  ]), 
           header = 'l zetaReal zetaImag',
           comments='' 
          )

    fname_ref = "dtn-Atmo-ref-{0}Hz.dat".format(f_hz)
    np.savetxt(fname=fname_ref,
           X=np.transpose([ np.arange(L_max), np.array(dtn_atmo).real,np.array(dtn_atmo).imag  ]), 
           header = 'l zetaReal zetaImag',
           comments='' 
          )

    if idx_choice == 1:

        #################################################### 
        # Filter out DtN numbers: 
        # take only those l which either fulfill
        # - l % L_spacing == 0 (equidistant sampling to 
        #   cover general behavior of dtn function
        # - those l where the dtn function has a large 
        #   gradient ( grad_dtn > 5*np.average(grad_dtn))
        ####################################################

        dtn_a = np.array(DtN_continuous_eigvals)
        lam_a = np.array(lam_continuous)
        diff_dtn = dtn_a[1:] - dtn_a[:-1]
        diff_lam = lam_a[1:] - lam_a[:-1]
        grad_dtn = np.abs( diff_dtn / diff_lam) 
        #print("np.average(grad_dtn) = ", np.average(grad_dtn) )
        ind_large = np.nonzero(grad_dtn > 2*np.average(grad_dtn ))[0]
        #print("ind_larg = ", ind_large)
        ind_large = 1 + ind_large 

        if show_plots:
            plt.plot(np.arange(L_max)[1:] ,grad_dtn,label='grad' )
            plt.plot(np.arange(L_max)[ind_large] ,grad_dtn[ind_large],label='selected idx',marker='.' )
            plt.legend()
            plt.show()

        L_filter = [] 
        w_weight = [] 
        alpha_decay = 8*L_spacing
        for l in np.arange(L_max):
            #if l in list(range(3370,3400)) or l in list(range(5490,5530)) or l in list(range(7412,7430)):
            if l in ind_large:
                L_filter.append(l)
                w_weight.append(exp(-l/alpha_decay))
            else:
                if l % L_spacing == 0:
                    L_filter.append(l)
                    w_weight.append(exp(-l/alpha_decay))

        print("len(L_filter)", len(L_filter))
        #print("L_filter = ", L_filter)
        #print("w_weight = ", w_weight)
        w_weight = np.array(w_weight)

        filtered_lam_all = np.array(lam_continuous)[L_filter]
        DtN_ODE_filtered_all = np.array(DtN_continuous_eigvals)[L_filter]

        #fname_interp = "dtn-VALC-interp-{0}Hz.dat".format(f_hz)
        #np.savetxt(fname=fname_interp,
        #           X=np.transpose([filtered_lam_all.real, DtN_ODE_filtered_all.real ]), 
        #           header = 'lam zeta',
        #           comments='' 
        #          )

        #################################################### 
        # Solve the minimization problem
        ####################################################

        lam = filtered_lam_all
        dtn_ref = DtN_ODE_filtered_all
        weights = w_weight

        A_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
        B_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
        l_dtn = opt.learned_dtn(lam,dtn_ref,weights**2)

        zeta_approx_evals = []

        A_IE = []
        B_IE = []

        for N in Nrs:
         
            l_dtn.Run(A_guess,B_guess,ansatz,flags)
            A_IE.append(A_guess.copy()), B_IE.append(B_guess.copy())
            A_guess,B_guess = new_initial_guess(A_IE[N],B_IE[N],ansatz)

            if ansatz in ["medium","minimalIC"]:
                learned_poles = [-A_IE[N][i,i]/B_IE[N][i,i] for i in range(1,A_IE[N].shape[0])]
                print("learned_poles = ", learned_poles)

            zeta_approx_plot = np.zeros(len(lam),dtype=complex)
            opt.eval_dtn_fct(A_IE[N],B_IE[N],np.array(lam,dtype=float),zeta_approx_plot)
            zeta_approx_evals.append(zeta_approx_plot)
         
            if show_plots:
                plt.plot(filtered_lam_all,DtN_ODE_filtered_all.real ,label="analytic")
                plt.plot(filtered_lam_all,zeta_approx_plot.real,label="app",linestyle="dashed")
                plt.xlabel("$\lambda$")
                plt.title("zeta")
                plt.legend()
                plt.show()
                
                plt.plot(filtered_lam_all,DtN_ODE_filtered_all.imag ,label="analytic")
                plt.plot(filtered_lam_all,zeta_approx_plot.imag,label="app",linestyle="dashed")
                plt.xlabel("$\lambda$")
                plt.title("zeta")
                plt.legend()
                plt.show()

        plot_collect_real = [ np.array(L_filter) ]
        plot_collect_imag = [ np.array(L_filter) ]
        header_str = 'l'
        for N_r,zeta_approx_plot in zip(Nrs,zeta_approx_evals):
            header_str += " N{0}".format(N_r)
            plot_collect_real.append(zeta_approx_plot.real)
            plot_collect_imag.append(zeta_approx_plot.imag)

        fname_learned_real ="learned-dtn-VALC-real-{0}Hz.dat".format(f_hz)
        fname_learned_imag ="learned-dtn-VALC-imag-{0}Hz.dat".format(f_hz)

        np.savetxt(fname=fname_learned_real,
                   X=np.transpose(plot_collect_real),
                   header=header_str,
                   comments='')
        np.savetxt(fname=fname_learned_imag,
                   X=np.transpose(plot_collect_imag),
                   header=header_str,
                   comments='')

        #################################################### 
        # Compute poles of dtn 
        ####################################################
        from GRPF import Poles_and_Roots 
        from cmath import sqrt as csqrt

        def fun(z): 
            DtN_list = [] 
            get_ODE_DtN([z],DtN_list)
            return DtN_list[0]

        param = {} # for searching poles
        param["Tol"] = 1e-9
        param["visual"] = 1 
        param["ItMax"] = 25
        param["NodesMax"] = 500000
        param["xrange"] = [500*(500+1),6000*(6000+1)]
        param["yrange"] = [-200000,1200000]
        param["h"] = 100000
        result = Poles_and_Roots(fun,param)

        def lam_to_ell(z):
            return -0.5+csqrt( 0.25+a**2*z)

        poles = result["poles"] 
        print("poles of dtn function")
        l_poles = [] 
        for pole in poles:
            l_pole = lam_to_ell(pole) 
            l_poles.append(l_pole)
            print("l = {0}".format(lam_to_ell(pole) ))
        l_poles = np.array(l_poles) 
        fname_analytic_poles = "poles_dtn_VALC_analytic_{0}Hz.dat".format(f_hz)
        header_str_analytic_poles = "poles_real poles_imag"
        plot_collect_analytic_poles = [l_poles.real,l_poles.imag]
        np.savetxt(fname= fname_analytic_poles,
                   X= np.transpose(plot_collect_analytic_poles ) ,
                   header= header_str_analytic_poles ,
                   comments='')

        roots = result["roots"] 
        print("roots of dtn function")
        for root in roots:
            print("l = {0}".format(lam_to_ell(root) ))

        print("learned poles:")
        for N in Nrs:
            print("N = {0}".format(N))
            learned_poles = [-A_IE[N][i,i]/B_IE[N][i,i] for i in range(1,A_IE[N].shape[0])]
            l_learned_poles = []
            for pole in learned_poles:
                l_learned_poles.append(lam_to_ell(pole))
                print("l = {0}".format(lam_to_ell(pole)))
            l_learned_poles = np.array(l_learned_poles) 
            fname_learned_poles = "poles_dtn_VALC_learned_{0}Hz_N{1}.dat".format(f_hz,N)
            header_str_learned_poles = "poles_real poles_imag"
            plot_collect_learned_poles = [l_learned_poles.real,l_learned_poles.imag]
            np.savetxt(fname= fname_learned_poles,
                       X= np.transpose(plot_collect_learned_poles ) ,
                       header= header_str_learned_poles ,
                       comments='')

