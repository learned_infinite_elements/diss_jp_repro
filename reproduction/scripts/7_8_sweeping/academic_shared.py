from netgen.meshing import *
from netgen.csg import *
from ngsolve import *
from math import pi,ceil
import scipy.sparse.linalg
import numpy as np
from ngsolve.solvers import GMRes
from decimal import Decimal
from scipy.linalg import lu_factor,lu_solve
from scipy.sparse import csr_matrix
import sys,os,copy 
sys.path.append(os.path.realpath(''))
from sweeping_geom import HybridLayered_Mesh,Layered_Mesh,Make1DMesh,Make1DMesh_refined,HybridLayered_Mesh_gen,HybridLayered_Mesh_U,HybridLayered_Mesh_halfDisk,Make1DMesh_givenpts
from sweeping_helper import BitArrayFromList,P_DoFs,Layered_Decomposition_cartesian,DOSM_IFassemble,DOSM,draw_mesh_tikz
import ceres_dtn as opt
import matplotlib.pyplot as plt
#from compute_pot import local_polynomial_estimator,logder
#from helio_helper import SolarModel
from xfem import * 

ngsglobals.msg_level = 0


def new_initial_guess(l1_old,l2_old,ansatz):
    N = l1_old.shape[0]
    l1_guess = np.zeros((N+1,N+1),dtype='complex')
    l2_guess = np.zeros((N+1,N+1),dtype='complex')
    if ansatz in ["medium","full"]:
        l1_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        l2_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        l1_guess[:N,:N] = l1_old[:]
        l2_guess[:N,:N] = l2_old[:]
        l1_guess[N,N] = 1.0
    elif ansatz == "minimalIC":
        l1_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        l1_guess[:N,:N] = l1_old[:]
        l2_guess[:N,:N] = l2_old[:]
        l1_guess[N,N] = (-100-100j)
        l2_guess[0,N] = 1e-3*(np.random.rand(1) + 1j*np.random.rand(1))         
    elif ansatz == "mediumSym":
        l1_guess[:N,:N] = l1_old[:]
        l2_guess[:N,:N] = l2_old[:]
        l1_guess[0,N]  = l1_guess[N,0] = 1e-3*(np.random.rand() + 1j*np.random.rand())
        l2_guess[0,N] =  l2_guess[N,0] = 1e-3*(np.random.rand() + 1j*np.random.rand()) 
        l1_guess[N,N] = (16/16)*(-100-100j)
        #A_guess[N,N] = -100-100j
        l2_guess[N,N] = 1.0
    return l1_guess,l2_guess


def calc_dtn_nr_layer(layer_nr,lam,problem_config):
    
    use_PML = problem_config["use_PML"]
    pml_1D =  problem_config["pml_1D"]
    omega = problem_config["omega"]
    c_1D = problem_config["c_1D"]
    r_layer =  problem_config["r_layer"]
    order_ODE = problem_config["order_ODE"]
    bonus_intorder = problem_config["bonus_intorder"]
    
    dtn_nr_layer = []

    mesh_1D = Make1DMesh(layer_nr,r_layer)
    
    fes_DtN = H1(mesh_1D, complex=True,  order=order_ODE, dirichlet=[1])
    u_DtN,v_DtN = fes_DtN.TnT()
    gfu_DtN = GridFunction (fes_DtN)
    f_DtN = LinearForm(fes_DtN)
    f_DtN.Assemble()
    r_DtN = f_DtN.vec.CreateVector()
    frees_DtN = [i for i in range(1,fes_DtN.ndof)]

    for idx_lami,lami in enumerate(lam):
        if use_PML:
            mesh_1D.SetPML(pml_1D,definedon=1)
        a_DtN = BilinearForm (fes_DtN, symmetric=False, check_unused=False)
         
        a_DtN += SymbolicBFI( grad(u_DtN)*grad(v_DtN), bonus_intorder=  bonus_intorder)  
        a_DtN += SymbolicBFI(  lami.item()*u_DtN*v_DtN, bonus_intorder= bonus_intorder)
        a_DtN += SymbolicBFI(  (-1)*(omega**2/(c_1D**2)  )*u_DtN*v_DtN, bonus_intorder= bonus_intorder)
        
        a_DtN.Assemble()

        if use_PML:
            mesh_1D.UnSetPML(1)
        
        gfu_DtN.vec[:] = 0.0
        gfu_DtN.Set(1.0, BND)
        r_DtN.data = f_DtN.vec - a_DtN.mat * gfu_DtN.vec
        gfu_DtN.vec.data += a_DtN.mat.Inverse(freedofs=fes_DtN.FreeDofs()) * r_DtN
        
        rows_DtN,cols_DtN,vals_DtN = a_DtN.mat.COO()
        A_DtN = scipy.sparse.csr_matrix((vals_DtN,(rows_DtN,cols_DtN)))
        val1 = P_DoFs(A_DtN,[0],frees_DtN).dot(gfu_DtN.vec.FV().NumPy()[frees_DtN])
        val2 = P_DoFs(A_DtN,[0],[0]).dot(gfu_DtN.vec.FV().NumPy()[0])
        val = -P_DoFs(A_DtN,[0],frees_DtN).dot(gfu_DtN.vec.FV().NumPy()[frees_DtN])-P_DoFs(A_DtN,[0],[0]).dot(gfu_DtN.vec.FV().NumPy()[0])

        dtn_nr_layer.append(-val.item())
    
    return dtn_nr_layer



def precompute_DtN_coeff(decomp,problem_config,return_mass_layer = False):
        
    r_layer =  problem_config["r_layer"]
    use_discrete_eigenvalues = problem_config["use_discrete_eigenvalues"]
    bonus_intorder = problem_config["bonus_intorder"]

    DtN_coeff_layer = [ [] for i in range(decomp.n_layers)]  
    DtN_PML_coeff_layer = [ [] for i in range(decomp.n_layers)]  
    lam_layer = [ None for i in range(decomp.n_layers)] 
    lam_vec_layer = [ None for i in range(decomp.n_layers)] 

    if return_mass_layer: 
         mass_layer = [ None for i in range(decomp.n_layers)] 

    output_DtN_eigenbasis_on_layer = None
    if "output_DtN_eigenbasis_on_layer" in problem_config: 
        output_DtN_eigenbasis_on_layer = problem_config["output_DtN_eigenbasis_on_layer"]
    #C_phys = C_PML
    #R_max = r_layer[-1]

    for layer_nr in range(1,decomp.n_layers):

        if_idx = layer_nr
        R_min = r_layer[-1-layer_nr]
        

        lam = np.array( [ (k*2*pi)**2 for k in range( problem_config["lam_max"]) ] ,dtype=complex) 
        lam_layer[layer_nr] = lam.copy()

        if use_discrete_eigenvalues or output_DtN_eigenbasis_on_layer:
            print("computing discrete eigenvalues")
            u = decomp.fes.TrialFunction() 
            v = decomp.fes.TestFunction() 
            ba_dofs = BitArrayFromList(decomp.IF2dof[if_idx],decomp.mesh,decomp.fes,elements=False)
            m_IF = BilinearForm (decomp.fes, symmetric=False, check_unused=False)
            m_IF += SymbolicBFI ( u*v, definedon=decomp.mesh.Boundaries("layer"+str(if_idx)+"-bnd"),bonus_intorder =  bonus_intorder )
            m_IF.Assemble()
            k_IF = BilinearForm (decomp.fes, symmetric=False, check_unused=False)
            k_IF += SymbolicBFI ( grad(u).Trace()*grad(v).Trace(), definedon=decomp.mesh.Boundaries("layer"+str(if_idx)+"-bnd"), bonus_intorder =  bonus_intorder) 
            k_IF.Assemble()
            
            rows_m,cols_m,vals_m = m_IF.mat.COO()
            M_if = P_DoFs(csr_matrix((vals_m,(rows_m,cols_m))),decomp.IF2dof[if_idx],decomp.IF2dof[if_idx]).todense() 
            rows_k,cols_k,vals_k = k_IF.mat.COO()
            K_if = P_DoFs(csr_matrix((vals_k,(rows_k,cols_k))),decomp.IF2dof[if_idx],decomp.IF2dof[if_idx]).todense()

            if return_mass_layer: 
                mass_layer[layer_nr] = M_if

            lam_disc,lam_vec =  scipy.linalg.eig(K_if, b=M_if, left=False, right=True, overwrite_a=False, overwrite_b=False, check_finite=True)
            
            # sorting eigenpairs 
            idx_sorted = np.argsort(lam_disc)
            lam_disc = lam_disc[idx_sorted] 
            lam_vec = lam_vec[:,idx_sorted]

            lam_diag = np.diag(lam_disc)
            lam_vec = lam_vec @ np.diag(  1/np.sqrt(np.diag( lam_vec.conj().T @ (M_if @ lam_vec))) )
           
            if use_discrete_eigenvalues: 
                lam_layer[layer_nr] = lam_disc.copy()
            lam_vec_layer[layer_nr] = lam_vec.copy()

        #else:
        #    lam = np.array( [ (k*2*pi)**2 for k in range( problem_config["lam_max"]) ] ,dtype=complex) 
        #    lam_layer[layer_nr] = lam.copy()

        DtN_coeff_layer[layer_nr] = calc_dtn_nr_layer(layer_nr,lam,problem_config).copy()
        
     
    if return_mass_layer: 
        return DtN_coeff_layer,lam_layer,lam_vec_layer,mass_layer
    else:
        return DtN_coeff_layer,lam_layer,lam_vec_layer



class LearnedInverse(BaseMatrix):
 
    def __init__(self,gfu_X,invX):
        super(LearnedInverse, self).__init__()
        self.gfu_X = gfu_X
        self.invX = invX
        self.res = gfu_X.vec.CreateVector()
        self.nfes = len(self.gfu_X.components[0].vec)
    
    def IsComplex(self):
        return True

    def Height(self):
        return len(gfu.vec)

    def Width(self):
        return len(gfu.vec)
    
    def Mult(self, x, y):
        self.gfu_X.vec[:] = 0.0
        self.gfu_X.components[0].vec.data = x
        self.res.data = self.gfu_X.vec
        self.gfu_X.vec.data = self.invX * self.res
        y.data = self.gfu_X.components[0].vec 

class LearnedDtN():
 
    def __init__(self,v_store,decomp,layer,proj_IF,proj_outer,mat,outer_inverse=None):
        self.v_store = v_store
        self.v_store1 = self.v_store.CreateVector()
        self.decomp = decomp
        self.layer = layer 
        self.IF_dofs_nr = self.decomp.IF2dof[self.layer]
        self.proj_IF = proj_IF
        self.proj_outer = proj_outer
        self.mat = mat 
        self.outer_inverse = outer_inverse
     
    def Apply(self, v_in, v_out):

        self.v_store.FV().NumPy()[self.IF_dofs_nr]  = v_in.FV().NumPy()[self.decomp.IF2dof[self.layer]]
        self.v_store.data -= self.proj_IF * self.v_store
        self.v_store1.data = self.mat * self.v_store
        self.v_store1.data -= self.proj_IF * self.v_store1
        v_out.FV().NumPy()[self.decomp.IF2dof[self.layer]] = self.v_store1.FV().NumPy()[self.IF_dofs_nr]

        if self.outer_inverse:
            #print("applying self.outer_inverse")
            self.v_store1.data = self.mat * self.v_store
            self.v_store1.data -= self.proj_outer * self.v_store1
            self.v_store.data = self.outer_inverse * self.v_store1
            self.v_store.data -= self.proj_outer * self.v_store
            self.v_store1.data = self.mat * self.v_store 
            self.v_store1.data -= self.proj_IF * self.v_store1

            v_out.FV().NumPy()[self.decomp.IF2dof[self.layer]] -= self.v_store1.FV().NumPy()[self.IF_dofs_nr]


def Learn_dtn_function(decomp,problem_config,spectral_info,output_learned_poles=False): 

    Lones_layer = [None for i in range(decomp.n_layers)] 
    Ltwos_layer = [None for i in range(decomp.n_layers)] 
    learned_dtn_layer = [None for i in range(decomp.n_layers)]

    lam_layer = spectral_info["lam_layer"]
    lam_vec_layer = spectral_info["lam_vec_layer"]
    DtN_coeff_layer = spectral_info["DtN_coeff_layer"]

    for layer in range(1,decomp.n_layers):

        lam = lam_layer[layer]
        dtn_nr = DtN_coeff_layer[layer]

        Lone = []
        Ltwo = [] 
        dtn_learned = [] 
        relative_residuals = []

        np.random.seed(seed=123)
        
        l1_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
        l2_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
        Lmax = len(lam)
        
        #weights = np.array([10**7* exp(-l*2/10) for l in range(Lmax)])
        if problem_config["use_PML"]:
            weights = np.array([1 for l in range(Lmax)])
            lam_filtered = lam
            dtn_nr_filtered = dtn_nr 
        else:
            
            #weights = np.array([10**1* exp(-l*2/40) for l in range(Lmax)])
            weights = np.array([10**1* exp(-l*5/problem_config["omega"]) for l in range(Lmax)])
            lam_filtered = lam
            dtn_nr_filtered = dtn_nr 
            
        #print("weights = ", weights)
        final_res = np.zeros(len(lam),dtype=float)
        l_dtn = opt.learned_dtn(lam_filtered,dtn_nr_filtered,weights**2)

        for N in problem_config["Ns"]: 
            l_dtn.Run(l1_guess,l2_guess,problem_config["ansatz"],problem_config["flags"],final_res)
            Lone.append(l1_guess.copy()), Ltwo.append(l2_guess.copy()),relative_residuals.append(final_res.copy())
            dtn_approx = np.zeros(len(lam),dtype='complex')
            lam_sample_small = lam.real.copy()
            opt.eval_dtn_fct(Lone[N],Ltwo[N],lam_sample_small,dtn_approx)
            dtn_learned.append(dtn_approx)
            l1_guess,l2_guess = new_initial_guess(Lone[N],Ltwo[N],problem_config["ansatz"])

        Lones_layer[layer] = Lone
        Ltwos_layer[layer] = Ltwo
        learned_dtn_layer[layer] = dtn_learned
        if len(problem_config["Ns"]) > 1:
            print("relative_residuals, avg = {0}, max = {1}  = ".format(np.average(relative_residuals[-1]),np.max( relative_residuals[-1]) ) )

        if problem_config["output_dtn_on_layer"] and layer == problem_config["output_dtn_on_layer"]:
            lam_sample = problem_config["lam_sample"] 
            dtn_nr_sample = problem_config["dtn_nr_sample"]  
            learned_dtn_output_layer = []
            plot_collect = [lam_sample.real,np.array(dtn_nr_sample).real]
            header_str = "lam dtn "
            N_start = 6
            for N in problem_config["Ns"]: 
                dtn_approx = np.zeros(len(lam_sample),dtype='complex')
                print("--------------------------")
                opt.eval_dtn_fct(Lones_layer[layer][N],Ltwos_layer[layer][N],lam_sample.copy(),dtn_approx,False)
                learned_dtn_output_layer.append(dtn_approx)
                if N  >= N_start:
                    header_str += "N{0} ".format(N)
                    plot_collect.append(dtn_approx.real)
            
            if problem_config["use_PML"]:
                fname_learned = "learned_dtn_omega{0}_PMLbc_{1}_real.dat".format(problem_config["omega"], problem_config["type_str"])
                fname_dtnev = "dtn_at_eigenv_omega{0}_PMLbc_{1}_real.dat".format(problem_config["omega"], problem_config["type_str"])

            else:
                fname_learned = "learned_dtn_omega{0}_Neumannbc_{1}_real.dat".format(problem_config["omega"], problem_config["type_str"])
                fname_dtnev = "dtn_at_eigenv_omega{0}_Neumannbc_{1}_real.dat".format(problem_config["omega"], problem_config["type_str"])
            
            np.savetxt(fname=fname_learned,
               X=np.transpose(plot_collect),
               header=header_str,
               comments='')

            plot_collect_ev = [lam.real,np.array(dtn_nr).real]
            header_ev = "lam dtn"
            np.savetxt(fname=fname_dtnev ,
               X=np.transpose(plot_collect_ev),
               header=header_ev,
               comments='')


            if problem_config["show_dtn_approx"]:
                
                #plt.plot(lam_layer[layer_idx],np.array(DtN_coeff_layer[layer_idx]).real,label='dtn',linewidth=4,color='lightgray' )
                plt.plot(lam_sample,np.array(dtn_nr_sample).real,label='dtn',linewidth=4,color='lightgray' )
                plt.plot(lam,np.array(dtn_nr).real,label='dtn-samples',linewidth=2,color='b',linestyle="None",marker='x' )
                #dtn_approx = learned_dtn_layer[layer_idx] 
                
                for N,linestyle in zip(problem_config["Ns"],['solid','solid','solid','solid','dotted','dotted','dashed','dashed','dashed','dashed','dashed','dashed','dashed','dashed','dotted','dotted']):
                    if N % 2 == 0 and N > 5:
                        plt.plot(lam_sample,np.array(learned_dtn_output_layer[N]).real,label='N={0}'.format(N),linewidth=2,linestyle=linestyle) 
                        #plt.plot(lam_layer[layer_idx],np.array(dtn_approx[N]).real,label='N={0}'.format(N),linewidth=2,linestyle=linestyle) 
                plt.xlabel("$\\lambda$")
                plt.title("Real part")
                plt.legend()
                plt.show()
    
                plt.plot(lam_sample,np.array(dtn_nr_sample).imag,label='dtn',linewidth=4,color='lightgray' )
                plt.plot(lam,np.array(dtn_nr).imag,label='dtn-samples',linewidth=2,color='b',linestyle="None",marker='x' ) 
                for N,linestyle in zip(problem_config["Ns"],['solid','solid','solid','solid','dotted','dotted','dashed','dashed','dashed','dashed','dashed','dashed','dashed','dashed','dotted','dotted']):
                    if N % 2 == 0 and N > 5:
                        plt.plot(lam_sample,np.array(learned_dtn_output_layer[N]).imag,label='N={0}'.format(N),linewidth=2,linestyle=linestyle) 
                plt.xlabel("$\\lambda$")
                plt.title("Imaginary part")
                plt.legend()
                plt.show()
     
    if output_learned_poles: 
        learned_poles_layer = np.array([-Lones_layer[output_learned_poles][-1][j,j] for j in range(1, Lones_layer[output_learned_poles][-1].shape[0])])
        learned_poles_layer = learned_poles_layer[np.argsort(learned_poles_layer.imag)]
        print("learned_poles_layer =" , learned_poles_layer)
        fname_learned_poles = "academic_Neumann_poles_learned.dat"
        plot_collect_learned_poles = [learned_poles_layer.real,learned_poles_layer.imag]
        header_str_learned_poles = "poles_real poles_imag"

        np.savetxt(fname=fname_learned_poles,
            X=np.transpose(plot_collect_learned_poles),
            header=header_str_learned_poles,
            comments='')

    return Lones_layer,Ltwos_layer,learned_dtn_layer


def get_LearnedDtN_and_ext_inv(decomp,problem_config,N_fixed,Lones_layer,Ltwos_layer): 

    layer_and_truncated_ext_inv = [None for i in range(decomp.n_layers)]
    LearnedDtNs  = [None for i in range(decomp.n_layers)]

    pml_phys =  problem_config["pml_phys"]

    u = decomp.fes.TrialFunction() 
    v = decomp.fes.TestFunction() 
    for layer in range(decomp.n_layers):

        if layer == 0: 
            if problem_config["use_PML"]:
                decomp.mesh.SetPML(problem_config["pml_phys"],definedon=problem_config["definedon_PML"])
            ba_els = BitArrayFromList(decomp.layer_and_outer_el[layer],decomp.mesh,decomp.fes,elements=True) 
            ba_dofs = BitArrayFromList(decomp.layer_and_outer_dof[layer],decomp.mesh,decomp.fes,elements=False)

            a_layer_and_outer =  RestrictedBilinearForm(decomp.fes, "a_layer_and_outer", check_unused=False,
                           element_restriction=ba_els,
                           symmetric = problem_config["is_symmetric"])
            a_layer_and_outer += SymbolicBFI ( grad(u)*grad(v) -  (problem_config["omega"]**2/problem_config["c"]**2)*u*v  ,definedonelements= ba_els,bonus_intorder =  problem_config["bonus_intorder"])  
            a_layer_and_outer.Assemble() 
            
            if problem_config["use_PML"]:
                decomp.mesh.UnSetPML(problem_config["definedon_PML"])
            layer_and_truncated_ext_inv[layer] = a_layer_and_outer.mat.Inverse(ba_dofs,inverse=problem_config["solver"])
        else: 
            ba_els = BitArrayFromList(decomp.layer2el[layer],decomp.mesh,decomp.fes,elements=True)
            
            L_one = Lones_layer[layer][N_fixed]
            L_two = Ltwos_layer[layer][N_fixed] 
            ext_learnedIE_layer = {}
            ext_learnedIE_layer["N"]  =  L_one.shape[0]
            
            fes_surf = Compress(  Periodic(H1(decomp.mesh,order=problem_config["order"],complex=True,definedon=decomp.mesh.Boundaries("layer"+str(layer)+"-bnd") )))
            inf_outer = [ fes_surf for i in range(L_one.shape[0] -1)]
            XX = FESpace( [decomp.fes] + inf_outer)
            
            gfuXX = GridFunction (XX)
            uXX = XX.TrialFunction() 
            vXX = XX.TestFunction() 
            
            ba_dofs = BitArray(XX.FreeDofs())
            ba_outer_dofs = BitArray(XX.FreeDofs())
            IF_dofs = BitArray(XX.FreeDofs())
            
            IF_dofs[:] = 0
            for i in range(decomp.fes.ndof):
                ba_dofs[i] = 0
                ba_outer_dofs[i] = 0
            for dof_nr in decomp.InteriorPlusIFdof[layer]:
                ba_dofs[dof_nr] = 1
            for dof_nr in decomp.IF2dof[layer]:
                IF_dofs[dof_nr] = 1

            outer_nr = [] 
            for nr,bb in enumerate(ba_outer_dofs):
                if bb:
                    outer_nr.append(nr)

            proj_IF = Projector(IF_dofs,False)
            proj_outer = Projector(ba_outer_dofs,False)
            
            aXX =  RestrictedBilinearForm(XX, "aXX", check_unused=False,
                           element_restriction=ba_els,
                           symmetric = problem_config["is_symmetric"])
            #aXX = BilinearForm(XX, symmetric=is_symmetric)
            aXX += SymbolicBFI ( grad(uXX[0])*grad(vXX[0]) - (  problem_config["omega"]**2/problem_config["c"]**2)*uXX[0]*vXX[0], definedonelements = ba_els, bonus_intorder = problem_config["bonus_intorder"] )

            for i in range(L_one.shape[0]):
                u_trial = uXX[i]
                gradu_trial = grad(u_trial).Trace()
                for j in range(L_one.shape[0]):
                    v_test = vXX[j]
                    gradv_test = grad(v_test).Trace()
                    if abs(L_one[j,i]) > 1e-14 or abs(L_two[j,i]>1e-14):
                        aXX += SymbolicBFI ( L_two[j,i]*gradu_trial*gradv_test + L_one[j,i]*u_trial*v_test,definedon= decomp.mesh.Boundaries("layer"+str(layer)+"-bnd"), bonus_intorder =  problem_config["bonus_intorder"]  ) 
            aXX.Assemble()

            invXX = aXX.mat.Inverse(freedofs=ba_dofs, inverse= problem_config["solver"]) 
            learned_inverse =  LearnedInverse(gfuXX,invXX)
            layer_and_truncated_ext_inv[layer] = learned_inverse

            del invXX

            v_store = gfuXX.vec.CreateVector()
            
            if ext_learnedIE_layer["N"] > 1: 
                LearnedDtNs[layer] = LearnedDtN(v_store,decomp,layer,proj_IF,proj_outer,aXX.mat, aXX.mat.Inverse(freedofs=ba_outer_dofs, inverse=problem_config["solver"]))
            else:
                LearnedDtNs[layer] = LearnedDtN(v_store,decomp,layer,proj_IF,proj_outer,aXX.mat)

    return LearnedDtNs,layer_and_truncated_ext_inv


class AlgebraicDtN():
 
    def __init__(self,tmp_DtN,layer,proj_IF,proj_outer,layermat_outer,layerinverse_outer):
        self.tmp_DtN  = tmp_DtN  
        self.tmp_DtN1 = self.tmp_DtN.CreateVector()
        self.layer = layer 
        self.proj_IF = proj_IF
        self.proj_outer = proj_outer
        self.layermat_outer = layermat_outer
        self.layerinverse_outer = layerinverse_outer

    def Apply(self, v_in, v_out):
        self.tmp_DtN.data = v_in
        self.tmp_DtN.data -= self.proj_IF * self.tmp_DtN
        self.tmp_DtN1.data = self.layermat_outer * self.tmp_DtN 
        self.tmp_DtN1.data -= self.proj_IF * self.tmp_DtN1
        v_out.data  = self.tmp_DtN1
        self.tmp_DtN1.data =  self.layermat_outer * self.tmp_DtN
        self.tmp_DtN1.data -= self.proj_outer * self.tmp_DtN1
        self.tmp_DtN.data =  self.layerinverse_outer * self.tmp_DtN1
        self.tmp_DtN.data -= self.proj_outer * self.tmp_DtN

        self.tmp_DtN1.data = self.layermat_outer * self.tmp_DtN
        self.tmp_DtN1.data -= self.proj_IF * self.tmp_DtN1
        v_out.data -= self.tmp_DtN1



def get_AlgebraicDtN_and_ext_inv(decomp,problem_config):
    
    u = decomp.fes.TrialFunction() 
    v = decomp.fes.TestFunction() 
    AlgebraicDtNs = [None for i in range(decomp.n_layers)]
    layer_and_truncated_ext_inv = [None for i in range(decomp.n_layers)]
    layer_and_truncated_ext = [None for i in range(decomp.n_layers)]
    for layer in range(decomp.n_layers):
        if problem_config["use_PML"]:
            decomp.mesh.SetPML(problem_config["pml_phys"],definedon=problem_config["definedon_PML"])
        ba_els = BitArrayFromList(decomp.layer_and_outer_el[layer],decomp.mesh,decomp.fes,elements=True) 
        ba_dofs = BitArrayFromList(decomp.layer_and_outer_dof[layer],decomp.mesh,decomp.fes,elements=False)
        a_layer_and_outer = BilinearForm (decomp.fes, symmetric=problem_config["is_symmetric"], check_unused=False)
        a_layer_and_outer += SymbolicBFI ( grad(u)*grad(v) -(problem_config["omega"]**2/problem_config["c"]**2)*u*v  ,definedonelements= ba_els,bonus_intorder = problem_config["bonus_intorder"])   
        a_layer_and_outer.Assemble()
        if problem_config["use_PML"]:
            decomp.mesh.UnSetPML(problem_config["definedon_PML"])
        layer_and_truncated_ext[layer] = a_layer_and_outer.mat
        layer_and_truncated_ext_inv[layer] = a_layer_and_outer.mat.Inverse(ba_dofs,inverse=problem_config["solver"])


    gfu_tmp = GridFunction (decomp.fes)
    tmp_DtN = gfu_tmp.vec.CreateVector()
    tmp1_DtN = gfu_tmp.vec.CreateVector()

    layermat_outer = [None for i in range(decomp.n_layers)]
    layerinverse_outer= [None for i in range(decomp.n_layers)]
    for layer in range(1,decomp.n_layers):
        layer_name = "layer0"
        for j in range(1,layer):
            layer_name += "|layer"+str(j) 
        if problem_config["use_PML"]:
            decomp.mesh.SetPML( problem_config["pml_phys"], problem_config["definedon_PML"] )
        ba_els = BitArrayFromList(decomp.layer2all_outer_el[layer],decomp.mesh,decomp.fes,elements=True) 
        
        #a_outer = BilinearForm (Xa, symmetric=is_symmetric, check_unused=False)
        a_outer =  RestrictedBilinearForm(decomp.fes, "a_layer_and_outer", check_unused=False,
                           element_restriction=ba_els,
                           symmetric =problem_config["is_symmetric"] )
        a_outer += SymbolicBFI ( grad(u)*grad(v) - (problem_config["omega"]**2/problem_config["c"]**2)*u*v  ,definedonelements= ba_els,bonus_intorder =  problem_config["bonus_intorder"]) 

        a_outer.Assemble()
        layermat_outer[layer] = a_outer.mat
        ba_dofs = BitArrayFromList(decomp.layer2all_outer_dof[layer],decomp.mesh,decomp.fes,elements=False)
        layerinverse_outer[layer] = a_outer.mat.Inverse(ba_dofs,inverse=problem_config["solver"])
        
        if problem_config["use_PML"]:
            decomp.mesh.UnSetPML(problem_config["definedon_PML"])
        
        #proj_IF_outer = Projector(BitArrayFromList(decomp.IF2dof[layer],mesh,fes,elements=False),False)
        proj_IF = Projector(BitArrayFromList(decomp.IF2dof[layer],decomp.mesh,decomp.fes,elements=False),False)
        proj_outer = Projector(BitArrayFromList(decomp.layer2all_outer_dof[layer],decomp.mesh,decomp.fes,elements=False),False)
        AlgebraicDtNs[layer] =  AlgebraicDtN(tmp_DtN,layer,proj_IF,proj_outer,layermat_outer[layer],layerinverse_outer[layer])

    return AlgebraicDtNs,layer_and_truncated_ext_inv


class MovingPMLDtN():
 
    def __init__(self,tmp_DtN,layer,proj_IF,proj_outer,layermat_outer,layerinverse_outer):
        self.tmp_DtN  = tmp_DtN  
        self.tmp_DtN1 = self.tmp_DtN.CreateVector()
        self.layer = layer 
        self.proj_IF = proj_IF
        self.proj_outer = proj_outer
        self.layermat_outer = layermat_outer
        self.layerinverse_outer = layerinverse_outer

    def Apply(self, v_in, v_out):
        self.tmp_DtN.data = v_in
        self.tmp_DtN.data -= self.proj_IF * self.tmp_DtN
        self.tmp_DtN1.data = self.layermat_outer * self.tmp_DtN 
        self.tmp_DtN1.data -= self.proj_IF * self.tmp_DtN1
        v_out.data  = self.tmp_DtN1
        self.tmp_DtN1.data =  self.layermat_outer * self.tmp_DtN
        self.tmp_DtN1.data -= self.proj_outer * self.tmp_DtN1
        self.tmp_DtN.data =  self.layerinverse_outer * self.tmp_DtN1
        self.tmp_DtN.data -= self.proj_outer * self.tmp_DtN

        self.tmp_DtN1.data = self.layermat_outer * self.tmp_DtN
        self.tmp_DtN1.data -= self.proj_IF * self.tmp_DtN1
        v_out.data -= self.tmp_DtN1


def get_MovingPMLDtN_and_ext_inv(decomp,problem_config):
    
    layermat_moving_PML = [None for i in range(decomp.n_layers)]
    layerinverse_moving_PML= [None for i in range(decomp.n_layers)]
    layer_and_truncated_ext_inv = [None for i in range(decomp.n_layers)] 
    MovingPMLDtNs = [None for i in range(decomp.n_layers)]
    r_layer =  problem_config["r_layer"]
    
    u = decomp.fes.TrialFunction() 
    v = decomp.fes.TestFunction() 
    
    gfu_tmp = GridFunction (decomp.fes)
    tmp_DtN = gfu_tmp.vec.CreateVector()
    
    for layer in range(decomp.n_layers):
         
        C_sweep = problem_config["C_PML"]
        layer_name = "layer"+str(layer)
        definedon_PML_move = 'default'
        rad_min = r_layer[-1-layer]
        rad_max = r_layer[-1-layer+1]
       
        f0 = problem_config["omega"]
        eta = rad_max-rad_min
        #print("layer_name = {0}, rad_min = {1}, rad_max = {2}".format(layer_name,rad_min,rad_max))
        movepml_sigma = IfPos(y-rad_min,IfPos(rad_max-y,C_sweep/eta*(-rad_max+y+eta)**2/eta**2,0),0)
        G_move = (C_sweep/3)*(eta/C_sweep)**(3/2)*movepml_sigma**(3/2)    
        movepml_trafo = CoefficientFunction( (x,
                                             y+(1j/f0)*(C_sweep/3)*(eta*movepml_sigma/C_sweep)**(3/2) ) )
        movepml_jac = CoefficientFunction((1.0,
                                           0.0,
                                           0.0,
                                           1+(1j/f0)*movepml_sigma 
                                           ),
                                          dims=(2,2))   
        
        pml_move = pml.Custom(trafo=movepml_trafo, jac = movepml_jac ) 

        if layer == 0:
            ba_els = BitArrayFromList(decomp.layer_and_outer_el[layer],decomp.mesh,decomp.fes,elements=True) 
            ba_dofs = BitArrayFromList(decomp.layer_and_outer_dof[layer],decomp.mesh,decomp.fes,elements=False)
        else:
            ba_els_comp = list( set(decomp.layer2el[layer]) |  set(decomp.layer2el[layer-1]) )
            ba_els_comp.sort()
            ba_els = BitArrayFromList(ba_els_comp,decomp.mesh,decomp.fes,elements=True) 
            relevant_dofs = list(set(decomp.layer2dof[layer-1]) | set(decomp.IF2dof[layer]) | set(decomp.Interior2dof[layer]) )
            relevant_dofs.sort()
            ba_dofs = BitArrayFromList(relevant_dofs,decomp.mesh,decomp.fes,elements=False)
       
        if layer > 1:
            decomp.mesh.SetPML(pml_move,definedon_PML_move)
        else:
            if problem_config["use_PML"]:
                decomp.mesh.SetPML(problem_config["pml_phys"], problem_config["definedon_PML"])
        a_move = BilinearForm (decomp.fes, symmetric=problem_config["is_symmetric"], check_unused=False)
        a_move += SymbolicBFI (  grad(u)*grad(v) - (problem_config["omega"]**2/problem_config["c"]**2)*u*v , definedonelements= ba_els,bonus_intorder = problem_config["bonus_intorder"])   
        a_move.Assemble()
        layermat_moving_PML[layer] = a_move.mat  
        
        layer_and_truncated_ext_inv[layer] = a_move.mat.Inverse(ba_dofs,inverse=problem_config["solver"])
        layerinverse_moving_PML[layer] = a_move.mat.Inverse(ba_dofs,inverse=problem_config["solver"])
        if layer > 1: 
            decomp.mesh.UnSetPML(definedon_PML_move) 
        else:
            if problem_config["use_PML"]:
                decomp.mesh.UnSetPML(problem_config["definedon_PML"])

        proj_IF = Projector(BitArrayFromList(decomp.IF2dof[layer],decomp.mesh,decomp.fes,elements=False),False)
        proj_outer = Projector(BitArrayFromList(decomp.layer2all_outer_dof[layer],decomp.mesh,decomp.fes,elements=False),False)
        MovingPMLDtNs[layer] =  MovingPMLDtN(tmp_DtN,layer,proj_IF,proj_outer,layermat_moving_PML[layer],layerinverse_moving_PML[layer])
        
        if problem_config["output_dtn_on_layer"] and layer == problem_config["output_dtn_on_layer"]:
            
            pml_move_1D_sigma = IfPos(x-rad_min,IfPos(rad_max -x,C_sweep/eta*(x-rad_min)**2/eta**2,0),0)
            pml_move_1D = pml.Custom(trafo=x+(1j/f0)*(C_sweep/3)*(eta/C_sweep)**(3/2)*pml_move_1D_sigma**(3/2), jac = -1 - 1j*pml_move_1D_sigma/f0 )
            G_pml_move_1D = (C_sweep/3)*(eta/C_sweep)**(3/2)*pml_move_1D_sigma**(3/2)    
            
            relevant_mesh_pts = [rad_min, rad_min + 0.5*(rad_max-rad_min), rad_max]
            print("relevant_mesh_pts = ", relevant_mesh_pts)
            relevant_mesh_1D = Make1DMesh_givenpts(relevant_mesh_pts)

            fes_PML = H1(relevant_mesh_1D, complex=True,  order= problem_config["order"], dirichlet=[])
            u_PML,v_PML = fes_PML.TnT()

            relevant_mesh_1D.SetPML(pml_move_1D,definedon=1)

            A_PML = BilinearForm (fes_PML, symmetric=False)
            A_PML += SymbolicBFI(  grad(u_PML)*grad(v_PML) )
            A_PML += SymbolicBFI( - ( problem_config["omega"]**2/problem_config["c_1D"]**2 )*u_PML*v_PML)
            A_PML.Assemble()

            B_PML = BilinearForm (fes_PML, symmetric=False )
            B_PML += SymbolicBFI( u_PML*v_PML )
            B_PML.Assemble()

            relevant_mesh_1D.UnSetPML(1)
            Nze =  A_PML.mat.nze
            print("A_PML.mat.nze = ",Nze)
            print("A_PML.mat.shape = ", A_PML.mat.shape  )
            rows_1,cols_1,vals_1 = A_PML.mat.COO()
            A_csr = csr_matrix((vals_1,(rows_1,cols_1))) 
            A_PML = np.asarray(A_csr.todense())
            
            rows_2,cols_2,vals_2 = B_PML.mat.COO()
            B_csr = csr_matrix((vals_2,(rows_2,cols_2))) 
            B_PML = np.asarray(B_csr.todense())

            lam_sample = problem_config["lam_sample"] 
            
            dtn_approx = np.zeros(len(lam_sample),dtype='complex')
            print("--------------------------")
            opt.eval_dtn_fct(A_PML,B_PML,lam_sample.copy(),dtn_approx,False)

            if problem_config["use_PML"]:
                fname_mPML = "mPML_dtn_omega{0}_PMLbc_{1}_real.dat".format(problem_config["omega"], problem_config["type_str"])
            else:
                fname_mPML = "mPML_dtn_omega{0}_Neumannbc_{1}_real.dat".format(problem_config["omega"], problem_config["type_str"])
           
            plot_collect_mPML = [lam_sample, dtn_approx.real]
            header_mPML = "lam dtn"
            np.savetxt(fname=fname_mPML ,
               X=np.transpose(plot_collect_mPML),
               header=header_mPML,
               comments='')

            if problem_config["show_dtn_approx"]:
                
                dtn_nr_sample = problem_config["dtn_nr_sample"]  
                plt.plot(lam_sample,np.array(dtn_nr_sample).real,label='dtn',linewidth=4,color='lightgray' )
                plt.plot(lam_sample,dtn_approx.real,label='PML',linewidth=2,color='r' )
                plt.title("Real part") 
                plt.xlabel("$\\lambda$")
                plt.legend()
                plt.show()

    return MovingPMLDtNs,layer_and_truncated_ext_inv

def get_physical_pml(problem_config):

    f0_solve = problem_config["omega"]
    rad_min = problem_config["r_layer"][-2]
    rad_max = problem_config["r_layer"][-1]
    eta = rad_max-rad_min
    f0 = f0_solve
    C_PML = problem_config["C_PML"]

    physpml_sigma = IfPos(y-rad_min,IfPos(rad_max-y,C_PML/eta*(-rad_max+y+eta)**2/eta**2,0),0) 
    physpml_trafo = CoefficientFunction( (x,
                                         y+(1j/f0)*(C_PML/3)*(eta*physpml_sigma/C_PML)**(3/2) ) )
    physpml_jac = CoefficientFunction((1.0,
                                       0.0,
                                       0.0,
                                       1+(1j/f0)*physpml_sigma 
                                       ),
                                  dims=(2,2))   

    pml_phys = pml.Custom(trafo=physpml_trafo, jac = physpml_jac ) 


    pml_1D_sigma = IfPos(x-rad_min,IfPos(rad_max -x,C_PML/eta*(x-rad_min)**2/eta**2,0),0)
    pml_1D = pml.Custom(trafo=x+(1j/f0)*(C_PML/3)*(eta/C_PML)**(3/2)*pml_1D_sigma**(3/2), jac = -1 - 1j*pml_1D_sigma/f0 )

    return pml_phys,pml_1D

def apply_DOSM_to_rhs(dosm,rhs,gfu_DOSM_in,gfu_DOSM_out,udirect,mesh,DOSM_iter=1):
    DOSM_error = [] 
    print("one application of DOSM")
    for i in range(DOSM_iter):
        gfu_DOSM_in.vec.data = gfu_DOSM_out.vec
        dosm.ApplyDOSM(rhs,gfu_DOSM_out.vec,gfu_DOSM_in.vec)
        l2_error = sqrt (Integrate ( (gfu_DOSM_out.real-udirect.real)*(gfu_DOSM_out.real-udirect.real) 
              + (gfu_DOSM_out.imag-udirect.imag)*(gfu_DOSM_out.imag-udirect.imag), mesh))
        print("DOSM-iteration = {0}, L2-error = {1}".format(i+1,l2_error))
        DOSM_error.append(l2_error)
    Draw (gfu_DOSM_out, mesh, "DOSM-sol")
    Draw (sqrt((udirect.real-gfu_DOSM_out.real)**2 + (udirect.imag-gfu_DOSM_out.imag)**2), mesh, "error")

def run_preconditioned_GMRES(C,aX,rhs,udirect,gfu_GMRES,mesh,maxsteps=250):

    res = []
    global cnt2
    cnt2 = 0

    def callback_full(v):
        global cnt2
        gfu_GMRES.vec.data = v
        l2_error_iter = sqrt (Integrate ( (gfu_GMRES.real-udirect.real)*(gfu_GMRES.real-udirect.real) 
                                           + (gfu_GMRES.imag-udirect.imag)*(gfu_GMRES.imag-udirect.imag), mesh))
        res.append(l2_error_iter)
        cnt2 += 1
    print("Start GMRES iteration with preconditioner")

    with TaskManager(): 
        gfu_GMRES.vec.data = GMRes(aX.mat, rhs, pre=C, maxsteps = maxsteps, tol = 1e-7,
              callback=callback_full, restart=None, startiteration=0, printrates=True)
    Draw (gfu_GMRES, mesh, "gmres-sol")
    print("L2-errors in iterations = {0}".format(res))
    if res == []:
        return 1
    else:
        return len(res)+1
