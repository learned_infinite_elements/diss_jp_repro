import numpy as np
from math import pi

def local_polynomial_estimator(f0,r,w,nargout):
    N = 30
    deg = 3
    f = np.zeros(len(r))
    if nargout > 1:
        df = np.zeros(len(r))
    if nargout >2:
        ddf = np.zeros(len(r))
    for j in range(len(r)):
        if j < N:
            jind = np.arange(0,2*N+1)
        elif j > len(r)-1-N:
            jind = np.arange(len(r)-2*N-1,len(r))
        else:
            jind = np.arange(j-N,j+N+1)
        r_loc = r[jind]
        if r_loc[-1] - r_loc[0] < 2e-3:
            jind = np.nonzero( np.abs(r-r[j]) < 1e-3 )[0]
            r_loc = r[jind]
        scal = r_loc[-1] - r_loc[0]
        weight_loc = w[jind]*np.exp(-16*(r[j]-r_loc)**2/scal**2)
        mat = np.zeros((len(jind),deg+1))
        for l in range(0,deg+1):
            mat[:,l] = weight_loc*((r_loc-r[j])/scal)**l
        rhs = weight_loc*f0[jind]
        coeff = np.linalg.lstsq(mat,rhs,rcond=None)[0]
        f[j] = coeff[0] 
        if nargout > 1:
            df[j] = coeff[1]/scal
        if nargout > 2:
            ddf[j] = 2*coeff[2]/scal**2
    
    if nargout == 1:
        return [f]
    elif nargout == 2:
        df[j] = coeff[1]/scal
        return [f,df]
    else:
        ddf[j] = 2*coeff[2]/scal**2
        return [f,df,ddf]

def logder(f0,r,w,nargout):
    lpe = local_polynomial_estimator(np.log(f0),r,w,nargout)
    f = np.exp(lpe[0])
    if nargout == 1:
        return f
    if nargout >= 2:
        dlf = lpe[1]
        df = dlf*f
        if nargout == 2:
            return (f,dlf)
        else:
            ddlf = lpe[2]
            ddf = ddlf*f + df**2/f
            return (f,df,ddf)


if __name__ == "__main__":
    
    import matplotlib.pyplot as plt

    rS,cS,rhoS,_,__,___ = np.loadtxt("modelSinput.txt",unpack=True)
    rV,cV,rhoV  = np.loadtxt("VALCinput.txt",unpack=True)

    # overlap interval
    rmin = rV[-1]
    rmax = rS[0]

    #print("rmax-rmin = ", rmax-rmin)

    weightS = np.minimum(1, (rmax-rS)/(rmax-rmin))
    weightV = np.minimum(1, (rV-rmin)/(rmax-rmin))

    #print("weightS = ", weightS)
    #print("weightV = ", weightV)

    r = np.concatenate((rS,rV)) 
    ind = np.argsort(r)
    r = r[ind]
    c = np.concatenate((cS,cV))[ind]
    rho = np.concatenate((rhoS,rhoV))[ind]
    weight = np.concatenate((weightS,weightV))[ind]

    RSun = 6.963e10 # radius of the Sun in cm
    c0 = 6.855e5 # sound speed at interface [cm/s]
    H = 1.25e7 # density scale height in [cm]
    omega_c = 0.0052*2*pi # c0/(2*H) cut-off frequency in Herz

    rho_clean = logder(rho,r,weight,1)
    c_clean = logder(c,r,weight,1)

    i0 = np.min( np.nonzero(r > 0.99)[0] ) 
    #plt.semilogy( r,rho_clean,label='rho')
    #plt.semilogy( r[i0:],c_clean[i0:],label='c')
    #plt.legend()
    #plt.ylabel("[solar radius $^{-2}$]")
    #plt.xlabel("r [solar radius]")
    #plt.show()

    np.savetxt(fname="c-rho-VALC.dat",
               X=np.transpose([r[i0:],c_clean[i0:],rho_clean[i0:]]), 
               header = "r c rho",
               comments='' 
              )

    f,df,ddf = logder(1/np.sqrt(rho),r,weight,3)
    pot_rho = np.sqrt(rho_clean)*(ddf+2*df/r)
    pot_c = RSun**2*omega_c**2/c_clean**2

    i0 = np.min( np.nonzero(r > 0.99)[0] )

    plot_collect = [r[i0:]]
    header = "r "
    for f_hz in [0.003,0.0052,0.007]:
        plot_collect.append(  pot_rho[i0:]-(f_hz*2*pi/omega_c)**2*pot_c[i0:] )
        header += "f{0} ".format(f_hz)

    np.savetxt(fname="pot-VALC.dat",
               X=np.transpose(plot_collect), 
               header = header,
               comments='' 
              )

    plt.plot( r[i0:],pot_rho[i0:]-(0.003*2*pi/omega_c)**2*pot_c[i0:],label=" $\omega = 2 \pi \cdot 3$ mHz",color='r',linestyle='dashed')
    plt.plot( r[i0:],pot_rho[i0:]-(0.0052*2*pi/omega_c)**2*pot_c[i0:],label= " $\omega = 2 \pi \cdot 5.2$ mHz",color='b')
    plt.plot( r[i0:],pot_rho[i0:]-(0.007*2*pi/omega_c)**2*pot_c[i0:],label=" $\omega = 2 \pi \cdot 7$ mHz",color='k',linestyle='dotted')
    plt.axhline(y=0,color='k')
    plt.legend()
    plt.title("potential $v$")
    plt.ylabel("[solar radius $^{-2}$]")
    plt.xlabel("r [solar radius]")
    plt.show()

