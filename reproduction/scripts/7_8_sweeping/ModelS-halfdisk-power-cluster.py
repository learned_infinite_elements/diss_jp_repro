from netgen.meshing import *
from netgen.csg import *
from ngsolve import *
from math import pi,ceil
import scipy.sparse.linalg
import numpy as np
from ngsolve.solvers import GMRes
from decimal import Decimal
#from scipy.linalg import lu_factor,lu_solve
from scipy.sparse import csr_matrix
import sys,os,copy 
sys.path.append(os.path.realpath(''))
from sweeping_geom import HybridLayered_Mesh,Layered_Mesh,Make1DMesh,Make1DMesh_refined,HybridLayered_Mesh_gen,HybridLayered_Mesh_U,HybridLayered_Mesh_halfDisk,Make1DMesh_givenpts
from sweeping_helper import BitArrayFromList,P_DoFs,Layered_Decomposition,DOSM_IFassemble,DOSM
#import ceres_dtn as opt
#import matplotlib.pyplot as plt
from compute_pot import local_polynomial_estimator,logder
from helio_helper import SolarModel,sqrt_2branch
#plt.rc('legend',fontsize=14)
#plt.rc('axes',titlesize=14)
#plt.rc('axes',labelsize=14)
#plt.rc('xtick',labelsize=12)
#plt.rc('ytick',labelsize=16)

ngsglobals.msg_level = 0

#########################################################################
# Parameters (to be changed by the user)
#########################################################################

#f_hz = 0.003 # frequency in Hz
f_hz = 0.003 # frequency in Hz
#omega = 64  # wavenumer 
#n_layers = 24 # number of layers (J in the paper)

atmospheric_model = "VAL-C"
#atmospheric_model = "Atmo" # not tested 

is_symmetric = True
gamma_damping = 0 # add damping to moving PML preconditioner 
use_moving_PML = False  # set 'True' to use moving PML approximation of the DtN
                       # and 'False' to use the tensor product DtN
show_dtn_approx = False
use_learnedIE = True
use_algebraic_DtN = False
use_discrete_eigenvalues = False
lam_max = min(int(1000*f_hz*200),800)
print("lam_max = ", lam_max)
DOSM_versions = ["IF_assemble","nonassemble"]
DOSM_version = DOSM_versions[1]
direct_solver_precond = False 
Nmax = 4 
#Nmax = 2
#Nmax = 1
Ns = list(range(1,Nmax+1))
Nmax_a = Nmax
Ns_a = list(range(1,Nmax_a+1))
Mref = 0
Mc = 5  #Mc = 7 power spectrum
a = 1.0 # radius of truncation boundary 
#a = 1.0007126 # radius of truncation boundary 

prec_start = 500
L_max_a = 1000

R_max_ODE =  1.0033
if atmospheric_model == "VALC-C":
    R_max_ODE =  1.0033
elif atmospheric_model == "Atmo": 
    R_max_ODE =  1.0007126

#R_max_ODE =  1.0033
#R_max_ODE =  1.0007126
#Ra = #Mc = 0#
#Mc = 0

#########################################################################
# Further parameters
#########################################################################

order_input = int(sys.argv[1])
if order_input not in range(1,20):
    print("You have chosen order = {0}, which is an invalid choice. I will compute with order = 7 instead.".format(order_input))
    order =  7  # order of FEM 
else:
    order =  order_input  # order of FEM
print("Computing with order = ", order)
#order =  3   # order of FEM 
#order_ODE = order # order of ODE discretization
order_ODE = 7
order_geom = order 

spline_order = 1
bonus_intorder = 0
#nx = 
#nx = int(2*n_layers) # number of elements in horizontal direction
use_PML = False 
#solver = "pardiso" # direct solver (e.g. used for subdomain problems)
solver = "sparsecholesky" # direct solver (e.g. used for subdomain problems)
#solver = "umfpack" # direct solver (e.g. used for subdomain problems)
C_PML = 20 # amplitude of the PML
#C_PML = 10000 # amplitude of the PML

def new_initial_guess(l1_old,l2_old,ansatz):
    N = l1_old.shape[0]
    l1_guess = np.zeros((N+1,N+1),dtype='complex')
    l2_guess = np.zeros((N+1,N+1),dtype='complex')
    if ansatz in ["medium","full"]:
        l1_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        l2_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        l1_guess[:N,:N] = l1_old[:]
        l2_guess[:N,:N] = l2_old[:]
        l1_guess[N,N] = 1.0
    elif ansatz == "minimalIC":
        l1_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        l1_guess[:N,:N] = l1_old[:]
        l2_guess[:N,:N] = l2_old[:]
        l1_guess[N,N] = (-100-100j)
        l2_guess[0,N] = 1e-3*(np.random.rand(1) + 1j*np.random.rand(1))         
    elif ansatz == "mediumSym":
        l1_guess[:N,:N] = l1_old[:]
        l2_guess[:N,:N] = l2_old[:]
        l1_guess[0,N]  = l1_guess[N,0] = 1e-3*(np.random.rand() + 1j*np.random.rand())
        l2_guess[0,N] =  l2_guess[N,0] = 1e-3*(np.random.rand() + 1j*np.random.rand()) 
        l1_guess[N,N] = (16/16)*(-100-100j)
        #A_guess[N,N] = -100-100j
        l2_guess[N,N] = 1.0
    return l1_guess,l2_guess

#ansatz = "minimalIC"
ansatz = "mediumSym"
flags = {"max_num_iterations":10000,
         "use_nonmonotonic_steps":True,
         "minimizer_progress_to_stdout":False,
         "num_threads":4,
         "report_level":"Brief",
         "function_tolerance":1e-11,
         "parameter_tolerance":1e-11,
         "gradient_tolerance":1e-11}


#################################################### 
# load solar models and compute potential
####################################################

solar_model = SolarModel(atmospheric_model,"power-law")
c,rho,rho_pot,c_1D,rho_1D,rho_pot_1D = solar_model.get_coeff()

#c_perturb = cos(x*y)*sin(x*y)*IfPos(y-1.0,0.0,1.0)
c_perturb = 1.0*IfPos(y-1.0,0.0,1.0)
eps_perturbs = [0.0,0.0001,0.001,0.004,0.01,0.02,0.04,0.06] 
eps_perturb = eps_perturbs[0]
c_perturbed = c*sqrt(1-eps_perturb*c_perturb)


#################################################### 
# 1D FES for computing dtn numbers in atmosphere
####################################################

n_outer = 10
start_point = a
#mesh_above_surface = [start_point, start_point+0.5*(R_max_ODE-start_point),R_max_ODE]

mesh_above_surface = np.linspace(start_point,R_max_ODE,10)

#mesh_above_surface = [start_point,R_max_ODE]
#for i in range(1,n_outer+1):
#    new_point = start_point + (R_max_ODE - start_point)*i/n_outer
#    mesh_above_surface.append(new_point)
print("mesh_above_surface = ", mesh_above_surface)

mesh_1D = Make1DMesh_givenpts(mesh_above_surface)  
fes_DtN = H1(mesh_1D, complex=True,  order=order_ODE, dirichlet=[1])
u_DtN,v_DtN = fes_DtN.TnT()
gfu_DtN = GridFunction (fes_DtN)

f_DtN = LinearForm(fes_DtN)
f_DtN.Assemble()
r_DtN = f_DtN.vec.CreateVector()
frees_DtN = [i for i in range(1,fes_DtN.ndof)]

#################################################### 
# damping model
####################################################

RSun =  6.96*10**8

#omega,omega_squared = solar_model.get_damping(f_hz)


N_l = 1000
N_omega = 1800
#N_omega = 800
#N_omega = 250
L_range = np.arange(0,N_l+1)
lam_a = np.array([l*(l+1)/a**2 for l in L_range ] )
f0s = 1e-3*np.linspace(1.0,8.3,N_omega)
dtn_nr = np.zeros((len(f0s),len(L_range)),dtype="complex")


def precompute_dtn_numbers(f_hzs,L_range,dtn_nr):
    
    prec = prec_start
    if atmospheric_model == "Atmo":
        
        from ngs_arb import Whitw_deriv_over_Whitw,Whitw_deriv_over_Whitw_par,Whitw_deriv_over_Whitw_spar
        L_max = L_range[-1]
        alpha_infty,c_infty = solar_model.get_alpha_and_c_infty(a)
        #print("alpha_infty = {0}, c_infty = {1}".format(alpha_infty,c_infty))

        for idx_f,f_hz in enumerate(f_hzs):
            print("idx = {0}, f_hz = {1}".format(idx_f,f_hz))
            sigma, sigma_squared = solar_model.get_damping(f_hz)

            k_squared = sigma_squared/c_infty**2 - 0.25*alpha_infty**2
            k_omega = sqrt_2branch(k_squared)
            #print("k_square = {0}, k_omega = {1}".format(k_squared,k_omega))
            eta = alpha_infty/(2*k_omega)
            chi = -1j*eta
            z_arg = -2*1j*k_omega*a
            
            #print("chi = {0}, z_arg = {1}".format(chi,z_arg))
            #kappas_l = [chi for l in L_range ]
            #mus_l = [l+0.5 for l in L_range ]
            #zs_l = [z_arg for l in L_range ]
            #wh_qout =  np.array(Whitw_deriv_over_Whitw( kappas_l,mus_l,zs_l,prec)) 
            #wh_qout =  np.array(Whitw_deriv_over_Whitw_par( kappas_l,mus_l,zs_l,prec)) 
            
            values_ok = False
            while not values_ok:
                wh_qout =  np.array(Whitw_deriv_over_Whitw_spar(chi,z_arg,L_max,prec)) 
                if not np.isnan(wh_qout).any():
                    values_ok = True
                else:
                    print("Whittaker function returned nans, increasing precision!")
                    prec += 500
                    

            DtN_atmo =  1/a + 2*1j*k_omega*wh_qout
            #print("dtn ", DtN_atmo[:10])
            #print("dtn ", DtN_atmo[:-10])
            
            dtn_a = [] 

            pot_1D = rho_pot_1D - solar_model.get_damping(f_hz)[1]/c_1D**2
            
            for idx_lami,lami in enumerate(lam_a):    
                
                if use_PML:
                    mesh_1D.SetPML(pml_1D,definedon=1)
         
                a_DtN = BilinearForm (fes_DtN, symmetric=False)
                a_DtN += SymbolicBFI( (x**2)*grad(u_DtN)*grad(v_DtN), bonus_intorder=  bonus_intorder)  
                a_DtN += SymbolicBFI(  ( a**2*lami.item() + (pot_1D*x**2 ) )*u_DtN*v_DtN, bonus_intorder= bonus_intorder)
                a_DtN += SymbolicBFI(x**2*DtN_atmo[idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))
                a_DtN.Assemble()

                gfu_DtN.vec[:] = 0.0
                gfu_DtN.Set(1.0, BND)
                r_DtN.data = f_DtN.vec - a_DtN.mat * gfu_DtN.vec
                gfu_DtN.vec.data += a_DtN.mat.Inverse(freedofs=fes_DtN.FreeDofs(),inverse=solver) * r_DtN
               
                rows_DtN,cols_DtN,vals_DtN = a_DtN.mat.COO()
                A_DtN = csr_matrix((vals_DtN,(rows_DtN,cols_DtN)))
                val = -P_DoFs(A_DtN,[0],frees_DtN).dot(gfu_DtN.vec.FV().NumPy()[frees_DtN])-P_DoFs(A_DtN,[0],[0]).dot(gfu_DtN.vec.FV().NumPy()[0])
                dtn_a.append(-val.item()/a**2)

            dtn_nr[idx_f,:] = dtn_a[:]
        
    elif atmospheric_model == "VAL-C":
        
        for idx_f,f_hz in enumerate(f_hzs):
            
            pot_1D = rho_pot_1D - solar_model.get_damping(f_hz)[1]/c_1D**2
            dtn_a = [] 

            for lami in lam_a:    
                
                if use_PML:
                    mesh_1D.SetPML(pml_1D,definedon=1)
         
                a_DtN = BilinearForm (fes_DtN, symmetric=False)
                a_DtN += SymbolicBFI( (x**2)*grad(u_DtN)*grad(v_DtN), bonus_intorder=  bonus_intorder)  
                a_DtN += SymbolicBFI(  ( a**2*lami.item() + (pot_1D*x**2 ) )*u_DtN*v_DtN, bonus_intorder= bonus_intorder)
                a_DtN.Assemble()

                gfu_DtN.vec[:] = 0.0
                gfu_DtN.Set(1.0, BND)
                r_DtN.data = f_DtN.vec - a_DtN.mat * gfu_DtN.vec
                gfu_DtN.vec.data += a_DtN.mat.Inverse(freedofs=fes_DtN.FreeDofs(),inverse=solver) * r_DtN
               
                rows_DtN,cols_DtN,vals_DtN = a_DtN.mat.COO()
                A_DtN = csr_matrix((vals_DtN,(rows_DtN,cols_DtN)))
                val = -P_DoFs(A_DtN,[0],frees_DtN).dot(gfu_DtN.vec.FV().NumPy()[frees_DtN])-P_DoFs(A_DtN,[0],[0]).dot(gfu_DtN.vec.FV().NumPy()[0])
                dtn_a.append(-val.item()/a**2)
            
            dtn_nr[idx_f,:] = dtn_a[:]
        
        #np.savetxt('dtn_nr_Atmo.out',dtn_nr,delimiter=',')
        
    else:
        raise ValueError('Unknown atmospheric model.')
    
    #np.savetxt('dtn_nr_{0}.out'.format(atmospheric_model),dtn_nr,delimiter=',')
    np.savetxt('dtn_nr_{0}.out'.format(atmospheric_model),dtn_nr,delimiter=',')


try:
    dtn_nr = np.loadtxt('dtn_nr_{0}.out'.format(atmospheric_model),dtype="complex",delimiter=',')
except:
    precompute_dtn_numbers(f0s,L_range,dtn_nr)
#dtn_nr = np.loadtxt('dtn_nr_{0}_1800.out'.format(atmospheric_model),dtype="complex",delimiter=',')
#dtn_nr = np.loadtxt('dtn_nr_{0}_more.out'.format(atmospheric_model),dtype="complex",delimiter=',')


def get_learned_IE_matrices(f_hzs,dtn_nr,lam_a=lam_a):

    LIE_A = {} 
    LIE_B = {} 

    import ceres_dtn as opt    

    for idx_f,f_hz in enumerate(f_hzs):
        
        L_spacing = 10
        #dtn_a = np.array(get_dtn_a(f_hz))
        #dtn_a = dtn_nr[idx_f,:]
        dtn_a =  dtn_nr[idx_f,:]

        lam_a = np.array(lam_a)
        diff_dtn = dtn_a[1:] - dtn_a[:-1]
        diff_lam = lam_a[1:] - lam_a[:-1]
        grad_dtn = np.abs( diff_dtn / diff_lam) 
        #print("np.average(grad_dtn) = ", np.average(grad_dtn) )
        ind_large = np.nonzero(grad_dtn > 3*np.average(grad_dtn ))[0]
        #print("ind_larg = ", ind_large)
        ind_large = 1 + ind_large 

        #if show_plots:

        L_filter = [] 
        w_weight = [] 
        alpha_decay = 8*L_spacing
        for l in np.arange(L_max_a):
            if l in ind_large:
                L_filter.append(l)
                w_weight.append(exp(-l/alpha_decay))
            else:
                if l % L_spacing == 0:
                    L_filter.append(l)
                    w_weight.append(exp(-l/alpha_decay))
        weights = np.array(w_weight)

        #weights[:] = 1.0

        lam_a_filtered = np.array(lam_a)[L_filter]
        dtn_a_filtered = np.array(dtn_a)[L_filter]

        A_a  = [None]
        B_a = [None] 
        np.random.seed(seed=123)        
        A_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
        B_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
                
        l_dtn = opt.learned_dtn(lam_a_filtered,dtn_a_filtered,weights**2)

        for N in Ns_a: 
            l_dtn.Run(A_guess,B_guess,ansatz,flags)
            A_a.append(A_guess.copy()), B_a.append(B_guess.copy())
            A_guess,B_guess = new_initial_guess(A_a[N],B_a[N],ansatz)

        LIE_A[f_hz] = A_a[-1]
        LIE_B[f_hz] = B_a[-1]

    return LIE_A,LIE_B

LIE_A,LIE_B = get_learned_IE_matrices(f0s,dtn_nr)

class HelioAxi():
    def __init__(self,lam_a):
        self.mesh = None
        self.theta_h = None
        self.Xa = None
        self.uX = None
        self.vX = None
        self.rhs = None
        self.udirect = None
        self.gfuXa = None
        self.res_direct = None
        self.lam_a = lam_a
    def Update(self,mesh,theta_h,Xa,uX,vX,rhs,udirect,gfuXa):
        self.mesh = mesh
        self.theta_h = theta_h
        self.Xa = Xa
        self.uX = uX 
        self.vX = vX 
        self.rhs = rhs
        self.udirect = udirect
        self.gfuXa = gfuXa 
        self.res_direct = self.gfuXa.vec.CreateVector()

helio = HelioAxi(lam_a)

def compute_G(f_hz,dtn_a,LIE_A,LIE_B,helio,update=True):

    omega,omega_squared = solar_model.get_damping(f_hz) 
    pot = rho_pot - omega_squared/c**2   
    
    #pot_1D = rho_pot_1D - omega_squared/c_1D**2

    # weights
    weight_r = 1.0 
    #weight_r_1D = x**2
    #weight_theta = 1.0

    weight_theta = x
    weight_factor = CoefficientFunction(weight_r * weight_theta)

    # Determine dtn numbers for solar atmosphere 

    A_N = LIE_A[f_hz] 
    B_N = LIE_B[f_hz]

    ###############

    if update:
        #Determine position of layers according to local wavelength
        lambda_eff  = 2*pi*c_1D/omega.real
        w_mesh_pts = np.linspace(0.0,a,1000).tolist()
        mesh_1D_w = Make1DMesh_givenpts(w_mesh_pts)
        N_wavelen = Integrate(1/lambda_eff,mesh_1D_w,order=10)

        r_samples = np.linspace(0.0,0.8,30,endpoint=False).tolist() + np.linspace(0.8,0.95,50,endpoint=False).tolist() + np.linspace(0.95,0.98,100,endpoint=False).tolist() + np.linspace(0.98,a,220).tolist()
        N_wavelen_r = [] 
        for r_sample in r_samples:
            N_wavelen_r.append(Integrate(IfPos(r_sample-x,1.0,0.0)*1/lambda_eff,mesh_1D_w))
        r_layer = [0.0]
        N_current = 1
        N_layer = [] 
        for r_sample,N_sample in zip(r_samples,N_wavelen_r):
            if N_sample > N_current:
                r_layer.append(r_sample)
                N_layer.append(N_sample)
                N_current += 1
        if abs(N_layer[-1]-N_wavelen) < 0.25:
            print("Replace last wavelen mesh point with a")
            r_layer[-1] = a
        else:
            r_layer.append(a)
        # prevent first layer from becoming too thick
        if (r_layer[1] > 0.3):
            r_layer.insert(1,0.5*(r_layer[1]+r_layer[0]))

        #########################################################################
        # Layered mesh
        #########################################################################

        r_start = 0

        nx = 2*len(r_layer)
        refinement_marker = [False for i in range(len(r_layer))]
        for i in range(Mc+1):
            if i < len(refinement_marker): 
                refinement_marker[i] = True 
        quads_marker = [True for i in range(len(r_layer))]

        h_inner = 0.5*(r_layer[1]-r_layer[0])

        print("r_layer =" , r_layer)
        mesh,theta_h = HybridLayered_Mesh_halfDisk(r_layer,h_inner,quads_marker,refinement_marker,'D',2*pi,order_geom,True)
    

        #########################################################################
        # FEM space (Proxy and-GridFunctions) 
        #########################################################################

        fes_H1 = H1(mesh, complex=True,  order=order,dirichlet=[])
        fes = fes_H1
        fes = Compress(fes)

        fes_surf = Compress(H1(mesh,order=order,complex=True,definedon= mesh.Boundaries("outer") ))
        inf_outer = [ fes_surf for i in range(A_N.shape[0] -1)]
        Xa = FESpace( [fes] + inf_outer)

        gfuXa = GridFunction (Xa)
        udirect = GridFunction (Xa)

        uX = Xa.TrialFunction() 
        vX = Xa.TestFunction() 
        u = uX[0]
        v = vX[0]
        
        f = LinearForm (Xa)
        f.Assemble()
        
        dirac_source_pos = (0.0,1.0)
        mesh_pt = mesh(dirac_source_pos[0],dirac_source_pos[1])
        gfuXa.vec[:] = 0.0
        rhs = f.vec.CreateVector()
        gfuXa.vec[:] = 0.0
        # implementation of Dirac delta 
        for i in range(fes.ndof):    
            gfuXa.vec[i] = 1.0
            f.vec.FV().NumPy()[i] = gfuXa.components[0](mesh_pt)
            gfuXa.vec[i] = 0.0
        rhs.data = f.vec

        helio.Update(mesh,theta_h,Xa,uX,vX,rhs,udirect,gfuXa)

    
    helio.mesh.SetDeformation(helio.theta_h)

    aX = BilinearForm (helio.Xa, symmetric=is_symmetric)
    aX += SymbolicBFI (  (grad( helio.uX[0])*grad(helio.vX[0]) + pot*helio.uX[0]*helio.vX[0])*weight_factor , bonus_intorder = bonus_intorder  )
    for i in range(A_N.shape[0]):
        for j in range(A_N.shape[0]):
            if abs(A_N[j,i]) > 1e-10 or abs(B_N[j,i]>1e-10):
                graduX = grad(helio.uX[i]).Trace()
                gradvX = grad(helio.vX[j]).Trace()  
                aX += SymbolicBFI ( (B_N[j,i]*graduX*gradvX + A_N[j,i]*helio.uX[i]*helio.vX[j])*weight_theta  ,definedon= helio.mesh.Boundaries("outer") )
    aX.Assemble()
 
    helio.udirect.vec[:] = 0.0
    helio.res_direct.data = helio.rhs - aX.mat * helio.udirect.vec
    invA = aX.mat.Inverse(freedofs=helio.Xa.FreeDofs(), inverse=solver)
    helio.udirect.vec.data += invA * helio.res_direct

    del aX
    del invA 

    vals = []
    for l in L_range:
        Pl = Legendre(l)
        result = Integrate((helio.udirect.components[0].imag)*Pl*x,helio.mesh,order=30,VOL_or_BND=BND,definedon=helio.mesh.Boundaries("outer"))
        #result = Integrate((Gl.imag)*Pl*x,mesh,VOL_or_BND=BND,definedon=mesh.Boundaries("layer1-bnd"))
        vals.append(abs(result)) 
    
    helio.mesh.UnsetDeformation()
    
    return vals

from helio_tools import Legendre


power_spectrum = np.zeros((N_omega,N_l+1))
with TaskManager(): 
    for j,f0 in enumerate(f0s):
        print("f_hz = ", f0*1e3)
        #pi_fac = 1 / (1 + ((2*pi*abs(f0)-3.3*1e-3*2*pi)/ (0.6*1e-3*2*pi) )**2) 
        #if j % 40 == 0:
        if j % 20 == 0:
            vals = compute_G(f0,dtn_a = dtn_nr[j,:],LIE_A=LIE_A,LIE_B=LIE_B,helio=helio,update=True)
        else:
            vals = compute_G(f0,dtn_a = dtn_nr[j,:],LIE_A=LIE_A,LIE_B=LIE_B,helio=helio,update=False)
        #scaled_vals =  pi_fac*np.array(vals)/f0
        scaled_vals =  np.array(vals)
        power_spectrum[j,:] = scaled_vals[:]

np.savetxt('power-spectrum_{0}_order{1}_angleref1p5_1800freq.out'.format(atmospheric_model,order),power_spectrum,delimiter=',')

#power_spectrum = np.abs(power_spectrum)  
#power_spectrum *= 1/power_spectrum.max() 




