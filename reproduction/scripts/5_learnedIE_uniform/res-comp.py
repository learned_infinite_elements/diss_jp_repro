import numpy as np
from math import sqrt,exp
from scipy.special import hankel1,h1vp
import ceres_dtn as opt
import mpmath as mp
import matplotlib.pyplot as plt

omega = 16 # wavenumber
a = 0.5 # coupling radius
Lmax = 72 # number of modes
alpha_decay = 2/3 # decay of weights
Nmax = 7 # maximal number of infinite element dofs
Ns = list(range(Nmax))


def hankel1(nu,omega):
    return complex(mp.hankel1(nu,a*omega))

def h1vp(nu,omega):
    return complex(a*0.5*(mp.hankel1(nu-1,a*omega) - mp.hankel1(nu+1,a*omega)))

def dh1vp(nu,omega):
    return complex((0.5*a)**2*( mp.hankel1(nu-2,a*omega) - 2*mp.hankel1(nu,a*omega) +  mp.hankel1(nu+2,a*omega))) 

#def dtn_ref(nu):
#    return -omega*h1vp(nu,omega*a) / hankel1(nu,omega*a)


print("Computing analytic poles")
from GRPF import Poles_and_Roots 
param = {}
# rectangular domain definition z=x+jy x\in[xb,xe], y \in [yb,ye]
param["xrange"] = [0,30]
param["yrange"] = [-35,1]
param["h"] = 0.5
param["Tol"] = 1e-9
param["visual"] = 0 
param["ItMax"] = 20
param["NodesMax"] = 500000 
res = [] 
res_collect = [] 
#for l in range(0,20):
l_sample = list(range(0,25))
from scipy.optimize import newton
for l in l_sample:
    print("l = {0}".format(l))
    #result = Poles_and_Roots(lambda z:h1vp(l,z),param)
    result = Poles_and_Roots(lambda z:hankel1(l,z),param)
    if result != {}:
        #res.append(result["roots"])
        for i in range(len( result["roots"])):
            #root,r_result = newton(lambda z:h1vp(l,z), result["roots"][i],lambda z:dh1vp(l,z),full_output=True,disp=False)
            root,r_result = newton(lambda z:hankel1(l,z), result["roots"][i],lambda z:h1vp(l,z),full_output=True,disp=False)
            if abs(root) < 1e-3: 
                print("root close to zero")
            if r_result.converged:
                val_at_root = hankel1(l,root)
                if abs(val_at_root) < 1e-7:
                    res_collect.append(root)
                print("l = {0}, newton_root = {1}, value at root = {2}".format(l,root,val_at_root))
            else:
                res_collect.append( result["roots"][i] )
                print("l = {0}, root = {1}, value at root = {2}".format(l, result["roots"][i] ,h1vp(l,result["roots"][i] )))

#for l_idx,l in enumerate(l_sample):
#    print("l = {0}, res = {1}".format(l,res[l_idx]))

'''
res_collect = [] 
newton_roots = [] 
for l_idx,l in enumerate(l_sample):
    for i in range(len(res[l_idx])):
        root,r_result = newton(lambda z:h1vp(l,z),res[l_idx][i],lambda z:dh1vp(l,z),full_output=True,disp=False)
        if r_result.converged:
            res_collect.append(root)
            print("l = {0}, root = {1}, orig = {2}, 1vp(l,z) = {3}".format(l,root, res[l_idx][i], h1vp(l,root)))
        else:
            res_collect.append(res[l_idx][i])
'''

res_collect = np.array(res_collect) 
plt.scatter(res_collect.real,res_collect.imag)
plt.show() 


#fname = "exact-res-small.dat"
fname = "exact-res-Dirichlet.dat"
results = [res_collect]
#header_str = "res"
np.savetxt(fname =fname,
           X = np.transpose(results),
           comments = '')


fname = "exact-res-Dirichlet-tuple.dat"
with open(fname, 'a') as the_file:
    the_file.write('x y \n')
    for res_z in res_collect:
        the_file.write('{0} {1} \n'.format(res_z.real,res_z.imag))
    the_file.write('\n')


