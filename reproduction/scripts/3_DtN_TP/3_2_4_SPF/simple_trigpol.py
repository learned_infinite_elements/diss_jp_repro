import numpy as np
import matplotlib.pyplot as plt
#import mpmath as mp
import scipy.integrate as int_scipy
from math import sqrt,sin,cos,pi
from numpy.polynomial import Chebyshev
from numpy.polynomial import Polynomial


plot_results = False
output_deg = 6

degs = np.arange(2,80,1)

#bs = [1,2,4,8]
#bs = [0.5,1.0,1.5,2.0,2.5,3.0]
bs = [1]
err_trafo_all = np.zeros( (len(bs),len(degs)) )
err_original_all = np.zeros( (len(bs),len(degs)) )

for ic,bb in enumerate(bs):
    
    a = 0
    b = bb

    a1 = 0.2+0.01j
    a2 = 0.4+0.015j
    a3 = 0.7+0.02j
    eps = 1e-1
    eps1 = 1
    
    def G(x,eps=eps,eps1=eps1):
        return eps*(np.exp(2*pi*1j*x))+eps1*(1+x)**(3/2) + np.log( (x-a1)*(x-a2)*(x-a3) )

    def f(x,eps=eps,eps1=eps1):
        return eps*(2*pi*1j*np.exp(2*pi*1j*x))+eps1*(3/2)*(1+x)**0.5 + 1/(x-a1) + 1/(x-a2) + 1/(x-a3)

    def F(x):
        return G(x)-G(a)

    def Chebyshev_nodes(a, b, N):
        return [0.5*(a+b) + 0.5*(b-a)*cos( (float(2*i+1)/(2*N+1))*pi) for i in range(N+1)]

    eval_points = np.linspace(a,b,300)

    dtn_nr = np.array([ f(lami) for lami in eval_points ])
    dtn_nr_only_poles = np.array([ f(lami) for lami in eval_points ])


    errors = []
    errors_original = []
    for jd,deg in enumerate(degs):
        #if deg == output_deg:
        #     print("deg =", deg)
        #    plot_results = True
        #else:
        #    plot_results = False
        lam = np.array(Chebyshev_nodes(a, b, 2*deg**2))

        
        #dtn_nr = np.array([ f(lami) for lami in eval_points ])

        def trafo_f(x):
            return f(x)*np.exp(F(x))

        samples = lam

        trafo_f_vals = np.array([trafo_f(xx) for xx in samples])
        print(trafo_f_vals)

        p = Chebyshev.fit(samples, trafo_f_vals, deg=deg)
        P = p.integ()

        trafo_f_eval = np.array([trafo_f(xx) for xx in eval_points])

        trafo_fit = p(eval_points) 
        print("error-nodes-fit =", np.linalg.norm(trafo_f_vals-p(samples) , ord=np.inf) )
        original_fit = p(eval_points )/(1+P(eval_points) - P(a) ) 
        errors.append( np.linalg.norm(trafo_f_eval-trafo_fit, ord=np.inf) /  np.linalg.norm(trafo_f_eval, ord=np.inf) ) 
        #errors.append( np.linalg.norm(trafo_f_eval-trafo_fit, ord=np.inf)  )  
        errors_original.append( np.linalg.norm(   dtn_nr - original_fit  , ord=np.inf) / np.linalg.norm(   dtn_nr , ord=np.inf)  ) 
        #errors_original.append( np.linalg.norm(   dtn_nr - original_fit  , ord=np.inf)   ) 
        print("error-trafo-fit =", errors[-1] )
       
        if deg == output_deg:
            
            fname_trafo = "SPF-trafo_deg{0}_b{1}.dat".format(output_deg,b)
            header_trafo = "x ExactReal ExactImag AppReal AppImag"
            results_trafo = [eval_points, trafo_f_eval.real,trafo_f_eval.imag,trafo_fit.real,trafo_fit.imag]
            np.savetxt(fname = fname_trafo ,
                       X = np.transpose(results_trafo),
                       header = header_trafo,
                       comments = '')
            
            fname_original = "SPF-original_deg{0}_b{1}.dat".format(output_deg,b)
            header_original = "x ExactReal ExactImag AppReal AppImag"
            results_original = [eval_points,dtn_nr.real,dtn_nr.imag, original_fit.real, original_fit.imag]
            np.savetxt(fname = fname_original ,
                       X = np.transpose(results_original),
                       header = header_original,
                       comments = '')


        if deg == output_deg and plot_results:

            plt.plot(eval_points,trafo_f_eval.real,label="exact")
            plt.plot(eval_points,trafo_fit.real ,label="fit",linestyle="dashed")
            #plt.plot(samples,f(samples),label="exact")
            #plt.plot(samples,p(samples),label="fit")
            plt.legend()
            plt.title("fit of transformed fct.-real")
            plt.show()

            plt.plot(eval_points,trafo_f_eval.imag,label="exact")
            plt.plot(eval_points,trafo_fit.imag,label="fit")
            #plt.plot(samples,f(samples),label="exact")
            #plt.plot(samples,p(samples),label="fit")
            plt.legend()
            plt.title("fit of transformed fct.-imag")
            plt.show()


            plt.semilogy(eval_points,np.abs(trafo_f_eval -trafo_fit),label="abs-error")
            plt.semilogy(eval_points,np.abs(trafo_f_eval -trafo_fit)/np.abs(trafo_f_eval) ,label="rel-error")
            plt.legend()
            plt.title("error transformed")
            plt.show()



            #print("dtn_nr =", dtn_nr)
            plt.plot(eval_points,dtn_nr.real,label="exact",linestyle="dashed")
            plt.plot(eval_points, original_fit.real ,label="fit")
            plt.legend()
            plt.title("original fct.real")
            plt.show()

            plt.plot(eval_points,dtn_nr.imag,label="exact")
            plt.plot(eval_points, original_fit.imag ,label="fit")
            plt.legend()
            plt.title("original fct-imag")
            plt.show()

            print("error-orgin =", np.linalg.norm(dtn_nr - original_fit,  ord=np.inf )  )
            
            plt.semilogy(eval_points,np.abs(dtn_nr -  original_fit ) ,label="abs-error")
            plt.semilogy(eval_points,np.abs(dtn_nr -  original_fit ) / np.abs(dtn_nr) ,label="rel-error")
            plt.legend()
            plt.title("error original")
            plt.show()

    
    errors = np.array(errors)
    errors_original = np.array(errors_original)
    
    err_trafo_all[ic,:] = errors[:]
    err_original_all[ic,:] = errors_original[:]

    if plot_results:
        plt.semilogy(degs,errors,label="error-trafo")
        plt.semilogy(degs,errors_original,label="error-original")
        plt.legend()
        plt.xlabel("$N$")
        plt.show()
   
    fname_relerr = "SPF-relerr_b{0}.dat".format(b)
    header_relerr = "deg trafoError originalError"
    results_relerr = [degs,errors,errors_original]
    np.savetxt(fname = fname_relerr ,
               X = np.transpose(results_relerr),
               header = header_relerr,
               comments = '')

if len(bs) > 1:
    plt.semilogy(bs,err_original_all[:,10] / err_trafo_all[:,10] ,  label="rato")
    plt.semilogy(bs , np.exp(bs),label="exp(interval length) ")
    plt.legend()
    plt.xlabel("$interval length$")
    plt.show()
