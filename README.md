# README 
This repository contains the software, data and instructions to reproduce the experiments in the PhD thesis 
> ___Learned infinite elements for helioseismology___ 
> 
> * author: Janosch Preuss 
> * advisors: Christoph Lehrenfeld, Thorsten Hohage, Damien Fournier 
> * University of Göttingen, 2021

# How to run / install

We describe two options to setup the software for running the experiments. 

* downloading a `docker image` from `GRO.data` or `Docker Hub` which contains all dependencies and tools to run the application,
* or installing everything manually on your own local machine. 

The first option is quick and convenient while the second option provides higher flexibility. 
Therefore, we recommend to start with the `docker image`. If you would like to integrate 
parts of the application into your own code (e.g. coupling the optimization routine for dtn 
to another FEM library) then eventually you need to go for a manual installation. Please 
contact <j.preuss@math.uni-goettingen.de> in case of problems. 

## Pulling the docker image from Docker Hub 
* Please install the `docker` platform for your distribution as described [here](https://docs.docker.com/get-docker/).
* After installation the `Docker daemon` has to be started. This can either be done on boot or manually. In most Linux 
distribution the command for the latter is either `systemctl start docker` or `service docker start`.
* Pull the docker image for learned IEs using the command `docker pull janosch2888/diss_jp:191021`. 
* Run the image with `docker run -it janosch2888/diss_jp:191021 bash`. 
* Proceed further as described in [How to reproduce](#repro).

## Downloading the docker image from GRO.data 
* For this option the first two steps are the same as above. 
* Assuming that `learnedIE_diss_repro.tar` is the filename of the downloaded image, please load the image with `docker load < learnedIE_diss_repro.tar`.
* Run the image with `docker run -it janosch2888/diss_jp:191021 bash`.
* Proceed further as described in [How to reproduce](#repro).

## Manual installation 

For a local installation it in principle suffices to execute the same commands that are described in the [Dockerfile](https://github.com/janoschpreuss/docker-diss-jp/blob/main/Dockerfile)
for assembling the `docker image`. For convenience an overview of the main components is given here. 
Firstly, we need some basic tools. Probably most of these (except for *mpmath*) are already installed on your system.

* [Python3](https://www.python.org/) 
  * [NumPy/SciPy](https://www.scipy.org/install.html)
  * [mpmath](http://mpmath.org/): This is a multi-precision library which we use for evaluation of some of the special functions. 
* [cmake](https://cmake.org/)

We proceed to the more specific software. The components *ngs_refsol*,*ceres_dtn*, *ngs_arb*,*helio_tools* and *pole_finder* described below
are submodules of the repository at hand. They can be pulled with 

```git clone https://gitlab.gwdg.de/learned_infinite_elements/diss_jp_repro.git```. 

The main tools are:

* [Netgen/NGSove](https://ngsolve.org/) is the FEM library used for implementation in the thesis (if you are only interested in the optimization routine for dtn this 
  does not have to be installed). Installation instructions for common operating systems are provided [here](https://ngsolve.org/downloads).
    * *ngs_refsol*: Minor extension of *NGSolve* that provides some sample solutions for scattering problems. For installation it should be sufficient to 
      execute `python3 setup.py install --user` in the top folder of this repository. Alternatively, one may proceed by a manual installation: 
      
      ```bash
      mdkir build 
      cd build 
      cmake ..
      ``` 
    * *helio_tools*: Minor extension of *NGSolve* providing some auxiliary tools for computational helioseismology. Can be installed analogously to 
      *ngs_refsol*. This extension is required only for a single experiment (computation of power spectrum for axisymmetric discretization). Users 
      who are not interested in this experiment can ignore this extension. 
* [ngsxfem](https://github.com/ngsxfem/ngsxfem) is an add-on library to NGSolve for unfitted finite element technologies. We use some of the features that this library provides 
  in the experiments featuring sweeping preconditioners.
* [Ceres Solver](http://ceres-solver.org) Nonlinear least squares solver utilized for solving the optimization problem for dtn. Installation instructions 
  are provided [here](http://ceres-solver.org/installation.html). Dependencies: [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page) and [cmake](https://cmake.org/).
    * *ceres_dtn*: Solves the optimization problem for learned IEs using *Ceres Solver*. Can be installed analogously to *ngs_refsol*.

* *pole_finder*: Routine for computing poles and roots of meromorphic functions. The implementation is in *Python* and independent of the previous two components (but requires 
  *NumPy/SciPy*). Can be installed with `python3 setup.py install --user`.

* [arb](https://arblib.org/) A *C* library for arbitrary precision arithmetic. This is only needed for reproducing the numerical experiments in the helioseismology chapter 
for the Atmo model. Otherwise, this dependency can be ignored.
    * *ngs_arb*: Some *python* wrappers for *arb* to compute the dtn function for the Atmo model. Can be installed analogously to *ngs_refsol*.

# <a name="repro"></a> How to reproduce 

We offer two levels of reproduction.

1. The [first](#reproPlots") lets you reproduce the figures and tables presented in the thesis based on exactly the same data that was used in the thesis. 
This data is provided in the folder *data* of this repository. The folder *plots* contains *latex* scripts which can be run to produce the figures and tables. 
2. The [second](#reproNumexp) lets you reproduce the data itself. To this end, the scripts contained in the *scripts* folder have to be executed as described below. 
If you want to generate the plots based on the data generated by running the scripts, simply copy this data into the corresponding *data/...* folder and run the *latex* scripts in the *plots/..* 
folder.

# <a name="reproPlots"></a> Reproduction of figures and tables based on already provided data

* The `docker image` includes a full `texlive` installation. This allows you to directly compile the figures within 
  the image. For example, change to the directory `plots/2_DtN_sep/dtn_fcts` and run `lualatex dtn-helmholtz.tex`.
  This will generate the figure `dtn-helmholtz.pdf`. To copy this figure to the host system on which you run docker, 
  open a separate terminal and execute 
  
      CONTAINER_ID=$(docker ps -alq)
      docker cp $CONTAINER_ID:/home/app/diss_jp_repro/reproduction/plots/2_DtN_sep/dtn_fcts/dtn-helmholtz.pdf /path/on/host/dtn-helmholtz.pdf
  
  This will copy the file `dtn-helmholtz.pdf` into the folder `/path/on/host` on the host system. 

* If you want to generate __all__ the Latex-based plots in one go:
  
      cd plot
      make figures

* Directions to generate only __specific__ figures (and other non-latex figures) are given below.
  The ordering is given according to the chapters of the thesis.



## Chapter 2: DtN maps for time-harmonic waves in separable exterior domains
Change to directory `plots/2_DtN_sep`

### Fig. 2.2
Change to directory `plots/2_DtN_sep/dtn_fcts`. 
Run `lualatex dtn-helmholtz.tex`.

### Fig. 2.3
Change to directory `plots/2_DtN_sep/2_2_1_dtn_hom_expansion`.
Run `lualatex dtn-hom-expansion.tex`.

### Fig. 2.4
Change to directory `plots/2_DtN_sep/dtn_fcts`. 
Run `lualatex dtn-jump.tex`.

### Fig. 2.5
Change to directory `plots/2_DtN_sep/dtn_fcts`. 
Run `lualatex dtn-ellipse.tex`.

### Fig. 2.6
Change to directory `plots/2_DtN_sep/dtn_fcts`. 
Run `lualatex dtn-waveguide.tex`.

### Fig. 2.7
Change to the folder `plots/2_DtN_sep/solar_coeff`. 

* (__a__): Run twice (to get labels correct) `lualatex c-rho-ModelS.tex`.
* (__b__): Run twice `lualatex c-rho-VALC-Atmo.tex`.

### Fig. 2.8
Change to directory `plots/2_DtN_sep/dtn_fcts`. 
Run `lualatex dtn-VALC.tex`.

 
## Chapter 3: Tensor-product discretizations of DtN
Change to directory `plots/3_DtN_TP`.

### Fig. 3.3
Change to directory `plots/3_DtN_TP/3_2_4_SPF`.

* (__a__): Run ` lualatex SPF-trafo-deg6-b1.tex`.
* (__b__): Run `lualatex SPF-original-deg6-b1.tex`.
* (__c__): Run `lualatex SPF-relerr-b1.tex`.

### Fig. 3.4
Change to directory `plots/3_DtN_TP/3_4_comp`.
Run `lualatex dtn-TP-loworder.tex`. 

### Table 3.1 
Change to directory `plots/3_DtN_TP/3_4_comp`.
Run `lualatex TP-PML-table.tex`.

### Fig. 3.5
Change to directory `plots/3_DtN_TP/3_4_comp`.

* (__a__) Run `lualatex cond-TP-omega16.tex`.
* (__b__) Run `lualatex cond-UBurnett-omega16.tex`.
* (__c__) Run `lualatex cond-AS-omega16.tex`.
* (__d__) Run `lualatex cond-HSIE-omega16.tex`.

### Fig 3.6
Change to directory `plots/3_DtN_TP/3_4_comp`.

* (__a__) Run `lualatex dtn-compTP-omega16-N3.tex`.
* (__b__) Run `lualatex dtn-compTP-omega16-N6.tex`.
* (__c__) Run `lualatex dtn-compTP-omega16-N9.tex`.

## Chapter 4: Learned infinite elements for individual wavenumbers

### Fig. 4.1, Table 4.1, Fig. 4.2
Change to directory `plots/4_learnedIE_individual/optimization`.
 
* Fig. 4.1 (__a__): Run `lualatex learning-curves-comp.tex`. 
* Fig. 4.1 (__b__): Run `lualatex poles-plot.tex`. 
* Table 4.1: Run `lualatex opt-performance-table.tex`.
* Fig. 4.2 (__a__): Run `lualatex cond-learneIE-full.tex`. 
* Fig. 4.2 (__b__): Run `lualatex cond-learneIE-mediumSym.tex`. 

### Fig 4.3, Fig.4.4
Change to directory `plots/4_learnedIE_individual/spherical_geom/plane_wave_hom`. 

* Fig. 4.3 (__a__): Run `lualatex plane-wave-disc-relerr-p.tex`.
* Fig. 4.3 (__b__): Run `lualatex plane-wave-disc-relerr-k.tex`.
* Fig. 4.4 (__a__): Run `lualatex plane-wave-a-relerror-heu-weights.tex`. 
* Fig. 4.4 (__b__): Run `lualatex plane-wave-a-relerror-opt-weight.tex`. 
* Fig. 4.4 (__c__): Run `lualatex plane-wave-a-weightedl2-err-heu-weights.tex`. 

### Fig. 4.5
Change to directory `plots/4_learnedIE_individual/spherical_geom/point_source`. 

* (__a__): Run `lualatex ptsource-close-ndof.tex`. 
* (__b__): Run `lualatex ptsource-close-nze.tex`.
* (__c__): Run `lualatex ptsource-far-ndof.tex`. 
* (__d__): Run `lualatex ptsource-far-nze.tex`.

### Fig. 4.6
Change to directory `plots/4_learnedIE_individual/spherical_geom/jump`.

* (__a__) Run `lualatex dtn-jump-inf16-real.tex`.
* (__b__) Run `lualatex dtn-jump-inf8-real.tex`.

### Fig. 4.7
Change to directory `plots/4_learnedIE_individual/ellipse`.

* (__a__) Run `lualatex dtn-ellipse-app-kappaFourThird.tex`.
* (__b__) Run `lualatex dtn-ellipse-app-kappaEightThird.tex`.
* (__c__) Run `lualatex dtn-ellipse-app-kappaTwelveThird.tex`.
* (__d__) Run `lualatex ellipse-scatter-sketch.tex` and afterwards `lualatex ellipse-relerr.tex`.

### Fig. 4.8
Change to directory `plots/4_learnedIE_individual/waveguide`.

* (__a__): Run `lualatex waveguide-dtn-imag-group.tex`.
* (__b__): Run `lualatex waveguide-relerr-p-k16.5.tex`. 

## Chapter 5: Learned IEs providing uniform approximation in the wavenumber

### Fig. 5.1
Change to directory `plots/5_learnedIE_uniform/plane_wave`.

* (__a__): Run `lualatex interval-homog-sup-coarse.tex`.
* (__b__): Run `lualatex interval-homog-relerr-coarse.tex`.
* (__c__): Run `lualatex interval-homog-sup-fine.tex`.
* (__d__): Run `lualatex interval-homog-relerr-fine.tex`.

### Fig. 5.2
Change to the directory `5_learnedIE_uniform/res`. Run `lualatex res-group.tex`. 

## Chapter 6: Modelling the solar atmosphere with learned IEs

### Fig 6.1 
Change to directory `plots/6_helio_tbc/obs_helio`. Run 

    lualatex dtn-approx-VALC-3p5mHz-damping-real.tex
    lualatex dtn-approx-VALC-3p5mHz-damping-imag.tex
    lualatex dtn-atmo-vs-VALC-8mHz-real.tex
    lualatex dtn-atmo-vs-VALC-8mHz-imag.tex

### <a name="IntroHelioDownload"></a>    Fig 6.2, Fig. 6.5 (__b__) - (__d__), Fig. 6.5 (data postproc.), Fig. 6.6 (data postproc.)
Change to directory `scripts/6_helio_tbc/obs_helio`. Run 

    python3 postprocess_power_crosscovariance.py "download" 

This downloads the power spectra computed with the Atmo and VALC model, respectively, from the [gro.data](https://doi.org/10.25625/GJ4PGF) catalogue, plots them and 
runs postprocessing steps for computing the cross-covariances.
When the computations have finished the following output data will be contained in the folder.

* Fig 6.2 (__a__): The power spectrum based on the Atmo model *power-computed-Atmo.png*.
* Fig. 6.2 (__b__): The power spectrum based on the VAL-C model *power-computed-VALC.png*.
* Fig. 6.2 (__c__): Samples of the absolute value of dtn for the Atmo model as function of frequency and harmonic degree *abs-dtn-Atmo-model.png*.
* Fig. 6.2 (__d__): Samples of the absolute value of dtn for the VAL-C model as function of frequency and harmonic degree *abs-dtn-VALC-model.png*.
* Fig. 6.2 (__e__): The values of the dtn function for the Atmo model at l=200 are available in the file *dtn-freq-Atmo-model-l200.dat*. The first column *mHz* is the frequency in mHz. 
                    The second column *dtnReal* contains the real part of dtn as a function of frequency while the last column *dtnImag* contains the imaginary part.
* Fig. 6.2 (__f__): The values of the dtn function for the VAL-C model at l=200 are available in the file *dtn-freq-VALC-model-l200.dat*. The columns are labelled as in  Fig. 6.2 (__e__).
* Fig. 6.5 (__b__): Time-distance diagram based on MDI observations *Time-Distance-observed.png*. 
* Fig. 6.5 (__c__): Time-distance diagram based on Atmo model *Time-Distance-Atmo.png*. 
* Fig. 6.5 (__d__): Time-distance diagram based on VAL-C model *Time-Distance-VALC.png*.
* Fig. 6.4: 
    * The file *power-f-low-l200-comp.dat* contains the power for the two atmospheric models at low frequencies shown in the upper panel of Fig. 6.4. 
      The column *f* gives the frequency in mHz. The column *Atmo* contains the power for the Atmo model while the column *VALC* gives the power for the VAL-C model.
      The power from the MDI observations is in the separate file *power-f-low-l200-obs.dat*.
    * The power files *power-f-high-l200-comp.dat* and *power-f-high-l200-obs.dat* analogously contain the data at high frequencies.
* Fig. 6.6: The data for each of the cases MDI,Atmo and VAL-C is stored in a separate file. These files share the same data structure. 
            The column *t* denotes the time-samples. The columns *30deg*,*60deg* and *90deg* contain the values of the expectation value of the cross-covariance at the 
            respective degrees. 
    * The data for the MDI observations is avaialable in the file *cross-obs.dat*.
    * The data for the Atmo model is available in the file *cross-tt-Atmo.dat*.
    * The data for the VAL-C model is available in the file *cross-tt-VALC.dat*.

If you would like to reproduce the latex plots of Fig. 6.4 and Fig. 6.6 with the data just computed, then 
copy this data from the folder `scripts/6_helio_tbc/obs_helio` into the folder `data/6_helio_tbc/obs_helio` and follow the instructions for Fig. 6.4 and Fig. 6.6 below 
for compiling the latex figures. 

### Fig. 6.2
Change to directory `plots/6_helio_tbc/obs_helio`.  

* (__e__): Run `lualatex dtn-freq-Atmo.tex`.
* (__f__): Run `lualatex dtn-freq-VALC.tex`.

### Fig. 6.3 
Change to directory `plots/6_helio_tbc/obs_helio`. 

* (__a__): Run `lualatex FWHM_expl.tex`.
* (__b__): Run `lualatex power-law-damping-f.tex`.

### Fig. 6.4
Change to directory `plots/6_helio_tbc/obs_helio`. Run `lualatex power-cut.tex`. 

### Fig. 6.5 (__a__):
Change to directory `plots/6_helio_tbc/obs_helio`. Run `lualatex rays-modelS.tex`.

### Fig. 6.6
Change to directory `plots/6_helio_tbc/obs_helio`. Run `lualatex cross-tt.tex`.

### Fig. 6.7 
Change to directory `scripts/6_helio_tbc/obs_helio`. Run `python3 double-ridge.py`. 
After the script finished the following plots and data products will be available.

* (__a__): High-frequency time-distance diagram based on MDI data *double-ridge-MDI.png*. 
* (__b__): High-frequency time-distance diagram based on VAL-C atmosphere *double-ridge-VALC-meshed-atmo.png*.
* (__c__): High-frequency time-distance diagram based on Atmo mode *double-ridge-Atmo.png*.
* (__d__): The file *double-ridge-filter-freq.dat* contains the data for the frequency filter. The column *omega* is the frequency in mHz and the column *val* the value of the filter. 
           The file *double-ridge-filter-spatial.dat* contains the data for the spatial filter. The column *l* is the index of the eigenvalue and the column *val* the value of the filter.
           A copy of these data files is also available in the folder *data/6_helio_tbc/obs_helio*. By changing into the folder *plots/6_helio_tbc/obs_helio/* and running *lualatex double-ridge-filter.tex* 
           you can compile Fig. 6.7 (__d__).

### <a name="AtmoDownload"></a>  Fig. 6.8, Fig. 6.9, Fig. 6.10, Fig. 6.11
Change to directory `scripts/6_helio_tbc/tbc_Atmo`. Run 

    python3 postprocess-Atmo.py "download"

This will download the power spectra and dtn numbers from the [gro.data](https://doi.org/10.25625/WX473B) catalogue und perform postprocessing steps.
The following figures and data will be produced: 

* Fig.6.8: All of these figures are available directly as *.png* files.
    * (__a__): *power-Atmo-nonlocal.png*
    * (__b__): *power-Atmo-SHF1a.png*
    * (__c__): *power-Atmo-ARBC1.png*
    * (__d__): *power-Atmo-N0.png*
    * (__e__): *power-Atmo-N1.png*
    * (__f__): *power-Atmo-N4.png*
* Fig. 6.9: The data for the first and second horizontal panel is available in files of the form *dtn-Atmo-__RI__- __f__mHz.dat*. 
 Here, __RI__ is either "real" or "imag", which signifies that this file contains the real or imaginary part of the dtn function, respectively.
 The specification __f__ denotes the frequency where [3,5p3,6p5] corresponds to [3.0,5.3,6.5] mHz.
 The relative errors shown in the lower panel are contained in the files *dtn-Atmo-relerr- __f__mHz.dat*.
 All tables in these files share the same structure. 

    * The column *l* denotes the index of the eigenvalue.
    * The column *ref* is the exact dtn function (solid gray line in figure).
    * The columns *nonlocal* and *SHF1a* contain the data for the nonlocal and S-HF-1a condition.
    * The columns labelled *N__j__* give the results for learned IEs using N=__j__. 

 The folder `data/6_helio_tbc/tbc_Atmo` contains copies of these data files which you may want to replace with the data just computed. To compile the latex figures,
 then  change to the directory `plots/6_helio_tbc/tbc_Atmo` and run  
     
     lualatex dtn-Atmo-3mHz.tex
     lualatex dtn-Atmo-5p3mHz.tex
     lualatex dtn-Atmo-6p5mHz.tex   
* Fig. 6.10: 
 
    * (__a__): The filter is the same as for Fig. 6.13 (__a__) and therefore only output during the reproduction of of the latter figure.
    * (__b__): The plot *time-distance-Atmo-low-filter.png* is directly produced by the script.
    * (__c__): The expectation value of the cross covariance is contained in the file *cross-covariance-val-low-theta14.dat*. The first colum *t* denotes the time. 
                The column *ref* is the reference cross-covariance (solid gray) in the figure. The 
               other columns are the values for the transparent boundary conditions. The absolute differences which are shown in the panel on the right are available 
               in the file *cross-covariance-diff-low-theta14.dat*.
    * (__d__): This is as in (__c__) except that the corresponding filenames are *cross-covariance-val-low-theta28.dat* and *cross-covariance-diff-low-theta28.dat*.

  A copy of the produced data is available in the folder `data/6_helio_tbc/tbc_Atmo` which you may want to replace with the data just computed. To compile the latex 
  figures, then change to directory  `plots/6_helio_tbc/tbc_Atmo` and run
 
      lualatex cross-covariance-low-theta14.tex 
      lualatex cross-covariance-low-theta14.tex 
      lualatex cross-covariance-low-theta28.tex
      lualatex cross-covariance-low-theta28.tex
  Running lualatex twice is necessary here to get the labels correct.

* Fig. 6.11: 
    * (__a__): The filter is the same as for Fig. 6.14 (__a__) and therefore only output during the reproduction of of the latter figure.
    * (__b__): The plot *time-distance-Atmo-high-filter.png* is directly produced by the script.
    * (__c__): This is exactly the same as in Fig.6.10 (__c__) except for different filenames *cross-covariance-val-high-theta36.dat* and *cross-covariance-diff-high-theta36.dat*.
    * (__d__): This is exactly the same as in Fig.6.10 (__d__) except for different filenames *cross-covariance-val-high-theta72.dat* and *cross-covariance-diff-high-theta72.dat*.

  As in Fig. 6.10 you may copy the computed data into the folder `data/6_helio_tbc/tbc_Atmo`, switch to  `plots/6_helio_tbc/tbc_Atmo` and run
        
      lualatex cross-covariance-high-theta36.tex 
      lualatex cross-covariance-high-theta36.tex
      lualatex cross-covariance-high-theta72.tex  
      lualatex cross-covariance-high-theta72.tex  

### <a name="VALCDownload"></a> Fig. 6.12, 6.13, 6.14
Change to the folder `scripts/6_helio_tbc/tbc_VALC`. Run `python3 power-and-hl-filtered-cross.py "download"`.
This download the power spectra from [gro.data](https://doi.org/10.25625/N0S2QJ) and performs postprocessing steps.
The following plots and data products will be produced. 

* Fig. 6.12: The plots *power-computed-N__j__-VALC.png* which display the relative errors for N=__j__ with j in [0,1,2,3,4] are directly available after running the script.
* Fig 6.13: 
    * (__a__): The file *phase-filter-low.png* is directly available.
    * (__b__): The file *time-distance-low.png* is directly available.
    * (__c__): The data for the expectation value of the cross-covariance is stored in the file *cross-covariance-val-low-theta14.dat*. The column *t* is the time, the column *ref* the 
               reference cross-covariance (solid gray in the figure) and the columns *N__j__* are the results with learned IEs using N=__j__ for __j__ in [0,1,2,3,4]. 
               The absolute differences shown in the right panel are contained in the file *cross-covariance-diff-low-theta14.dat*. 
               The folder `data/6_helio_tbc/tbc_VALC` contains a copy of this data which you may want to replace with the results just computed. 
               To compile the figure, change into the folder `plots/6_helio_tbc/tbc_VALC` and run `lualatex cross-covariance-low-theta.tex`. 
* Fig 6.14: 
    * (__a__): The file *phase-filter-high.png* is directly available.
    * (__b__): The file *time-distance-high.png* is directly available.
    * (__c__): The data for the expectation value of the cross-covariance is stored in the file *cross-covariance-val-high-theta36.dat*. The column *t* is the time, the column *ref* the 
               reference cross-covariance (solid gray in the figure) and the column *N__j__* are the results with learned IEs using N=__j__ for __j__ in [0,1,2,3,4]. 
               The absolute differences shown in the right panel are contained in the file *cross-covariance-diff-high-theta36.dat*. 
               The folder `data/6_helio_tbc/tbc_VALC` contains a copy of this data which you may want to replace with the results just computed. 
               To compile the figure, change into the folder `plots/6_helio_tbc/tbc_VALC` and run `lualatex cross-covariance-high-theta.tex`. 

### Fig. 6.15
Change to the folder `scripts/6_helio_tbc/tbc_VALC`.
If you executed the instructions to reproduce Fig. 6.12, 6.13, 6.14, then the data is already available and it suffices to run `python3 travel-time-accuracy.py`.
Otherwise, run `python3 travel-time-accuracy.py "download"`.
The following data will be produced

* (__a__) The file *cross-tt.dat* contains the reference-cross covariance at [30,60,90] degrees in the columns [*30deg*,*60deg*,*90deg*]. 
          The window functions are contained in the file *window-tt.dat*.
* (__b__) The file *tt-error.dat* contains the travel-time perturbation in terms of *N* (first column) at [*30deg*,*60deg*,*90deg*]. 

The folder `data/6_helio_tbc/tbc_VALC` contains a copy of this data which you may want to replace with the results just computed. 
To compile the figures, change into the folder `plots/6_helio_tbc/tbc_VALC` and run 

    lualatex cross-tt.tex
    lualatex tt-error.tex

### Fig. 6.16
Change to the folder `scripts/6_helio_tbc/tbc_VALC`.
If you executed the instructions to reproduce Fig. 6.12, 6.13, 6.14 or Fig.14, then the data is already available and it suffices to run 
`python3 double-ridge.py`. 
Otherwise, run  `python3 double-ridge.py "download"`.
After running the script the figures (__b__) and (__c__) will be available as *double-ridge-VALC-meshed-atmo.png* and *double-ridge-learned-N0.png*. 
Since Fig. 6.16 (__a__) is identical to Fig. 6.7 (__a__), we do not produce this figure here again. 
Actually,  Fig. 6.16 (__b__) is also identical to Fig. 6.7 (__b__) and only reproduced here again for cross-validation.

### Fig. 6.17
Change to the directory `plots/6_helio_tbc/tbc_VALC`.  Run 

    lualatex dtn-approx-VALC-8mHz-damping-real.tex
    lualatex dtn-approx-VALC-8mHz-damping-imag.tex

### Fig. 6.18

* (__a__): Change to the directory `scripts/6_helio_tbc/discussion`. Run `python3 fit_damping.py`. This produces the shown plot *damping-models-comp.png*.
* (__b__): Change to the directory `plots/6_helio_tbc/discussion`. Run `lualatex power-damping-comp.tex`.

## Chapter 7: Learned IEs as transmission conditions in sweeping preconditioners

### Fig. 7.2
Change to the directory `plots/7_sweep_basic/cartesian_meshes`. Run 

    lualatex cartesian-mesh-3layers.tex
    lualatex cartesian-mesh-6layers.tex
    lualatex cartesian-mesh-12layers.tex
    lualatex cartesian-mesh-24layers.tex

### Table 7.1
Change to the directory `plots/7_sweep_basic/const`. 
Run `lualatex const-table.tex`.

### Fig. 7.3
Change to the directory `plots/7_sweep_basic/const`. 

* (__a__): Run `lualatex dtn-sweep-const-PML.tex`. 
* (__b__): Run `lualatex dtn-sweep-const-Neumann.tex`. 

### Table 7.2
Change to directory `plots/7_sweep_basic/jump`. Run `lualatex jump-table.tex`.

### Fig. 7.4
Change to directory `plots/7_sweep_basic/jump`. Run `lualatex dtn-sweep-jump.tex`.

### Table 7.3
Change to directory `plots/7_sweep_basic/perturb`. Run `lualatex perturb-table.tex`.

### Fig. 7.5
Change to directory `plots/7_sweep_basic/perturb`. Run  `lualatex dtn-c-perturb.tex`.

### Fig. 7.6
Change to directory `plots/7_sweep_basic/trig_meshes`. Run 

    lualatex simplex-mesh-3layers.tex
    lualatex simplex-mesh-6layers.tex
    lualatex simplex-mesh-12layers.tex
    lualatex simplex-mesh-24layers.tex

### Table 7.4
Change to directory `plots/7_sweep_basic/nonTP`. Run `lualatex simplex-table.tex`.

### Fig. 7.7
Change to directory `plots/7_sweep_basic/nonTP`. Run `lualatex DtN-nonTP-all-order.tex`.

## Chapter 8: Sweeping for helioseismology in axisymmetric setting

### Fig. 8.2
Change to directory `plots/8_sweep_helio/axi_meshes`. Run 

    lualatex axi-mesh-1.5mHz.tex
    lualatex axi-mesh-3.0mHz.tex
    lualatex axi-mesh-6.0mHz.tex 

### Table 8.1
Change to directory `plots/8_sweep_helio/background`. Run `lualatex helio-background-table.tex`.

### Table 8.2
Change to directory `plots/8_sweep_helio/perturb`. Run `lualatex helio-perturb-table.tex`.

### Fig. 8.4 
Change to directory `plots/8_sweep_helio/perturb`.

* (__a__): Run `lualatex dtn-helio-layer-perturb-3mHz.tex`.
* (__b__): Run `lualatex dtn-helio-layer-perturb-6mHz.tex`. 

# <a name="reproNumexp"></a>  Reproducing the data for the figures and tables by running the experiments 

Please switch into the folder `reproduction`.
The scripts for reproducing the experiments are located in subfolders of the folder `scripts`. The subfolders 
are labelled according to the corresponding sections of the paper. The scripts are run with the command 
`python3 scriptname.py `. While running the scripts the data for reproducing the results of the thesis will usually be written to 
specific files (located in the folder in which the script is run) which are explained below for each experiment. For comparison, the folder `data` contains the data 
which has been used in the thesis.
The ordering below is given according to the chapters of the thesis.

## Chapter 2: DtN maps for time-harmonic waves in separable exterior domains
Change to directory `scripts/2_DtN_sep`.
Many of the plots in this chapter are obtained as auxiliary data during computations carried out in subsequent chapters. 
In these cases we will give specific references to the corresponding reproduction instructions of the subsequent chapters. 

### Fig. 2.2
* The real and imaginary parts of dtn are contained in the column `zeta` of the files *jump-dtn-kinf16-real.dat* and *jump-dtn-kinf16-imag.dat*, respectively, which are generated while running the script 
  to reproduce [Fig. 4.6](#figJump).
* The poles of dtn, which are shown in the lower panel of the figure as black crosses, are contained in the file *dtn_hom_poles.dat* which is available after running the reproduction instructions for 
  [Fig 2.3](#figdtnHomExp).

### <a name="figdtnHomExp"></a> Fig. 2.3
Change to direction `scripts/2_DtN_sep/2_2_1_dtn_hom_expansion`.

* Run `python3 dcolor.py`. This produces the domain coloring plot `dtn-hom.png`.
* Run `python3 dtn_expansion.py`. Three files are produced.  
    * The file `dtn_hom_poles.dat` contains the poles of the dtn function. The column `poles_real` contains the real part. The imaginary part is in the column `poles_imag`.  
    * The file `dtn_hom_roots.dat` contains the roots of the dtn function. The column `roots_real` contains the real part. The imaginary part is in the column `roots_imag`.
    * The file `dtn-expansion-err.dat` contains the relative error shown in the right panel of Fig.2.3. The first column *N* gives the number of terms as in the figure. The second column `linear` is simply the reciprocal value of the first column (gray solid line in the figure). The other columns contain the results for the parameter *mu* in the figure. So, *mu* = [1/8,1/4,1/2,1,2,4,8] correspond to the column labels [eights,quarter,half,one,two,four,eight].

### Fig. 2.4
Change to directory `scripts/2_DtN_sep/domain_coloring`. 

* Run `python3 dcolor-jump.py`. This produces the domain coloring plot in the lower right panel.
* The real and imaginary parts of dtn are contained in the column *zeta* of the files *jump-dtn-kinf8-real.dat* and *jump-dtn-kinf8-imag.dat*, respectively, which are generated while running the script 
  to reproduce [Fig. 4.6](#figJump).
* The poles of dtn, which are shown in the lower panel of the figure as black crosses, are contained in the file *poles_analytic_kinf8.dat* which is also available 
  after running the reproduction instructions for [Fig. 4.6](#figJump).

### Fig. 2.5 (b)
The real and imaginary parts of dtn are stored in the columns *dtnReal*, respectively *dtnImag*, of the file *dtn-ellipse-ref-kappaEightThird-asymptotics.dat*, which is avaialable 
after running the reproduction instructions for [Fig.4.7](#figEllipse). 

### Fig. 2.6 (b)

* The real part of dtn is stored in the column *dtn* of the file *waveguide-dtn-k16.5-real.dat* which is available after running the reproduction instructions for [Fig.4.8](#figWaveguide).
* The imaginary part of dtn is stored in the column *dtn* of the file *waveguide-dtn-k16.5-imag.dat* which is available after running the reproduction instructions for [Fig.4.8](#figWaveguide).

### Fig. 2.7 
Change to the folder `scripts/6_helio_tbc/2_DtN_sep`. Run `python3 solar_coeff.py`. 

* (__a__): The data for this plot is contained in the file *c-rho-ModelS.dat*. The first column *r* is the distance to the center of the Sun in terms of the solar radius. 
           The second column *c* is the sound speed in m/sec. The third column *rho* is the density in kg/m^3.
* (__b__): The data for this plot is contained in the file *c-rho-VALC-Atmo.dat*. The sound speed for the VAL-C model is in the column *cV* while the column *cA* contains 
           the sound speed for the Atmo extension. Likewise, the column *rhoV* contains the density for the VAL-C model while the column *rhoA* gives the density for the 
           Atmo model.

### Fig. 2.8
Change to the directory `scripts/6_helio_tbc/2_DtN_sep`. Run `python3 dcolor-VALC.py`. 
Note: Producing the domain coloring plot may take a while. If you only want to obtain the dtn function and its poles (left panel of the figure), 
then kill the script after the poles have been computed (the domain coloring plot is produced last). 

* The values of the dtn function are available in the file *dtn-VALC-ref-0.007Hz.dat*. The column *l* is the index of the eigenvalue. 
  The columns *zetaReal* and *zetaImag* contains real and imaginary parts of the dtn function, respectively.
* The poles of dtn are available in the file *dtn-VALC-ref-0.007Hz-poles.dat*. The column *poles_real* contains the real part and 
  the column *poles_imag* the imaginary part.
* The domain coloring plot is output as *dtn-VALC-large-window.png*.

## Chapter 3: Tensor-product discretizations of DtN
Change to directory `scripts/3_DtN_TP`.

### Fig. 3.3
Change to directory `scripts/3_DtN_TP/3_2_4_SPF/`. 
Run `python3 simple_trigpol.py`. Three files are produced. 

* (__a__) The file *SPF-trafo_deg6_b1.dat* contains the data for the approximation of the transformed function. The first column *x* is the coordinate. 
The second column *ExactReal* is the real part of the exact transformed function at *x* (gray solid line in Fig. 3.3 (__a__)). The fourth column *AppReal* 
is the value of the approximation (blue dashed line).
* (__b__) The file *SPF-original_deg6_b1.dat* contains the data for the approximation of the original function. The columns are labelled as in (__a__).
* (__c__) The file *SPF-relerr_b1.dat* contains the relative errors. The first column *deg* gives the polynomial degree (N in the figure). The second column *trafoError* 
contains the relative error for the transformed function (blue solid line) and the third column *originalError* the relative error for the original function (red solid line) .

### Fig. 3.4
Change to directory `scripts/3_DtN_TP/3_4_comp/`. 
Run `python3 low_order.py`. The data for the figure is contained in the produced files *dtn-real-loworder-omega16-a4.dat* and *dtn-imag-loworder-omega16-a16.dat*.

* (__a__): The data for the real part is in *dtn-real-loworder-omega16-a4.dat*. 
    * The first column *lambda* are the samples. 
    * The second column *zeta* contain the values of the real part of the exact dtn function at *lambda* (solid gray line in figure).
    * The other columns [BGT,UB,AL,HSIE] give the discrete dtn approximation provided by [BGT (solid orange), U-Burnett (dashed green), Astley-Leis (dotted blue), HSIE (dashdotted red)].
* (__b__): The data for the imaginary part is in *dtn-imag-loworder-omega16-a16.dat*. The columns are labelled in the same way as in (__a__). 

### Table 3.1, Fig. 3.5, Fig. 3.6
Change to directory `scripts/3_DtN_TP/3_4_comp/`. 
Run `python3 comparison.py`. 
The data for the table and the figures is stored in the produced files.

* __Table 3.1__: The file DtN-comp-TP-PML-table.dat provides the data for this table.
    * The first column *Ndim* is the dimension of the matrix which equals *N+1* so the first column of Table 3.1.
    * The second column *NdimSquared* is the square of the first column.
    * The column *Nze* gives the number of nonzero matrix elements (*nze* in Table 3.1). 
    * The last column *Nelem* contains the number of mesh elements (*ne* in Table 3.1).
* __Fig. 3.5__: The data for these plots is in files of the form *condext-__x__-omega16.dat* where __x__ is the method. All files use the same labelling of the columns. The column *N* is the y-axis in 
in Fig. 3.5. The column *lam* is the x-axis so the eigenvalue and the column *cond* gives the condition number. 
    * (__a__) The data for the TP-PML is in *condext-TP-PML-omega16.dat*.
    * (__b__) The data for the U-Burnett infinite elements is in *condext-U-Burnett-omega16.dat*.
    * (__c__) The data for the Astley-Leis infinite elements is in *condext-Astley-Leis-omega16.dat*.
    * (__d__) The data for the HSIE is in *condext-HSIE-omega16.dat*.
* __Fig. 3.6__: The data for this plot is distributed among different files. 
    * (__a__): We describe the panels from top to bottom. 
        1. panel: The data for the real part of the exact dtn (solid gray line) can be taken from the columns *zeta* in *dtn-TP-PML-real-omega16.dat*. The column *lam* gives the samples. This file
                  also contains the approximation by TP-PML (solid orange line) in the column *Nze17*. The file *dtn-U-Burnett-real-omega16.dat* contains the data for U-Burnett (green dashed line) in 
                  the column *N3*. The file *dtn-Astley-Leis-real-omega16.dat* contains the data for Astley-Leis (blue dotted line) in the column *N3*. The file *compTP/dtn-HSIE-real-omega16.dat* contains 
                  the data for the HSIE (red dashdotted) in the column *N3*.  
        2. panel: The same as in 1. panel except that the specifier *real* in the filenames has to be replaced by *imag*. 
        3. panel: The relative error for the TP-PML is in the column *Nze17* of the file *relerrTP-PML-omega16.dat*. The relative error for U-Burnett is in the column *N3* of the file *relerrU-Burnett-omega16.dat*.
                  The relative error for *Astley-Leis* is in the column *N3* of the file *relerrAstley-Leis-omega16.dat*. The relative error for the HSIE is in the column *N3* of the file *relerrHSIE-omega16.dat*.
        4. panel: The exact poles (black crosses) are stored in the file *analytic-poles-omega16.dat*. The column *Re* contains the real parts and the column *Im* the imaginary parts. The columns in the other 
                  files are labelled the same way. The approximate poles for the TP-PML are stored in the file *approx-poles-TP-PML-omega16-Nzes17.dat*. The approximate poles for U-Burnett are stored in 
                  the file *approx-poles-U-Burnett-omega16-N3.dat*. The approximate poles for Astley-Leis are stored in the file *approx-poles-Astley-Leis-omega16-N3.dat*. The approximate poles for the HSIE 
                  are stored in the file *approx-poles-HSIE-omega16-N3.dat*.
    * (__b__): 
        1. panel: The same as in (__a__) except that the column *Nze17* for the *TP-PML* needs to be replaced by the column *Nze49* and that the column *N3* for the other methods has to be replaced by *N6*. 
        2. panel: The same as in (__a__) except that the column *Nze17* for the *TP-PML* needs to be replaced by the column *Nze49* and that the column *N3* for the other methods has to be replaced by *N6*. 
        3. panel: The same as in (__a__) except that the column *Nze17* for the *TP-PML* needs to be replaced by the column *Nze49* and that the column *N3* for the other methods has to be replaced by *N6*. 
        4. panel: The same as in (__a__) except that the specifier *Nze17* in the filename for the *TP-PML* needs to be replaced by *Nze49* and that the specifier *N3* in the filename of the other methods 
                  needs to be replaced by *N6*.
    * (__c__): 
        1. panel: The same as in (__a__) except that the column *Nze17* for the *TP-PML* needs to be replaced by the column *Nze97* and that the column *N3* for the other methods has to be replaced by *N9*. 
        2. panel: The same as in (__a__) except that the column *Nze17* for the *TP-PML* needs to be replaced by the column *Nze97* and that the column *N3* for the other methods has to be replaced by *N9*. 
        3. panel: The same as in (__a__) except that the column *Nze17* for the *TP-PML* needs to be replaced by the column *Nze97* and that the column *N3* for the other methods has to be replaced by *N9*. 
        4. panel: The same as in (__a__) except that the specifier *Nze17* in the filename for the *TP-PML* needs to be replaced by *Nze97* and that the specifier *N3* in the filename of the other methods 
                  needs to be replaced by *N9*.

## Chapter 4: Learned infinite elements for individual wavenumbers
Change to directory `scripts/4_learnedIE_individual`. 

### Fig. 4.1, Table 4.1, Fig. 4.2
Change to directory `scripts/4_learnedIE_individual/optimization`. Run `python3 learning_comp.py`. Several data files will be generated.

* __Fig. 4.1.__: 
    * (__a__) The relative errors are stored in files of the form *ansatz-residuals-N__j__.dat* where the integer __j__ denotes the value of N. Each of these files contains three columns. The first *l* 
              describes the index of the eigenvalue as in the figure. The second column *mediumSym* is the relative error for the reduced symmetric ansatz. The last column *full* contains the realtive 
              errors for the full ansatz (i.e. using dense matrices).
    * (__b__) The poles are stored in different files which all have the same structure. Each contains two columns: *poles_real* for the real part of the poles and *poles_imag* for the imaginary part.
              The exact poles are stored in *poles_analytic_opt.dat*. The poles for learned IEs are stored in files of the form *poles_learned_opt_N__j__.dat*, where the integer __j__ denotes the 
              value of N.
* __Table 4.1__: The file *solver_stats_mediumSym.dat* contains the statistics for the reduced symmetric ansatz while the file *solver_stats_full.dat* contains the statistics for the ansatz using dense matrices.
The name of the columns *N*, *iterations*, *finalcost* and *time* correspond to the respective columns of Table 4.1
* __Fig. 4.2.__: The structure of the data files is the same for (__a__) and (__b__). The first column *lam* is the eigenvalue, the second column *N* is self-explanatory and the last column *cond* is the condition number.  
     * (__a__): The data is contained in the file *condext-learned-full-k16.dat*.
     * (__b__): The data is contained in the file *condext-learned-mediumSym-k16.dat*.

### Fig 4.3
Change to directory `scripts/4_learnedIE_individual/spherical_geom/plane_wave_hom`. Run `python3 plane_wave.py`. 

* (__a__): The data for this plot is available in the file *plane-wave-k16.dat*. The first column *N* gives the data for the x-axis. The other columns labelled as *p__j__* contain the relative errors for polynomial degree
           p = __j__ for __j__ in [4,6,8,10,12]. The data for the plot of the real part of the solution is available in the file *plane-wave-k16.vtu* (can be opened with Paraview).
* (__b__): The data for this plot is available in the file *plane-wave-p12.dat*. The first column *N* gives the data for the x-axis. The other columns labelled as *k__j__* contain the relative errors for 
           wavenumber k = __j__ for __j__ in [4,8,16,32].

### Fig. 4.4
Change to directory `scripts/4_learnedIE_individual/spherical_geom/plane_wave_hom`. Run `python3 plane_wave_decrease_coupling_radius.py`.
The generated files share the same data structure. The first column *N* gives the data for the x-axis. 
The other columns, which contain the errors, are labelled as *a__val__*, where __val__ denotes the value of the parameter, i.e. a=__val__. 
The correlation between the colors in the plot and the label of the columns is given by [a0.5625,a0.625,a0.75,a1.0] = [orange, green,red,blue].

* (__a__): The data for this plot is available in the file *plane-wave-a-relerror-heu-weights.dat*.  
* (__b__): The data for this plot is available in the file *plane-wave-a-relerror-opt-weights.dat*. 
* (__c__): The data for this plot is available in the file *plane-wave-a-weightedl2-err-heu-weights.dat*.

### Fig. 4.5
Change to directory `scripts/4_learnedIE_individual/spherical_geom/point_source`. Run 

    python3 pt-source-learnedIE.py "mediumSym"
    python3 pt-source-learnedIE.py "full"
    python3 ptsource-TP-pml.py
    python3 pt-source-HSIE.py
    python3 pt-source-adaptive-pml.py

The following data files are generated: 

* For learned IEs based on the reduced symmetric ansatz: *pt-source-learned-mediumSym-heu-weights.dat* (blue color in the figure). For learned IEs based on the full ansatz: *pt-source-learned-full-heu-weights.dat* (red color).
* For the HSIE: *pt-source-HSIE.dat* (in green color).
* For the TP-PML: *pt-source-TP-PML.dat* (in violet).
* For the adaptive PML: *pt-source-adaptive-PML-close.dat* and *pt-source-adaptive-PML-far.dat* (orange).

Below we explain the data structure of these files.

* The data in the generated files for the learnedIEs, TP-PML and HSIE has the same structure. The column *ndof* gives the number of dofs, which is the data on x-axis of figure 
(__a__) and (__c__). The column *nze* gives the number of nonzero matrix entries which is the data on the x-axis of figure (__b__) and (__d__)  The column *close* contains the relative errors for the case when the point source is close to the coupling boundary,i.e. y=0.95 as shown in figures (__a__) and (__b__).  The column *far* contains the relative errors for the case when the point source far away to the coupling boundary,i.e. y=0.5 as shown in figures (__c__) and (__d__). 
* The only difference for the adaptive PML is that the data for the two cases "close" and "far" is in two separate files *pt-source-adaptive-PML-close.dat* and *pt-source-adaptive-PML-far.dat*, respectively.
The column which contains the relative error is then called *relerr*.

### <a name="figJump"></a> Fig. 4.6
Change to directory `scripts/4_learnedIE_individual/spherical_geom/jump`. Run `python3 jump.py`. 

* (__a__): We describe the different panels separately. 
    * Upper panel: The data for the dtn plot is cotained in the file *jump-dtn-kinf16-real.dat*. The column *lam* represents the data on the x-axis. 
                   The column *zeta* contains the values of the real part of the exact dtn function (gray solid line in the figure). 
                   The columns *N__j__* contain the values of the dtn approximate achieved by learned IEs with N=__j__.
    * Middle panel: The poles of the exact dtn function are stored in the file *poles_analytic_nojump.dat*. The column *poles_real* is the real part and the column *poles_imag* the imaginary part. 
                    The files *poles_learned_nojump_N__j__.dat* contain the poles of the learned IEs with N=__j__. The columns are structured as in *poles_analytic_nojump.dat*. 
    * Lower panel: The file *jump-l2_relerr.dat* cotains the relative L2-errors. The columns *kInf__val__* are labelled according to the value of the exterior wavenumver k_{\infinity} = kInf = __val__. 
                   For example, *kInf8* contains the data for the blue line in the figure, so __val__=8 here.
* (__b__): We describe the different panels separately.   
    * Upper panel: The data for the dtn plot is in the file *jump-dtn-kinf8-real.dat*. The columns are labelled in the same way as in (__a__) Upper panel. Additionally, the columns *real* in the  file *jump-dtn-kinf8-interp.dat* contains 
                   the values of the exact dtn function at the same points (eigenvalues) which are shown as black crosses in the figure. The data for the plot of the real part of the reference solution is available in 
                   the file *jump-sol-k_inf8.vtu* 
    * Middle Panel: The exact poles are contained in *poles_analytic_kinf8.dat*. The poles of the learned IEs are stored in the files *poles_learned_kinf8_N__j__.dat*, where we follow the same naming convention as in 
                    (__a__) Middle panel.
    * Lower panel: The data for the condition number plot is available in the file *condext-learned-kinf8.dat*. The first column *lam* is the eigenvalue, 
                   the second column *N* is self-explanatory and the last column *cond* is the condition number. 

### <a name="figEllipse"></a> Fig. 4.7
Change to directory `scripts/4_learnedIE_individual/ellipse`. Run `python3 ellipse.py`.

* (__a__): The data for the plot of the exact dtn function is available in the file *dtn-ellipse-ref-kappaFourThird.dat*. The column *lam* is the eigenvalue, i.e. the data on the x-axis. 
           The column *dtnReal* contains the real part of the exact dtn function (solid gray line in the plot). 
           The columns *N__j__* in the file *learned-dtn-ellipse-kappaFourThird-real.dat* contain the approximation with learned IEs for N=__j__ for __j__ in [1,3,5].
           The data for the plot of the reference solution displayed in the inset is available in the file *ellipse-kappaFourThird.vtu*.
* (__b__): The data for the plot of the exact dtn function is available in the file *dtn-ellipse-ref-kappaEightThird.dat*.
           The data for the approximation with learned IEs is available in the file *learned-dtn-ellipse-kappaEightThird-real.dat*.
           The naming conventions for the columns are as in (__a__). 
           The data for the plot of the reference solution displayed in the inset is available in the file *ellipse-kappaEightThird.vtu*.
* (__c__): The data for the plot of the exact dtn function is available in the file *dtn-ellipse-ref-kappaTwelveThird.dat*.
           The data for the approximation with learned IEs is available in the file *learned-dtn-ellipse-kappaTwelveThird-real.dat*.
           The naming conventions for the columns are as in (__a__). 
           The data for the plot of the reference solution displayed in the inset is available in the file *ellipse-kappaTwelveThird.vtu*.
* (__d__): The relative errors are stored in the file *ellipse-k32-relerr.dat*. The columns [kappaFourThird,kappaEightThird,kappaTwelveThird] are labelled according to the different 
           values of the parameter kappa.

### <a name="figWaveguide"></a> Fig. 4.8
Change to directory `scripts/4_learnedIE_individual/waveguide`. Run `python3 waveguide.py`.

* (__a__): The data for the plot of the imaginary part of the exact dtn function (solid gray line in the figure) is contained in the column *dtn* of the file *waveguide-dtn-k16.5-imag.dat*. 
           The column *lam* always gives the data on the x-axis.
           The values of the exact dtn function at the sample points/eigenvalues (black crosses) are available in the column *imag* of the file *waveguide-dtn-k16.5-interp.dat*. 
           The learned IE approximations of the dtn function are also stored in the file *waveguide-dtn-k16.5-imag.dat*. 
           The columns *N__j__* contain the approximation with learned IEs using N==__j__. 
           The poles of the learned IEs are available in the files *poles_learned_guide_N__j__.dat* for N == __j__ in [1,2,3,..]. The column *poles_real* gives the real part of the poles
           and the column *poles_imag* the imaginary part.
* (__b__): The relative L2-errors are available in the file *waveguide-k16.5.dat*. The columns *p__j__* contain the relative errors for p=__j__.
           The data for the plot of the reference solution is available in the file *waveguide-refsol-k16.5.vtu*.

## Chapter 5: Learned IEs providing uniform approximation in the wavenumber
Change to directory `scripts/5_learnedIE_uniform`. 

### Fig. 5.1
Change to directory `scripts/5_learnedIE_uniform`. Run 
    
    python3 plane_wave_uniform.py 8
    python3 plane_wave_uniform.py 16

* (__a__): The data for this plot is available in the file *LIE-helm-interval-sup-Nomega8.dat*. The first column *N* is the data on the x-axis. The second column *omega* is the wavenumber k, so the data on the y-axis.
           The column *sup* then contains the supremum error of the modes at (N,k). 
* (__b__): The data for this plot is available in the file *LIE-helm-interval-Nomega8.dat*. The first column *N* is the data on the x-axis. The second column *omega* is the wavenumber k, so the data on the y-axis.
           The column *sup* then contains the relative error at (N,k). 
* (__c__): The data for the upper panel is provided in the file *LIE-helm-interval-sup-Nomega16.dat*. The columns are labelled as in (__a__). The data for the lower panel (case N=9) is contained in the file 
           *LIE-helm-interval-sup-Nomega16-N9.dat*. The column *omega* gives the value of the wavenumber k and the column *sup* the supremum error of the modes at (N=9,k).
* (__d__): The data for the upper panel is provided in the file *LIE-helm-interval-Nomega16.dat*. The columns are labelled as in (__b__). The data for the lower panel (case N=9) is contained in the file 
           *LIE-helm-interval-relerr-Nomega16-N9.dat*. The column *omega* gives the value of the wavenumber *k* and the colum *relerr* the relative error at (N=9,k).

### Fig. 5.2
Change to the directory `scripts/5_learnedIE_uniform`. Run

    python3 res-comp.py
    python3 resonances-disk.py

Warning: These computations may require some time to finish.

* The value of the objective function shown in the upper left panel of the figure is available in the column *finalcost* of the file *solver_stats_res.dat*. The column *N* is the data
  on the x-axis.
* The exact resonances (shown as black crosses in the plot) are available in the file *exact-res-Dirichlet-tuple.dat*. The column *x* is the real part and the column 
  *y* the imaginary part. 
* The computed resonances based on learned IEs using N = __j__ are available in the file *learned-res-Dirichlet-tuple-N__j__.dat*. The column *x* is the real part and the column 
  *y* the imaginary part. 
* The samples for k used in the dtn approximation are stored in the file *res-Dirichlet-omega-samples.dat*. The column *x* is the real part and the column 
  *y* the imaginary part. 

## Chapter 6: Modelling the solar atmosphere with learned IEs
Change to directory `6_helio_tbc`. 

### Fig 6.1, Fig. 6.17
Change to directory `scripts/6_helio_tbc/obs_helio`. Run `python3 learn-helio-VALC-Atmo-comp.py`. 

* Fig. 6.1
    * (__a__): The real part of dtn for the VAL-C model is contained in the column *zetaReal* of the file *dtn-VALC-ref-0.0035Hz.dat*. The column *l* gives the index of the eigenvalue. 
           The real part of dtn for the Atmo model is contained in the column *zetaReal* of the file *dtn-Atmo-ref-0.0035Hz.dat*.  
    * (__b__)  The imaginary part of dtn for the VAL-C model is contained in the column *zetaImag* of the file *dtn-VALC-ref-0.0035Hz.dat*. 
           The imaginary part of dtn for the Atmo model is contained in the column *zetaImag* of the file *dtn-Atmo-ref-0.0035Hz.dat*.  
    * (__c__): The real part of dtn for the VAL-C model is available in the column *zetaReal* of file *dtn-VALC-ref-0.008Hz.dat*. The poles of the exact dtn function are contained in the file 
           *poles_dtn_VALC_analytic_0.008Hz.dat*. The column *poles_real* contains the real part and the column *poles_imag* the imaginary part. 
           The real part of dtn for the Atmo model is contained in the column *zetaReal* of the file *dtn-Atmo-ref-0.008Hz.dat*.  
    * (__d__): The imaginary part of dtn for the VAL-C model is contained in the column *zetaImag* of the file *dtn-VALC-ref-0.008Hz.dat*. 
           The imaginary part of dtn for the Atmo model is contained in the column *zetaImag* of the file *dtn-Atmo-ref-0.008Hz.dat*.  
* Fig. 6.17
    * (__a__): The real part of the learned dtn function for N==__j__ is available in the columns *N__j__*  of file *learned-dtn-VALC-real-0.008Hz.dat* for __j__ in [1,2,3,4].
               The learned poles are available in the files *poles_dtn_VALC_learned_0.008Hz_N__j__.dat* for __j__ in [1,2,3,4].
    * (__b__): The imaginary part of the learned dtn function for N==__j__ is available in the columns *N__j__*  of file *learned-dtn-VALC-imag-0.008Hz.dat* for __j__ in [1,2,3,4].
               The learned poles are available in the files *poles_dtn_VALC_learned_0.008Hz_N__j__.dat* for __j__ in [1,2,3,4].

### Fig. 6.2, Fig.6.4, Fig. 6.5 (__b__)-(__d__), Fig.6.6, Fig. 6.7
[Above](#IntroHelioDownload) we already explained how to reproduce these figures using power-spectra which were downloaded from the gro.data catalogue. 
It therefore remains now only to compute the power spectra for the VAL-C model with the meshed atmosphere *power-spectrum-VALC-meshed.out* and
for the Atmo model *power-spectrum-Atmo-whitw.out*. To this end, change into the folder `scripts/6_helio_tbc/obs_helio`. The computation in
the script `power-spectrum.py` consists of two steps.

* First the dtn numbers *dtn_nr_VALC_intro.out* or *dtn_nr_Whitw_intro.out* have to be computed. For the VAL-C model this is done by solving ODEs and 
   for the Atmo model it amounts to evaluating the Whittaker function. Especially the latter computation requires a lot of time. Therefore, we offer the option 
   to skip this step by downloading the dtn numbers from gro.data. To compute the desired power spectra for the VAL-C and Atmo model with precomputed
   dtn numbers one can run
       
       python3 power-spectrum.py "VALC" "download"
       python3 power-spectrum.py "Atmo" "download"     
   This will produce the power spectra *power-spectrum-VALC-meshed.out* and *power-spectrum-Atmo-whitw.out*. 
* If you would prefer to also compute the dtn numbers yourself, then omit the "download" flag

       python3 power-spectrum.py "VALC" 
       python3 power-spectrum.py "Atmo"     

First of all, these script actually compute the poles of the dtn functions with respect to the frequency which are displayed in the lower 
panels of Fig. 6.2 (__e__) and Fig. 6.2 (__f__). They are stored in the files *dtn-freq-Atmo-model-l200.dat* and *dtn-freq-VALC-model-l200.dat*. 
The columns *poles_real* and *poles_imag* contain the real and imaginary parts of the poles, respectively.

After finishing these computations please continue with the instructions given for the case of already provided data, see e.g.[here](#IntroHelioDownload). The "download" flag 
in these instructions can now be omitted. 

### Fig. 6.3 (__b__)
Change to the directory `scripts/6_helio_tbc/discussion`. Run `python3 fit_damping.py`. 
The produced file *f-FWHM-modes.dat* contains the data for the FWHM of the modes. The column *f* is the frequency and the 
column *FWHM* the FWHM.

### Fig. 6.5 (__a__):
Change to directory `scripts/6_helio_tbc/obs_helio`. Run `python3 rays-plot.py`. This will generate the file `rays-modelS.tex`, which can be 
compiled with `lualatex rays-modelS.tex`.


###  Fig. 6.8, Fig. 6.9, Fig. 6.10, Fig. 6.11
[Above](#AtmoDownload) we already explained how to reproduce these figures with power spectra and dtn numbers downloaded from gro.data catalogue.
Now we want to show how to compute this downloaded data. To this end, change into the folder `scripts/6_helio_tbc/tbc_Atmo`. 
The following computations should preferably be run on a compute cluster. 

* Exact dtn numbers (from Whittaker function): Run `python3 power-Atmo.py "exact"`. Afterwards, the dtn numbers *dtn_nr_Whitw-comp.out* and the 
  power spectrum *power-spectrum-Atmo-whitw-comp.out* will be available.
* Nonlocal condition: Run  `python3 power-Atmo.py "nonlocal"`. Afterwards, the dtn numbers *dtn_nr_nonlocal.out* and the power spectrum  *power-spectrum-nonlocal.out* 
  will be avaialble.
* S-HF-1a: Run `python3 power-Atmo.py "SHF1a"`. Afterwards, the dtn numbers *dtn_nr_S-HF-1a.out* and the power spectrum *power-spectrum-SHF1a.out* 
  will be avaialble.
* A-RBC-1:  Run `python3 power-Atmo.py "ARBC1"`. Afterwards, the dtn numbers *dtn_nr_ARBC1.out* and the power spectrum *power-spectrum-ARBC1.out* 
  will be avaialble.
* Learned: Here we separate the learning step from the computation of the power spectra. 
    * First run `python3 power-Atmo.py "only_learning"`. Afterwards, the dtn numbers *dtn_nr_learned_N__j__-Atmo.out* for __j__ in [0,1,2,3,4] will be available.
    * Then run 
        
            python3 power-Atmo.py "learned" 0
            python3 power-Atmo.py "learned" 1
            python3 power-Atmo.py "learned" 2
            python3 power-Atmo.py "learned" 3
            python3 power-Atmo.py "learned" 4
    
      The command `python3 power-Atmo.py "learned" __j__` computes the power spectrum for learned IEs using N = __j__. Since these computations are independent, 
      it makes sense to distribute them onto different compute nodes if you have access to a cluster.
      The computed power spectra will be available in the files *power-spectrum-N__j__-Atmo.out* for __j__ in [0,1,2,3,4].

To produce the figures based on the computed data proceed as in the description [above](#AtmoDownload) omitting the "download" flag. 
That is, change to the folder `scripts/6_helio_tbc/tbc_Atmo` and run `python3 postprocess-Atmo.py`.


### Fig.6.12 - Fig. 6.17
[Above](#VALCDownload) we already explained how to reproduce these figures with power spectra downloaded from gro.data catalogue.
Now we want to show how to compute this downloaded data. To this end, change into the folder `scripts/6_helio_tbc/tbc_VALC`. 
The following computations should preferably be run on a compute cluster. 

* Exact dtn numbers (from solving ODEs): Run `python3 power-VALC.py "exact"`. Afterwards, the dtn numbers *ODE_dtn_nr.out* and the 
  power spectrum *power-spectrum-VALC-meshed.out* will be available
* Learned: Here we again separate the learning step from the computation of the power spectra. 
    * First run `python3 power-VALC.py "only_learning"`. Afterwards, the dtn numbers *dtn_nr_learned_N1-VALC.out* for __j__ in [0,1,2,3,4] will be available.
    * Then run 
        
            python3 power-VALC.py "learned" 0
            python3 power-VALC.py "learned" 1
            python3 power-VALC.py "learned" 2
            python3 power-VALC.py "learned" 3
            python3 power-VALC.py "learned" 4
            
      The command `python3 power-VALC.py "learned" __j__` computes the power spectrum for learned IEs using N = __j__. Since these computations are independent, 
      it makes sense to distribute them onto different compute nodes if you have access to a cluster.
      The computed power spectra will be available in the files *power-spectrum-N__j__-VALC.out* for __j__ in [0,1,2,3,4].

To produce the figures based on the computed data, proceed as in the description [above](#VALCDownload) omitting the "download" flag. 

### Fig. 6.18 (__b__)
We have to produce the data for the power spectra based on the different damping models. 
The power spctrum based on the power law model is already available from the reproduction of Fig. 6.2 (we will download it here from gro.data).
For the l-dependent daping model we will only produce slices at l=50 and l=300 through the power spectrum which suffices to produce the figure.
Change to the directory `scripts/6_helio_tbc/discussion`. 
Run 

    python3 fit_damping.py
    python3 power-damping.py
    python3 load-power-damping.py

Two files, one for l=50 and the other for l=300, will be produced.

* The file *power-f-low-l50-damping-comp.dat* contains the data for the slice through the power spectrum at l=50. 
  The colum *f* is the frequency. The column *plaw* contains the power for the power-law damping model and the column *opt* the 
  power for the l-dependent damping model.
* The file *power-f-low-l300-damping-comp.dat* contains the results for l=300. The columns are labelled as above.

The folder `data/6_helio_tbc/discussion` contains a copy of this data which you may want to replace with the data just produced.
Then switch to the folder `plots/6_helio_tbc/discussion` and run `lualatex power-damping-comp.tex` to compile the figure.

## Chapter 7: Learned IEs as transmission conditions in sweeping preconditioners
Change to directory `7_8_sweeping`.

### Table 7.1, Fig. 7.2, Fig. 7.3
Run `python3 academic-cartesian-const.py`. This will generate all data for the figures and the table. 

* Fig. 7.2:The meshes shown in Fig. 7.2 are available in the files *cartesian-mesh-__j__layers.tex* where __j__ in [3,6,12,24] denotes the number of layers.
* Table 7.1 (upper table): The iteration numbers for the PML at the top are avaialable in the file *const_iter_PML.dat*. The first column *omega* is the value of the wavenumber k. 
  The second column *nlayers* denotes the number of layer *J*. The columns *N__j__* contain the iterations numbers using learned IEs with N=__j__. The last column *mPML* contains 
  the iteration numbers for the moving PML.
* Table 7.1 (lower table): The iteration numbers for the Neumann boundary condition at the top are available in the file *const_iter_Neumann.dat*. The columns are ordered in 
  the same wave as explained above for *const_iter_PML.dat*.
* Fig. 7.3 (__a__): 

    * The data for the exact dtn function (solid gray line in the figure) is available in the file *learned_dtn_omega96_PMLbc_const_real.dat*. 
      The column *lam* is the data on the x-axis and the column *dtn* the real part of dtn. 
    * The data for the exact dtn function at the samples (eigenvalues) is available in the column *dtn* of the file *dtn_at_eigenv_omega96_PMLbc_const_real.dat*.
    * The data for the learned IE approximation of dtn (dotted blue line) using N=10 is available in the column *N10* of the file *learned_dtn_omega96_PMLbc_const_real.dat*.
    * The data for the moving PML (mPML) approximation of dtn (dashed red line) is available in the column *dtn* of the file *mPML_dtn_omega96_PMLbc_const_real.dat*. 
* Fig. 7.3 (__b__): 

    * The data for the exact dtn function (solid gray line in the figure) is available in the file *learned_dtn_omega96_Neumannbc_const_real.dat*. 
      The column *lam* is the data on the x-axis and the column *dtn* the real part of dtn. 
    * The data for the exact dtn function at the samples (eigenvalues) is available in the column *dtn* of the file *dtn_at_eigenv_omega96_Neumannbc_const_real.dat*.
    * The data for the learned IE approximation of dtn (dotted blue line) using N=10 is available in the column *N10* of the file *learned_dtn_omega96_Neumannbc_const_real.dat*. 
    * The data for the moving PML (mPML) approximation of dtn (dashed red line) is available in the column *dtn* of the file *mPML_dtn_omega96_Neumannbc_const_real.dat*.
    * The poles of the exact dtn function (orange crosses in lower panel) are stored in the file *academic_Neumann_poles_analytic.dat*. The column *poles_real* gives the
      real part and the column *poles_imag* the imaginary part.
    * The poles of the learned IE approximation of dtn using N=10 are available in the file *academic_Neumann_poles_learned.dat*. The columns are labelled as explained 
      above for *academic_Neumann_poles_analytic.dat*. 


###  Table 7.2, Fig. 7.4,
Run `python3 academic-cartesian-jump.py`. This will generate all data for the table and the figure.

* Table 7.2: 
    * Moving PML: The iteration numbers for the moving PML (uppermost table) are available in the file *jump_iter_mPML.dat*. The first column *omega* is the wavenumber k.
      The second column *nlayer* denotes the number of layers J. The other columns [alpha0,alphaSixteenth,alphaEights,alphaQuarter,alphaHalf,alpha1] give the iteration numbers 
      for alpha = [0,1/16,1/8,1/4,1/2,1] (ordered from left to right as in Table 7.2)
    * Learned IE N=12: The iteration numbers shown in the middle table are contained in the file *jump_iter_learned_N12.dat*. The columns are labelled as for *jump_iter_mPML.dat*
      explained above.
    * Learned IE alpha=1/4: The iteration numbers shown in the lowest table are available in the file *jump_iter_learned_alphaQuarter.dat*. The columns *N__j__* contain 
      the iteration numbers for N=__j__ for __j__ in [2,4,6,8,10,12].
* Fig. 7.4: 
    * The data for the exact dtn function (solid gray line in the figure) is available in the file *learned_dtn_omega96_PMLbc_jump_alpha1.0_real.dat*. 
      The column *lam* is the data on the x-axis and the column *dtn* the real part of dtn. 
    * The data for the exact dtn function at the samples (eigenvalues) is available in the column *dtn* of the file *dtn_at_eigenv_omega96_PMLbc_jump_alpha1.0_real.dat*.
    * The data for the learned IE approximation of dtn (dotted blue line) using N=12 is available in the column *N12* of the file *learned_dtn_omega96_PMLbc_jump_alpha1.0_real.dat*. 
    * The data for the moving PML (mPML) approximation of dtn (dashed red line) is available in the column *dtn* of the file *mPML_dtn_omega96_PMLbc_jump_alpha1.0_real.dat*.


### Table 7.3, Fig. 7.5
Run `python3 academic-cartesian-perturbation.py`. This will generate all data for the table and the figure, except the poles and roots shown in the lower panel. 
To obtain those we have to run `python3 academic-cartesian-const.py` (note: if you habe already run the instructions to reproduce Table 7.1, Fig. 7.2 and Fig. 7.3
given above then this data will already be available).
 

* Table 7.3: The structure of all the files is the same. The first column *omega* is the wavenumber k. The second column *nlayers* gives the number of layers *J*.
  The other columns [zero,sixteenth,eigths,quarter,half,one,two,four] contain the iteration number for the strength of the perturbation 
  epsilon = [0.0%,0.00625%,0.125%,0.25%,0.5%,1.0%,2.0%,4.0%].
    * Moving PML (first table): The data for the Neumann boundary condition at the top of the domain is contained in the file *perturb_iter_mPML_Neumann.dat*. 
      The data for the PML boundary condition at the top of the domain is contained in the file *perturb_iter_mPML_PML.dat*.
    * Direct solver inverse of background model: The data for the Neumann boundary condition at the top of the domain is contained in the file *perturb_iter_DirectSolverBackground_Neumann.dat*.
      The data for the PML boundary condition at the top of the domain is contained in the file *perturb_iter_DirectSolverBackground_PML.dat*. 
    * Learned IE for background model on layer:  The data for the Neumann boundary condition at the top of the domain is contained in the file *perturb_iter_learned_Neumann_sweep4background_N10.dat*. 
      The data for the PML boundary condition at the top of the domain is contained in the file *perturb_iter_learned_PML_sweep4background_N10.dat*. 
    * Learned IE for perturbed model on layer:  The data for the Neumann boundary condition at the top of the domain is contained in the file *perturb_iter_learned_Neumann_sweep4pert_N10.dat*. 
      The data for the PML boundary condition at the top of the domain is contained in the file *perturb/perturb_iter_learned_PML_sweep4pert_N10.dat*.
 * Fig. 7.5: 
    * The real parts of the perturbed dtn function for the Neumann boundary condition are available in the file *perturbed_sound_speed_Neumann.dat*. The column *lam* gives the sample of the x-axis. 
      the columns [eps0.0,eps0.0025,eps0.01,eps0.01] gives the function values for perturbation epsilon in [0.0%,0.25%,1.0%,4.0%].
    * The real parts of the perturbed dtn function for the PML boundary condition are available in the file *perturbed_sound_speed_PML.dat*. The columns are labelled as described above 
      for *perturbed_sound_speed_Neumann.dat*.
    * The relative errors shown in the middle panel on the left for the Neumann boundary condition are contained in the file *perturbed_sound_speed_Neumann_relerr.dat*. The columns are 
      labelled as above.
    * The relative errors shown in the middle panel on the left for the PML boundary condition are contained in the file *perturbed_sound_speed_PML_relerr.dat*. The columns are 
      labelled as above.
    * The poles of the  dtn function for the background model are stored in the file *academic_Neumann_poles_analytic.dat*. The column *poles_real* gives the
      real part and the column *poles_imag* the imaginary part.
      The roots of the dtn function for the background model are stored in the file *academic_Neumann_roots_analytic*. The columns *roots_real* gives the 
      real part and the column *roots_imag* the imaginary part.

### Fig. 7.6, Table 7.4, Fig. 7.7
Run `python3 academic-cartesian-simplex-vs-quads.py`. This generates all data for the table and the figures.

* Fig. 7.6: The meshes shown in Fig. 7.6 are available in the files *simplex-mesh-__j__layers.tex* where __j__ in [3,6,12,24] denotes the number of layers.
* Table 7.4: 
    * PML at top: The iteration numbers for the upper table are stored in the file *const_simplices_iter_PML.dat*. The first column *omega* denotes the wavenumber k. 
      The second column *nlayers* gives the number of layers J. 
      The columns *N__j__* contain the iteration numbers for learned IEs using N=__j__.
      The last column *mPML* gives the iteration numbers for the moving PML.
    * Hom. Neumann b.c. at top: The iteration numbers for the lower table are stored in the file *const_simplices_iter_Neumann.dat*. The columns are labelled as explained above 
      for *const_simplices_iter_PML.dat*.
* Fig. 7.7: The data for this plot is available in the files *DtN-nonTP-order__p__.dat* where __p__ in [1,2,4] denotes the FEM order (denoted p in the figure). 
            Each of these files contains three columns. The first two columns *col* and *row* denote the column and row index of the matrix, respectively. 
            The column *abs* gives the absolute value of the corresponding matrix entry.


## Chapter 8: Sweeping for helioseismology in axisymmetric setting
Warning: The experiments in this chapter are very demanding in terms of computational resources and should therefore be run on a compute cluster.
Change to directory `7_8_sweeping`.

### Figure 8.3: 

#### Computing the data for the power spectra
Calling `python3 ModelS-halfdisk-power-cluster.py p` computes the power spectrum for order *p*. 
Since we want *p* in [3,5,7], we execute: 

    python3 ModelS-halfdisk-power-cluster.py 7 
    python3 ModelS-halfdisk-power-cluster.py 5 
    python3 ModelS-halfdisk-power-cluster.py 3 


Now the data for the power spectra is available in the files *power-spectrum_VAL-C_order__p___angleref1p5_1800freq.out* for __p__ in [3,5,7].

#### Plotting the power spectra
Now we can generate the plots for the power spectra by running `python3 python3 load-power.py`. 
This will generate the plots *power-spectrum_VAL-C_order__p___angleref1p5_1800freq.png* for __p__ in [3,5,7] which correspond 
to subfigures [(__a__),(__b__),(__c__)].

### Fig. 8.2, Table 8.1
Run `python3 helio-background.py`. This will generate all the data.

* Fig. 8.2: The produced latex files for the frequencies __f__ at which sweeping is performed are called *axi-mesh-__f__mHz.pdf*. If you would like to produce meshes for other frequencies (and also obtain iteration 
  numbers) just change the array *fs_mHz* in *helio-background.py* according to your needs.
* Table 8.1: The iteration numbers for order __p__ are contained in the files *helio_VAL-C_iter_learned_order__p__.dat* for __p__ in [4,6]. The column *mHz* is denotes the frequency in Millihertz. 
             The column *nlayers* gives the number of layers. The columns N__j__ contain the iteration numbers for learned IEs using N=__j__. 
             The maximum number of iterations has been set to 250. So a 250 means that the tolerance was not achieved after 250 iterations.

### Table 8.2, Fig. 8.4
Run `python3 helio-perturb.py`. This will generate all the data.

* Table 8.2: The iteration numbers for learned IEs and for preconditioning with the inverse of the background model (in round brackets in the table) are stored in two different files which share the same data structure.
             The column *mHz* is denotes the frequency in Millihertz. The column *nlayers* gives the number of layers.
             The columns [zero,sixteenth,eigths,quarter,half,one,two] contain the GMRES iteration numbers for perturbation epsilon in [0.0%,0.0625%,0.125%,0.25%,0.5%,1.0%,2.0%] 
             (ordered as in Table 8.2. from left to right). 
    * Learned IE N=7: Iteration numbers for this case are stored in the file *helio_perturb_iter_learned_sweep4background_N7.dat*.
    * Direct solver inverse of background model: Iteration numbers for this case are stored in the file *helio_perturb_iter_DirectSolverBackground.dat*.
* Fig. 8.4: The structure of the data files for cases (__a__) and (__b__) is the same. The data for the dtn functions is contained in the files *perturbed_sound_speed_helio_fhz__f__.dat* 
            where __f__ denotes the frequency. The columns [epszero,epssixteenth,epsquarter,epsone] contain the real part of dtn for epsilon in [0.0%,0.0625%,0.25%,1.0%] as shown in the 
            upper panel of the subfigures.  
            The files *perturbed_sound_speed_helio_relerr_fhz__f__.dat* cotain the relative dtn errors shown in the lower panel. The columns are labelled as explained above for 
            *perturbed_sound_speed_helio_fhz__f__.dat* 
    * (__a__): The data for the dtn function and the relative error for 3.0 mHz are available in the files *perturbed_sound_speed_helio_fhz0.003.dat* and 
               *perturbed_sound_speed_helio_relerr_fhz0.003.dat*, respectively.
    * (__b__):  The data for the dtn function and the relative error for 6.0 mHz are available in the files *perturbed_sound_speed_helio_fhz0.006.dat* and 
               *perturbed_sound_speed_helio_relerr_fhz0.006.dat*, respectively.


 
