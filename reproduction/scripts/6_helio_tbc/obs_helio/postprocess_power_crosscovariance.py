from math import pi
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import sys
import urllib.request 

plt.rc('legend', fontsize=18)
plt.rc('axes', titlesize=18)
plt.rc('axes', labelsize=18)
plt.rc('xtick', labelsize=14)    
plt.rc('ytick', labelsize=14)

plt.rcParams['savefig.dpi'] = 300
plt.rcParams["figure.dpi"] = 100

if ( len(sys.argv) > 1 and sys.argv[1] == "download"):
    print("Downloading data from Gro.data catalogue")
    file_atmo, header_atmo = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/GJ4PGF/WQCNHP","power-spectrum-Atmo-whitw.out")
    file_VALC, header_VALC = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/GJ4PGF/RQBANI","power-spectrum-VALC-meshed.out")
    file_MDI, header_MDI = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/GJ4PGF/HXFM3S","mdi_medium_l.np")
    
    file_atmo_dtn, header_atmo_dtn = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/GJ4PGF/INWQFM","dtn_nr_Whitw_intro.out")
    file_VALC_dtn, header_VALC_dtn = urllib.request.urlretrieve("https://data.goettingen-research-online.de/api/access/datafile/:persistentId?persistentId=doi:10.25625/GJ4PGF/HIP9BG","dtn_nr_VALC_intro.out")
    print("done")


L_min = 0
L_max = 1000  # how many DtN numbers to take
#L_max = 500  # how many DtN numbers to take
L_spacing = 1
L_range = np.arange(L_min,L_max+1,L_spacing)
#f_hzs = np.linspace(0.001,8.3,7200)
#f_hzs = np.linspace(0.001,8.3,300)

#f_hzs = np.linspace(0,8.3,1000)
f_hzs = np.linspace(0,8.3,7200)
#f_hzs = np.linspace(0,12.0,2400)
#f_hzs = np.linspace(0.001,8.3,100)
Ns = range(5)

power_spectrum_obs = np.load('mdi_medium_l.np')
f_max = 0.0803755*103680/1e6  # according to website
#f_hz_obs = 1e-3*2*pi*np.linspace(0.0,f_max*1e3,power_spectrum_obs.shape[0])

f_hz_obs = 1e-3*np.linspace(0.0,f_max*1e3,power_spectrum_obs.shape[0])

index_1mhz = np.where( f_hzs > 1.0)[0][0]


#f_hzs = np.linspace(0.001,8.3,300)
f_hzs = (10**-3)*f_hzs

dtn_nr_VALC = np.loadtxt('dtn_nr_VALC_intro.out',dtype="complex",delimiter=',')
dtn_nr_Atmo = np.loadtxt("dtn_nr_Whitw_intro.out",dtype="complex",delimiter=',')

idx_l = 200
for dtn_nr,dtn_str in zip([dtn_nr_VALC, dtn_nr_Atmo],["VALC","Atmo"]):

    #plt.figure(figsize=(6,5),constrained_layout=True)
    plt.figure(figsize=(6,3),constrained_layout=True)

    #im = plt.pcolormesh(L_range,10**3*f_hzs,np.abs(dtn_nr)/np.max( np.abs(dtn_nr) ),
    #im = plt.pcolormesh(L_range,10**3*f_hzs,np.imag(dtn_nr) ,
    im = plt.pcolormesh(L_range,10**3*f_hzs,np.abs(dtn_nr) ,
                       cmap=plt.get_cmap('viridis'),
                       norm=colors.LogNorm( vmin=  302   ,vmax= 14792 )
                       #norm=colors.LogNorm( vmin=  np.abs(dtn_nr).min()   ,vmax= np.abs(dtn_nr).max()    )
                       )

    plt.axvline(x=idx_l ,color='r',linestyle='--',linewidth=3,ymin= 10**3*f_hzs[10]   , ymax= 10**3*f_hzs[-10]  )
    plt.rc('legend', fontsize=16)
    plt.rc('axes', titlesize=16)
    plt.rc('axes', labelsize=16)
    plt.xlabel("Harmonic degree $\ell$")
    plt.ylabel("Frequency (mHz)")
    #plt.title("Model S + PML")
    #plt.title("Model S + smoothed VAL-C + PML")
    cbar = plt.colorbar(im,)
    #cbar.set_ticks([1e3,5e3,1e4])
    #cbar.set_ticklabels(['$10^3$','$10^4$'])
    plt.savefig("abs-dtn-{0}-model.png".format(dtn_str),transparent=True,dpi=300)
    plt.show()
    
    plt.plot(10**3*f_hzs,dtn_nr[:,idx_l].real,label="real")
    plt.plot(10**3*f_hzs,dtn_nr[:,idx_l].imag,label="imag")
    #plt.title("Real")
    plt.show()
    
    #plt.plot(10**3*f_hzs,dtn_nr[:,idx_l].imag)
    #plt.title("Imag")
    #plt.show()

    fname_dtn = "dtn-freq-{0}-model-l{1}.dat".format(dtn_str,idx_l)
    np.savetxt(fname=fname_dtn,
               X=np.transpose([ 10**3*f_hzs, dtn_nr[:,idx_l].real , dtn_nr[:,idx_l].imag ]), 
               header = 'mHz dtnReal dtnImag',
               comments='' 
              )


#################################################### 
# load reference power spectrum
####################################################

mdi_data = np.loadtxt("../multiplets-mdi-2001.dat",unpack=False)
hmi_data = np.loadtxt("../multiplets-hmi-2010.dat",unpack=False)
#n_ref = hmi_data[:,0]
print("hmi_data = ", hmi_data)

def select_data(data):
    l_ref = data[:,1]
    nu_ref = data[:,2]
    ref_selected_idx = np.array([],dtype=int)
    N_ref_samples = 12
    for l in [int(l_ref[20]+(l_ref[-100]-l_ref[20])*i/(N_ref_samples-1)) for i in range(N_ref_samples)]:
        idx_l = np.where(l_ref==l)[0]
        ref_selected_idx = np.append(ref_selected_idx,idx_l)
    return l_ref,nu_ref,ref_selected_idx

l_ref,nu_ref,ref_selected_idx = select_data(mdi_data) 
l_ref_hmi,nu_ref_hmi,ref_selected_idx_hmi = select_data(hmi_data) 

print("ref_selected_idx =" , ref_selected_idx)
print("l_ref[ref_selected_idx] = ", l_ref[ref_selected_idx])
print("nu_ref[ref_selected_idx] = ", nu_ref[ref_selected_idx])


def scale_power_spectrum(power,f_hzs):

    def PI_scal(f):
        result = 1 + ((abs(f)-3.3*1e-3)/ (0.6*1e-3) )**2
        #return 1.0
        return 1/result 
    
    #return power
    #def PI_scal(f):
    #    result = 1 + ((abs(f)-3.3*1e-3)/ (0.6*1e-3) )**2
    #    return 1/result 
    
    scaled_power = np.zeros(power.shape) 
    for i  in range(power.shape[0]):
        if abs(f_hzs[i]) > 1e-10:
            scaled_power[i,:] = power[i,:]*0.5*PI_scal(f_hzs[i])/(2*pi*f_hzs[i])
        #scaled_power[i,:] = power[i,:]*0.5*PI_scal(f_hzs[i])
    
    return scaled_power



def apply_Granulation_filter(power,f_hz):    
    
    def Granu_filter(f):
        #print("f = {0}, filtered = {1}".format(f,np.tanh(3*f/0.0015)))
        return np.tanh(3*f/0.0015)

    filtered_power = np.zeros(power.shape) 
    for i in range(power.shape[0]):
        filtered_power[i,:] = power[i,:]*Granu_filter(f_hz[i])
    
    return filtered_power


# load power spectra and apply scaling:
power_spectrum_Atmo = np.loadtxt('power-spectrum-Atmo-whitw.out',delimiter=',')
power_spectrum_VALC = np.loadtxt('power-spectrum-VALC-meshed.out',delimiter=',')

def post_process_power(ps):
    ps = np.abs(ps)
    #ps *= 1/np.max(ps)
    ps = scale_power_spectrum(ps,f_hzs)
    #ps = apply_Granulation_filter(ps,2*pi*f_hzs)
    #print("max =", np.max(ps))
    return ps
    #ps *= 1/np.max(ps)
    return ps

power_spectrum_Atmo = post_process_power(power_spectrum_Atmo)
power_spectrum_VALC = post_process_power(power_spectrum_VALC)

power_spectrum_obs = np.abs(power_spectrum_obs)/1e5 
power_spectrum_obs =  apply_Granulation_filter(power_spectrum_obs,2*pi*f_hz_obs)
power_spectrum_Atmo = apply_Granulation_filter(power_spectrum_Atmo,2*pi*f_hzs)
power_spectrum_VALC = apply_Granulation_filter(power_spectrum_VALC,2*pi*f_hzs)


#power_spectrum_scaled = np.abs(power_spectrum_scaled) #remove sign of values like -1e-14 ... 

i0_obs = np.min( np.nonzero(f_hz_obs > 0.0052)[0] ) 
i0 = np.min( np.nonzero(f_hzs > 0.0052)[0] ) 
i1_obs = np.min( np.nonzero(f_hz_obs > 0.00519)[0] ) 
i1 = np.min( np.nonzero(f_hzs > 0.00519)[0] ) 
small_freqs = f_hzs[:i0]
small_freqs_obs = f_hz_obs[:i0_obs]
high_freqs_obs = f_hz_obs[i1_obs:]
high_freqs = f_hzs[i1:]
#plt.plot(10**3*f_hzs,power_spectrum_scaled[:len(f_hzs)/2,200])

step_low = 15
step_high = 100
small_freqs_obs = small_freqs_obs[::step_low]
high_freqs_obs = high_freqs_obs[::step_high]
for l in [200]:
    print("l =",l)

    # low frequencies
    plot_collect_obs = [] 
    plot_collect_obs.append(1e3*small_freqs_obs) 
    header_obs = "f "

    plot_collect_comp = [] 
    plot_collect_comp.append(1e3*small_freqs)
    header_comp = "f "
    
    p_normal_Atmo = power_spectrum_Atmo[:i0,l]/np.max( power_spectrum_Atmo[:i0,l] ) 
    plot_collect_comp.append(p_normal_Atmo)
    header_comp += "Atmo "

    p_normal_VALC = power_spectrum_VALC[:i0,l]/np.max( power_spectrum_VALC[:i0,l] ) 
    plot_collect_comp.append(p_normal_VALC)
    header_comp += "VALC"
    
    p_obs = power_spectrum_obs[:i0_obs,l]
    p_obs = p_obs[::step_low]
    p_normal_obs = p_obs / np.max(p_obs) 
    plot_collect_obs.append(p_normal_obs)
    header_obs += "obs "
    
    plt.plot(10**3*small_freqs,p_normal_Atmo,label="Atmo" )
    plt.plot(10**3*small_freqs,p_normal_VALC,label="VAL-C" )
    plt.plot(10**3*small_freqs_obs,p_normal_obs,label="MDI" )
    plt.xlabel("Frequency (mHz)")
    plt.legend()
    plt.show()

    fname_comp  = "power-f-low-l{0}-comp.dat".format(l) 
    np.savetxt(fname = fname_comp, 
               X = np.transpose(plot_collect_comp),
               header = header_comp,
               comments = '')
    
    fname_obs  = "power-f-low-l{0}-obs.dat".format(l) 
    np.savetxt(fname = fname_obs, 
               X = np.transpose(plot_collect_obs),
               header = header_obs,
               comments = '')

    # high frequencies
    plot_collect_obs = [] 
    plot_collect_obs.append(1e3*high_freqs_obs) 
    header_obs = "f "

    plot_collect_comp = [] 
    plot_collect_comp.append(1e3*high_freqs)
    header_comp = "f "

    p_normal_Atmo = power_spectrum_Atmo[i1:,l]/np.max( power_spectrum_Atmo[i1:,l] )
    plot_collect_comp.append(p_normal_Atmo)
    header_comp += "Atmo "

    p_normal_VALC = power_spectrum_VALC[i1:,l]/np.max( power_spectrum_VALC[i1:,l] ) 
    plot_collect_comp.append(p_normal_VALC)
    header_comp += "VALC"

    p_obs = power_spectrum_obs[i1_obs:,l]
    p_obs = p_obs[::step_high]
    p_normal_obs = p_obs/np.max( p_obs ) 
    plot_collect_obs.append(p_normal_obs)
    header_obs += "obs "

    plt.plot(10**3*high_freqs,p_normal_Atmo,label="Atmo" )
    plt.plot(10**3*high_freqs,p_normal_VALC,label="VAL-C" )
    plt.plot(10**3*high_freqs_obs,p_normal_obs,label="MDI" )
    plt.xlabel("Frequency (mHz)")
    plt.legend()
    plt.show()

    fname_comp  = "power-f-high-l{0}-comp.dat".format(l) 
    np.savetxt(fname = fname_comp, 
               X = np.transpose(plot_collect_comp),
               header = header_comp,
               comments = '')
    
    fname_obs  = "power-f-high-l{0}-obs.dat".format(l) 
    np.savetxt(fname = fname_obs, 
               X = np.transpose(plot_collect_obs),
               header = header_obs,
               comments = '')


for ps,mod in zip([power_spectrum_Atmo,power_spectrum_VALC],["Atmo","VALC"]):

    plt.figure(figsize=(6,5),constrained_layout=True)

    im = plt.pcolormesh(L_range,10**3*f_hzs,ps/np.max(ps),
                       cmap=plt.get_cmap('viridis'),
                       #cmap=plt.get_cmap('Reds'),
                       norm=colors.LogNorm( vmin= 2e-4 ,vmax=1e-0   ) )

    plt.rc('legend', fontsize=16)
    plt.rc('axes', titlesize=16)
    plt.rc('axes', labelsize=16)
    plt.xlabel("Harmonic degree $\ell$")
    plt.ylabel("Frequency (mHz)")
    #plt.title("Model S + PML")
    #plt.title("Model S + smoothed VAL-C + PML")
    plt.scatter(l_ref[ref_selected_idx], 10**(-3)*nu_ref[ref_selected_idx],s=35,marker='+',color="w")
    plt.colorbar(im)
    plt.savefig("power-computed-{0}.png".format(mod),transparent=True,dpi=300)
    plt.show()


# compute cross-covariance

Nt = 2**12
N_theta = 1000
thetas = np.linspace(0,pi,N_theta)

idxx = [ np.argmin(np.abs(thetas - i*pi/6 ) ) for i in range(1,4)]
idxx_label = ["30deg ","60deg ","90deg "]
max_vals = [0.95,0.75,0.8]
max_vals = [0.95,0.95,0.95]
#max_vals = [0.1,0.1,0.1]

ts = np.linspace(0,300,Nt)
tss =  60*ts/(2*pi)

from scipy.special import eval_legendre
from scipy.integrate import simps 

def compute_TD_diagram(power_spectrum,f_hz_input,name_str="dummy.png",min_scale = 2e11,only_omega=True,W_tau=False,obs=False):

    f_hz = 2*pi*f_hz_input
    
    domega = f_hz[-1]/power_spectrum.shape[0]
    dt = tss[-1]/(Nt)
    dt_phys = 2*pi*dt
    Ntmp = int(1/(domega*dt))

    def F_ell(l):
        if l < 300:
            return 0.5*( 1-np.tanh(0.03*l-3))
        else:
            return 0.0   
   
    def PI_scal(f):
        result = 1 + ((abs(f)-3.3*1e-3*2*pi)/ (0.6*1e-3*2*pi) )**2
        #return 1.0
        return 1/result 
    
    def Granu_filter(f):
        return np.tanh(3*f/0.0015)
    
    C_tau = np.zeros(( power_spectrum.shape[0], len(thetas)), dtype="float")
    precomputed_factors = np.zeros( power_spectrum.shape)  
    for i in range(power_spectrum.shape[0]):
        for l in range(power_spectrum.shape[1]):            
            #precomputed_factors[i,l] = 0.5*F_ell(l)*(1/(f_hz[i]))*PI_scal(f_hz[i])
            precomputed_factors[i,l] = 0.5*F_ell(l) 
            #precomputed_factors[i,l] = (1/(f_hz[i]))*PI_scal(f_hz[i])*0.5*F_ell(l)*Granu_filter(f_hz[i])


    for j in range(len(thetas)):
        l_legendre = np.array([(l+0.5)*eval_legendre(l,np.cos(thetas[j])) for l in range(power_spectrum.shape[1]) ])
        print("thetas[j] = ", thetas[j])
        for i in range(power_spectrum.shape[0]):
            li_factor = np.multiply(precomputed_factors[i,:],l_legendre)
            C_tau[i,j] = np.dot(power_spectrum[i,:],li_factor)

    if W_tau: 
        C_t = np.zeros((Nt,len(thetas)), dtype="complex") 
        C_omega_multiplied = np.zeros(( power_spectrum.shape[0], len(thetas)), dtype="complex")
        for i in range(power_spectrum.shape[0]):
            C_omega_multiplied[i,:] = -1j*f_hz[i]*C_tau[i,:]
        
        C_partial_t = np.zeros((Nt,len(thetas)), dtype="float") 
        W_omega  = np.zeros(( power_spectrum.shape[0], len(thetas)), dtype="complex")
        for i in range(len(thetas)):
            C_partial_t[:,i] = np.fft.fft(C_omega_multiplied[:,i],n=Ntmp)[:Nt].real

        window_header = "t  " 
        cross_header = "t  " 
        plot_collect_window = [ts]
        plot_collect_cross  = [ts]
        for j in range(len(thetas)):  
            if j in idxx:
                cross_header += idxx_label[idxx.index(j)]
                window_header += idxx_label[idxx.index(j)]
                maxi = max_vals[idxx.index(j)]
                C_t[:,j] = np.fft.fft(C_tau[:,j],n=Ntmp)[:Nt]
                print("C_t[:,j] =", C_t[:,j])  
                normalized_C = C_t[:,j].real/ np.max(np.abs(C_t[:,j].real))
                print("ts = ", ts)
                print("normalized_C =", normalized_C)  
                print("np.where( normalized_C > maxi )[0][0]  = ", np.where( normalized_C > maxi )[0][0] )
                tg = ts[  np.where( normalized_C > maxi )[0][0] ]
                sigma = 12
                weights = np.exp(-0.5*(ts-tg)**2/sigma**2)
                plot_collect_window.append(weights)
                plot_collect_cross.append(normalized_C)
                plt.plot(ts,weights,label="weights")
                plt.plot(ts,normalized_C,label="C")
                plt.legend()
                plt.show()
            else:
                weights = np.ones(len(ts))

            l2_norm = simps(weights[:]*C_partial_t[:,j]**2,x=None,dx=dt_phys)
            W_omega[:,j] = np.fft.ifft(weights[:]*C_partial_t[:,j],n=Ntmp)[:power_spectrum.shape[0]]
            W_omega[:,j] *= 1/l2_norm
        
        np.savetxt(fname = "cross-tt-"+name_str+".dat", 
                       X = np.transpose(plot_collect_cross),
                       header = cross_header,
                       comments = '')
            
        np.savetxt(fname = "window-tt.dat", 
                       X = np.transpose(plot_collect_window),
                       header = window_header,
                       comments = '')

    if obs: 
        C_t = np.zeros((Nt,len(thetas)), dtype="complex")
         
        window_header = "t  " 
        cross_header = "t  " 
        plot_collect_window = [ts]
        plot_collect_cross  = [ts]
        for j in range(len(thetas)):  
            if j in idxx:
                cross_header += idxx_label[idxx.index(j)]
                window_header += idxx_label[idxx.index(j)]
                
                print(" C_tau[:,j] = " , C_tau[:,j])
                C_t[:,j] = np.fft.fft(C_tau[:,j],n=Ntmp)[:Nt]
                print(" C_t[:,j] =", C_t[:,j] )
                print("np.max(np.abs(C_t[:,j].real)) =", np.max(np.abs(C_t[:,j].real)) )
                normalized_C = C_t[:,j].real/ np.max(np.abs(C_t[:,j].real))
                plot_collect_cross.append(normalized_C)
                plt.plot(ts,normalized_C,label="C")
                plt.legend()
                plt.show()
        
        np.savetxt(fname = "cross-obs.dat", 
                       X = np.transpose(plot_collect_cross),
                       header = cross_header,
                       comments = '')
            
    print("Ntmp = ", Ntmp)
    C_t = np.zeros((Nt,len(thetas)), dtype="complex")
    for i in range(len(thetas)):
        C_t[:,i] = np.fft.fft(C_tau[:,i],n=Ntmp)[:Nt]
    print("C_t = ", C_t)
    print("np.max(C_t.real) = ", np.max(C_t.real)) 
    print("np.min(C_t.real) = ", np.min(C_t.real)) 
    
    #im = plt.pcolormesh(180*thetas/pi,ts, C_t.real,
    #                   cmap=plt.get_cmap('gray'),
    #                   norm=colors.Normalize( vmin= -10**0 ,vmax= 10**0   ) )
    # 
    
    
    fig = plt.figure(figsize=(5,5),constrained_layout=True)
    im = plt.pcolormesh(180*thetas/pi,ts, C_t.real,
                       cmap=plt.get_cmap('gray'),
                       norm=colors.Normalize( vmin= -min_scale ,vmax= min_scale   ) )
    #plt.colorbar(im)
    plt.xlabel("Distance (Degrees)")
    plt.ylabel("Time (Minutes)")
    plt.savefig("Time-Distance-"+name_str,transparent=True)
    plt.show()
    plt.cla()
    plt.clf()
    plt.close()
    
    if W_tau:
        return C_tau,C_t.real,W_tau
    else:
        return C_tau,C_t.real


#power_spectrum_meshed_atmo = np.loadtxt('results/power-spectrum-Atmo-whitw.out',delimiter=',')
#power_spectrum_meshed_atmo *= 1/np.max( power_spectrum_meshed_atmo   ) 
#C_tau,C_t,W_tau = compute_TD_diagram(power_spectrum = power_spectrum_meshed_atmo,f_hz_input=f_hzs,name_str="Atmo",min_scale = 0.2,only_omega=True,W_tau=True)
C_tau,C_t,W_tau = compute_TD_diagram(power_spectrum = power_spectrum_VALC,f_hz_input=f_hzs,name_str="VALC",min_scale = 8e-2,only_omega=True,W_tau=True)
C_tau,C_t,W_tau = compute_TD_diagram(power_spectrum = power_spectrum_Atmo,f_hz_input=f_hzs,name_str="Atmo",min_scale =8e-2,only_omega=True,W_tau=True)
C_t = compute_TD_diagram(power_spectrum=power_spectrum_obs,f_hz_input=f_hz_obs,name_str="observed",min_scale = 8e+4,only_omega=True,W_tau=False,obs=True )

