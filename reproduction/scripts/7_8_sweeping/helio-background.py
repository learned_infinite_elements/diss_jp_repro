from netgen.meshing import *
from netgen.csg import *
from ngsolve import *
from math import pi,ceil
import scipy.sparse.linalg
import numpy as np
from ngsolve.solvers import GMRes
from decimal import Decimal
from scipy.linalg import lu_factor,lu_solve
from scipy.sparse import csr_matrix
import sys,os,copy 
sys.path.append(os.path.realpath(''))
from sweeping_geom import HybridLayered_Mesh,Layered_Mesh,Make1DMesh,Make1DMesh_refined,HybridLayered_Mesh_gen,HybridLayered_Mesh_U,HybridLayered_Mesh_halfDisk,Make1DMesh_givenpts
from sweeping_helper import BitArrayFromList,P_DoFs,Layered_Decomposition_cartesian,DOSM_IFassemble,DOSM,draw_mesh_tikz
import ceres_dtn as opt
import matplotlib.pyplot as plt
from xfem import * 
from academic_shared import new_initial_guess,precompute_DtN_coeff,calc_dtn_nr_layer,Learn_dtn_function,LearnedInverse,get_LearnedDtN_and_ext_inv,AlgebraicDtN,get_AlgebraicDtN_and_ext_inv,get_MovingPMLDtN_and_ext_inv,MovingPMLDtN,get_physical_pml,apply_DOSM_to_rhs,run_preconditioned_GMRES

from sweeping_geom import HybridLayered_Mesh,Layered_Mesh,Make1DMesh,Make1DMesh_refined,HybridLayered_Mesh_gen,HybridLayered_Mesh_U,HybridLayered_Mesh_halfDisk,Make1DMesh_givenpts
from sweeping_helper import BitArrayFromList,P_DoFs,Layered_Decomposition,DOSM_IFassemble,DOSM,draw_mesh_tikz

from helio_helper import SolarModel,get_rlayer,Learn_atmospheric_model,calc_dtn_nr_layer,precompute_DtN_coeff,Learn_dtn_function,LearnedInverse,LearnedDtN,get_LearnedDtN_and_ext_inv,MovingPMLDtN,get_MovingPMLDtN_and_ext_inv,apply_DOSM_to_rhs,run_preconditioned_GMRES
plt.rc('legend',fontsize=14)
plt.rc('axes',titlesize=14)
plt.rc('axes',labelsize=14)
plt.rc('xtick',labelsize=12)
plt.rc('ytick',labelsize=16)

ngsglobals.msg_level = 0

#########################################################################
# Parameters (to be changed by the user)
#########################################################################
Ns_compute = [0,1,2,3,4,5,6,7]

is_symmetric = True
gamma_damping = 0 # add damping to moving PML preconditioner 
                       # and 'False' to use the tensor product DtN
show_dtn_approx = False
use_algebraic_DtN = False
use_discrete_eigenvalues = False

direct_solver_precond = False
Nmax = 7
Ns = list(range(Nmax+1))
Nmax_a = 7
Ns_a = list(range(1,Nmax_a+1))
a = 1.0 # radius of truncation boundary 

#L_max_a = 1000

type_str = 'background'

spline_order = 1

bonus_intorder = 0
solver = "sparsecholesky" # direct solver (e.g. used for subdomain problems)
#solver = "pardiso" 
C_PML = 50 # amplitude of the PML

# choose solar model and damping
solar_model = SolarModel("VAL-C","power-law")
#solar_model = SolarModel("Atmo","power-law")
# load solar models and compute potential
#Ra = 1.0033

c,rho,rho_pot,c_1D,rho_1D,rho_pot_1D = solar_model.get_coeff()


# weights
weight_r = 1.0 
weight_r_1D = x**2
weight_theta = 1.0
weight_theta = x
weight_factor = CoefficientFunction(weight_r * weight_theta)



ansatz = "mediumSym"
flags = {"max_num_iterations":10000,
         "use_nonmonotonic_steps":True,
         "minimizer_progress_to_stdout":False,
         "num_threads":4,
         "report_level":"Brief",
         "function_tolerance":1e-11,
         "parameter_tolerance":1e-11,
         "gradient_tolerance":1e-11}


def SolveProblem(f_hz,mPML_or_learned,sweep_for_background=True,order=5):

    c,rho,rho_pot,c_1D,rho_1D,rho_pot_1D = solar_model.get_coeff()
    omega,omega_squared = solar_model.get_damping(f_hz)
    
    #lam_max = min(int(1000*f_hz*200),800)
    lam_max = int(1000*f_hz*200)
    order_ODE = order # order of ODE discretization
    order_geom = order

    pot = rho_pot - omega_squared/c**2
    pot_1D = rho_pot_1D - omega_squared/c_1D**2
    pot_sweep = rho_pot - omega_squared/c**2

    print("")
    print("Computing for f_hz = {0}".format(f_hz))
    use_moving_PML = False 
    use_learnedIE = False
    if mPML_or_learned == "mPML":
        use_moving_PML = True
    else:
        use_learnedIE = True

    output_dtn_on_layer = None
    output_learned_poles = None 
    if f_hz == 0.003:
        output_dtn_on_layer = 5
        output_learned_poles = 5  
    
    problem_config = {"omega":omega, 
                      "omega_squared":omega_squared, 
                      "use_discrete_eigenvalues": use_discrete_eigenvalues,
                      "order_ODE": order_ODE,
                      "bonus_intorder":bonus_intorder,
                      "use_algebraic_DtN":use_algebraic_DtN,
                      "use_learnedIE":use_learnedIE,
                      "use_moving_PML":use_moving_PML,
                      "Ns":Ns,
                      "Ns_a":Ns_a,
                      "output_dtn_on_layer":output_dtn_on_layer,
                      "show_dtn_approx":show_dtn_approx, 
                      "ansatz":ansatz,
                      "flags":flags,
                      "type_str":type_str,
                      "is_symmetric":is_symmetric,
                      "solver":solver,
                      "order":order,
                      "C_PML":C_PML,
                      "spline_order":spline_order,
                      "c_1D":c_1D,
                      "pot": pot,
                      "pot_1D": pot_1D,
                      "c":c,
                      "a":a,
                      "L_max_a":lam_max,
                      "sweep_for_background":sweep_for_background,
                      "f_hz":f_hz,
                      "weight_factor":weight_factor,
                      "weight_theta":weight_theta,
                      "R_max_ODE":solar_model.Ra,
                      "output_learned_poles":None
                      }

    if sweep_for_background:
        problem_config["pot_sweep"] = pot_sweep 

    #########################################################################
    # Layered mesh
    #########################################################################
    
    r_layer = get_rlayer(problem_config) 
    n_layers = len(r_layer)-1

    problem_config["r_layer"] = r_layer 
    problem_config["n_layers"] = n_layers

    refinement_marker = [False for i in range(len(r_layer))]
    quads_marker = [True for i in range(len(r_layer))]
    h_inner = 0.5*(r_layer[1]-r_layer[0])
    mesh,theta_h = HybridLayered_Mesh_halfDisk(r_layer,h_inner,quads_marker,refinement_marker,'D',2*pi,order_geom,True)
    Draw(mesh)
    mesh.SetDeformation(theta_h)

    #########################################################################
    # Learned IEs for solar atmosphere 
    #########################################################################
    
    A_N,B_N,dtn_outer_bnd  = Learn_atmospheric_model(problem_config,solar_model)
    problem_config["dtn_outer_bnd"] = dtn_outer_bnd
    
    problem_config["A_N"] = A_N 
    problem_config["B_N"] = B_N
    
    #  only for debugging moving PML
    #c = 1.0 
    #omega = 16
    #omega_squared = omega**2
    #pot =  - omega_squared/c**2
    #problem_config["c"] = c
    #problem_config["omega"] = omega 
    #problem_config["f_hz"] = omega 
    #problem_config["omega_squared"] = omega_squared
    #problem_config["pot"] = pot

    #########################################################################
    # FEM space (Proxy and-GridFunctions) 
    #########################################################################

    fes_H1 = H1(mesh, complex=True,  order=order,dirichlet=[])
    fes = fes_H1
    fes = Compress(fes)

    fes_surf = Compress(H1(mesh,order=order,complex=True,definedon= mesh.Boundaries("outer") ))
    inf_outer = [ fes_surf for i in range(A_N.shape[0] -1)]
    Xa = FESpace( [fes] + inf_outer)

    print("fes.ndof =", fes.ndof)
    print("Xa.ndof =", Xa.ndof)
    new_dof = np.arange(fes.ndof,Xa.ndof)
    print("new_dof =", new_dof)
    gfuXa = GridFunction(Xa)

    free_dofs = [] 
    for nr,b in enumerate(fes.FreeDofs()): 
        if b:
            free_dofs.append(nr)

    uX = Xa.TrialFunction() 
    vX = Xa.TestFunction() 
    u = uX[0]
    v = vX[0]

    gfuXa = GridFunction (Xa)
    gfu_GMRES = GridFunction (Xa)
    udirect = GridFunction (Xa)

    gfu = GridFunction (fes)
    u_in = GridFunction (fes)
    u_out = GridFunction (fes)
    gfu_DOSM_in = GridFunction (Xa)
    gfu_DOSM_out = GridFunction (Xa)
    gfu_DOSM_interm = GridFunction (Xa)

    #########################################################################
    # Domain decomposition 
    #########################################################################

    rr = r_layer
    r_layer = rr[1:]
    intervals = [ (rr[i],rr[i+1]) for i in range(len(rr)-1)]
    print("intervals =", intervals)
    intervals = intervals[::-1]

    decomp = Layered_Decomposition(mesh,fes,n_layers,intervals)
    decomp.Decompose()

    if use_learnedIE:
        spy_flags = {"on":(0.75,0.5),
                     "at":(0.9,-0.65),
                     "magnification":3.5,
                     "size":0.5,
                     "spy_size":11}
        draw_mesh_tikz(decomp=decomp,name="axi-mesh-{0}mHz".format(1e3*f_hz),spy_flags=spy_flags)
    
    
    decomp.Xtend(Xa)

    #########################################################################
    # Solution with direct solver  
    #########################################################################

    aX = BilinearForm (Xa, symmetric=is_symmetric)
    aX += SymbolicBFI (  (grad(u)*grad(v) + pot*u*v)*weight_factor , bonus_intorder = bonus_intorder  )
    for i in range(A_N.shape[0]):
        for j in range(A_N.shape[0]):
            if abs(A_N[j,i]) > 1e-10 or abs(B_N[j,i]>1e-10):
                graduX = grad(uX[i]).Trace()
                gradvX = grad(vX[j]).Trace()  
                aX += SymbolicBFI ( (B_N[j,i]*graduX*gradvX + A_N[j,i]*uX[i]*vX[j])*weight_theta  ,definedon= mesh.Boundaries("outer") )
    aX.Assemble()

    if direct_solver_precond:
        aX_background = BilinearForm (Xa, symmetric=is_symmetric)
        aX_background += SymbolicBFI (  (grad(u)*grad(v) + pot_sweep*u*v)*weight_factor , bonus_intorder = bonus_intorder  )
        for i in range(A_N.shape[0]):
            for j in range(A_N.shape[0]):
                if abs(A_N[j,i]) > 1e-10 or abs(B_N[j,i]>1e-10):
                    graduX = grad(uX[i]).Trace()
                    gradvX = grad(vX[j]).Trace()  
                    aX_background += SymbolicBFI ( (B_N[j,i]*graduX*gradvX + A_N[j,i]*uX[i]*vX[j])*weight_theta  ,definedon= mesh.Boundaries("outer") )
        aX_background.Assemble()

    f = LinearForm (Xa)
    f.Assemble()

    dirac_source_pos = (0.0,1.0)
    gfuXa.vec[:] = 0.0
    rhs = f.vec.CreateVector()
    gfuXa.vec[:] = 0.0
    # implementation of Dirac delta 
    for i in range(fes.ndof):    
        gfuXa.vec[i] = 1.0
        f.vec.FV().NumPy()[i] = gfuXa.components[0](mesh(dirac_source_pos[0],dirac_source_pos[1]))
        gfuXa.vec[i] = 0.0

    gfuXa.vec[:] = 0.0
    res_direct = gfuXa.vec.CreateVector()
    udirect.vec[:] = 0.0
    res_direct.data = f.vec - aX.mat * udirect.vec

    invA = aX.mat.Inverse(freedofs=Xa.FreeDofs(), inverse=solver)
    udirect.vec.data += invA * res_direct
    del invA 

    #########################################################################
    # Computation of DtN numbers (ODE solves) 
    #########################################################################

    problem_config["lam_max"] = lam_max
    DtN_coeff_layer,lam_layer,lam_vec_layer = precompute_DtN_coeff(decomp,problem_config)
    spectral_info = {"DtN_coeff_layer":DtN_coeff_layer,
                     "lam_layer": lam_layer,
                     "lam_vec_layer":lam_vec_layer  
                    }

    if output_dtn_on_layer: 
        #max_lam = lam_layer[output_dtn_on_layer].real[-1]
        lam_sample = lam_layer[output_dtn_on_layer] 
        dtn_nr_sample = calc_dtn_nr_layer(output_dtn_on_layer,lam_sample,problem_config)
        problem_config["lam_sample"] = lam_sample
        problem_config["dtn_nr_sample"] = dtn_nr_sample 

    #########################################################################
    # Compute learned IE matrices L_one,L_two for all layers
    #########################################################################

    if use_learnedIE:
        Lones_layer,Ltwos_layer,learned_dtn_layer = Learn_dtn_function(decomp,problem_config,spectral_info,output_learned_poles)
        #LearnedDtNs,layer_and_truncated_ext_inv = get_LearnedDtN_and_ext_inv(decomp,problem_config,Ns[-1],Lones_layer,Ltwos_layer) 

    #########################################################################
    # Layer matrices
    #########################################################################

    layermat = []
    for layer in range(n_layers):
        layermat.append(aX.mat)    

    #########################################################################
    # Transmission operators of DOSM
    #########################################################################

    tmp_DtN = gfu.vec.CreateVector()
    tmp1_DtN = gfu.vec.CreateVector()

    if use_algebraic_DtN or use_moving_PML:
    
        if use_algebraic_DtN:
            
            AlgebraicDtNs,layer_and_truncated_ext_inv = get_AlgebraicDtN_and_ext_inv(decomp,problem_config) 

            def DtN_from_algebraic(layer,v_in,v_out):
                proj_IF_outer = Projector(BitArrayFromList(decomp.IF2dof[layer],mesh,fes,elements=False),False)
                AlgebraicDtNs[layer].Apply(v_in,v_out)
                v_out.data -= proj_IF_outer * v_out

        if use_moving_PML:
            
            MovingPMLDtNs,layer_and_truncated_ext_inv = get_MovingPMLDtN_and_ext_inv(decomp,problem_config)
            def DtN_from_moving_PML(layer,v_in,v_out):
                #proj_IF_outer = Projector(BitArrayFromList(decomp.IF2dof[layer],mesh,fes,elements=False),False)
                proj_IF_outer = Projector(BitArrayFromList(decomp.IF2dof[layer],mesh,Xa,elements=False),False)
                MovingPMLDtNs[layer].Apply(v_in,v_out)
                v_out.data -= proj_IF_outer * v_out
    
        def P_outer(layer,v_in,v_out):
            if use_moving_PML:
                DtN_from_moving_PML(layer,v_in,v_out)
            else:
                DtN_from_algebraic(layer,v_in,v_out)
            
        gfu_DOSM_in.vec[:] = 0.0
        gfu_DOSM_out.vec[:] = 0.0

        dosm =  DOSM(decomp,layermat,layer_and_truncated_ext_inv,P_outer,gfuXa)
        
        class DOSMPrecond(BaseMatrix):
                
            def IsComplex(self):
                return True

            def Height(self):
                return len(gfuXa.vec)

            def Width(self):
                return len(gfuXa.vec)
                
            def Mult(self, x, y):
                gfu_DOSM_in.vec[:] = 0.0
                dosm.ApplyDOSM(x,y,gfu_DOSM_in.vec)
            
        C =  DOSMPrecond()

        apply_DOSM_to_rhs(dosm,f.vec,gfu_DOSM_in,gfu_DOSM_out,udirect,mesh,DOSM_iter=1)
        # Preconditioned GMRES iteration
        niter = run_preconditioned_GMRES(C,aX,f.vec,udirect,gfu_GMRES,mesh)
        return niter
 
    else: 
        learned_iter_omega = [] 
        for N_fixed in Ns_compute:
            print("")
            print("Use learned IEs of order N = {0}".format(N_fixed))
            LearnedDtNs,layer_and_truncated_ext_inv = get_LearnedDtN_and_ext_inv(decomp,problem_config,N_fixed,Lones_layer,Ltwos_layer) 
            def DtN_from_learnedIE(layer,v_in,v_out):        
                LearnedDtNs[layer].Apply(v_in,v_out)
                #proj_IF_outer = Projector(BitArrayFromList(decomp.IF2dof[layer],mesh,fes,elements=False),False)
                proj_IF_outer = Projector(BitArrayFromList(decomp.IF2dof[layer],mesh,Xa,elements=False),False)
                v_out.data -= proj_IF_outer * v_out

            def P_outer(layer,v_in,v_out):
                DtN_from_learnedIE(layer,v_in,v_out)

            gfu_DOSM_in.vec[:] = 0.0
            gfu_DOSM_out.vec[:] = 0.0

            dosm =  DOSM(decomp,layermat,layer_and_truncated_ext_inv,P_outer,gfuXa)
            
            class DOSMPrecond(BaseMatrix):
                
                def IsComplex(self):
                    return True

                def Height(self):
                    return len(gfuXa.vec)

                def Width(self):
                    return len(gfuXa.vec)
                
                def Mult(self, x, y):
                    gfu_DOSM_in.vec[:] = 0.0
                    dosm.ApplyDOSM(x,y,gfu_DOSM_in.vec)
                
            C =  DOSMPrecond()

            apply_DOSM_to_rhs(dosm,f.vec,gfu_DOSM_in,gfu_DOSM_out,udirect,mesh,DOSM_iter=1)
            # Preconditioned GMRES iteration
            learned_iter_omega.append(run_preconditioned_GMRES(C,aX,f.vec,udirect,gfu_GMRES,mesh))
        return learned_iter_omega,problem_config["n_layers"] 


#SolveProblem(f_hz=0.003,mPML_or_learned="learned",sweep_for_background=True,order=5)
#SolveProblem(f_hz=0.0045,mPML_or_learned="mPML",sweep_for_background=True)

SolveProblem(f_hz=0.003,mPML_or_learned="learned",sweep_for_background=True,order=5)

fs_mHz = np.arange(1,8)
fs_Hz = 1e-3*fs_mHz

for order in [4,6]:
    learned_iter = []
    plot_collect_learned = [fs_mHz]
    nlayers = []
    for f_hz in fs_Hz:
        niter,nlayer = SolveProblem(f_hz=f_hz,mPML_or_learned="learned",sweep_for_background=True,order=order)
        learned_iter.append(niter)
        nlayers.append(nlayer)
    fname_learned ="helio_{0}_iter_learned_order{1}.dat".format(solar_model.atmo_type,order)
    header_learned = "mHz nlayers "
    nlayers = np.array(nlayers) 
    plot_collect_learned.append(nlayers)

    for idx,N_fixed in enumerate(Ns_compute):
        header_learned += "N{0} ".format(N_fixed)
    for idx_N,N_fixed in enumerate(Ns_compute):
        learned_iter_N = []  
        for idx_f,f_hz in enumerate(fs_mHz):
            learned_iter_N.append(learned_iter[idx_f][idx_N])
        learned_iter_N = np.array(learned_iter_N,dtype=int)
        plot_collect_learned.append(learned_iter_N)

    np.savetxt(fname=fname_learned,
       X=np.transpose(plot_collect_learned),
       header=header_learned,
       comments='',
       fmt='%i')

