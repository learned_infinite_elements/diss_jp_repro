import ceres_dtn as opt
from ngsolve import *
from netgen.geom2d import SplineGeometry
from ngs_refsol import PlaneWaveHomSolution
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from scipy.special import hankel1,h1vp
#import mpmath as mp

a = 0.6 # coupling radius
R = 1/2 # radius of inner boundary
Lmax = 20 # number of considered modes for learning
#Lmax = 70 # number of considered modes for learning, tested value
Nmax = 20 # maximal number of infinite element dofs

#Lmax =  25

#Nmax = 5
Ns = list(range(Nmax))

orders = [4,6,8,10] # order of FEM
omegas = [4,8,16,32] # wavenumbers 


#Nomega = 12

N_real = 6
N_imag = 5
Nomega = N_real * N_imag 
omega_max_real = 18 
omega_min_real = -2.0
#omega_min_real = 0.5 
#omega_max_imag = 0
omega_max_imag = -2.0
omega_min_imag = -15 
#omega_min_imag = -20

l_sample = [] 
for l in range(Lmax):
    if l <= omega_max_real:
        l_sample.append(l)
    else:
        if l % 1 == 0:
            l_sample.append(l)

omega_sample = [] 
for k in range(N_real):
    for m in range(N_imag):
        if N_imag > 1:
            omega_sample.append( omega_min_real + k*(omega_max_real-omega_min_real)/(N_real-1) + 1j*( omega_min_imag + m*(omega_max_imag-omega_min_imag)/(N_imag-1) )  ) 
        else:
            omega_sample.append( omega_min_real + k*(omega_max_real-omega_min_real)/(N_real-1) + 1j*omega_max_imag )  
print("omega_sample = ", omega_sample)

fname = "res-Dirichlet-omega-samples.dat"
with open(fname, 'a') as the_file:
    the_file.write('x y \n')
    for omega_z in omega_sample:
        the_file.write('{0} {1} \n'.format(omega_z.real,omega_z.imag))
    the_file.write('\n')

omega_sample = np.array(omega_sample) 


#omega_sample = np.linspace(16,32,Nomega) 
#omega_sample = np.linspace(0.5,30,Nomega) 

g_omega = omega_sample**2
g_omega = np.array(g_omega,dtype="complex")

#omega_sample = np.linspace(0.5,8,Nomega) 

print("omega_sample = ", omega_sample) 

A1_IEs = {} # for storing results
A2_IEs = {} 
B_IEs  = {}

#ansatz = "full"
ansatz = "symmetric"
#ansatz = "symmetric_A2real"
flags = {"max_num_iterations":800,
         "use_nonmonotonic_steps":True,
         "minimizer_progress_to_stdout":False,
         "num_threads":12,
         "report_level":"solver_stats",
         "function_tolerance":1e-10,
         "parameter_tolerance":1e-10,
         "gradient_tolerance":1e-10,
         "check_gradients":False,
         "solver_stats":{}
         }

def new_initial_guess(A1_old,A2_old,B_old,ansatz):
    eps_scale = 1e-2
    N = A1_old.shape[0]
    if N >= 3:
        eps_scale = 1e-3
    if N >= 8:
        eps_scale = 5e-4
    A1_guess = np.zeros((N+1,N+1),dtype='complex')
    A2_guess = np.zeros((N+1,N+1),dtype='complex')
    B_guess = np.zeros((N+1,N+1),dtype='complex')
    #A1_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
    #A2_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
    #B_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
    A1_guess = eps_scale*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
    A2_guess = 1e-1*eps_scale*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
    B_guess = 1e-1*eps_scale*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
    A1_guess[:N,:N] = A1_old[:]
    A2_guess[:N,:N] = A2_old[:]
    B_guess[:N,:N] = B_old[:]
    A1_guess[N,N] = 1.0
    return A1_guess,A2_guess,B_guess


def dtn_ref(omega,nu):
    #return -omega*0.5*complex( (mp.hankel1(nu-1,omega*a) - mp.hankel1(nu+1,omega*a) ) /  mp.hankel1(nu,omega*a) )
    return -omega*h1vp(nu,omega*a) / hankel1(nu,omega*a)

#lam = np.array([(l/a)**2 for l in range(Lmax)]) 
lam = np.array([(l/a)**2 for l in l_sample]) 

dtn_omega = []
dtn_nr = np.zeros((len(lam),Nomega),dtype=complex) 
weights = np.zeros((len(lam),Nomega),dtype=float) 

for i in range(len(omega_sample)):
    for l_idx,l in enumerate(l_sample):
        dtn_nr[l_idx,i] = dtn_ref(omega_sample[i],sqrt(lam[l_idx])*a)
        

        #weights[l,i] = (10**2*abs(complex(mp.hankel1(l,omega_sample[i]*a) / mp.hankel1(l,omega_sample[i]*min(R*omega_sample[i]/8,R)) )))**2 
        
        #weights[l_idx,i] = (10**2*abs(exp(-l*1/5)))**2 ### tested choice 

        #weights[l,i] = (10**2*abs(exp(-l*1/6)))**2 
        
        #weights[l,i] = (10**2*abs(exp(-l*2/3)))**2 
        
        #weights[l,i] = (10**3*abs(complex(mp.hankel1(l,omega_sample[i]*a) / mp.hankel1(l,omega_sample[i]*min(R*omega_sample[i]/8,R)) )))**2 
        #weights[l,i] = (10**2*abs(complex(mp.hankel1(l,10*a) / mp.hankel1(l,10*min(R*10/8,R)) )))**2 
        #weights[l,i] = (10**2*abs(complex(mp.hankel1(l,omega_sample[i]*a) / mp.hankel1(l,omega_sample[i]*R) )))**2 
        #weights[l,i] = (10**2*abs(exp(-l*2/3)))**2 
        #weights[l,i] = (  (10**2) *abs(complex(mp.hankel1(l,10*a) / mp.hankel1(l,10*min(R,R)) )))**2 
        
        weights[l_idx,i] = 1.0 

print("weights =" , weights)

l_dtn = opt.learned_dtn_interval(lam,g_omega,dtn_nr,weights)


# perform minimization for each wavenumber 
A1_IE = []
A2_IE = []
B_IE = [] 
relative_residuals = []

np.random.seed(seed=123)

A1_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
A2_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
B_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)


rel_error = {} # key = omega

geo = SplineGeometry()
geo.AddCircle( (0,0), a, leftdomain=1, rightdomain=0,bc="outer-bnd")
geo.AddCircle( (0,0), R, leftdomain=0, rightdomain=1,bc="inner-bnd")
geo.SetMaterial(1, "inner")
mesh = Mesh(geo.GenerateMesh (maxh=0.05,quad_dominated=False))
mesh.Curve(10)

dtn_fct_error = {} 

def ComputeResonances(A1_N,A2_N,B_N,order=10):
    
    #fes_inner = H1(mesh,complex=True,order=order,dirichlet=[])
    fes_inner = H1(mesh,complex=True,order=order,dirichlet="inner-bnd")
    #N = 0
    #N = Ns[-1] 
    N = A1_N.shape[0]
    print("N = {0}".format(N))
    #La,Lb,Lc,neum_repr = get_HSIEM_matrices(20,a,4*a)
    
    #A1_N = A1_IE[N]
    #A2_N = A2_IE[N]
    #B_N = B_IE[N]
    
    #A1_N = La
    #A2_N = -Lc
    #B_N = Lb

    fes_surf = H1(mesh,order=order,complex=True,definedon=mesh.Boundaries("outer-bnd"))
    inf_outer = [fes_surf for i in range(A1_N.shape[0]-1)]
    #X = FESpace(inf_outer)
    X = FESpace([fes_inner]+inf_outer)
    uX = X.TrialFunction() 
    vX = X.TestFunction() 

    aX = BilinearForm(X, symmetric=False)
    mX = BilinearForm(X, symmetric=False)
    
    aX += SymbolicBFI ( grad(uX[0])*grad(vX[0]))
    mX += SymbolicBFI ( uX[0]*vX[0] )
     
    for i in range(A1_N.shape[0]):
        for j in range(A1_N.shape[0]):
            if abs(A1_N[j,i]) > 1e-10 or abs(A2_N[j,i]) > 1e-10 or abs(B_N[j,i]>1e-10):
                graduX = grad(uX[i]).Trace()
                gradvX = grad(vX[j]).Trace()  
                aX += SymbolicBFI ( B_N[j,i]*graduX*gradvX + (A1_N[j,i])*uX[i]*vX[j]  ,definedon=mesh.Boundaries("outer-bnd"))
                mX += SymbolicBFI ( -A2_N[j,i]*uX[i]*vX[j]  ,definedon=mesh.Boundaries("outer-bnd"))
    
    aX.Assemble()
    mX.Assemble()
    u_res = GridFunction(X, multidim=250, name='resonances')
    #u_res = GridFunction(X, multidim=150, name='resonances')

    if N > 6:
        with TaskManager():
            lam_res = ArnoldiSolver(aX.mat, mX.mat, X.FreeDofs(), u_res.vecs, shift=(10-5j)**2)
            #lam_res = ArnoldiSolver(aX.mat, mX.mat, X.FreeDofs(), u_res.vecs, shift=(6-6j)**2)
            ##lam_res = ArnoldiSolver(aX.mat, mX.mat, X.FreeDofs(), u_res.vecs, shift=(5-5j)**2)
            #lam_res = ArnoldiSolver(aX.mat, mX.mat, X.FreeDofs(), u_res.vecs, shift= (12-10j)**2)
        comp_resonances = np.sqrt(np.array(lam_res))
        #exact_resonances = np.loadtxt("exact-res-Dirichlet.dat",dtype="complex")
        #plt.plot(exact_resonances.real,exact_resonances.imag,marker='o',linestyle="None",label='exact')
        #plt.plot(comp_resonances.real,comp_resonances.imag,marker='x',linestyle="None",label='computed')
        #plt.legend()
        #plt.show()

        fname = "learned-res-Dirichlet-tuple-N{0}.dat".format(N-1)
        with open(fname, 'a') as the_file:
            the_file.write('x y \n')
            for res_z in comp_resonances:
                the_file.write('{0} {1} \n'.format(res_z.real,res_z.imag))
            the_file.write('\n')

header_solver_stats = "N iterations finalcost time"
fname_solver_stats = "solver_stats_res.dat"
iterations_res = [] 
final_cost_res = []
time_res = []

for N in Ns: 
    l_dtn.Run(A1_guess,A2_guess,B_guess,ansatz,flags)
    A1_IE.append(A1_guess.copy()),A2_IE.append(A2_guess.copy()),B_IE.append(B_guess.copy())
    #print("A1_guess = ", A1_guess)
    #print("A2_guess = ", A2_guess)
    #print("B_guess = ", B_guess)

    iterations_res.append(flags["solver_stats"]["iterations"]) 
    final_cost_res.append(flags["solver_stats"]["final_cost"]) 
    time_res.append(flags["solver_stats"]["total_time_in_seconds"]) 

    ComputeResonances(A1_guess,A2_guess,B_guess,order=10)
    A1_guess,A2_guess,B_guess = new_initial_guess(A1_IE[N],A2_IE[N],B_IE[N],ansatz)

results_stats = [np.array(Ns,dtype=int),np.array(iterations_res,dtype=int),np.array( final_cost_res),np.array(time_res)]
np.savetxt(fname =fname_solver_stats ,
           X = np.transpose(results_stats),
           header = header_solver_stats,
           comments = '')

