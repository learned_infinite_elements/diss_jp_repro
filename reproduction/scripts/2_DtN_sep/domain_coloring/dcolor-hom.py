import numpy as np
import matplotlib.pyplot as plt
from colorsys import hsv_to_rgb
from math import pi
from mpmath import pi,sin,cos,exp,log,hankel1,sqrt

Rmin = 1e-1
Rmax = 5e+2

def bump(x,z0,D,a):
    return exp(log(a)*(D/(x-z0))**2)

def fadeOut(r):
    return bump(r,Rmax,Rmax/2,0.5)

def fadeIn(r):
    return bump(r,Rmin,8*Rmin,0.5)

def my_color_scheme(z):

    if np.isnan(z):
        pass

    # get polar coordinates, angle in [0,2pi]
    phi = np.angle(z)+pi
    r = np.absolute(z)

    if r <= Rmin:
        return hsv_to_rgb(0,0,0),1
        #return hsv_to_rgb(0,0,0.5),1
    if r >= Rmax:
        return hsv_to_rgb(0,0,1),1

    if np.isnan(z):
        h = 0
    else:
        h = phi/(2*pi)
    sat = fadeOut(r)
    bri = fadeIn(r)
    # Conversion from hsv to rgb colors space 
    return hsv_to_rgb(h,sat,bri),1


def my_grid_scheme(z):
    n_circles = 12
    D_circles = 0.5 
    D_circles = 2
    if np.isnan(z):
        pass

    # get polar coordinates, angle in [0,2pi]
    phi = np.angle(z) + pi 
    r = np.absolute(z)

    d_theta = np.min(np.abs( phi/(2*pi)  -np.arange(0,n_circles)/n_circles ) )
    d_c = np.min(np.abs( r  - D_circles*np.arange(0,10*n_circles) ) )
    
    tres_theta = 0.004 
    tres_theta = 0.003
    tres_c = 0.08
    #tres_c = 0

    #if d_theta < tres_theta:
    #    if d_c > tres_c:
    #        print("d_theta < tres_theta at but d_c > tres_c z = {0}".format(z))


    if d_c < tres_c: 
        #return hsv_to_rgb(0,0,0.5), (tres_c - d_c)/tres_c
        return hsv_to_rgb(0,0,0), (tres_c - d_c)/tres_c
    elif d_theta < tres_theta:
        #return hsv_to_rgb(0,0,0.5), (tres_theta - d_theta)/tres_theta
        return hsv_to_rgb(0,0,0), (tres_theta - d_theta)/tres_theta
    else:
        return hsv_to_rgb(0,0,1), 0


def A_over_B(C_a,alpha_a,C_b,alpha_b):

    img = np.empty(shape=C_a.shape)
    alphas = np.empty(shape=alpha_a.shape)
    alphas = alpha_a + alpha_b*(1-alpha_a)
    for i in range(C_a.shape[0]):
        for j in range(C_a.shape[1]):
            img[i][j] = (C_a[i][j]*alpha_a[i,j] + C_b[i][j]*alpha_b[i,j]*(1-alpha_a[i,j] ))/alphas[i,j]
    return img,alphas


def plot(f,domain=[-1,1,-1,1],res=(200,200)):
    left = domain[0]
    right = domain[1]
    bottom = domain[2]
    top = domain[3]

    dx = (right-left)/res[0]
    dy = (top-bottom)/res[1]

    C_a = np.empty(shape=(res[1],res[0],3))
    alpha_a = np.empty(shape=(res[1],res[0]))
    C_b = np.empty(shape=(res[1],res[0],3))
    alpha_b = np.empty(shape=(res[1],res[0]))

    for i in range(res[1]):
        y = top - dy*i
        for j in range(res[0]):
            x = left + dx*j
            z = np.complex64(x+1j*y)
            fz = f(z)
            c_a,a_a = my_color_scheme(fz)
            C_a[i][j] = c_a
            alpha_a[i,j] = a_a 
            c_b,a_b = my_grid_scheme(fz)
            C_b[i][j] = c_b
            alpha_b[i,j] = a_b 
    
    #img,alphas = A_over_B(C_a,alpha_a,C_b,alpha_b)
    img,alphas = A_over_B(C_b,alpha_b,C_a,alpha_a)
    
    #plt.imshow(C_a,origin="lower",alpha=alpha_a,extent=domain)
    #plt.show()
    
    #plt.imshow(C_a,origin="lower",extent=domain)
    #plt.show()
    
    plt.imshow(C_a,extent=domain)
    plt.show()
    
    plt.imshow(C_b,alpha=alpha_b,extent=domain)
    plt.show()
    
    #plt.imshow(C_a,origin="lower",alpha=alpha_a,extent=domain)
    plt.imshow(img,alpha=alphas,extent=domain)
    plt.xlabel("Re$(\lambda)$")
    plt.ylabel("Im$(\lambda)$")
    plt.savefig("dtn-hom.png",transparent=True)
    #plt.imshow(C_a,origin="lower",alpha=alpha_a,extent=domain)
    plt.show()

# Define a test domain
#domain = [-0.25,0.25,-0.25,0.25]
domain = [-2,2,-2,2]

# Define a test function
#f = lambda z: sin(1/z)

a = 1.0
omega = 16

def h1vp(nu,omega):
    return 0.5*(hankel1(nu-1,omega) - hankel1(nu+1,omega)) 


import mpmath as mp


a = 1.0  # radius of coupling boundary
R_inf = 2.0 # location of wavespeed discontinuity
omega_I = 16 # interior wavespeed
omega_inf = 8

def h1vp(nu,omega):
    return 0.5*(mp.hankel1(nu-1,omega) - mp.hankel1(nu+1,omega)) 

def jv(nu,omega):
    return mp.besselj(nu,omega)

def jvp(nu,omega):
    return mp.besselj(nu,omega,derivative=1)

def yv(nu,omega):
    return mp.bessely(nu,omega)

def yvp(nu,omega):
    return mp.bessely(nu,omega,derivative=1)

def M_nu(nu):
    tmp1  = -(omega_inf/omega_I)*(h1vp(nu,omega_inf*R_inf)/jv(nu,omega_I*a))*( jv(nu,omega_I*a)*yv(nu,omega_I*R_inf) - yv(nu,omega_I*a)*jv(nu,omega_I*R_inf) )
    tmp2 = (hankel1(nu,omega_inf*R_inf)/jv(nu,omega_I*a))*( yvp(nu,omega_I*R_inf)*jv(nu,omega_I*a) - yv(nu,omega_I*a)*jvp(nu,omega_I*R_inf))
    if abs(tmp1+tmp2) < 1e-12:
        print("nu = {0}, tmp1+tmp2 ={1} ".format(nu,tmp1+tmp2))
    return tmp1+tmp2

def B_nu(nu):
    tmp1 = (omega_inf/omega_I)*h1vp(nu,omega_inf*R_inf)*jv(nu,omega_I*R_inf)/jv(nu,omega_I*a) 
    tmp2 = -hankel1(nu,omega_inf*R_inf)*jvp(nu,omega_I*R_inf)/jv(nu,omega_I*a)
    return (tmp1+tmp2)/M_nu(nu)

def A_nu(nu):
    return (1-B_nu(nu)*yv(nu,omega_I*a))/jv(nu,omega_I*a)

def C_nu(nu):
    tmp1 = ( yvp(nu,omega_I*R_inf)*jv(nu,omega_I*a) - yv(nu,omega_I*a)*jvp(nu,omega_I*R_inf) )*( jv(nu,omega_I*R_inf) / jv(nu,omega_I*a)**2)
    tmp2 = -( jv(nu,omega_I*a)*yv(nu,omega_I*R_inf) - yv(nu,omega_I*a)*jv(nu,omega_I*R_inf) )*( jvp(nu,omega_I*R_inf) / jv(nu,omega_I*a)**2   )
    return (tmp1+tmp2)/M_nu(nu)


def dtn_ref(nu):
    return complex(-omega_I*( A_nu(nu)*jvp(nu,omega_I*a) + B_nu(nu)*yvp(nu,omega_I*a)))

def dtn_ref(nu):
    return -omega*h1vp(nu,omega*a) / hankel1(nu,omega*a)

def dtn(z):
    return complex(dtn_ref(sqrt(z)*a))

#domain_dtn = [180,500,1,420]
domain_dtn = [180,480,1,600]
#domain_dtn = [1,500,1,1500]

#domain_dtn = [1,500,-100,150]
#domain_dtn = [1,420,-25,75]

#plot(dtn,domain_dtn,res=(100,200))
plot(dtn,domain_dtn,res=(700,1400))

def f(z):
    #print("z = {0}",z)
    #return complex(sin(1/z))
    #return complex(log(z))
    #return complex(sin(z))
    result = 1/(z-0.54-0.54j)
    result *= 1/(z+0.54+0.54j)
    result *= 1/(z+0.54-0.54j)
    return result

#f = lambda z:  1/ (z-(0.5+0.5j))

#plot(f,domain,res=(400,400))


