from ngsolve import * 
from math import pi
import numpy as np
from compute_pot import local_polynomial_estimator,logder
import scipy.sparse.linalg
from cmath import sqrt as csqrt
import ceres_dtn as opt
from scipy.sparse import csr_matrix
from sweeping_helper import BitArrayFromList,P_DoFs,Layered_Decomposition_cartesian,DOSM_IFassemble,DOSM,draw_mesh_tikz
from sweeping_geom import HybridLayered_Mesh,Layered_Mesh,Make1DMesh,Make1DMesh_refined,HybridLayered_Mesh_gen,HybridLayered_Mesh_U,HybridLayered_Mesh_halfDisk,Make1DMesh_givenpts
from xfem import * 
from ngsolve.solvers import GMRes

def sqrt_2branch(z):
    if z.real < 0 and z.imag <0:
        return - csqrt(z)
    else:
        return csqrt(z)

class SolarModel():
    def __init__(self,atmo_type="VALC-",damping_model = "power-law",spline_order=1):
        self.atmo_type = atmo_type
        self.damping_model = damping_model
        self.c = None 
        self.rho = None 
        self.rho_pot = None
        
        self.c_1D = None 
        self.rho_1D = None 
        self.rho_pot_1D = None

        self.rr = None
        self.c_clean = None
        self.rho_clean = None
        self.drho = None
        
        self.RSun = 6.96*10**8 # solar radius in m
        self.spline_order = spline_order
        self.Ra = None
        if atmo_type == "VAL-C":
            self.Ra = 1.0033
        elif atmo_type == "Atmo":
            self.Ra = 1.0007126 
 

    def get_coeff(self):
        if not all([self.c,self.rho,self.rho_pot,self.c_1D,self.rho_1D,self.rho_pot_1D]):
            if self.atmo_type == "VAL-C":
                rS,cS,rhoS,_,__,___ = np.loadtxt("modelSinput.txt",unpack=True)
                cS = (10**-2)*cS
                rhoS = (10**3)*rhoS
                print("using Model S + VAL-C")
                rV,vV,rhoV,Tv  = np.loadtxt("VALCinput.txt",unpack=True)

                # sound speed is not given in VAL-C model
                # calculate it from temperature using the ideal gas law
                gamma_ideal = 5/3 # adiabatic index
                mu_ideal = 1.3*10**(-3) # mean molecular weight 
                R_gas = 8.31446261815324 # universal gas constant 
                cV = np.sqrt(gamma_ideal*R_gas*Tv/mu_ideal)
                rhoV = (10**3)*rhoV

                # overlap interval
                rmin = rV[-1]
                rmax = rS[0]

                weightS = np.minimum(1, (rmax-rS)/(rmax-rmin))
                weightV = np.minimum(1, (rV-rmin)/(rmax-rmin))

                rr = np.concatenate((rS,rV)) 
                ind = np.argsort(rr)
                rr = rr[ind]
                c = np.concatenate((cS,cV))[ind]
                rho = np.concatenate((rhoS,rhoV))[ind]
                weight = np.concatenate((weightS,weightV))[ind]

            elif self.atmo_type == "Atmo":
                rS,cS,rhoS,_,__,___ = np.loadtxt("modelSinput.txt",unpack=True)
                cS = (10**-2)*cS
                rhoS = (10**3)*rhoS
                ind = np.argsort(rS)
                rr = rS[ind]
                c = cS[ind]
                rho = rhoS[ind]
                weight = np.ones(len(rr))
            else: 
                raise ValueError('Unknwon atmospheric model.')
            
            self.rr = rr 
            
            rho_clean,drho,ddrho = logder(rho,rr,weight,3)
            c_clean = logder(c,rr,weight,1)
            self.c_clean = c_clean 
            self.rho_clean = rho_clean
            self.drho = drho
            #i0 = np.min( np.nonzero(rr > 0.99)[0] ) 

            f,df,ddf = logder(1/np.sqrt(rho),rr,weight,3)
            pot_rho = np.sqrt(rho_clean)*(ddf+2*df/rr)

            pot_rho[0] = pot_rho[1] # treating the NaN value at r = 0

            pot_c = c_clean.copy()

            #################################################### 
            # BSpline approximation of c,rho and potential 
            ####################################################

            r_beg = rr[0]
            pot_rho_beg = pot_rho[0]
            pot_c_beg = pot_c[0]
            r_end = rr[-1]
            pot_rho_end = pot_rho[-1]
            pot_c_end = pot_c[-1]

            rho_beg = rho_clean[0]
            c_beg = c_clean[0]
            rho_end = rho_clean[-1]
            c_end = c_clean[-1]

            rr = np.append([r_beg for i in range(self.spline_order)],rr)
            pot_rho = np.append([pot_rho_beg for i in range(self.spline_order)],pot_rho)
            pot_c = np.append([pot_c_beg for i in range(self.spline_order)],pot_c)

            c_clean = np.append([c_beg for i in range(self.spline_order)],c_clean)
            rho_clean = np.append([rho_beg for i in range(self.spline_order)],rho_clean)
             
            rr = np.append(rr,[r_end for i in range(self.spline_order)])
            pot_rho = np.append(pot_rho,[pot_rho_end for i in range(self.spline_order)])
            pot_c = np.append(pot_c,[pot_c_end for i in range(self.spline_order)])

            c_clean = np.append(c_clean,[c_end for i in range(self.spline_order)])
            rho_clean = np.append(rho_clean,[rho_end for i in range(self.spline_order)])

            rr_ = rr.tolist()
            pot_rho = pot_rho.tolist()
            pot_c = pot_c.tolist()
            c_clean = c_clean.tolist()
            rho_clean = rho_clean.tolist()

            r = sqrt(x**2+y**2)
            pot_c_B = BSpline(self.spline_order,rr_,pot_c)(r)

            self.c = BSpline(self.spline_order,rr_,pot_c)(r)
            self.rho = BSpline(self.spline_order,rr_,rho_clean)(r)
            self.rho_pot = BSpline(self.spline_order,rr_,pot_rho)(r)
            
            self.c_1D = BSpline(self.spline_order,rr_,pot_c)(x)
            self.rho_1D = BSpline(self.spline_order,rr_,rho_clean)(x)
            self.rho_pot_1D = BSpline(self.spline_order,rr_,pot_rho)(x)

        return self.c,self.rho,self.rho_pot,self.c_1D,self.rho_1D,self.rho_pot_1D

    def get_damping(self,f_hz):
        if self.damping_model == "power-law": 
            omega_f = f_hz*2*pi*self.RSun
            gamma0 = 2*pi*4.29*1e-6*self.RSun
            omega0 = 0.003*2*pi*self.RSun
            if f_hz < 0.0053:
                gamma = gamma0*(omega_f/omega0)**(5.77)
            else:
                gamma = gamma0*(0.0053*2*pi*self.RSun/omega0)**(5.77)
         
            omega_squared = omega_f**2+2*1j*omega_f*gamma

            #prefac = 1 + (  (omega_f/self.RSun-0.0033*2*pi)/( 0.5*0.0012*2*pi )   )**2
            #prefac = 1/prefac
            return sqrt_2branch(omega_squared),omega_squared
        else: 
            raise ValueError('Unknown damping model.')
    
    def get_alpha_and_c_infty(self,a):
        idx1 = np.argmin(np.abs(self.rr - a))
        alpha_infty = (-self.drho/self.rho_clean)[idx1]
        c_infty = self.c_clean[idx1]
        return alpha_infty,c_infty

def new_initial_guess(l1_old,l2_old,ansatz):
    N = l1_old.shape[0]
    l1_guess = np.zeros((N+1,N+1),dtype='complex')
    l2_guess = np.zeros((N+1,N+1),dtype='complex')
    if ansatz in ["medium","full"]:
        l1_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        l2_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        l1_guess[:N,:N] = l1_old[:]
        l2_guess[:N,:N] = l2_old[:]
        l1_guess[N,N] = 1.0
    elif ansatz == "minimalIC":
        l1_guess = 1e-3*(np.random.rand(N+1,N+1) + 1j*np.random.rand(N+1,N+1))
        l1_guess[:N,:N] = l1_old[:]
        l2_guess[:N,:N] = l2_old[:]
        l1_guess[N,N] = (-100-100j)
        l2_guess[0,N] = 1e-3*(np.random.rand(1) + 1j*np.random.rand(1))         
    elif ansatz == "mediumSym":
        l1_guess[:N,:N] = l1_old[:]
        l2_guess[:N,:N] = l2_old[:]
        l1_guess[0,N]  = l1_guess[N,0] = 1e-3*(np.random.rand() + 1j*np.random.rand())
        l2_guess[0,N] =  l2_guess[N,0] = 1e-3*(np.random.rand() + 1j*np.random.rand()) 
        l1_guess[N,N] = (16/16)*(-100-100j)
        #A_guess[N,N] = -100-100j
        l2_guess[N,N] = 1.0
    return l1_guess,l2_guess

def get_rlayer(problem_config):
    
    a = problem_config["a"]
    #Determine position of layers according to local wavelength
    lambda_eff  = 2*pi*problem_config["c_1D"]/ problem_config["omega"].real
    w_mesh_pts = np.linspace(0.0,problem_config["a"],1000).tolist()
    mesh_1D_w = Make1DMesh_givenpts(w_mesh_pts)
    N_wavelen = Integrate(1/lambda_eff,mesh_1D_w,order=10)
    print("N_wavelen = ", N_wavelen)
    print("ceil(N_wavelen) = ", ceil(N_wavelen))

    r_samples = np.linspace(0.0,0.8,30,endpoint=False).tolist() + np.linspace(0.8,0.95,50,endpoint=False).tolist() + np.linspace(0.95,0.98,100,endpoint=False).tolist() + np.linspace(0.98,a,220).tolist()
    N_wavelen_r = [] 
    for r_sample in r_samples:
        N_wavelen_r.append(Integrate(IfPos(r_sample-x,1.0,0.0)*1/lambda_eff,mesh_1D_w))
    #plt.plot(r_samples,N_wavelen_r)
    #plt.show()
    r_layer = [0.0]
    N_current = 1
    N_layer = [] 
    for r_sample,N_sample in zip(r_samples,N_wavelen_r):
        if N_sample > N_current:
            r_layer.append(r_sample)
            N_layer.append(N_sample)
            N_current += 1
    if abs(N_layer[-1]-N_wavelen) < 0.25:
        print("Replace last wavelen mesh point with a")
        r_layer[-1] = a
    else:
        r_layer.append(problem_config["a"])
    # prevent first layer from becoming too thick
    if (r_layer[1] > 0.3):
        r_layer.insert(1,0.5*(r_layer[1]+r_layer[0]))
        #r_layer = [r_layer[0]] + [0.5*(r_layer[1]+r_layer[0])] + r_layer[1:] 
    #print("N_layer =" , N_layer)
    #print("r_layer =", r_layer)
    #print("N_current = ", N_current)
    return r_layer


def get_atmospheric_dtn_numbers(problem_config,solar_model,lam_input = []):

    a = problem_config["a"]
    R_max_ODE = solar_model.Ra
    
    L_max_a = problem_config["L_max_a"]
    f_hz = problem_config["f_hz"]

    lam_a = np.array([l*(l+1)/a**2 for l in np.arange(L_max_a) ] )
    if lam_input != []:
        if solar_model.atmo_type == "Atmo":
            raise ValueError('Fine sampling not implemente for Atmo model.')
        else:
            lam_a = lam_input

    n_outer = 10
    start_point = a
    mesh_above_surface = [start_point, start_point+0.5*(R_max_ODE-start_point),R_max_ODE]
    #print("mesh_above_surface = ", mesh_above_surface)

    mesh_1D = Make1DMesh_givenpts(mesh_above_surface)  
    fes_DtN = H1(mesh_1D, complex=True,  order=problem_config["order_ODE"], dirichlet=[1])
    u_DtN,v_DtN = fes_DtN.TnT()
    gfu_DtN = GridFunction (fes_DtN)

    f_DtN = LinearForm(fes_DtN)
    f_DtN.Assemble()
    r_DtN = f_DtN.vec.CreateVector()
    frees_DtN = [i for i in range(1,fes_DtN.ndof)]
    
    def get_dtn_a(f_hz):

        #pot_1D = rho_pot_1D - solar_model.get_damping(f_hz)[1]/problem_config["c_1D"]**2
        pot_1D = problem_config["pot_1D"]
        dtn_a = [] 

        if solar_model.atmo_type == "VAL-C":
            for lami in lam_a:    
                 
                a_DtN = BilinearForm (fes_DtN, symmetric=False)
                a_DtN += SymbolicBFI( (x**2)*grad(u_DtN)*grad(v_DtN), bonus_intorder= problem_config["bonus_intorder"] )  
                a_DtN += SymbolicBFI(  ( a**2*lami.item() + (pot_1D*x**2 ) )*u_DtN*v_DtN, bonus_intorder=problem_config["bonus_intorder"] )
                a_DtN.Assemble()

                gfu_DtN.vec[:] = 0.0
                gfu_DtN.Set(1.0, BND)
                r_DtN.data = f_DtN.vec - a_DtN.mat * gfu_DtN.vec
                gfu_DtN.vec.data += a_DtN.mat.Inverse(freedofs=fes_DtN.FreeDofs(),inverse=problem_config["solver"]) * r_DtN
               
                rows_DtN,cols_DtN,vals_DtN = a_DtN.mat.COO()
                A_DtN = csr_matrix((vals_DtN,(rows_DtN,cols_DtN)))
                val = -P_DoFs(A_DtN,[0],frees_DtN).dot(gfu_DtN.vec.FV().NumPy()[frees_DtN])-P_DoFs(A_DtN,[0],[0]).dot(gfu_DtN.vec.FV().NumPy()[0])
                dtn_a.append(-val.item()/a**2)

            return dtn_a 
        elif solar_model.atmo_type == "Atmo":
            L_max = len(lam_a)
            alpha_infty,c_infty = solar_model.get_alpha_and_c_infty(solar_model.Ra)
            from ngs_arb import Whitw_deriv_over_Whitw,Whitw_deriv_over_Whitw_par,Whitw_deriv_over_Whitw_spar
            prec_start = 500
            prec = prec_start
            sigma, sigma_squared = solar_model.get_damping(f_hz)
            k_squared = sigma_squared/c_infty**2 - 0.25*alpha_infty**2
            k_omega = sqrt_2branch(k_squared)
            eta = alpha_infty/(2*k_omega)
            chi = -1j*eta
            z_arg = -2*1j*k_omega*a
            
            values_ok = False
            while not values_ok:
                wh_qout =  np.array(Whitw_deriv_over_Whitw_spar(chi,z_arg,L_max,prec)) 
                if not np.isnan(wh_qout).any():
                    values_ok = True
                else:
                    print("Whittaker function returned nans, increasing precision!")
                    prec += 500
                    
            DtN_atmo =  1/solar_model.Ra + 2*1j*k_omega*wh_qout
            
            for idx_lami,lami in enumerate(lam_a):    
                 
                a_DtN = BilinearForm (fes_DtN, symmetric=False)
                a_DtN += SymbolicBFI( (x**2)*grad(u_DtN)*grad(v_DtN), bonus_intorder=  problem_config["bonus_intorder"]  )  
                a_DtN += SymbolicBFI(  ( a**2*lami.item() + (pot_1D*x**2 ) )*u_DtN*v_DtN, bonus_intorder=problem_config["bonus_intorder"]  )
                a_DtN += SymbolicBFI(x**2*DtN_atmo[idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))
                a_DtN.Assemble()

                gfu_DtN.vec[:] = 0.0
                gfu_DtN.Set(1.0, BND)
                r_DtN.data = f_DtN.vec - a_DtN.mat * gfu_DtN.vec
                gfu_DtN.vec.data += a_DtN.mat.Inverse(freedofs=fes_DtN.FreeDofs(),inverse=problem_config["solver"]) * r_DtN
               
                rows_DtN,cols_DtN,vals_DtN = a_DtN.mat.COO()
                A_DtN = csr_matrix((vals_DtN,(rows_DtN,cols_DtN)))
                val = -P_DoFs(A_DtN,[0],frees_DtN).dot(gfu_DtN.vec.FV().NumPy()[frees_DtN])-P_DoFs(A_DtN,[0],[0]).dot(gfu_DtN.vec.FV().NumPy()[0])
                dtn_a.append(-val.item()/a**2)
            return dtn_a
    
    return np.array(get_dtn_a(f_hz))



def Learn_atmospheric_model(problem_config,solar_model):

    a = problem_config["a"]
    R_max_ODE = solar_model.Ra
    
    L_max_a = problem_config["L_max_a"]
    f_hz = problem_config["f_hz"]

    lam_a = np.array([l*(l+1)/a**2 for l in np.arange(L_max_a) ] )

    dtn_a = get_atmospheric_dtn_numbers(problem_config,solar_model,lam_input = [])
    L_spacing = 10
    dtn_outer_bnd = dtn_a.copy()

    lam_a = np.array(lam_a)
    diff_dtn = dtn_a[1:] - dtn_a[:-1]
    diff_lam = lam_a[1:] - lam_a[:-1]
    grad_dtn = np.abs( diff_dtn / diff_lam) 
    #print("np.average(grad_dtn) = ", np.average(grad_dtn) )
    ind_large = np.nonzero(grad_dtn > 3*np.average(grad_dtn ))[0]
    #print("ind_larg = ", ind_large)
    ind_large = 1 + ind_large 

    #if show_plots:
    if False:
        plt.plot(np.arange(L_max_a)[1:] ,grad_dtn,label='grad' )
        plt.plot(np.arange(L_max_a)[ind_large] ,grad_dtn[ind_large],label='selected idx',marker='.' )
        plt.legend()
        plt.show()

    L_filter = [] 
    w_weight = [] 
    alpha_decay = 8*L_spacing
    for l in np.arange(L_max_a):
        if l in ind_large:
            L_filter.append(l)
            w_weight.append(exp(-l/alpha_decay))
        else:
            if l % L_spacing == 0:
                L_filter.append(l)
                w_weight.append(exp(-l/alpha_decay))
    weights = np.array(w_weight)

    lam_a = np.array(lam_a)[L_filter]
    dtn_a = np.array(dtn_a)[L_filter]

    A_a  = [None]
    B_a = [None] 
    np.random.seed(seed=123)        
    A_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
    B_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
            
    #plt.plot(np.arange(len(lam_a)),np.array(dtn_a).real)
    #plt.show()

    #print("weights = ", weights[:100])
    l_dtn = opt.learned_dtn(lam_a,dtn_a,weights**2)

    for N in problem_config["Ns_a"]: 
        l_dtn.Run(A_guess,B_guess,problem_config["ansatz"],problem_config["flags"])
        A_a.append(A_guess.copy()), B_a.append(B_guess.copy())
        A_guess,B_guess = new_initial_guess(A_a[N],B_a[N],problem_config["ansatz"])

    A_N = A_a[-1]
    B_N = B_a[-1]
    return A_N, B_N,dtn_outer_bnd


'''
def Learn_atmospheric_model(problem_config,solar_model):
    
    # Determine dtn numbers for solar atmosphere 
    a = problem_config["a"]
    R_max_ODE = solar_model.Ra
    
    L_max_a = problem_config["L_max_a"]
    f_hz = problem_config["f_hz"]

    lam_a = np.array([l*(l+1)/a**2 for l in np.arange(L_max_a) ] )

    n_outer = 10
    start_point = a
    mesh_above_surface = [start_point, start_point+0.5*(R_max_ODE-start_point),R_max_ODE]
    #print("mesh_above_surface = ", mesh_above_surface)

    mesh_1D = Make1DMesh_givenpts(mesh_above_surface)  
    fes_DtN = H1(mesh_1D, complex=True,  order=problem_config["order_ODE"], dirichlet=[1])
    u_DtN,v_DtN = fes_DtN.TnT()
    gfu_DtN = GridFunction (fes_DtN)

    f_DtN = LinearForm(fes_DtN)
    f_DtN.Assemble()
    r_DtN = f_DtN.vec.CreateVector()
    frees_DtN = [i for i in range(1,fes_DtN.ndof)]
    
    def get_dtn_a(f_hz):

        #pot_1D = rho_pot_1D - solar_model.get_damping(f_hz)[1]/problem_config["c_1D"]**2
        pot_1D = problem_config["pot_1D"]
        dtn_a = [] 

        if solar_model.atmo_type == "VAL-C":
            for lami in lam_a:    
                 
                a_DtN = BilinearForm (fes_DtN, symmetric=False)
                a_DtN += SymbolicBFI( (x**2)*grad(u_DtN)*grad(v_DtN), bonus_intorder= problem_config["bonus_intorder"] )  
                a_DtN += SymbolicBFI(  ( a**2*lami.item() + (pot_1D*x**2 ) )*u_DtN*v_DtN, bonus_intorder=problem_config["bonus_intorder"] )
                a_DtN.Assemble()

                gfu_DtN.vec[:] = 0.0
                gfu_DtN.Set(1.0, BND)
                r_DtN.data = f_DtN.vec - a_DtN.mat * gfu_DtN.vec
                gfu_DtN.vec.data += a_DtN.mat.Inverse(freedofs=fes_DtN.FreeDofs(),inverse=problem_config["solver"]) * r_DtN
               
                rows_DtN,cols_DtN,vals_DtN = a_DtN.mat.COO()
                A_DtN = csr_matrix((vals_DtN,(rows_DtN,cols_DtN)))
                val = -P_DoFs(A_DtN,[0],frees_DtN).dot(gfu_DtN.vec.FV().NumPy()[frees_DtN])-P_DoFs(A_DtN,[0],[0]).dot(gfu_DtN.vec.FV().NumPy()[0])
                dtn_a.append(-val.item()/a**2)

            return dtn_a 
        elif solar_model.atmo_type == "Atmo":
            L_max = len(lam_a)
            alpha_infty,c_infty = solar_model.get_alpha_and_c_infty(solar_model.Ra)
            from ngs_arb import Whitw_deriv_over_Whitw,Whitw_deriv_over_Whitw_par,Whitw_deriv_over_Whitw_spar
            prec_start = 500
            prec = prec_start
            sigma, sigma_squared = solar_model.get_damping(f_hz)
            k_squared = sigma_squared/c_infty**2 - 0.25*alpha_infty**2
            k_omega = sqrt_2branch(k_squared)
            eta = alpha_infty/(2*k_omega)
            chi = -1j*eta
            z_arg = -2*1j*k_omega*a
            
            values_ok = False
            while not values_ok:
                wh_qout =  np.array(Whitw_deriv_over_Whitw_spar(chi,z_arg,L_max,prec)) 
                if not np.isnan(wh_qout).any():
                    values_ok = True
                else:
                    print("Whittaker function returned nans, increasing precision!")
                    prec += 500
                    
            DtN_atmo =  1/solar_model.Ra + 2*1j*k_omega*wh_qout
            
            for idx_lami,lami in enumerate(lam_a):    
                 
                a_DtN = BilinearForm (fes_DtN, symmetric=False)
                a_DtN += SymbolicBFI( (x**2)*grad(u_DtN)*grad(v_DtN), bonus_intorder=  problem_config["bonus_intorder"]  )  
                a_DtN += SymbolicBFI(  ( a**2*lami.item() + (pot_1D*x**2 ) )*u_DtN*v_DtN, bonus_intorder=problem_config["bonus_intorder"]  )
                a_DtN += SymbolicBFI(x**2*DtN_atmo[idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))
                a_DtN.Assemble()

                gfu_DtN.vec[:] = 0.0
                gfu_DtN.Set(1.0, BND)
                r_DtN.data = f_DtN.vec - a_DtN.mat * gfu_DtN.vec
                gfu_DtN.vec.data += a_DtN.mat.Inverse(freedofs=fes_DtN.FreeDofs(),inverse=problem_config["solver"]) * r_DtN
               
                rows_DtN,cols_DtN,vals_DtN = a_DtN.mat.COO()
                A_DtN = csr_matrix((vals_DtN,(rows_DtN,cols_DtN)))
                val = -P_DoFs(A_DtN,[0],frees_DtN).dot(gfu_DtN.vec.FV().NumPy()[frees_DtN])-P_DoFs(A_DtN,[0],[0]).dot(gfu_DtN.vec.FV().NumPy()[0])
                dtn_a.append(-val.item()/a**2)
            return dtn_a
        else:
            raise ValueError('Unknown atmospheric model.')

    L_spacing = 10
    dtn_a = np.array(get_dtn_a(f_hz))
    dtn_outer_bnd = dtn_a.copy()

    lam_a = np.array(lam_a)
    diff_dtn = dtn_a[1:] - dtn_a[:-1]
    diff_lam = lam_a[1:] - lam_a[:-1]
    grad_dtn = np.abs( diff_dtn / diff_lam) 
    #print("np.average(grad_dtn) = ", np.average(grad_dtn) )
    ind_large = np.nonzero(grad_dtn > 3*np.average(grad_dtn ))[0]
    #print("ind_larg = ", ind_large)
    ind_large = 1 + ind_large 

    #if show_plots:
    if False:
        plt.plot(np.arange(L_max_a)[1:] ,grad_dtn,label='grad' )
        plt.plot(np.arange(L_max_a)[ind_large] ,grad_dtn[ind_large],label='selected idx',marker='.' )
        plt.legend()
        plt.show()

    L_filter = [] 
    w_weight = [] 
    alpha_decay = 8*L_spacing
    for l in np.arange(L_max_a):
        if l in ind_large:
            L_filter.append(l)
            w_weight.append(exp(-l/alpha_decay))
        else:
            if l % L_spacing == 0:
                L_filter.append(l)
                w_weight.append(exp(-l/alpha_decay))
    weights = np.array(w_weight)

    lam_a = np.array(lam_a)[L_filter]
    dtn_a = np.array(dtn_a)[L_filter]

    A_a  = [None]
    B_a = [None] 
    np.random.seed(seed=123)        
    A_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
    B_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
            
    #plt.plot(np.arange(len(lam_a)),np.array(dtn_a).real)
    #plt.show()

    #print("weights = ", weights[:100])
    l_dtn = opt.learned_dtn(lam_a,dtn_a,weights**2)

    for N in problem_config["Ns_a"]: 
        l_dtn.Run(A_guess,B_guess,problem_config["ansatz"],problem_config["flags"])
        A_a.append(A_guess.copy()), B_a.append(B_guess.copy())
        A_guess,B_guess = new_initial_guess(A_a[N],B_a[N],problem_config["ansatz"])

    A_N = A_a[-1]
    B_N = B_a[-1]
    return A_N, B_N,dtn_outer_bnd
'''

def calc_dtn_nr_layer(layer_nr,lam,problem_config,fine_sampling=False):
    
    omega = problem_config["omega"]
    omega_squared = problem_config["omega_squared"]
    pot_1D = problem_config["pot_1D"]
    r_layer =  problem_config["r_layer"]
    order_ODE = problem_config["order_ODE"]
    bonus_intorder = problem_config["bonus_intorder"]
    dtn_outer_bnd =  problem_config["dtn_outer_bnd"]

    if fine_sampling:
        dtn_outer_bnd  = get_atmospheric_dtn_numbers(problem_config,problem_config["solar_model"],lam_input = lam)

    dtn_nr_layer = []

    mesh_1D = Make1DMesh(layer_nr,r_layer )
     
    fes_DtN = H1(mesh_1D, complex=True,  order=order_ODE, dirichlet=[1])
    u_DtN,v_DtN = fes_DtN.TnT()
    gfu_DtN = GridFunction (fes_DtN)
    f_DtN = LinearForm(fes_DtN)
    f_DtN.Assemble()
    r_DtN = f_DtN.vec.CreateVector()
    frees_DtN = [i for i in range(1,fes_DtN.ndof)]

    DtN_exact_plot = []

    #if_idx = layer_nr
    R_min = r_layer[-1-layer_nr]
    
    for idx_lami,lami in enumerate(lam):
        a_DtN = BilinearForm (fes_DtN, symmetric=False, check_unused=False) 
        a_DtN += SymbolicBFI( (x**2)*grad(u_DtN)*grad(v_DtN), bonus_intorder=  bonus_intorder)  
        a_DtN += SymbolicBFI(  ( R_min**2*lami.item() + (pot_1D*x**2 ) )*u_DtN*v_DtN, bonus_intorder= bonus_intorder)
        a_DtN += SymbolicBFI(x**2*dtn_outer_bnd[idx_lami]*u_DtN*v_DtN,definedon=mesh_1D.Boundaries("right"))    
        a_DtN.Assemble()

        gfu_DtN.vec[:] = 0.0
        gfu_DtN.Set(1.0, BND)
        r_DtN.data = f_DtN.vec - a_DtN.mat * gfu_DtN.vec
        gfu_DtN.vec.data += a_DtN.mat.Inverse(freedofs=fes_DtN.FreeDofs()) * r_DtN
        
        rows_DtN,cols_DtN,vals_DtN = a_DtN.mat.COO()
        A_DtN = scipy.sparse.csr_matrix((vals_DtN,(rows_DtN,cols_DtN)))
        val1 = P_DoFs(A_DtN,[0],frees_DtN).dot(gfu_DtN.vec.FV().NumPy()[frees_DtN])
        val2 = P_DoFs(A_DtN,[0],[0]).dot(gfu_DtN.vec.FV().NumPy()[0])
        val = -P_DoFs(A_DtN,[0],frees_DtN).dot(gfu_DtN.vec.FV().NumPy()[frees_DtN])-P_DoFs(A_DtN,[0],[0]).dot(gfu_DtN.vec.FV().NumPy()[0]) 
        dtn_nr_layer.append(-val.item()/R_min**2)
    
    return dtn_nr_layer


def precompute_DtN_coeff(decomp,problem_config):
     
    DtN_coeff_layer = [ [] for i in range(decomp.n_layers)]  
    DtN_PML_coeff_layer = [ [] for i in range(decomp.n_layers)]  
    lam_layer = [ None for i in range(decomp.n_layers)] 
    lam_vec_layer = [ None for i in range(decomp.n_layers)] 
     
    r_layer = problem_config["r_layer"]
    R_max = r_layer[-1]
    use_discrete_eigenvalues = problem_config["use_discrete_eigenvalues"]
    bonus_intorder = problem_config["bonus_intorder"]
    dtn_outer_bnd = problem_config["dtn_outer_bnd"] 

    output_DtN_eigenbasis_on_layer = None
    if "output_DtN_eigenbasis_on_layer" in problem_config: 
        output_DtN_eigenbasis_on_layer = problem_config["output_DtN_eigenbasis_on_layer"]
    
    for layer_nr in range(1,decomp.n_layers):

        #if_idx = layer_nr
        R_min = r_layer[-1-layer_nr]
        lam = np.array( [ k*(k+1)/R_min**2 for k in range(problem_config["lam_max"])] ,dtype=complex)  
        lam_layer[layer_nr] = lam.copy()
        
        DtN_coeff_layer[layer_nr] = calc_dtn_nr_layer(layer_nr,lam,problem_config).copy()

    return DtN_coeff_layer,lam_layer,lam_vec_layer


def Learn_dtn_function(decomp,problem_config,spectral_info,output_learned_poles=False): 

    Lones_layer = [None for i in range(decomp.n_layers)] 
    Ltwos_layer = [None for i in range(decomp.n_layers)] 
    learned_dtn_layer = [None for i in range(decomp.n_layers)]

    lam_layer = spectral_info["lam_layer"]
    lam_vec_layer = spectral_info["lam_vec_layer"]
    DtN_coeff_layer = spectral_info["DtN_coeff_layer"]

    for layer in range(1,decomp.n_layers):

        lam = lam_layer[layer]
        dtn_nr = DtN_coeff_layer[layer]

        Lone = []
        Ltwo = [] 
        dtn_learned = [] 
        relative_residuals = []

        np.random.seed(seed=123)
        
        l1_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
        l2_guess = np.random.rand(1,1) + 1j*np.random.rand(1,1)
        Lmax = len(lam)
        lam_filtered = lam 
        dtn_nr_filtered = dtn_nr 
        weights = np.array([1 for l in range(Lmax)])
            
        #print("weights = ", weights)
        final_res = np.zeros(len(lam),dtype=float)
        l_dtn = opt.learned_dtn(lam_filtered,dtn_nr_filtered,weights**2)

        for N in problem_config["Ns"]: 
            l_dtn.Run(l1_guess,l2_guess,problem_config["ansatz"],problem_config["flags"],final_res)
            Lone.append(l1_guess.copy()), Ltwo.append(l2_guess.copy()),relative_residuals.append(final_res.copy())
            dtn_approx = np.zeros(len(lam),dtype='complex')
            lam_sample_small = lam.real.copy()
            opt.eval_dtn_fct(Lone[N],Ltwo[N],lam_sample_small,dtn_approx)
            dtn_learned.append(dtn_approx)
            l1_guess,l2_guess = new_initial_guess(Lone[N],Ltwo[N],problem_config["ansatz"])

        Lones_layer[layer] = Lone
        Ltwos_layer[layer] = Ltwo
        learned_dtn_layer[layer] = dtn_learned
        if len(problem_config["Ns"]) > 1:
            print("relative_residuals, avg = {0}, max = {1}  = ".format(np.average(relative_residuals[-1]),np.max( relative_residuals[-1]) ) )

        if problem_config["output_dtn_on_layer"] and layer == problem_config["output_dtn_on_layer"]:
            R_current = problem_config["r_layer"][-1-layer]
            print("output dtn at R = ", R_current)
            lam_sample = problem_config["lam_sample"].real 
            dtn_nr_sample = problem_config["dtn_nr_sample"]  
            learned_dtn_output_layer = []
            plot_collect = [lam_sample.real,np.array(dtn_nr_sample).real]
            header_str = "lam dtn "
            N_start = 4
            for N in problem_config["Ns"]: 
                dtn_approx = np.zeros(len(lam_sample),dtype='complex')
                print("--------------------------")
                opt.eval_dtn_fct(Lones_layer[layer][N],Ltwos_layer[layer][N],lam_sample.copy(),dtn_approx,False)
                learned_dtn_output_layer.append(dtn_approx)
                if N  >= N_start:
                    header_str += "N{0} ".format(N)
                    plot_collect.append(dtn_approx.real)
            
            fname_learned = "learned_dtn_helio_fhz{0}_real.dat".format(problem_config["f_hz"])
            fname_dtnev = "dtn_at_eigenv_helio_fhz{0}_real.dat".format(problem_config["f_hz"])
            
            np.savetxt(fname=fname_learned,
               X=np.transpose(plot_collect),
               header=header_str,
               comments='')

            plot_collect_ev = [lam.real,np.array(dtn_nr).real]
            header_ev = "lam dtn"
            np.savetxt(fname=fname_dtnev ,
               X=np.transpose(plot_collect_ev),
               header=header_ev,
               comments='')


            if problem_config["show_dtn_approx"]:
                
                #plt.plot(lam_layer[layer_idx],np.array(DtN_coeff_layer[layer_idx]).real,label='dtn',linewidth=4,color='lightgray' )
                plt.plot(lam_sample,np.array(dtn_nr_sample).real,label='dtn',linewidth=4,color='lightgray' )
                plt.plot(lam,np.array(dtn_nr).real,label='dtn-samples',linewidth=2,color='b',linestyle="None",marker='x' )
                #dtn_approx = learned_dtn_layer[layer_idx] 
                
                for N,linestyle in zip(problem_config["Ns"],['solid','solid','solid','solid','dotted','dotted','dashed','dashed','dashed','dashed','dashed','dashed','dashed','dashed','dotted','dotted']):
                    if N % 2 == 0 and N > 5:
                        plt.plot(lam_sample,np.array(learned_dtn_output_layer[N]).real,label='N={0}'.format(N),linewidth=2,linestyle=linestyle) 
                        #plt.plot(lam_layer[layer_idx],np.array(dtn_approx[N]).real,label='N={0}'.format(N),linewidth=2,linestyle=linestyle) 
                plt.xlabel("$\\lambda$")
                plt.title("Real part")
                plt.legend()
                plt.show()
    
                plt.plot(lam_sample,np.array(dtn_nr_sample).imag,label='dtn',linewidth=4,color='lightgray' )
                plt.plot(lam,np.array(dtn_nr).imag,label='dtn-samples',linewidth=2,color='b',linestyle="None",marker='x' ) 
                for N,linestyle in zip(problem_config["Ns"],['solid','solid','solid','solid','dotted','dotted','dashed','dashed','dashed','dashed','dashed','dashed','dashed','dashed','dotted','dotted']):
                    if N % 2 == 0 and N > 5:
                        plt.plot(lam_sample,np.array(learned_dtn_output_layer[N]).imag,label='N={0}'.format(N),linewidth=2,linestyle=linestyle) 
                plt.xlabel("$\\lambda$")
                plt.title("Imaginary part")
                plt.legend()
                plt.show()
     
    if problem_config["output_learned_poles"]: 
        learned_poles_layer = np.array([-Lones_layer[output_learned_poles][-1][j,j] for j in range(1, Lones_layer[output_learned_poles][-1].shape[0])])
        learned_poles_layer = learned_poles_layer[np.argsort(learned_poles_layer.imag)]
        print("learned_poles_layer =" , learned_poles_layer)
        fname_learned_poles = "helio_poles_learned.dat"
        plot_collect_learned_poles = [learned_poles_layer.real,learned_poles_layer.imag]
        header_str_learned_poles = "poles_real poles_imag"

        np.savetxt(fname=fname_learned_poles,
            X=np.transpose(plot_collect_learned_poles),
            header=header_str_learned_poles,
            comments='')

    return Lones_layer,Ltwos_layer,learned_dtn_layer


class LearnedInverse(BaseMatrix):
 
    def __init__(self,gfu_X,invX):
        super(LearnedInverse, self).__init__()
        self.gfu_X = gfu_X
        self.invX = invX
        self.res = gfu_X.vec.CreateVector()
        self.nfes = len(self.gfu_X.components[0].vec)
    
    def IsComplex(self):
        return True

    def Height(self):
        return len(gfu.vec)

    def Width(self):
        return len(gfu.vec)
    
    def Mult(self, x, y):
        self.gfu_X.vec[:] = 0.0
        self.gfu_X.components[0].vec.data = x
        self.res.data = self.gfu_X.vec
        self.gfu_X.vec.data = self.invX * self.res
        y.data = self.gfu_X.components[0].vec 

class LearnedDtN():
 
    def __init__(self,v_store,decomp,layer,proj_IF,proj_outer,mat,outer_inverse=None):
        self.v_store = v_store
        self.v_store1 = self.v_store.CreateVector()
        self.decomp = decomp
        self.layer = layer 
        self.IF_dofs_nr = self.decomp.IF2dof[self.layer]
        self.proj_IF = proj_IF
        self.proj_outer = proj_outer
        self.mat = mat 
        self.outer_inverse = outer_inverse
     
    def Apply(self, v_in, v_out):

        self.v_store.FV().NumPy()[self.IF_dofs_nr]  = v_in.FV().NumPy()[self.decomp.IF2dof[self.layer]]
        self.v_store.data -= self.proj_IF * self.v_store
        self.v_store1.data = self.mat * self.v_store
        self.v_store1.data -= self.proj_IF * self.v_store1
        v_out.FV().NumPy()[self.decomp.IF2dof[self.layer]] = self.v_store1.FV().NumPy()[self.IF_dofs_nr]

        if self.outer_inverse:
            #print("applying self.outer_inverse")
            self.v_store1.data = self.mat * self.v_store
            self.v_store1.data -= self.proj_outer * self.v_store1
            self.v_store.data = self.outer_inverse * self.v_store1
            self.v_store.data -= self.proj_outer * self.v_store
            self.v_store1.data = self.mat * self.v_store 
            self.v_store1.data -= self.proj_IF * self.v_store1

            v_out.FV().NumPy()[self.decomp.IF2dof[self.layer]] -= self.v_store1.FV().NumPy()[self.IF_dofs_nr]


def get_LearnedDtN_and_ext_inv(decomp,problem_config,N_fixed,Lones_layer,Ltwos_layer): 

    layer_and_truncated_ext_inv = [None for i in range(decomp.n_layers)]
    LearnedDtNs  = [None for i in range(decomp.n_layers)]
    uX = decomp.fes.TrialFunction() 
    vX = decomp.fes.TestFunction()
    u = uX[0]
    v = vX[0]
    Xa = decomp.fes
    pot_sweep = problem_config["pot_sweep"]
    weight_theta = problem_config["weight_theta"]

    for layer in range(decomp.n_layers):

        if layer == 0: 
            A_N = problem_config["A_N"]
            B_N = problem_config["B_N"]
            ba_els = BitArrayFromList(decomp.layer_and_outer_el[layer],decomp.mesh,Xa,elements=True) 
            ba_dofs = BitArrayFromList(decomp.layer_and_outer_dof[layer],decomp.mesh,Xa,elements=False)

            a_layer_and_outer =  RestrictedBilinearForm(Xa, "a_layer_and_outer", check_unused=False,
                           element_restriction=ba_els,
                           symmetric = problem_config["is_symmetric"] ) 
            a_layer_and_outer += SymbolicBFI ( (grad(u)*grad(v) + pot_sweep*u*v)*problem_config["weight_factor"]  ,definedonelements= ba_els,bonus_intorder = problem_config["bonus_intorder"] )  
            
            for i in range(A_N.shape[0]):
                for j in range(A_N.shape[0]):
                    if abs(A_N[j,i]) > 1e-10 or abs(B_N[j,i]>1e-10):
                        graduX = grad(uX[i]).Trace()
                        gradvX = grad(vX[j]).Trace()  
                        a_layer_and_outer += SymbolicBFI ( (B_N[j,i]*graduX*gradvX + A_N[j,i]*uX[i]*vX[j])*problem_config["weight_theta"]  ,definedon= decomp.mesh.Boundaries("outer") ) 
            a_layer_and_outer.Assemble()
            
            layer_and_truncated_ext_inv[layer] = a_layer_and_outer.mat.Inverse(ba_dofs,inverse=problem_config["solver"])
        else: 
            ba_els = BitArrayFromList(decomp.layer2el[layer],decomp.mesh,Xa,elements=True)
            
            L_one = Lones_layer[layer][N_fixed]
            L_two = Ltwos_layer[layer][N_fixed] 
            ext_learnedIE_layer = {}
            ext_learnedIE_layer["N"]  =  L_one.shape[0]
            
            fes_surf = Compress(H1(decomp.mesh,order=problem_config["order"],complex=True,definedon= decomp.mesh.Boundaries("layer"+str(layer)+"-bnd") ))
            inf_outer = [ fes_surf for i in range(L_one.shape[0] -1)]
            XX = FESpace( [Xa] + inf_outer)
            
            gfuXX = GridFunction (XX)
            uXX = XX.TrialFunction() 
            vXX = XX.TestFunction() 
            
            ba_dofs = BitArray(XX.FreeDofs())
            ba_outer_dofs = BitArray(XX.FreeDofs())
            IF_dofs = BitArray(XX.FreeDofs())
            
            IF_dofs[:] = 0
            for i in range(Xa.ndof):
                ba_dofs[i] = 0
                ba_outer_dofs[i] = 0
            for dof_nr in decomp.InteriorPlusIFdof[layer]:
                ba_dofs[dof_nr] = 1
            for dof_nr in decomp.IF2dof[layer]:
                IF_dofs[dof_nr] = 1

            outer_nr = [] 
            for nr,bb in enumerate(ba_outer_dofs):
                if bb:
                    outer_nr.append(nr)

            proj_IF = Projector(IF_dofs,False)
            proj_outer = Projector(ba_outer_dofs,False)
            
            aXX =  RestrictedBilinearForm(XX, "aXX", check_unused=False,
                           element_restriction=ba_els,
                           symmetric =  problem_config["is_symmetric"] )
            aXX += SymbolicBFI ( (grad(uXX[0][0])*grad(vXX[0][0]) + pot_sweep*uXX[0][0]*vXX[0][0]) * problem_config["weight_theta"], definedonelements = ba_els, bonus_intorder = problem_config["bonus_intorder"] )

            for i in range(L_one.shape[0]):
                if i == 0:
                    u_trial = uXX[0][0]
                else:
                    u_trial = uXX[i]
                gradu_trial = grad(u_trial).Trace()
                for j in range(L_one.shape[0]):
                    if j == 0:
                        v_test = vXX[0][0]
                    else:
                        v_test = vXX[j]
                    gradv_test = grad(v_test).Trace()
                    if abs(L_one[j,i]) > 1e-10 or abs(L_two[j,i]>1e-10):
                        aXX += SymbolicBFI ( (L_two[j,i]*gradu_trial*gradv_test + L_one[j,i]*u_trial*v_test)*problem_config["weight_theta"],definedon= decomp.mesh.Boundaries("layer"+str(layer)+"-bnd")  ) 
            aXX.Assemble()

            invXX = aXX.mat.Inverse(freedofs=ba_dofs, inverse= problem_config["solver"]) 
            learned_inverse =  LearnedInverse(gfuXX,invXX)
            layer_and_truncated_ext_inv[layer] = learned_inverse

            del invXX
            del XX

            v_store = gfuXX.vec.CreateVector()
            
            if ext_learnedIE_layer["N"] > 1: 
                LearnedDtNs[layer] = LearnedDtN(v_store,decomp,layer,proj_IF,proj_outer,aXX.mat, aXX.mat.Inverse(freedofs=ba_outer_dofs, inverse=problem_config["solver"] ))
            else:
                LearnedDtNs[layer] = LearnedDtN(v_store,decomp,layer,proj_IF,proj_outer,aXX.mat)

    return LearnedDtNs,layer_and_truncated_ext_inv


class MovingPMLDtN():
 
    def __init__(self,tmp_DtN,layer,proj_IF,proj_outer,layermat_outer,layerinverse_outer):
        self.tmp_DtN  = tmp_DtN  
        self.tmp_DtN1 = self.tmp_DtN.CreateVector()
        self.layer = layer 
        self.proj_IF = proj_IF
        self.proj_outer = proj_outer
        self.layermat_outer = layermat_outer
        self.layerinverse_outer = layerinverse_outer

    def Apply(self, v_in, v_out):
        self.tmp_DtN.data = v_in
        self.tmp_DtN.data -= self.proj_IF * self.tmp_DtN
        self.tmp_DtN1.data = self.layermat_outer * self.tmp_DtN 
        self.tmp_DtN1.data -= self.proj_IF * self.tmp_DtN1
        v_out.data  = self.tmp_DtN1
        self.tmp_DtN1.data =  self.layermat_outer * self.tmp_DtN
        self.tmp_DtN1.data -= self.proj_outer * self.tmp_DtN1
        self.tmp_DtN.data =  self.layerinverse_outer * self.tmp_DtN1
        self.tmp_DtN.data -= self.proj_outer * self.tmp_DtN

        self.tmp_DtN1.data = self.layermat_outer * self.tmp_DtN
        self.tmp_DtN1.data -= self.proj_IF * self.tmp_DtN1
        v_out.data -= self.tmp_DtN1


def get_MovingPMLDtN_and_ext_inv(decomp,problem_config):
   
    r = sqrt(x*x+y*y)
    
    layermat_moving_PML = [None for i in range(decomp.n_layers)]
    layerinverse_moving_PML= [None for i in range(decomp.n_layers)]
    layer_and_truncated_ext_inv = [None for i in range(decomp.n_layers)] 
    MovingPMLDtNs = [None for i in range(decomp.n_layers)]
    r_layer =  problem_config["r_layer"]
    
    Xa = decomp.fes
    uX = decomp.fes.TrialFunction() 
    vX = decomp.fes.TestFunction() 
    u = uX[0]
    v = vX[0]

    gfu_tmp = GridFunction (decomp.fes)
    tmp_DtN = gfu_tmp.vec.CreateVector()

    A_N = problem_config["A_N"]
    B_N = problem_config["B_N"]
    pot = problem_config["pot"]

    for layer in range(decomp.n_layers):
        print("Set up moving PML for layer = {0}".format(layer))
        C_sweep = problem_config["C_PML"]

        layer_name = "layer"+str(layer-1)
        definedon_PML_move = "layer"+str(layer-1)
        rad_min = r_layer[-1-layer]
        rad_max = r_layer[-1-layer+1]
        f0 = problem_config["f_hz"]

        eta = rad_max-rad_min
        movepml_sigma = IfPos(r-rad_min,IfPos(rad_max-r,C_sweep/eta*(-rad_max+r+eta)**2/eta**2,0),0)
        G_move = (C_sweep/3)*(eta/C_sweep)**(3/2)*movepml_sigma**(3/2)    
        movepml_trafo = CoefficientFunction( (x+(1j/f0)*G_move*x/r,
                                        y+(1j/f0)*G_move*y/r) )
        movepml_jac = CoefficientFunction((
                                    1+ (1j/f0)*( (x**2/r)*(movepml_sigma/r - G_move/r**2) + G_move/r  ),
                                    (1j/f0)*(x*y/r)*(movepml_sigma/r - G_move/r**2),
                                    (1j/f0)*(x*y/r)*(movepml_sigma/r - G_move/r**2),
                                    1+ (1j/f0)*( (y**2/r)*(movepml_sigma/r - G_move/r**2) + G_move/r  )
                                    ),
                                    dims=(2,2))   

        pml_move = pml.Custom(trafo=movepml_trafo, jac = movepml_jac ) 

        if layer == 0:
            ba_els = BitArrayFromList(decomp.layer_and_outer_el[layer],decomp.mesh,Xa,elements=True) 
            ba_dofs = BitArrayFromList(decomp.layer_and_outer_dof[layer],decomp.mesh,Xa,elements=False)
        else:
            ba_els_comp = list( set(decomp.layer2el[layer]) |  set(decomp.layer2el[layer-1]) )
            ba_els_comp.sort()
            ba_els = BitArrayFromList(ba_els_comp,decomp.mesh,Xa,elements=True) 
            relevant_dofs = list(set(decomp.layer2dof[layer-1]) | set(decomp.IF2dof[layer]) | set(decomp.Interior2dof[layer]) )
            relevant_dofs.sort()
            #print("relevant_dofs =", relevant_dofs)
            ba_dofs = BitArrayFromList(relevant_dofs,decomp.mesh,Xa,elements=False)
       
        if layer > 1:
            #pass
            #decomp.mesh.SetPML(pml_move,1)
            decomp.mesh.SetPML(pml_move,definedon_PML_move)
        a_move = RestrictedBilinearForm (Xa, 
                                        element_restriction=ba_els,
                                        symmetric=problem_config["is_symmetric"], 
                                        check_unused=False)
        a_move += SymbolicBFI (  (grad(u)*grad(v) + pot*u*v)*problem_config["weight_factor"] , definedonelements= ba_els,bonus_intorder = problem_config["bonus_intorder"])   
        for i in range(A_N.shape[0]):
            for j in range(A_N.shape[0]):
                if abs(A_N[j,i]) > 1e-10 or abs(B_N[j,i]>1e-10):
                    graduX = grad(uX[i]).Trace()
                    gradvX = grad(vX[j]).Trace()  
                    a_move += SymbolicBFI ( (B_N[j,i]*graduX*gradvX + A_N[j,i]*uX[i]*vX[j])*problem_config["weight_theta"]  ,definedon= decomp.mesh.Boundaries("outer") )
        a_move.Assemble()
        layermat_moving_PML[layer] = a_move.mat  
        
        layer_and_truncated_ext_inv[layer] = a_move.mat.Inverse(ba_dofs,inverse=problem_config["solver"])
        layerinverse_moving_PML[layer] = a_move.mat.Inverse(ba_dofs,inverse=problem_config["solver"])
        if layer > 1: 
            #pass
            #decomp.mesh.UnSetPML(1) 
            decomp.mesh.UnSetPML(definedon_PML_move) 
        
        proj_IF = Projector(BitArrayFromList(decomp.IF2dof[layer],decomp.mesh,decomp.fes,elements=False),False)
        proj_outer = Projector(BitArrayFromList(decomp.layer2all_outer_dof[layer],decomp.mesh,decomp.fes,elements=False),False)
        MovingPMLDtNs[layer] =  MovingPMLDtN(tmp_DtN,layer,proj_IF,proj_outer,layermat_moving_PML[layer],layerinverse_moving_PML[layer])

    return MovingPMLDtNs,layer_and_truncated_ext_inv


def apply_DOSM_to_rhs(dosm,rhs,gfu_DOSM_in,gfu_DOSM_out,udirect,mesh,DOSM_iter=1):
    DOSM_error = [] 
    print("one application of DOSM")
    for i in range(DOSM_iter):
        gfu_DOSM_in.vec.data = gfu_DOSM_out.vec
        dosm.ApplyDOSM(rhs,gfu_DOSM_out.vec,gfu_DOSM_in.vec) 
        l2_error = sqrt (Integrate ( (gfu_DOSM_out.components[0].real-udirect.components[0].real)*(gfu_DOSM_out.components[0].real-udirect.components[0].real) 
          + (gfu_DOSM_out.components[0].imag-udirect.components[0].imag)*(gfu_DOSM_out.components[0].imag-udirect.components[0].imag), mesh))
        print("DOSM-iteration = {0}, L2-error = {1}".format(i+1,l2_error))
        DOSM_error.append(l2_error)

def run_preconditioned_GMRES(C,aX,rhs,udirect,gfu_GMRES,mesh,maxsteps=250):

    res = []
    global cnt2
    cnt2 = 0

    def callback_full(v):
        global cnt2
        gfu_GMRES.vec.data = v
        l2_error_iter = sqrt (Integrate ( (gfu_GMRES.components[0].real-udirect.components[0].real)*(gfu_GMRES.components[0].real-udirect.components[0].real) 
                                       + (gfu_GMRES.components[0].imag-udirect.components[0].imag)*(gfu_GMRES.components[0].imag-udirect.components[0].imag), mesh))
        res.append(l2_error_iter)
        cnt2 += 1
    print("Start GMRES iteration with preconditioner")

    with TaskManager(): 
        gfu_GMRES.vec.data = GMRes(aX.mat, rhs, pre=C, maxsteps = maxsteps, tol = 1e-6,
              callback=callback_full, restart=None, startiteration=0, printrates=True)
    Draw (gfu_GMRES, mesh, "gmres-sol")
    print("L2-errors in iterations = {0}".format(res))
    if res == []:
        return 1
    else:
        return len(res)+1
