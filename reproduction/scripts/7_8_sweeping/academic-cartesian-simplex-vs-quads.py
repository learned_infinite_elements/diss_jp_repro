from netgen.meshing import *
from netgen.csg import *
from ngsolve import *
from math import pi,ceil
import scipy.sparse.linalg
import numpy as np
from ngsolve.solvers import GMRes
from decimal import Decimal
from scipy.linalg import lu_factor,lu_solve
from scipy.sparse import csr_matrix
import sys,os,copy 
sys.path.append(os.path.realpath(''))
from sweeping_geom import HybridLayered_Mesh,Layered_Mesh,Make1DMesh,Make1DMesh_refined,HybridLayered_Mesh_gen,HybridLayered_Mesh_U,HybridLayered_Mesh_halfDisk,Make1DMesh_givenpts
from sweeping_helper import BitArrayFromList,P_DoFs,Layered_Decomposition_cartesian,DOSM_IFassemble,DOSM,draw_mesh_tikz
import ceres_dtn as opt
import matplotlib.pyplot as plt
from xfem import * 
from academic_shared import new_initial_guess,precompute_DtN_coeff,calc_dtn_nr_layer,Learn_dtn_function,LearnedInverse,get_LearnedDtN_and_ext_inv,AlgebraicDtN,get_AlgebraicDtN_and_ext_inv,get_MovingPMLDtN_and_ext_inv,MovingPMLDtN,get_physical_pml,apply_DOSM_to_rhs,run_preconditioned_GMRES
import matplotlib.colors as colors
plt.rc('legend',fontsize=14)
plt.rc('axes',titlesize=14)
plt.rc('axes',labelsize=14)
plt.rc('xtick',labelsize=12)
plt.rc('ytick',labelsize=16)

ngsglobals.msg_level = 0

#########################################################################
# Parameters (to be changed by the user)
#########################################################################
omegas = [12,24,48,96]
Ns_compute = [0,2,4,6,8,10]

is_symmetric = True
gamma_damping = 0 # add damping to moving PML preconditioner 
                       # and 'False' to use the tensor product DtN
show_dtn_approx = False
use_discrete_eigenvalues = False

output_dtn_on_layer = None
direct_solver_precond = False
Nmax = 10
Ns = list(range(Nmax+1))
Nmax_a = Nmax
a = 1.0 # radius of truncation boundary 

type_str = 'const'

order =  4  # order of FEM 

bonus_intorder = 0
solver = "sparsecholesky" # direct solver (e.g. used for subdomain problems)
#solver = "pardiso" # direct solver 
C_PML = 50 # amplitude of the PML

ansatz = "mediumSym"
flags = {"max_num_iterations":5000,
         "use_nonmonotonic_steps":True,
         "minimizer_progress_to_stdout":False,
         "num_threads":4,
         "report_level":"Brief",
         "function_tolerance":1e-13,
         "parameter_tolerance":1e-13,
         "gradient_tolerance":1e-13}

def SolveProblem(omega,use_TP_grid,use_PML,mPML_or_learned, output_DtN_eigenbasis_on_layer=False,order=4):

    print("")
    print("Computing for omega = {0}".format(omega))

    use_moving_PML = False 
    use_learnedIE = False
    use_algebraic_DtN = False
    if mPML_or_learned == "mPML":
        use_moving_PML = True
    elif mPML_or_learned == "learned":
        use_learnedIE = True
    else:
        use_algebraic_DtN = True

    order_ODE = order # order of ODE discretization
    order_geom = order
    
    lam_max = int(2*omega)
    problem_config = {"omega":omega, 
                      "use_PML":use_PML,
                      "use_discrete_eigenvalues": use_discrete_eigenvalues,
                      "order_ODE": order_ODE,
                      "bonus_intorder":bonus_intorder,
                      "use_algebraic_DtN":use_algebraic_DtN,
                      "use_learnedIE":use_learnedIE,
                      "use_moving_PML":use_moving_PML,
                      "Ns":Ns,
                      "output_dtn_on_layer":output_dtn_on_layer,
                      "show_dtn_approx":show_dtn_approx, 
                      "ansatz":ansatz,
                      "flags":flags,
                      "type_str":type_str,
                      "is_symmetric":is_symmetric,
                      "solver":solver,
                      "order":order,
                      "C_PML":C_PML
                     }

    #########################################################################
    # Layered mesh
    #########################################################################

    n_layers = int(omega/4) # -> 12 dofs per wavelength 
    r_layer = [ i*a / (n_layers) for i in range(n_layers+1)]
    nx = 2*len(r_layer)
    refinement_marker = [False for i in range(len(r_layer))]
    mesh = Layered_Mesh(r_layer,nx,use_TP_grid,refinement_marker,'periodic',1.0)
    Draw(mesh)

    #output_DtN_eigenbasis_on_layer = None
    #if omega == omegas[1] and not use_TP_grid and use_PML and use_learnedIE: 
    #    output_DtN_eigenbasis_on_layer = n_layers - 2
    problem_config["output_DtN_eigenbasis_on_layer"] = output_DtN_eigenbasis_on_layer  

    c = 1.0 
    c_1D = 1.0
    Draw(c, mesh, 'piecewise')

    problem_config["c_1D"] = c_1D
    problem_config["c"] = c

    #########################################################################
    # FEM space (Proxy and-GridFunctions) 
    #########################################################################

    fes_H1 = H1(mesh, complex=True,  order=order,dirichlet=[])
    fes = Periodic(fes_H1)
    fes = Compress(fes)

    free_dofs = [] 
    for nr,b in enumerate(fes.FreeDofs()): 
        if b:
            free_dofs.append(nr)

    u = fes.TrialFunction() 
    v = fes.TestFunction() 

    gfu_GMRES = GridFunction (fes)
    udirect = GridFunction (fes)

    gfu = GridFunction (fes)
    u_in = GridFunction (fes)
    u_out = GridFunction (fes)
    gfu_DOSM_in = GridFunction (fes)
    gfu_DOSM_out = GridFunction (fes)
    gfu_DOSM_interm = GridFunction (fes)

    #########################################################################
    # Domain decomposition 
    #########################################################################

    rr = r_layer
    r_layer = rr[1:]
    intervals = [ (rr[i],rr[i+1]) for i in range(len(rr)-1)]
    intervals = intervals[::-1]

    decomp = Layered_Decomposition_cartesian(mesh,fes,n_layers,intervals)
    decomp.Decompose()

    if not use_TP_grid:
        draw_mesh_tikz(decomp=decomp,name="simplex-mesh-{0}layers".format(n_layers))

    problem_config["r_layer"] = r_layer

    #########################################################################
    # PML 
    #########################################################################

    pml_phys,pml_1D = get_physical_pml(problem_config)
    definedon_PML = 'default'
    problem_config["pml_phys"] = pml_phys  
    problem_config["definedon_PML"] = definedon_PML
    problem_config["pml_1D"] = pml_1D

    #########################################################################
    # Solution with direct solver  
    #########################################################################

    if use_PML:
        mesh.SetPML(pml_phys,definedon='default')

    aX = BilinearForm (fes, symmetric=is_symmetric)
    aX += SymbolicBFI (  (grad(u)*grad(v) - (omega**2/c**2)*u*v) , bonus_intorder = bonus_intorder  )
    aX.Assemble()

    if direct_solver_precond:
        aX_background = BilinearForm (fes, symmetric=is_symmetric)
        aX_background += SymbolicBFI (  (grad(u)*grad(v) - (omega**2/c**2)*u*v) , bonus_intorder = bonus_intorder  )
        aX_background.Assemble()

    f = LinearForm (fes)
    f.Assemble()

    np.random.seed(123)
    rhs_random = np.random.rand(fes.ndof)
    f.vec.FV().NumPy()[free_dofs] = 0.1*rhs_random[free_dofs]
    f.vec.FV().NumPy()[decomp.layer2dof[0]] = 0.0

    if use_PML:
        mesh.UnSetPML(definedon_PML)

    gfu.vec[:] = 0.0
    res_direct = gfu.vec.CreateVector()
    udirect.vec[:] = 0.0
    res_direct.data = f.vec - aX.mat * udirect.vec

    invA = aX.mat.Inverse(freedofs=fes.FreeDofs(), inverse=solver)
    udirect.vec.data += invA * res_direct
    Draw (udirect, mesh, "direct")

    if direct_solver_precond:    
        print("Precondition with direct solver inverse of background model")
        invA_background = aX_background.mat.Inverse(freedofs=fes.FreeDofs(), inverse=solver)
        run_preconditioned_GMRES(invA_background,aX,f.vec,udirect,gfu_GMRES,mesh)
       
    #########################################################################
    # Computation of DtN numbers (ODE solves) 
    #########################################################################


    problem_config["lam_max"] = lam_max
    DtN_coeff_layer,lam_layer,lam_vec_layer,mass_layer = precompute_DtN_coeff(decomp,problem_config,True)
    spectral_info = {"DtN_coeff_layer":DtN_coeff_layer,
                     "lam_layer": lam_layer,
                     "lam_vec_layer":lam_vec_layer  
                    }

    if output_dtn_on_layer: 
        max_lam = lam_layer[output_dtn_on_layer].real[-1]
        lam_small = omega**2
        lam_mid = 6*omega**2
        if max_lam > lam_mid:
            lam_sample = np.append( np.linspace(0,lam_small,2500,endpoint=False),np.linspace(lam_small,lam_mid,2500,endpoint=False)) 
            lam_sample = np.append(lam_sample,np.linspace(lam_mid,max_lam,100))
        else:
            lam_sample = np.linspace(0.0,max_lam,1100)
        dtn_nr_sample = calc_dtn_nr_layer(output_dtn_on_layer,lam_sample,problem_config)
        problem_config["lam_sample"] = lam_sample
        problem_config["dtn_nr_sample"] = dtn_nr_sample 

    #########################################################################
    # Compute learned IE matrices L_one,L_two for all layers
    #########################################################################

    if use_learnedIE:
        output_learned_poles=False
        Lones_layer,Ltwos_layer,learned_dtn_layer = Learn_dtn_function(decomp,problem_config,spectral_info,output_learned_poles)
        #LearnedDtNs,layer_and_truncated_ext_inv = get_LearnedDtN_and_ext_inv(decomp,problem_config,Ns[-1],Lones_layer,Ltwos_layer) 

    #########################################################################
    # Layer matrices
    #########################################################################

    layermat = []
    for layer in range(n_layers):
        layermat.append(aX.mat)    

    #########################################################################
    # Transmission operators of DOSM
    #########################################################################

    tmp_DtN = gfu.vec.CreateVector()
    tmp1_DtN = gfu.vec.CreateVector()

    if use_algebraic_DtN or use_moving_PML or output_DtN_eigenbasis_on_layer:
        
        if use_moving_PML:
            
            MovingPMLDtNs,layer_and_truncated_ext_inv = get_MovingPMLDtN_and_ext_inv(decomp,problem_config)
            def DtN_from_moving_PML(layer,v_in,v_out):
                proj_IF_outer = Projector(BitArrayFromList(decomp.IF2dof[layer],mesh,fes,elements=False),False)
                MovingPMLDtNs[layer].Apply(v_in,v_out)
                v_out.data -= proj_IF_outer * v_out
    
        if use_algebraic_DtN or output_DtN_eigenbasis_on_layer:
            
            AlgebraicDtNs,layer_and_truncated_ext_inv = get_AlgebraicDtN_and_ext_inv(decomp,problem_config) 

            def DtN_from_algebraic(layer,v_in,v_out):
                proj_IF_outer = Projector(BitArrayFromList(decomp.IF2dof[layer],mesh,fes,elements=False),False)
                AlgebraicDtNs[layer].Apply(v_in,v_out)
                v_out.data -= proj_IF_outer * v_out

            if output_DtN_eigenbasis_on_layer: 
                layer = output_DtN_eigenbasis_on_layer 

                print("here")

                tmp_layer = gfu.vec.CreateVector()
                tmp1_layer = gfu.vec.CreateVector()
                layer = output_DtN_eigenbasis_on_layer
                n_len = len(decomp.IF2dof[layer])
                DtN_IF_mat = np.zeros((n_len,n_len),dtype=complex)
                
                for idx_j,dof_nr in enumerate(decomp.IF2dof[layer]):
                    tmp_layer.FV().NumPy()[:] = 0.0
                    tmp1_layer.FV().NumPy()[:] = 0.0
                    tmp_layer.FV().NumPy()[dof_nr] = 1.0
                    DtN_from_algebraic(layer,tmp_layer,tmp1_layer)

                    DtN_IF_mat[:,idx_j] = tmp1_layer.FV().NumPy()[decomp.IF2dof[layer]]
                
                Mass_inv_DtN_IF = np.linalg.solve(mass_layer[layer],DtN_IF_mat)
            
                tmp_layer = Mass_inv_DtN_IF @ lam_vec_layer[layer] 
                DtN_IF_mat_eigenbasis = np.linalg.solve(lam_vec_layer[layer],tmp_layer)
               
                #plt.matshow(np.abs(DtN_IF_mat_eigenbasis))
                #plt.show()

                abs_DtN_eigenbasis = np.abs(DtN_IF_mat_eigenbasis) 
                abs_DtN_eigenbasis *= 1/np.max(abs_DtN_eigenbasis)

                vmin = 1e-8
                abs_DtN_eigenbasis[ abs_DtN_eigenbasis < vmin ] =  vmin
                vmax = np.max(np.abs(abs_DtN_eigenbasis))
                
                if False:
                    plt.figure(figsize=(6.5,6),constrained_layout=True)
                    im = plt.pcolormesh(range(n_len),range(n_len)[::-1], abs_DtN_eigenbasis ,
                                   cmap=plt.get_cmap('jet'),
                                   norm=colors.LogNorm( vmin= vmin ,vmax= vmax )  )
                    plt.rc('legend', fontsize=16)
                    plt.rc('axes', titlesize=16)
                    plt.rc('axes', labelsize=16)
                    plt.colorbar(im)
                    #plt.savefig(str_name+".png",transparent=True)
                    plt.show()
                  
                if use_TP_grid:
                    fname ='DtN-TP-order{0}.dat'.format(order) 
                else:
                    fname ='DtN-nonTP-order{0}.dat'.format(order) 
                with open(fname,'a') as the_file:
                    the_file.write('col row abs \n')
                    for ii in range(n_len):
                        for jj in range(n_len):
                            the_file.write('{0} {1} {2} \n'.format(jj,ii,abs_DtN_eigenbasis[ii,jj] ) )
                        the_file.write('\n')        
        
        if use_algebraic_DtN or use_moving_PML: 
    
            def P_outer(layer,v_in,v_out):
                if use_moving_PML:
                    DtN_from_moving_PML(layer,v_in,v_out)
                else:
                    DtN_from_algebraic(layer,v_in,v_out)
                
            gfu_DOSM_in.vec[:] = 0.0
            gfu_DOSM_out.vec[:] = 0.0

            dosm =  DOSM(decomp,layermat,layer_and_truncated_ext_inv,P_outer,gfu)
            class DOSMPrecond(BaseMatrix):
                
                def IsComplex(self):
                    return True

                def Height(self):
                    return len(gfu.vec)

                def Width(self):
                    return len(gfu.vec)
                
                def Mult(self, x, y):
                    gfu_DOSM_in.vec[:] = 0.0
                    dosm.ApplyDOSM(x,y,gfu_DOSM_in.vec)
                
            C =  DOSMPrecond()

            apply_DOSM_to_rhs(dosm,f.vec,gfu_DOSM_in,gfu_DOSM_out,udirect,mesh,DOSM_iter=1)
            # Preconditioned GMRES iteration
            niter = run_preconditioned_GMRES(C,aX,f.vec,udirect,gfu_GMRES,mesh)
            return niter
 
    if use_learnedIE: 
        learned_iter_omega = [] 
        for N_fixed in Ns_compute:
            print("")
            print("Use learned IEs of order N = {0}".format(N_fixed))
            LearnedDtNs,layer_and_truncated_ext_inv = get_LearnedDtN_and_ext_inv(decomp,problem_config,N_fixed,Lones_layer,Ltwos_layer) 
            def DtN_from_learnedIE(layer,v_in,v_out):        
                LearnedDtNs[layer].Apply(v_in,v_out)
                proj_IF_outer = Projector(BitArrayFromList(decomp.IF2dof[layer],mesh,fes,elements=False),False)
                v_out.data -= proj_IF_outer * v_out

            def P_outer(layer,v_in,v_out):
                DtN_from_learnedIE(layer,v_in,v_out)

            gfu_DOSM_in.vec[:] = 0.0
            gfu_DOSM_out.vec[:] = 0.0

            dosm =  DOSM(decomp,layermat,layer_and_truncated_ext_inv,P_outer,gfu)
            class DOSMPrecond(BaseMatrix):
                
                def IsComplex(self):
                    return True

                def Height(self):
                    return len(gfu.vec)

                def Width(self):
                    return len(gfu.vec)
                
                def Mult(self, x, y):
                    gfu_DOSM_in.vec[:] = 0.0
                    dosm.ApplyDOSM(x,y,gfu_DOSM_in.vec)
                
            C =  DOSMPrecond()

            apply_DOSM_to_rhs(dosm,f.vec,gfu_DOSM_in,gfu_DOSM_out,udirect,mesh,DOSM_iter=1)
            # Preconditioned GMRES iteration
            learned_iter_omega.append(run_preconditioned_GMRES(C,aX,f.vec,udirect,gfu_GMRES,mesh))
        return learned_iter_omega


np_omegas = np.array(omegas,dtype=int)
layer_n = np.array([int(omega/4) for omega in omegas],dtype=int)

for order in [1,2,4]:
    SolveProblem(omegas[1],False,True,"algebraic",3,order)

for use_PML,bnd_cond_type in zip([True,False],["PML","Neumann"]):
    #for use_TP_grid,grid_type in zip([True,False],["TP","simplices"]):
    for use_TP_grid,grid_type in zip([False],["simplices"]):
    
        moving_PML_iter = [] 
        for omega in omegas: 
            moving_PML_iter.append(SolveProblem(omega,use_TP_grid,use_PML,"mPML" )) 
        moving_PML_iter = np.array(moving_PML_iter,dtype=int) 

        plot_collect_mPML = [np_omegas,layer_n,moving_PML_iter]
        header_mPML = "omega nlayers iter"
        fname_mPML ="const_{0}_iter_mPML_{1}.dat".format(grid_type,bnd_cond_type)
        np.savetxt(fname=fname_mPML ,
           X=np.transpose(plot_collect_mPML),
           header=header_mPML,
           comments='',
           fmt='%i')

        learned_iter = []
        for omega in omegas: 
            learned_iter.append(SolveProblem(omega,use_TP_grid,use_PML,"learned" ))
        print("learned_iter =" , learned_iter)
        
        plot_collect_learned = [np_omegas,layer_n]
        fname_learned ="const_{0}_iter_learned_{1}.dat".format(grid_type,bnd_cond_type)
        header_learned = "omega nlayers "
        for idx,N_fixed in enumerate(Ns_compute):
            header_learned += "N{0} ".format(N_fixed)
        for idx_N,N_fixed in enumerate(Ns_compute):
            learned_iter_N = []  
            for idx_omega,omega in enumerate(omegas):
                learned_iter_N.append(learned_iter[idx_omega][idx_N])
            learned_iter_N = np.array(learned_iter_N,dtype=int)
            plot_collect_learned.append(learned_iter_N)
        np.savetxt(fname=fname_learned,
           X=np.transpose(plot_collect_learned),
           header=header_learned,
           comments='',
           fmt='%i')
        
        header_combined = header_learned
        header_combined += "mPML"
        plot_collect_combined = plot_collect_learned
        plot_collect_combined.append(moving_PML_iter)
        fname_combined = "const_{0}_iter_{1}.dat".format(grid_type,bnd_cond_type)
        np.savetxt(fname=fname_combined,
           X=np.transpose(plot_collect_combined),
           header=header_combined,
           comments='',
           fmt='%i')

